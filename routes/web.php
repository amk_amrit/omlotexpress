<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'Frontend\HomeController@getHome')->name('fe.home');
Route::get('/range', 'Frontend\HomeController@rangeTest');




Route::get('quotation', 'Frontend\QuotationController@index')->name('quotation.get');

Route::post('quotation/store', 'Frontend\QuotationController@store')->name('quotation.store');

Route::get('countries/{iso?}', 'Shared\CountryController@getCountryByIso')->name('country.iso');
Route::get('countries/terminal-points/{iso?}', 'Shared\CountryController@getTerminalPointsByCountryIso')->name('country.iso.terminals');
Route::get('countries/terminal-points/{countryOneIso}/{countryTwoIso}', 'Shared\CountryController@getTerminalPointsByCountriesIso')->name('countries.iso.terminals');
Route::get('{media}/vehicles', 'Shared\TransportController@getVehiclesByMedia')->name('media.vehicles');
Route::get('/map', function () {
    return view('frontend.pages.map-test');
})->middleware(['CheckPermission'])->name('mapp');


Route::get('/image-test', function () {
    return view('test.image-test');
});
Route::post('/image-test', 'Frontend\HomeController@imageTest')->name('image.test');

Route::get('/gopal-courier', function () {
    return view('admin.pages.gopal.courier-form');
})->name('gopal.courier');

Route::get('/gopal-profile', function () {
    return view('');
})->name('gopal.profile');

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('getLoginForm');
    Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('login');
    Route::group(['middleware' => 'LoginMiddleware'], function () {
        Route::post('/logout', 'Admin\Auth\AdminLoginController@logout')->name('logout');
        Route::get('/', function () {
            return view('admin.pages.dashboard.dashboard');
        })->name('dashboard');


        Route::group(['middleware' => 'CheckPermission'], function () {

            //Route Function For Country Module Controll by Admin
            Route::resource('/countries', 'Admin\CountryController');

            Route::group(['prefix' => 'products'], function () {
                //Route Function For Product Module COntroll by Admin
                Route::resource('/categories', 'Admin\ProductCategoryController');
                Route::group(['prefix' => 'categorys'], function () {
                    //Route Function For Product Category Rates Controll by Admin
                    Route::resource('/rates', 'Admin\ProductCategoryRateController');
                    Route::get('/product-category-rate/{productCategory}', 'Admin\ProductCategoryRateController@getProductCategoryRateEdit')->name('getProductCategoryRate');
                    Route::get('/product-category-rate-view/{productCategory}', 'Admin\ProductCategoryRateController@getProductCategoryRateShow')->name('getProductCategoryRateView');
                });
            });
            //Route Function For Terminal Point  Controll by Admin
            Route::resource('/terminalpoints', 'Admin\TerminalPointController');
            Route::group(['prefix' => 'package', 'as' => 'package.'], function () {
                //Route Function For Package Type  Controll by Admin
                Route::resource('/types', 'Admin\PackageTypeController');
                Route::resource('/rates', 'Admin\PackageTypeRateController');
                //Route Function For Package Sensitivities   Controll by Admin
                Route::resource('/sensitivities', 'Admin\PackageSensitivityController');
                Route::group(['prefix' => 'sensitivity', 'as' => 'sensitivity.'], function () {
                    Route::resource('rates', 'Admin\PackageSensitivityRateController');
                });
            });

            //Route Function for Load Information Controll by Admin
            Route::get('/fullload', 'Admin\LoadInformationController@getFillLoad');
            Route::get('/partload', 'Admin\LoadInformationController@getPartLoad');
            Route::get('/partloadshow/{id}', 'Admin\LoadInformationController@getPartLoadView');
            Route::group(['prefix' => 'unit'], function () {
                //Route Function for Unit Category  Controll by Admin
                Route::resource('/categorys', 'Admin\UnitCategoryController');
                Route::resource('/units', 'Admin\UnitController');
            });
        });

        Route::resource('/courier-classes', 'Admin\Courier\CourierClassController');
        //Courier Route List
        Route::group(['prefix'=>'courier'], function(){
            Route::group(['prefix'=>'quantity', 'as' =>'quantity.'], function(){
                Route::resource('/ranges','Admin\Courier\QuantityGroupRangeController');
                Route::resource('/groups', 'Admin\Courier\QuantityGroupController');
                // Route::get('quantity-group-country-show/{coId}/{qaId}', 'Admin\Courier\QuantityGroupCountryController@quantityGroupCountryShow')->name('quantity-group-country-show');
                // Route::get('quantity-group-country-edit/{coId}/{qaId}', 'Admin\Courier\QuantityGroupCountryController@quantityGroupCountryEdit')->name('quantity-group-country-edit');
            });

            Route::resource('/vehicle-time-range', 'Admin\Courier\VehicleTimeTravelRangeController');
            
            // Route::resource('/time-travel-groups', 'Admin\Courier\TimeTravelGroupController');
            // Route::resource('/time-travel-group-rates', 'Admin\Courier\TimeTravelGroupRateController');
            // Route::get('/create-time-travel-group-rate', 'Admin\Courier\TimeTravelGroupRateController@timeTravelGroupData')->name('create-time-travel-group-rate');
            Route::resource('/airports', 'Admin\Courier\AirportController');
            Route::get('/airport-links/getdatacountryone', 'Admin\Courier\AirportLinkController@getDataCountryOne')->name('airport-links.getdatacountryone');
            Route::get('/airport-links/getdatacountrytwo', 'Admin\Courier\AirportLinkController@getDataCountryTwo')->name('airport-links.getdatacountrytwo');
            Route::resource('/airport-links', 'Admin\Courier\AirportLinkController');
            Route::get('/airport-charges/getQuantityGroupRange', 'Admin\Courier\AirportChargeController@getQuantityGroupRangedata')->name('airport-charges.getQuantityGroupRange');
            Route::resource('/airport-charges', 'Admin\Courier\AirportChargeController');
            Route::get('/get-airport-data', 'Admin\Courier\AirportLinkController@getAirpotData')->name('get-airport-data');
            Route::group(['prefix' => 'class', 'as' => 'class.'], function () {
                Route::resource('agencies', 'Admin\Courier\AgencyClassController');

         

                Route::resource('transits', 'Admin\Courier\TransitClassController');
                Route::group(['prefix' => 'charge', 'as' => 'charge.'], function () {
                    Route::resource('agencies', 'Admin\Courier\AgencyClassChargeController');
                    Route::get('/get-quantity-group-data', 'Admin\Courier\AgencyClassChargeController@getQuanityGroupData')->name('get-quantity-group-data');
                    Route::resource('transits', 'Admin\Courier\TransitClassChargeController');
                    Route::get('/get-transit-quantity-group-range', 'Admin\Courier\TransitClassChargeController@getQuanityGroupRange')->name('get-transit-quantity-group-range');
                    Route::get('/get-agency-quantity-group-range', 'Admin\Courier\AgencyClassChargeController@getQuanityGroupRange')->name('get-agency-quantity-group-range');
                });

            });
            Route::get('/agency-transit-vehicle-charge/getQuantityGroupRange', 'Admin\Courier\AgencyTransitVehicleChargeController@getQuantityGroupRangedata')->name('agency-transit-vehicle-charge.getQuantityGroupRange');
            Route::resource('/agency-transit-vehicle-', 'Admin\Courier\AgencyTransitVehicleChargeController');
            Route::resource('/services', 'Admin\Courier\ServiceController');
            Route::get('/admin-charges/getQuantityGroupRange', 'Admin\Courier\AdminChargerController@getQuantityGroupRangedata')->name('admin-charges.getQuantityGroupRange');
            Route::resource('/admin-charges', 'Admin\Courier\AdminChargerController');
            Route::get('/admin-create-charge', 'Admin\Courier\AdminChargerController@adminChargeCreate')->name('admin-create-charge');
            Route::resource('/transits', 'Admin\Courier\TransitController');
            Route::resource('transit-charges', 'Admin\Courier\TransitChargeController');
            Route::resource('transit-vehicle-charges', 'Admin\Courier\TransitVehicleChargeController');

            Route::resource('/agencies', 'Admin\Courier\AgencyController');
            Route::resource('/vehicle-agent-charges', 'Admin\Courier\VehicleAgentChargeController');
            Route::get('/vehicle-agent-charge-get-datas', 'Admin\Courier\VehicleAgentChargeController@vehicleAgentChargeGetData')->name('vehicle-agent-charge-get-datas');
            Route::get('/vehicle-agent-charge-create-datas', 'Admin\Courier\VehicleAgentChargeController@getVehicleAgentCreateData')->name('vehicle-agent-charge-create-datas');
            Route::resource('/agency-links', 'Admin\Courier\AgencyLinkController');
            Route::get('/agency-link-data', 'Admin\Courier\AgencyLinkController@agencyLinkData')->name('agency-link-data');
            Route::resource('/transit-links', 'Admin\Courier\TransitLinkController');
            Route::get('/transit-link-data', 'Admin\Courier\TransitLinkController@transitLinkData')->name('transit-link-data');
        });




        //Route Function For Terminal Point  Controll by Admin
        Route::resource('/terminalpoint', 'Admin\TerminalPointController');
        Route::resource('/packagetype', 'Admin\PackageTypeController');
        //Route Function For Package Sensitivities   Controll by Admin
        Route::resource('/packagesensitivity', 'Admin\PackageSensitivityController');
        //Route Function for Load Information COntroll by Admin
        Route::get('/fullload', 'Admin\LoadInformationController@getFillLoad')->name('fullload');
        Route::get('/partload', 'Admin\LoadInformationController@getpartload')->name('partload');
        Route::get('/partloadshow/{id}', 'Admin\LoadInformationController@getpartloadview');

        Route::resource('/shipment', 'Admin\ShipmentController'); //Shipment routes

        Route::group(['prefix' => 'transport'], function () {
            Route::resource('/medias', 'Admin\TransportMediaController'); //Transport medias 
            Route::resource('/vehicles', 'Admin\TransportVehicleController'); //Transport Vehicles
            Route::group(['prefix' => 'vehicle', 'as' => 'vehicles.'], function () {
                Route::resource('/dimensions', 'Admin\VehicleDimensionController'); //Transport Vehicles
                Route::resource('/rates', 'Admin\VehicleDimensionRateController'); //VehicleDimensionRatesController
            });
        });
        Route::resource('/departments', 'Admin\DepartmentController');    //Department routes
        Route::resource('/designations', 'Admin\DesignationController');    //Designation routes


        Route::resource('/offices', 'Admin\OfficeController');    //Office routes
        //agent office route
        Route::resource('/agency-offices', 'Admin\Office\AgentOfficeController');

        //transit office route
        Route::resource('/transit-offices', 'Admin\Office\TransitOfficeController');

        //transit office connections route
        Route::get('/transit-offices/{officeId}/connections',
            'Admin\Office\TransitOfficeController@getTransitOfficeConnections')->name('transit-offices.connections');
        Route::post('/transit-offices/{officeId}/connections/store',
            'Admin\Office\TransitOfficeController@storeConnectionOfTransit')->name('transit-offices.connections.store');
        Route::get('/transit-offices/{officeId}/connections/{transitLinkId}/edit',
            'Admin\Office\TransitOfficeController@editConnectionOfTransit')->name('transit-offices.connections.edit');
        Route::put('/transit-offices/{officeId}/connections/{transitLinkId}/edit',
            'Admin\Office\TransitOfficeController@updateConnectionOfTransit')->name('transit-offices.connections.update');
        Route::delete('/transit-offices/{officeId}/connections/{transitLinkId}/toggle-status',
            'Admin\Office\TransitOfficeController@toggleConnectionStatusOfTransit')->name('transit-offices.connections.toggleStatus');
        Route::delete('/transit-offices/{officeId}/connections/{transitLinkId}/destroy',
            'Admin\Office\TransitOfficeController@deleteConnectionOfTransit')->name('transit-offices.connections.destroy');

        //agency transit connections route by agency
        Route::get('/agency-offices/{officeId}/agency-transit-connections',
            'Admin\Office\AgencyTransitConnectionController@getTransitConnectionsOfAgency')->name('agency-transit.connections');
        Route::post('/agency-offices/{officeId}/agency-transit-connections/store',
            'Admin\Office\AgencyTransitConnectionController@storeTransitConnectionOfAgency')->name('agency-transit.connections.store');
        Route::get('/agency-offices/{agencyOfficeId}/agency-transit-connections/{transitOfficeId}/edit',
            'Admin\Office\AgencyTransitConnectionController@editTransitConnectionOfAgency')->name('agency-transit.connections.edit');
        Route::put('/agency-offices/{agencyOfficeId}/agency-transit-connections/{transitOfficeId}/edit',
            'Admin\Office\AgencyTransitConnectionController@updateTransitConnectionOfAgency')->name('agency-transit.connections.update');
        Route::delete('/agency-offices/{agencyOfficeId}/agency-transit-connections/{transitOfficeId}/toggle-status',
            'Admin\Office\AgencyTransitConnectionController@toggleTransitConnectionStatusOfAgency')->name('agency-transit.connections.toggleStatus');
        Route::delete('/agency-offices/{agencyOfficeId}/agency-transit-connections/{transitOfficeId}/destroy',
            'Admin\Office\AgencyTransitConnectionController@deleteTransitConnectionOfAgency')->name('agency-transit.connections.destroy');
        Route::resource('/roles', 'Admin\RoleController');       //Role routes

        Route::resource('/employees', 'Admin\EmployeeController');    //Employee routes


    });
});

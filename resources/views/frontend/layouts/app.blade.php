<!doctype html>
<html lang="en">
<head>
    @include('frontend.includes.head')
</head>
<body>
    <div class="wrapper">

        <!--header-->
        @include('frontend.includes.header')
        <!--/header-->

        <!--content-->
        @yield('content')
        <!--/content-->

        <!--footer-->
        @include('frontend.includes.footer')
        <!--/footer-->
    </div>

    <!---scripts-->
    @include('frontend.includes.scripts')

    @stack('scripts')

</body>
</html>

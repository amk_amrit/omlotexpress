<section id="fancyboxLayout3" class="fancybox-layout3 p-0 m-t-40">
    <div class="container">
        <div class="row fancybox-boxes-wrap">
            <div class="col-sm-6 col-md-6 col-lg-3 fancybox-item">
                <div class="fancybox__icon text-center">
                    <i class="icon-wallet"></i>
                </div>
                <div class="fancybox__content text-center">
                    <h4 class="fancybox__title"><a href="#">Transparent Pricing </a></h4>
                    <p class="fancybox__desc">International supply chains involves challenging regulations.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3 fancybox-item">
                <div class="fancybox__icon text-center">
                    <i class="icon-search"></i>
                </div>
                <div class="fancybox__content text-center">
                    <h4 class="fancybox__title"><a href="#">Real-Time Tracking </a></h4>
                    <p class="fancybox__desc">Ensure customers’ supply chains are fully compliant by practices.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3 fancybox-item">
                <div class="fancybox__icon text-center">
                    <i class="icon-trolley"></i>
                </div>
                <div class="fancybox__content text-center">
                    <h4 class="fancybox__title"><a href="#">Warehouse Storage </a></h4>
                    <p class="fancybox__desc">Depending on your product, we provide warehouse activities.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3 fancybox-item">
                <div class="fancybox__icon text-center">
                    <i class="icon-package-6"></i>
                </div>
                <div class="fancybox__content">
                    <h4 class="fancybox__title text-center"><a href="#">Security For Cargo</a></h4>
                    <p class="fancybox__desc">High security requirements and are certified to local standards.</p>
                </div>
            </div>
        </div>
    </div>
</section>

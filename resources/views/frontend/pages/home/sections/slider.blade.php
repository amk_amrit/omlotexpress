<section id="slider2" class="slider slider-2">
    <div class="carousel owl-carousel carousel-arrows carousel-dots carousel-dots-white" data-slide="1"
         data-slide-md="1" data-slide-sm="1" data-autoplay="true" data-nav="true" data-dots="true" data-space="0"
         data-loop="true" data-speed="3000" data-transition="fade" data-animate-out="fadeOut"
         data-animate-in="fadeIn">
        <div class="slide-item align-v-h bg-overlay">
            <div class="bg-img"><img src="{{asset('frontend/main/images/sliders/2.jpg')}}" alt="slide img"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-10">
                        <div class="slide__content">
                            <h2 class="slide__title">Affordable Price, <br> Certified experts & <br> Innovative
                                Solutions.</h2>
                            <p class="slide__desc">Competitive advantages to some of the largest companies allover
                                the world.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide-item align-v-h bg-overlay">
            <div class="bg-img"><img src="{{asset('frontend/main/images/sliders/6.jpg')}}" alt="slide img"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-10">
                        <div class="slide__content">
                            <h2 class="slide__title">Flexible, Accelerated <br> & Improved Delivery Services For
                                You! </h2>
                            <p class="slide__desc">Try and get best service to grow your business.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

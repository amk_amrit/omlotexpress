
<div class="request-quote-panel">
    <div class="request__form-body">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                    <select class="js-example-basic-single" style="width: 100%;">
                        <option selected="selected">Shipment Type</option>
                        <option>By Air</option>
                        <option>By Road</option>
                        <option>Courier</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                    <label>Tracking Number</label>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="You can enter up to a maximum of 10 Tracking bill numbers for tracking."></textarea>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <button class="btn btn__primary btn__block">Track & Trace</button>
            </div>
        </div>
    </div>
    <div class="widget widget-download bg-theme">
        <div class="widget__content">
            <h5>Industry<br>Solutions!</h5>
            <p>
                Our worldwide presence ensures the timeliness, cost efficiency and compliance adherence required to ensure your production timelines are met.
            </p>
            <a href="#" class="btn btn__primary btn__hover2 btn__block">
                <span>Download 2019 Brochure</span><i class="icon-arrow-right"></i>
            </a>
        </div>
    </div>
</div>


<div class="request-quote-panel wizard-content p-40">
    <form action="{{route('quotation.store')}}" method="post" class="tab-wizard wizard-circle" id="quotation-form">

        {{csrf_field()}}
        <h5>Transportation Cost</h5>
        <div id="error-message-hidden" class="alert alert-danger error-message-hidden alert-dismissible fade show" role="alert">

        </div>

        <section>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h6>Personal Data</h6>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="Name">
                        <small id='error_name' class="error_message-hidden text-danger">

                        </small>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Phone</label> 
                        <input type="text" name="phone" value="{{old('phone')}}" class="form-control" placeholder="Phone">
                        <small id='error_phone' class="error_message-hidden text-danger">

                        </small>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Email Address</label> 
                        <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="Email">
                        <small id='error_email' class="error_message-hidden text-danger">

                        </small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h6 class="mt-5">Shipment Data</h6>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Shipment Type</label> 
                        <select id="transport_media" name="transport_media" class="js-example-basic-single">
                            {{--<option disabled selected>Transport Media</option>--}}
                            <option value="by-road" data-url="{{route('media.vehicles','by-road')}}" selected>
                                By Road
                            </option>

                        </select>
                        <small id='error_transport_media' class="error_message-hidden text-danger">

                        </small>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Select Pickup Country</label> 
                        <select id="origin_country" name="origin_country" class="js-example-basic-single">
                            <option disabled selected>Select Country</option>
                            @foreach($countries as $country)
                                <option value="{{$country->iso}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                        <small id='error_origin_country' class="error_message-hidden text-danger">

                        </small>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Pickup Point</label> 
                        <input id="pac-input" class="form-control" type="text" placeholder="Search Address">
                        <a href="#">
                            <i class="fas fa-search-location"></i>
                        </a>
                        <input type="hidden" id="origin_local_address" name="origin_local_address" >
                        <small id='error_origin_local_address' class="error_message-hidden text-danger">

                        </small>
                    </div>

                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Select Delivery Country</label> 
                        <select id="destination_country" name="destination_country" class="js-example-basic-single">
                            <option disabled selected>Select Country To Deliver</option>
                            @foreach($countries as $country)
                                <option value="{{$country->iso}}">
                                    {{$country->name}}
                                </option>
                            @endforeach
                        </select>
                        <small id='error_destination_country' class="error_message-hidden text-danger">

                        </small>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Delivery Point</label> 
                        <input id="destination_input" class="form-control" type="text" placeholder="Search Address">
                        <a href="#">
                            <i class="fas fa-search-location"></i>
                        </a>
                        <input type="hidden" id="destination_local_address" name="destination_local_address" >
                        <small id='error_destination_local_address' class="error_message-hidden text-danger">

                        </small>
                    </div> 
                </div> 

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Select Terminal</label> 
                        <select id="terminal_point" name="terminal_point" class="js-example-basic-single">
                            <option disabled selected>Select Terminal Points</option>
                        </select>
                        <input type="hidden" id="total_distance" name="total_distance">
                        <small id='error_terminal_point' class="error_message-hidden text-danger">

                        </small>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Select Sensitivity</label> 
                        <select name="sensitivity" class="js-example-basic-single">
                            <option disabled selected>Sensitivity</option>
                            @foreach($activeSensitivities as $sensitivity)
                                <option value="{{$sensitivity->slug}}">{{$sensitivity->title}}</option>
                            @endforeach
                        </select>
                        <small id='error_sensitivity' class="error_message-hidden text-danger">

                        </small>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Select Product Category</label> 
                        <select name="product_category" class="js-example-basic-single">
                            <option disabled selected>Product Category</option>
                            @foreach($productCategories as $category)
                                <option value="{{$category->slug}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        <small id='error_product_category' class="error_message-hidden text-danger">

                        </small>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Total Amount</label> 
                        <input type="text" name="user_bill_amount" class="form-control" placeholder="Total Amount Bill">
                        <small id='error_user_bill_amount' class="error_message-hidden text-danger">

                        </small>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 d-flex flex-wrap">
                    <label class="m-r-10">Choose Load Type</label>
                    <div class="form-group input-radio">
                        <label class="label-radio">Part Load
                            <input type="radio" id="show1-1" name="load_type" value="part_load"
                                {{old('load_type') == 'part_load' ? 'checked' : ''}}>
                            <span class="radio-indicator"></span>
                        </label>
                    </div>
                    <div class="form-group input-radio">
                        <label class="label-radio">Full Load
                            <input type="radio" id="show1-2" name="load_type" value="full_load"
                                {{old('load_type') == 'full_load' ? 'checked' : ''}}>
                            <span class="radio-indicator"></span>
                        </label>
                    </div>
                    <small id='error_load_type' class="error_message-hidden text-danger">

                    </small>

                </div>
                <div class="col-lg-12" id="hide-show1-1">
                    <div class="row form-w-100"> 
                        <div class="col-sm-6 col-md-3 col-lg-3 d-flex">
                            <div class="form-group">
                                <label>Package Type</label>
                                <select name="package_type" id="package-type" class="js-example-basic-single">
                                    <option disabled selected>Package Type</option>
                                    @foreach($packageTypes as $pType)
                                        <option value="{{$pType->slug}}">{{$pType->type_name}}</option>
                                    @endforeach
                                </select>
                                <small id='error_package_type' class="error_message-hidden text-danger">

                                </small>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3 col-lg-3 d-flex">
                            <div class="form-group">
                                <label>Number of items</label>
                                <input type="text" name="item_num" value="{{old('item_num')}}"class="form-control" placeholder="No. of Items">
                                <small id='error_item_num' class="error_message-hidden text-danger">

                                </small>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3 d-flex">
                            <div class="form-group">
                                <label>Total Kgs</label>
                                <input type="text" name="total_weight" value="{{old('total_weight')}}" class="form-control" placeholder="Kgs">
                                <small id='error_total_weight' class="error_message-hidden text-danger">

                                </small>
                            </div>
                        </div>
                        {{--<div class="col-sm-6 col-md-3 col-lg-3 d-flex">
                            <div class="form-group">
                                <label>Weight Unit</label>
                                <select name="weight_unit" class="js-example-basic-single">
                                    <option disabled selected>Weight Unit</option>
                                    @foreach($weightUnits as $wUnit)
                                        <option value="{{$wUnit->id}}">{{$wUnit->name}}</option>
                                    @endforeach
                                </select>
                                <small id='error_weight_unit' class="error_message-hidden text-danger">

                                </small>
                            </div>
                        </div>--}}

                        {{--<div class="col-sm-6 col-md-3 col-lg-3 d-flex box-input">
                            <div class="form-group">
                                <label>Dimension Unit</label>
                                <select name="dimension_unit" class="js-example-basic-single">
                                    <option disabled selected>Dimension Unit</option>
                                    @foreach($dimensionUnits as $dUnit)
                                        <option value="{{$dUnit->id}}">{{$dUnit->name}}</option>
                                    @endforeach
                                </select>
                                <small id='error_dimension_unit' class="error_message-hidden text-danger">

                                </small>
                            </div>
                        </div>--}}

                        <div class="col-sm-6 col-md-3 col-lg-3 d-flex box-input">
                            <div class="form-group">
                                <label>Height</label>
                                <input type="text"name="height" value="{{old('height')}}" class="form-control" placeholder="Height(cm)">
                                <small id='error_height' class="error_message-hidden text-danger">

                                </small>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3 col-lg-3 d-flex box-input">
                            <div class="form-group">
                                <label>Width</label>
                                <input type="text" name="width" value="{{old('width')}}" class="form-control" placeholder="Width(cm)">
                                <small id='error_width' class="error_message-hidden text-danger">

                                </small>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3 col-lg-3 d-flex box-input">
                            <div class="form-group">
                                <label>Length</label>
                                <input type="text" name="length" value="{{old('length')}}" class="form-control" placeholder="Length(cm)">
                                <small id='error_length' class="error_message-hidden text-danger">

                                </small>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-12" id="hide-show1-2" style="display: none;">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label>Vehicle Type</label>
                                <select id="vehicle_type" name="vehicle_type" class="js-example-basic-single" style="width: 100%;">
                                    <option disabled selected>Choose Vehicle</option> 
                                </select>
                                <small id='error_vehicle_type' class="error_message-hidden text-danger">

                                </small>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label>Total Number of Vechile</label>
                                <input type="number" class="form-control"name="no_of_vehicles" value="{{old('no_of_vehicles')}}" placeholder="Total Number Of Vehicles">
                                <small id='error_no_of_vehicles' class="error_message-hidden text-danger">

                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <button id="calculate_btn" class="btn btn__primary calculate-transport">Calculate</button>

                    <button id="submit" class="btn btn__primary calculate-transport">submit</button>
                    <button type="button" id="reset-btn" class="btn btn__primary calculate-transport">
                        Reset
                    </button>

                </div>
                <div class="col-lg-12">
                    <p class="total-amt-p">
                        (Estimate) Total Amount of transportation :
                        <span>NRs. 50,000.</span>
                    </p>
                </div>
            </div>
        </section>
        <h5>Document Service</h5>
        <section>
            <div class="row">
                <div class="col-lg-12">
                    <h6>(a). If you dont have EXIM and IEC Code Tick the following:</h6>
                    <div class="custom-control custom-checkbox my-1 mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="customControlInline1">
                        <label class="custom-control-label" for="customControlInline1">Exim:  NRs. : 4000.</label>
                    </div>
                    <div class="custom-control custom-checkbox my-1 mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="customControlInline2">
                        <label class="custom-control-label" for="customControlInline2">IEC:  NRs. : 4000.</label>
                    </div>
                </div>
                <div class="col-lg-12">
                    <h6 class="mb-10">
                        (b). Our Service Charge changes according to your total bill amount
                    </h6>
                    <p class="service-p">Service Charge: NRs. 4000.</p>

                </div>
                <div class="col-lg-12">
                    <button class="btn btn__primary calculate-transport">Calculate</button>
                </div>
                <div class="col-lg-12">
                    <p class="total-amt-p">
                        Total Amount (including: transportation cost, Custom charge and Service Charge) :
                        <span>NRs. 50,000.</span>
                    </p>
                </div>
            </div>
        </section>
        <h5>Custom Duty Cost</h5>
        <section>
            <div class="row">
                <div class="col-lg-12">
                    <h6 class="text-center">Choose your HSN code and product category for custom charges.</h6>
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr class="text-center">
                            <th>S.n</th>
                            <th>H.S.N Code</th>
                            <th>Product Title</th>
                            <th>Amount (Nrs.)</th>
                            <th>Import Duty</th>
                            <th>VAT</th>
                            <th>EXICS Duty</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td>1.</td>
                            <td>
                                <div class="form-group m-b-0">
                                    <select class="js-example-basic-single w-100" name="state">
                                        <option></option>
                                        <option>01</option>
                                        <option>02</option>
                                        <option>03</option>
                                        <option>04</option>
                                        <option>05</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group m-b-0">
                                    <select class="js-example-basic-single">
                                        <option></option>
                                        <option>Clothing</option>
                                        <option>Live Plant</option>
                                        <option>Chemicals</option>
                                        <option>Petrolem and gas</option>
                                        <option>medicine</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <p>1500 per unit</p>
                            </td>
                            <td>
                                <p>15%</p>
                            </td>
                            <td>
                                <p>10%</p>
                            </td>
                            <td>
                                <p>10%</p>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>1.</td>
                            <td>
                                <div class="form-group m-b-0">
                                    <select class="js-example-basic-single" name="state">
                                        <option></option>
                                        <option>01</option>
                                        <option>02</option>
                                        <option>03</option>
                                        <option>04</option>
                                        <option>05</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group m-b-0">
                                    <select class="js-example-basic-single">
                                        <option></option>
                                        <option>Clothing</option>
                                        <option>Live Plant</option>
                                        <option>Chemicals</option>
                                        <option>Petrolem and gas</option>
                                        <option>medicine</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <p>1500 per unit</p>
                            </td>
                            <td>
                                <p>13%</p>
                            </td>
                            <td>
                                <p>5%</p>
                            </td>
                            <td>
                                <p>10%</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12">
                    <button class="btn btn__primary calculate-transport">Calculate</button>
                </div>
                <div class="col-lg-12">
                    <p class="total-amt-p">
                        (Estimated) Total Amount of Custom :
                        <span>NRs. 50,000.</span>
                    </p>
                </div>
            </div>
        </section>
    </form> 
</div>

 


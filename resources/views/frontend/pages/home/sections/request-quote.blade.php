<section id="requestQuoteTabs" class="request-quote request-quote-tabs sect-pad-t">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="request__form">
                    <nav class="nav nav-tabs">
                        <a class="nav__link active" data-toggle="tab" href="#quote">Request A Quote</a>
                        <a class="nav__link" data-toggle="tab" href="#track">Track & Trace</a>
                    </nav>
                    <div class="tab-content">

                        @include('error-messages.message')
                        <div class="tab-pane fade show active" id="quote">
                            @include('frontend.pages.home.sections.request-quote-partials.request-quote-form')
                        </div>

                       <!--track trace tab-->
                        <div class="tab-pane fade" id="track">
                            {{--@include('frontend.pages.home.sections.request-quote-partials.track-trace-tab')--}}
                        </div>
                       <!--/track trace tab-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="generateBillModal" tabindex="-1" role="dialog" aria-labelledby="generateBillTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="generateBillTitle">Logistic Bill</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-t-0">
                {{--ajax bta aauxa --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn__primary calculate-transport" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn__primary calculate-transport">
                    Print
                </button>
            </div>
        </div>
    </div>
</div>

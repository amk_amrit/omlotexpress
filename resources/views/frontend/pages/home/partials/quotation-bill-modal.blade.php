<div class="bill-wrapper">
    <img src="{{asset('frontend/main/images/logo.png')}}" class="water-mark-wrapper img-fluid">
    <div class="bill-logo-div">
        <div class="row">
            <div class="col-lg-12 text-center">
                <img src="{{asset('frontend/main/images/logo.png')}}" class="logo-dark img-fluid" alt="logo">
            </div>
        </div>
    </div>
    <div class="name-wrapper-div">
        <div class="row">
            <div class="col-lg-6">
                <h5>Name:</h5> <p>{{$shipment->transportQuotationFirst()->name}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Phone Number:</h5> <p>{{$shipment->transportQuotationFirst()->phone}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Email:</h5> <p>{{$shipment->transportQuotationFirst()->email}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Shipment Data:</h5> <p>{{$shipment->transportMedia->name}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Origin Country:</h5> <p>{{$shipment->originCountry->name}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Pickup Point:</h5> <p>{{$shipment->local_origin}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Delivery Country:</h5> <p>{{$shipment->destinationCountry->name}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Delivery Point:</h5> <p>{{$shipment->local_destination}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Terminal:</h5> <p>{{$shipment->terminalPoint->name}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Sensitivity:</h5> <p>{{$shipment->sensitivity->title}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Product Category:</h5> <p>{{$shipment->productCategory->name}}</p>
            </div>
            <div class="col-lg-6">
                <h5>Total Amount Bill:</h5> <p>Nrs. {{$shipment->user_bill_amount}}</p>
            </div>
        </div>
    </div>
    <div class="load-wrapper-div">
        <div class="row">
            <div class="col-lg-12">

                @if($shipment->load_type == 'part_load')
                    <fieldset>
                        <legend>Part Load </legend>
                        <div class="row">
                            <div class="col-lg-3">
                                <h5>Package Type: </h5>
                                <p>{{$shipment->partLoadInformation->packageType->type_name}}</p>
                            </div>
                            <div class="col-lg-3">
                                <h5>No. of Items: </h5>
                                <p>{{$shipment->partLoadInformation->num_of_items}}</p>
                            </div>
                            <div class="col-lg-3">
                                <h5>Total K.G: </h5>
                                <p>{{$shipment->partLoadInformation->total_weight}} kg</p>
                            </div>
                            <div class="col-lg-3">
                                <h5>Weight Unit: </h5>
                                <p>kg</p>
                            </div>
                            <div class="col-lg-3">
                                <h5>Dimension Unit: </h5>
                                <p>cm</p>
                            </div>
                            <div class="col-lg-3">
                                <h5>Height: </h5>
                                <p>{{$shipment->partLoadInformation->height}} cm</p>
                            </div>
                            <div class="col-lg-3">
                                <h5>Width: </h5>
                                <p>{{$shipment->partLoadInformation->width}} cm</p>
                            </div>
                            <div class="col-lg-3">
                                <h5>Length: </h5>
                                <p>{{$shipment->partLoadInformation->length}} cm</p>
                            </div>
                        </div>
                    </fieldset>
                @elseif($shipment->load_type == 'full_load')
                    <fieldset class="m-t-10">
                        <legend>Full Load </legend>
                        <div class="row">
                            <div class="col-lg-3">
                                <h5>Vehicle: </h5>
                                <p>{{$shipment->fullLoadInformation->vehicleDimension->title}}</p>
                            </div>
                            <div class="col-lg-3">
                                <h5>Number Of Vehicles: </h5>
                                <p>{{$shipment->fullLoadInformation->no_of_vehicles}}</p>
                            </div>

                        </div>
                    </fieldset>
                @endif

            </div>
        </div>
    </div>
    <div class="price-cat-wrapper table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>S.N</th>
                <th>Category Name</th>
                <th>Amount(Nrs)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1.</td>
                <td>{{$shipment->productCategory->name}}</td>
                <td>{{$shipment->total_bill_amount}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="total-amt-wrapper">
        <h5>Total Estimated amount for your selected consignment :</h5>
        <span>Nrs. {{$shipment->total_bill_amount}}.</span>
        <p>Amount can vary according to changes on your consignment data.</p>
    </div>
</div>
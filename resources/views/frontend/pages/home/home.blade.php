@extends('frontend.layouts.app')
@section('title','Home')
@section('content')
 

    <!--fancybox-->
    @include('frontend.pages.home.sections.fancybox')
    <!--/fancybox-->

    <!--services-->
    @include('frontend.pages.home.sections.services')
    <!--/services-->

    
@endsection
@push('scripts')
   @include('frontend.pages.home.home-scripts')
    @include('shared.scripts.map-scripts')
    @include('shared.scripts.shipment-scripts-v2')
@endpush


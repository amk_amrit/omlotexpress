<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

    // widzard tabs
    $(".tab-wizard").steps({
        headerTag: "h5"
        , bodyTag: "section"
        , transitionEffect: "fade"
        , titleTemplate: '<span class="step">#index#</span> #title#'
        , labels: {
            finish: "Submit"
        }
        , onFinished: function (event, currentIndex) {
            $( "#quotation-form" ).submit();
        }
    });

    //  hide and show of radio button

    $("#show1-2").click(function(){
        $("#hide-show1-1").hide();
        $("#hide-show1-2").show();
    }).trigger( "click" );

    $("#show1-1").click(function(){
        $("#hide-show1-2").hide();
        $("#hide-show1-1").show();
    }).trigger( "click" );

    $("#package-type").change(function() {
        let packageType=$("#package-type option:selected" ).val();

        //alert(packageType);
        let boxInput = $('.box-input');

        if(packageType == 'sack'){
            boxInput.removeClass("d-flex").addClass("d-none");
        }
        else{
            boxInput.removeClass("d-none").addClass("d-flex");
        }
    });

</script>

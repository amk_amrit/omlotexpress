<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="X-UA-Compatible" content="ie=edge" />
<meta name="description" content="Logistic and transportation compamy for national to international">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('title')</title>
<!-- <link href="assets/images/favicon/favicon.png" rel="icon"> -->
<link rel="stylesheet" href="{{asset('frontend/main/css/bootstrap.min.css')}}">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{asset('frontend/main/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('shared/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('frontend/main/css/libraries.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<link rel="stylesheet" href="{{asset('frontend/main/css/style.css')}}">
<link rel="stylesheet" href="{{asset('shared/css/custom.css')}}">

<script src="{{asset('frontend/main/js/jquery.min.js')}}"></script>
<script src="{{asset('frontend/main/js/popper.min.js')}}"></script>
<script src="{{asset('frontend/main/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend/main/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('frontend/main/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('shared/select2/select2.min.js')}}"></script>
<script src="{{asset('frontend/main/js/jquery.steps.min.js')}}"></script>
<script src="{{asset('frontend/main/js/plugins.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{asset('frontend/main/js/main.js')}}"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>

<header id="header" class="header header-white header-full">

    <nav class="navbar navbar-expand-lg sticky-navbar">
        <div class="container">
            <a class="navbar-brand" href="index.php">
                <img src="{{asset('frontend/main/images/logo.png')}}" class="logo-dark" alt="logo">
            </a>
            <button class="navbar-toggler" type="button">
                <span class="menu-lines"><span></span></span>
            </button>
            <div class="collapse navbar-collapse" id="mainNavigation">
                <ul class="navbar-nav ml-auto">
                    <li class="nav__item ">
                        <a href="index.php" class=" nav__item-link active">Home</a>
                    </li>
                    <li class="nav__item">
                        <a href="contact.php" class="nav__item-link">About Us</a>
                    </li>
                    <li class="nav__item">
                        <a href="contact.php" class="nav__item-link">Contact Us</a>
                    </li>
                    <!-- <li class="nav__item with-dropdown">
                        <a href="about-us.php" class="dropdown-toggle nav__item-link">Company</a>
                        <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                        <ul class="dropdown-menu">
                            <li class="nav__item"><a href="about-us.php" class="nav__item-link">About Us</a></li>

                            <li class="nav__item"><a href="why-us.php" class="nav__item-link">Why Choose Us</a></li>

                            <li class="nav__item"><a href="leadership-team.php" class="nav__item-link">Leadership Team</a></li>

                            <li class="nav__item"><a href="global-locations.php" class="nav__item-link">Global Locations</a></li>

                            <li class="nav__item"><a href="gallery.php" class="nav__item-link">Our Gallery</a></li>

                            <li class="nav__item"><a href="careers.php" class="nav__item-link">careers</a></li>

                        </ul>
                    </li>
                    <li class="nav__item with-dropdown">
                        <a href="services.php" class="dropdown-toggle nav__item-link">Services</a>
                        <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                        <ul class="dropdown-menu wide-dropdown-menu">
                            <li class="nav__item">
                                <div class="row mx-0">
                                    <div class="col-sm-6 dropdown-menu-col">
                                        <h6>Transport Services</h6>
                                        <ul class="nav flex-column">
                                            <li class="nav__item"><a class="nav__item-link" href="single-service.php">Warehousing</a>
                                            </li>
                                            <li class="nav__item"><a class="nav__item-link" href="single-service.php">Air Freight</a>
                                            </li>
                                            <li class="nav__item"><a class="nav__item-link" href="single-service.php">Ocean Freight</a>
                                            </li>
                                            <li class="nav__item"><a class="nav__item-link" href="single-service.php">Road Freight</a>
                                            </li>
                                            <li class="nav__item"><a class="nav__item-link" href="single-service.php">Supply Chain</a>
                                            </li>
                                            <li class="nav__item">
                                                <a class="nav__item-link" href="single-service.php">Packaging</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6 dropdown-menu-col">
                                        <h6>Industry Solutions</h6>
                                        <ul class="nav flex-column">
                                            <li class="nav__item"><a class="nav__item-link" href="single-industry.php">Retail &
                                                    Consumer</a></li>
                                            <li class="nav__item"><a class="nav__item-link" href="single-industry.php">Sciences &
                                                    Healthcare</a></li>
                                            <li class="nav__item"><a class="nav__item-link" href="single-industry.php">Industrial &
                                                    Chemical</a></li>
                                            <li class="nav__item"><a class="nav__item-link" href="single-industry.php">Power
                                                    Generation</a></li>
                                            <li class="nav__item"><a class="nav__item-link" href="single-industry.php">Food &
                                                    Beverage</a></li>
                                            <li class="nav__item"><a class="nav__item-link" href="single-industry.php">Oil & Gas</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="nav__item with-dropdown">
                        <a href="blog-grid.php" class="dropdown-toggle nav__item-link">News & Media</a>
                        <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                        <ul class="dropdown-menu">
                            <li class="nav__item">
                                <a href="blog-grid.php" class="nav__item-link">blog grid</a>
                            </li>
                            <li class="nav__item">
                                <a href="blog-single-post.php" class="nav__item-link">blog single post</a>
                            </li>
                            <li class="nav__item">
                                <a href="#" class="nav__item-link">case studies</a>
                            </li>
                            <li class="nav__item">
                                <a href="#" class="nav__item-link">case study Single</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav__item with-dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav__item-link">Features</a>
                        <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                        <ul class="dropdown-menu">
                            <li class="nav__item">
                                <a href="request-quote.php" class="nav__item-link">request a quote</a>
                            </li>
                            <li class="nav__item">
                                <a href="track-shipment.php" class="nav__item-link">track and trace</a>
                            </li>
                            <li class="nav__item">
                                <a href="find-location.php" class="nav__item-link">find Location</a>
                            </li>
                            <li class="nav__item">
                                <a href="rates.php" class="nav__item-link">Rates & Pricing</a>
                            </li>
                            <li class="nav__item">
                                <a href="faqs.php" class="nav__item-link">help and faqs</a>
                            </li>
                        </ul>
                    </li> -->

                </ul>
            </div>
            <div class="navbar-modules">
                <ul class="modules__wrapper d-flex align-items-center list-unstyled">
                    <li>
                        <a href="#" class="module__btn module__btn-search"><i class="fa fa-search"></i></a>
                    </li>
                    <li class="d-none d-lg-block">
                        <a href="request-quote.php" class="module__btn btn__request btn">
                            <span>Request A Quote</span><i class="icon-arrow-right"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
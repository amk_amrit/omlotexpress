
@include('error-messages.message')
<!-- Form -->
<form id="form" method="post" action="{{route('image.test')}}" enctype="multipart/form-data">

{{ csrf_field()}}




    <div class="row">

        <div class="col-xl-6">
            <!-- Input Group -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Upload</span>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input upload_img" name="company_logo" id="upload_img">
                    <label class="custom-file-label" for="upload_img">Choose company's logo</label>
                </div>
            </div>


        </div>
    </div>



    <button class="btn btn-primary" type="submit">Add</button>


</form>

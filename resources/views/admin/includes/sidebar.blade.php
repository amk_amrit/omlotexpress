<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <div class="user-profile">
            <div class="profile-img">

                <img src="{{asset('backend/images/users/user-img.png')}}" alt="user" />
            </div>
            <div class="profile-text">
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                    {{auth()->user()->name}}
                </a>
                <div class="dropdown-menu animated flipInY">
                    <a href="#" class="dropdown-item">
                        <i class="ti-user"></i>
                        My Profile
                    </a>
                    <a href="#" class="dropdown-item"><i class="ti-wallet"></i>
                        My Balance
                    </a>
                    <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                    <div class="dropdown-divider"></div> <a href="#" class="dropdown-item">
                        <i class="ti-settings"></i>
                        Account Setting
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="login.html" class="dropdown-item">
                        <i class="fa fa-power-off"></i>
                        Logout
                    </a>
                </div>
            </div>
        </div>
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">Forms</li>
                <li>
                    <a class="" href="{{route('gopal.courier')}}" aria-expanded="false">
                        <i class="fa fa-globe"></i>
                        <span class="hide-menu">Gopal</span>
                    </a>
                </li>
               
                @if(authEmployee()->can('view-country'))
                    <li>
                        <a class="" href="{{route('admin.countries.index')}}" aria-expanded="false">
                            <i class="fa fa-globe"></i>
                            <span class="hide-menu">Country</span>
                        </a>
                    </li>
                @endif

                @if(authEmployee()->can('view-department'))
                    <li>
                        <a class="" href="{{route('admin.departments.index')}}" aria-expanded="false">
                            <i class="fa fa-briefcase"></i>
                            <span class="hide-menu">Departments</span>
                        </a>
                    </li>
                @endif

                @if(authEmployee()->can('view-designation'))
                    <li>
                        <a class="" href="{{route('admin.designations.index')}}" aria-expanded="false">
                            <i class="fas fa-address-card"></i>
                            <span class="hide-menu">Designations</span>
                        </a>
                    </li>
                @endif

                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="far fa-building"></i>
                        <span class="hide-menu">Offices</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{ route('admin.offices.index') }}"><i class="far fa-building" aria-hidden="true"></i>
                                <span>View Offices</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('admin.transit-offices.index') }}"><i class="far fa-building" aria-hidden="true"></i>
                                <span>Transit Offices</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.agency-offices.index') }}"><i class="far fa-building" aria-hidden="true"></i>
                                <span>Agency Offices</span>
                            </a>
                        </li>

                        <li>
                        <a href="{{route('admin.roles.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Manage Roles</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <li>
                    <a class="" href="{{route('admin.employees.index')}}" aria-expanded="false">
                        <i class="fas fa-users"></i>
                        <span class="hide-menu">Employees</span>
                    </a>
                </li>

                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-laptop-windows"></i>
                        <span class="hide-menu">Product</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.categories.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Category</span>
                            </a>
                        </li>

                        <!-- <li><a href="#">Product SubCategory</a></li> -->
                        <li>
                            <a href="{{route('admin.rates.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Category Rate</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-ship"></i>
                        <span class="hide-menu">Shipping</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.terminalpoints.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Terminal Point</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-gift"></i>
                        <span class="hide-menu">Package</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.package.types.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Package Type</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.package.rates.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Package  Type Rates </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.package.sensitivities.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span> Package Sensitivities</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.package.sensitivity.rates.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Package Sensitivity Rates </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        <span class="hide-menu">Load Information</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.fullload')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span> Full Load</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.partload')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span> Part Load</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-cart-plus" aria-hidden="true"></i><span class="hide-menu">Shipments</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.shipment.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>View Shipments</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-flask" aria-hidden="true"></i>
                        <span class="hide-menu">Unit</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.categorys.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span> Unit Category</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.units.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Units</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-truck" aria-hidden="true"></i>
                        <span class="hide-menu">Transport</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.medias.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Medium</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.vehicles.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Vehicles</span>
                            </a>
                        </li>
                        <li><a href="{{route('admin.vehicles.dimensions.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Dimensions</span>
                            </a>
                        </li>
                        <li><a href="{{route('admin.vehicles.rates.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Vehicle Dimension Rates</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-devider"></li>
                <li class="nav-devider"></li>
                <li class="nav-small-cap">COURIER</li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-truck" aria-hidden="true"></i>
                        <span class="hide-menu">Quantity Group</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.quantity.groups.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Quantity Groups</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.quantity.ranges.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Quantity Group Range</span>
                            </a>
                        </li>
                    </ul>
                <li>
                    <a href=" {{ route('admin.vehicle-time-range.index') }} " aria-expanded="false">
                        <i class="fa fa-truck" aria-hidden="true"></i>
                        <span class="hide-menu">Vehicle Time Range</span>
                    </a>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-truck" aria-hidden="true"></i>
                        <span class="hide-menu">Airport</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.airports.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Airports</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.airport-links.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Airport Links</span>
                            </a>
                        </li>

                    </ul>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-truck" aria-hidden="true"></i>
                        <span class="hide-menu">Class</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.class.agencies.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Agency Class</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.courier-classes.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Courier Class</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.class.charge.agencies.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Agency Class Charge</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.class.charge.transits.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Transit Class Charge</span>
                            </a>
                        </li>
                    </ul>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-truck" aria-hidden="true"></i>
                        <span class="hide-menu">Company</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.services.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Services </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.admin-charges.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Admin Charge </span>
                            </a>
                        </li>
                    </ul>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="fa fa-truck" aria-hidden="true"></i>
                        <span class="hide-menu">Transit</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.transits.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Transits </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.transit-links.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Transit Link </span>
                            </a>
                        </li>
                        <li>
                            <a href="#"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Transit Vehicle Charge </span>
                            </a>
                        </li>
                    </ul>
            <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                    <i class="fa fa-truck" aria-hidden="true"></i>
                        <span class="hide-menu">Agency</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{route('admin.agencies.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Agencys </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.agency-links.index')}}"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                                <span>Agency links </span>
                            </a>
                        </li>
                        <li>
            
                        </li>
            </ul>
                <li class="nav-small-cap">TABLE</li>
                <li>
                    <a class="" href="#" aria-expanded="false">
                        <i class="mdi mdi-file"></i>
                        <span class="hide-menu">Table</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="sidebar-footer">
        <a href="#" class="link" data-toggle="tooltip" title="Settings">
            <i class="ti-settings"></i>
        </a>
        <a href="#" class="link" data-toggle="tooltip" title="Email">
            <i class="mdi mdi-gmail"></i>
        </a>
        <a href="#" class="link" data-toggle="tooltip" title="Logout">
            <i class="mdi mdi-power"></i>
        </a>
    </div>
</aside>
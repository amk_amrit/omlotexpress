<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png"> -->
<title>@yield('title') | Admin</title>
<link href="{{asset('backend/css/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('backend/css/summernote-bs4.css')}}" rel="stylesheet">
<link href="{{asset('backend/css/style.css')}}" rel="stylesheet">
<link href="{{asset('backend/css/blue.css')}}" id="theme" rel="stylesheet">
<link href="{{asset('backend/css/prajwal.css')}}" id="theme" rel="stylesheet">
<link href="{{asset('shared/css/custom.css')}}" id="theme" rel="stylesheet">
<link href="{{asset('shared/summernote/summernote-bs4.css')}}" id="theme" rel="stylesheet">

<link rel="stylesheet" href="{{asset('shared/select2/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backend/css/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('backend/css/responsive.dataTables.min.css')}}">


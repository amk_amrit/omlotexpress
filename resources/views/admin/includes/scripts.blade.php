<script src="{{asset('backend/js/jquery.min.js')}}"></script>
<script src="{{asset('backend/js/popper.min.js')}}"></script>
<script src="{{asset('backend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('backend/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('backend/js/waves.js')}}"></script>
<script src="{{asset('backend/js/sidebarmenu.js')}}"></script>
<script src="{{asset('backend/js/sticky-kit.min.js')}}"></script>
<script src="{{asset('backend/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backend/js/custom.min.js')}}"></script>
<script src="{{asset('backend/js/jQuery.style.switcher.js')}}"></script>
<script src="{{asset('backend/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('shared/select2/select2.min.js')}}"></script>
<script src="{{asset('shared/summernote/summernote-bs4.js')}}"></script>

<script>
    $(document).ready( function () {
        $('#myTable').DataTable();
    } );
</script>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>

<script>
    $(function () {

        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });

    });

    window.edit = function () {
        $(".click2edit").summernote()
    },
        window.save = function () {
            $(".click2edit").summernote('destroy');
        }
</script>
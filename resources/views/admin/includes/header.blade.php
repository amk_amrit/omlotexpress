<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header">
        <a class="navbar-brand" href="{{ route('admin.dashboard') }}">
                <img src="{{asset('backend/images/purple-bg.png')}}" alt="homepage" class="dark-logo" />
            </a>
        </div>
        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto mt-md-0">
                <li class="nav-item"> <a
                        class="nav-link nav-toggler d-block d-md-none text-muted waves-effect waves-dark"
                        href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                <li class="nav-item"> <a
                        class="nav-link sidebartoggler d-none d-md-block text-muted waves-effect waves-dark"
                        href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item d-none d-md-block search-box"> <a
                        class="nav-link d-none d-md-block text-muted waves-effect waves-dark"
                        href="javascript:void(0)"><i class="ti-search"></i></a>
                    <form class="app-search">
                        <input type="text" class="form-control" placeholder="Search & enter"> <a
                            class="srh-btn"><i class="ti-close"></i></a> </form>
                </li>
            </ul>
            <ul class="navbar-nav my-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="#"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                            class="mdi mdi-message"></i>
                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                        <ul>
                            <li>
                                <h5 class="font-medium py-3 px-4 border-bottom mb-0">Notifications</h5>
                            </li>
                            <li>
                                <div class="message-center position-relative">
                                    <!-- Message -->
                                    <a href="#" class="border-bottom d-block text-decoration-none py-2 px-3">
                                        <div class="btn btn-danger btn-circle mr-2"><i class="fa fa-link"></i>
                                        </div>
                                        <div class="mail-contnet d-inline-block align-middle">
                                            <h5 class="my-1">Luanch Admin</h5> <span
                                                class="mail-desc font-12 text-truncate overflow-hidden text-nowrap d-block">Just
                                                        see the my new
                                                        admin!</span> <span
                                                class="time font-12 mt-1 text-truncate overflow-hidden text-nowrap d-block">9:30
                                                        AM</span>
                                        </div>
                                    </a>
                                    <!-- Message -->
                                    <a href="#" class="border-bottom d-block text-decoration-none py-2 px-3">
                                        <div class="btn btn-success btn-circle mr-2"><i class="ti-calendar"></i>
                                        </div>
                                        <div class="mail-contnet d-inline-block align-middle">
                                            <h5 class="my-1">Event today</h5> <span
                                                class="mail-desc font-12 text-truncate overflow-hidden text-nowrap d-block">Just
                                                        a reminder that
                                                        you have event</span> <span
                                                class="time font-12 mt-1 text-truncate overflow-hidden text-nowrap d-block">9:10
                                                        AM</span>
                                        </div>
                                    </a>
                                    <!-- Message -->
                                    <a href="#" class="border-bottom d-block text-decoration-none py-2 px-3">
                                        <div class="btn btn-info btn-circle mr-2"><i class="ti-settings"></i>
                                        </div>
                                        <div class="mail-contnet d-inline-block align-middle">
                                            <h5 class="my-1">Settings</h5> <span
                                                class="mail-desc font-12 text-truncate overflow-hidden text-nowrap d-block">You
                                                        can customize this
                                                        template as you want</span> <span
                                                class="time font-12 mt-1 text-truncate overflow-hidden text-nowrap d-block">9:08
                                                        AM</span>
                                        </div>
                                    </a>
                                    <!-- Message -->
                                    <a href="#" class="border-bottom d-block text-decoration-none py-2 px-3">
                                        <div class="btn btn-primary btn-circle mr-2"><i class="ti-user"></i>
                                        </div>
                                        <div class="mail-contnet d-inline-block align-middle">
                                            <h5 class="my-1">Pavan kumar</h5> <span
                                                class="mail-desc font-12 text-truncate overflow-hidden text-nowrap d-block">Just
                                                        see the my
                                                        admin!</span>
                                            <span
                                                class="time font-12 mt-1 text-truncate overflow-hidden text-nowrap d-block">9:02
                                                        AM</span>
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="nav-link text-center border-top pt-3" href="javascript:void(0);">
                                    <strong>Check all
                                        notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" id="2"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i
                            class="mdi mdi-email"></i>
                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </a>
                    <div class="dropdown-menu mailbox dropdown-menu-right scale-up" aria-labelledby="2">
                        <ul>
                            <li>
                                <h5 class="font-medium py-3 px-4 border-bottom mb-0">You have 4 new messages</h5>
                            </li>
                            <li>
                                <div class="message-center">
                                    <!-- Message -->
                                    <a href="#" class="border-bottom d-block text-decoration-none py-2 px-3">
                                        <div class="user-img position-relative d-inline-block mr-2 mb-3"> <img
                                                src="{{asset('backend/images/users/1.jpg')}}" alt="user"
                                                class="rounded-circle"> <span
                                                class="profile-status pull-right d-inline-block position-absolute bg-success rounded-circle"></span>
                                        </div>
                                        <div class="mail-contnet d-inline-block align-middle">
                                            <h5 class="my-1">Pavan kumar</h5> <span
                                                class="mail-desc font-12 text-truncate overflow-hidden text-nowrap d-block">Just
                                                        see the my
                                                        admin!</span>
                                            <span
                                                class="time font-12 mt-1 text-truncate overflow-hidden text-nowrap d-block">9:30
                                                        AM</span>
                                        </div>
                                    </a>
                                    <!-- Message -->
                                    <a href="#" class="border-bottom d-block text-decoration-none py-2 px-3">
                                        <div class="user-img position-relative d-inline-block mr-2 mb-3"> <img
                                                src="{{asset('backend/images/users/2.jpg')}}" alt="user"
                                                class="rounded-circle"> <span
                                                class="profile-status pull-right d-inline-block position-absolute bg-danger rounded-circle"></span>
                                        </div>
                                        <div class="mail-contnet d-inline-block align-middle">
                                            <h5 class="my-1">Sonu Nigam</h5> <span
                                                class="mail-desc font-12 text-truncate overflow-hidden text-nowrap d-block">I've
                                                        sung a song! See
                                                        you at</span> <span
                                                class="time font-12 mt-1 text-truncate overflow-hidden text-nowrap d-block">9:10
                                                        AM</span>
                                        </div>
                                    </a>
                                    <!-- Message -->
                                    <a href="#" class="border-bottom d-block text-decoration-none py-2 px-3">
                                        <div class="user-img position-relative d-inline-block mr-2 mb-3"> <img
                                                src="{{asset('backend/images/users/3.jpg')}}" alt="user"
                                                class="rounded-circle"> <span
                                                class="profile-status pull-right d-inline-block position-absolute bg-warning rounded-circle"></span>
                                        </div>
                                        <div class="mail-contnet d-inline-block align-middle">
                                            <h5 class="my-1">Arijit Sinh</h5> <span
                                                class="mail-desc font-12 text-truncate overflow-hidden text-nowrap d-block">I
                                                        am a singer!</span>
                                            <span
                                                class="time font-12 mt-1 text-truncate overflow-hidden text-nowrap d-block">9:08
                                                        AM</span>
                                        </div>
                                    </a>
                                    <!-- Message -->
                                    <a href="#" class="border-bottom d-block text-decoration-none py-2 px-3">
                                        <div class="user-img position-relative d-inline-block mr-2 mb-3"> <img
                                                src="{{asset('backend/images/users/4.jpg')}}" alt="user"
                                                class="rounded-circle"> <span
                                                class="profile-status pull-right d-inline-block position-absolute bg-warning rounded-circle"></span>
                                        </div>
                                        <div class="mail-contnet d-inline-block align-middle">
                                            <h5 class="my-1">Pavan kumar</h5> <span
                                                class="mail-desc font-12 text-truncate overflow-hidden text-nowrap d-block">Just
                                                        see the my
                                                        admin!</span>
                                            <span
                                                class="time font-12 mt-1 text-truncate overflow-hidden text-nowrap d-block">9:02
                                                        AM</span>
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="nav-link text-center border-top pt-3" href="javascript:void(0);">
                                    <strong>See all
                                        e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img
                            src="{{asset('backend/images/users/1.jpg')}}" alt="user" class="profile-pic" /></a>
                    <div class="dropdown-menu dropdown-menu-right scale-up">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="{{asset('backend/images/users/user-img.png')}}" alt="user"></div>
                                    <div class="u-text">
                                        <h4>Gopal Pratap Basnet</h4>
                                        <p class="text-muted">
                                            <a href="profile.php" class="btn btn-rounded btn-danger btn-sm">View
                                                Profile</a>
                                    </div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="{{ route('admin.logout') }}" onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i> Logout
                                </a>
                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>

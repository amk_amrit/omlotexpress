@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.categories.index')}}"> Product Category</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@include('error-messages.message')

<form action="{{route('admin.categories.update', [$category->id])}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            @include('admin.pages.product-category.form', [$formMode="Edit"])
</form>
@endsection

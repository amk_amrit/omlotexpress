@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.categories.index')}}"> Product Category</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.categories.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Slug</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($category as $categorys)
                        
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$categorys->name}}</td>
                                    <td>{{$categorys->slug}}</td>
                                    <td>
                                        @if($categorys->status==1)
                                        Active
                                        @else
                                        Deactive
                                        @endif
                                    </td>
                                    <td class="table-flex">
                                        <a href="{{route('admin.categories.show', $categorys->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                        <a href="{{route('admin.categories.edit', $categorys->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
    
                                        <form action="{{ route('admin.categories.destroy', $categorys->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            @if($categorys->status==0)
                                                <button type="submit" class="btn btn-success">Activate</button>
                                            @else
                                                <button type="submit" class="btn btn-danger">Deactivate</button>
                                            @endif
    
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    </div>
    
@endsection

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label class="control-label" >Name</label>
                    <input type="text" class="form-control" name="name" value="{{ isset($category) ? $category->name : old('name') }}">
                    @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                </div>
                <div class="form-group">
                    <label class="control-label">HSN Code</label>
                    <input type="text" class="form-control" name="hsn_code" value="{{ isset($category) ? $category->hsn_code : old('hsn_code') }}">
                    @if ($errors->has('hsn_code')) <p style="color:red;">{{ $errors->first('hsn_code') }}</p> @endif <br>
                </div>
                <div class="form-group">
                    <label class="control-label">Select</label>
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{ isset($category) && $category->status == 1 ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultChecked">Active</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{ isset($category) && $category->status == 0 ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label"  >Images</label>
                    <input type="file" class="form-control" name="image" value="{{ isset($category) ? $category->image : old('image') }}">
                    @if ($errors->has('image')) <p style="color:red;">{{ $errors->first('image') }}</p> @endif <br>
                </div>
                <div class="form-group">
                <input type="submit" class="btn btn-success"
                value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
                </div>
            </div>
        </div>
    </div>
</div>
       
@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.categories.index')}}"> Product Category</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@include('error-messages.message')
<form action="{{route('admin.categories.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.pages.product-category.form', [$formMode="Publish"])
</form>
@endsection

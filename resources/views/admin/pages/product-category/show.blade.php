@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.categories.index')}}"> Product Category</a></li>
<li class="breadcrumb-item active">{{$category->name}}</li>
@endsection
<div class="row">
    <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
        <a href="{{route('admin.categories.create')}}" class="btn btn-success">Create</a>

    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>ID:</th>
                                <td>{{$category->id}}</td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>{{$category->name}}</td>
                            </tr>
                            <tr>
                                <th>Slug</th>
                                <td>{{$category->slug}}</td>
                            </tr>
                            <tr>
                                <th>HSN Code</th>
                                <td>{{$category->hsn_code}}</td>
                            </tr>
                            <tr>
                                <th>Image</th>
                                <td>{{$category->image}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($category->status==1)
                                    Active
                                    @else
                                    Deactive
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div> -->
                <div class="country-div-list">
                    <ul>
                        <li>
                            <h4>ID : </h4>
                            <h6>{{$category->id}}</h6>
                        </li>
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$category->name}}</h6>
                        </li>
                        <li>
                            <h4>Slug : </h4>
                            <h6>{{$category->slug}}</h6>
                        </li>
                        <li>
                            <h4>HSN Code : </h4>
                            <h6>{{$category->hsn_code}}</h6>
                        </li>
                        <li>
                            <h4>Image : </h4>
                            <h6>{{$category->image}}</h6>
                        </li>
                        <li>
                            <h4>Status : </h4>
                            <h6>
                                @if($category->status==1)
                                Active
                                @else
                                Deactive
                                @endif
                            </h6>
                        </li>
                    </ul>
                </div>
                <div class="form-btn">
                    <a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('admin.categories.destroy', $category->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($category->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
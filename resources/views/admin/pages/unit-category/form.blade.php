<form >
        <div class="form-group">
            <label  class="control-label" >Name</label>
            <input type="text" class="form-control" name="name" value="{{ isset($unitCategories) ? $unitCategories->name : '' }}">
            @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
        </div>
</form>
@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.categorys.index')}}"> Units</a></li>
<li class="breadcrumb-item active">View</li>
@endsection
@include('error-messages.message')
<div class="row">
    <div class="col-12">
        <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
            <a href="{{route('admin.categorys.create')}}" class="btn btn-success">Create New</a>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        @foreach($unitCategory as $unitCategorys)
                        <tbody>
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$unitCategorys->name}}</td>
                                <td>
                                    <a href="{{ route('admin.categorys.edit' , $unitCategorys->id) }}" class="btn btn-warning">Edit</a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
{{ $unitCategory->links() }}


@endsection
@extends('admin.layouts.app')
@section('title','Package Type Rate')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.package.rates.index')}}">Package Type Rates</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
@include('error-messages.message')
<form action="{{route('admin.package.rates.update', $rate->id)}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            @include('admin.pages.package-type-rate.form', [$formMode="Update"])
</form>
@endsection

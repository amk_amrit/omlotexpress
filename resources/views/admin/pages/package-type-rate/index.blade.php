@extends('admin.layouts.app')
@section('title','Package Type Rate')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.package.rates.index')}}">Package Type Rates</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.package.rates.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table">
                        <thead>
                            <tr>
                                <th scope="col"> S.N </th>
                                <th scope="col"> Country </th>
                                <th scope="col"> Package Type</th>
                                <th scope="col"> Currency </th>
                                <th scope="col"> Handling Rate</th>
                                <th scope="col"> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rates as $rate)
                            
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$rate->country->name}}</td>
                                    <td>{{$rate->packageType->type_name}}</td>
                                    <td>{{$rate->country->currency}}</td>
                                    <td>{{$rate->handling_rate}}</td>
                                    <td class="table-flex">
                                        <a href="{{route('admin.package.rates.show', $rate->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                        <a href="{{route('admin.package.rates.edit', $rate->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
    
                                        <form action="{{ route('admin.package.rates.destroy', $rate->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            @if($rate->status==0)
                                                <button type="submit" class="btn btn-success">Activate</button>
                                            @else
                                                <button type="submit" class="btn btn-danger">Deactivate</button>
                                            @endif
    
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>   
</div>
@endsection

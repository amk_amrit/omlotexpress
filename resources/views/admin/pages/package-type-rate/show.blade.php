@extends('admin.layouts.app')
@section('title','Package Type Rate')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.package.rates.index')}}">Package Type Rates</a></li>
    <li class="breadcrumb-item active">Show</li>
@endsection
@section('content')
@include('error-messages.message')
    <div class="row">
        <div class="col-lg-12 text-right">
            <a href="{{route('admin.package.rates.create')}}" class="btn btn-success m-b-10">Create</a>
        </div>
    </div> 

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                           
                            <tr>
                                <th>Country</th>
                                <td>{{$rate->country->name}}</td>
                            </tr>
    
                            <tr>
                                <th>Package Type </th>
                                <td>{{ $rate->packageType->type_name }}</td>
                            </tr>
        
                            <tr>
                                <th>Currency</th>
                                <td>{{$rate->country->currency}}</td>
                            </tr>
        
                            <tr>
                                <th>Handling Rae</th>
                                <td>{{$rate->handling_rate}}</td>
                            </tr>

                            <tr>
                                <th>Status</th>
                                <td>
                                @if($rate->status == 1)
                                    <span style ="color:red"> Activated</span>
                                @else
                                    <span style ="color:blue"> Deactivated</span>
                                @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-btn" >
                        <a href="{{ route('admin.package.rates.edit', $rate->id) }}" class="btn btn-warning">Edit</a>
                        <form action="{{ route('admin.package.rates.destroy', $rate->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                            @if($rate->status==0)
                                <button type="submit" class="btn btn-success">Activate</button>
                            @else
                                <button type="submit" class="btn btn-danger">Deactivate</button>
                            @endif
                            </form>
                    </div>
                </div>
            </div>
        </div>    
    </div>
               
@endsection
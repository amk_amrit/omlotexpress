@extends('admin.layouts.app')
@section('title','Package Type Rate')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.package.rates.index')}}">Package Type Rates</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')
@include('error-messages.message')
<form action="{{route('admin.package.rates.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.pages.package-type-rate.form', [$formMode="Publish"])
</form>
@endsection

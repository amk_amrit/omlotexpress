@extends('admin.layouts.app')
@section('title','Shipments')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item active">Shipment</li>
@endsection

@section('content')

@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<div class="row">
    <div class="col-12">
        <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
            <a href="{{route('admin.shipment.create')}}" class="btn btn-success">Create</a>
        </div>
    </div> 
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="">
                            <tr>
                                <th scope="col"> ID </th>
                                <th scope="col"> Load Type </th>
                                <th scope="col"> Transport Media </th>
                                <th scope="col"> Origin Country </th>
                                <th scope="col"> Local Origin </th>
                                <th scope="col"> Destination Country </th>
                                <th scope="col"> Local Destination </th>
                                <th scope="col"> Product Category </th>
                                <th scope="col"> Terminal Point </th>
                                <th scope="col"> Sensitivity </th>
                                <th scope="col"> Total Bill Amount </th>

                            </tr>
                        </thead>
                        @foreach($shipments as $shipment)
                        <tbody>
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$shipment->load_type}}</td>
                                <td>{{$shipment->transportMedia->name}}</td>
                                <td>{{$shipment->originCountry->name}}</td>
                                <td>{{$shipment->local_origin}}</td>
                                <td>{{$shipment->destinationCountry->name}}</td>
                                <td>{{$shipment->local_destination}}</td>
                                <td>{{$shipment->productCategory->name}}</td>
                                <td>{{$shipment->terminalPoint->name}}</td>
                                <td>{{$shipment->sensitivity->title}}</td>
                                <td>{{$shipment->total_bill_amount}}</td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>



@endsection
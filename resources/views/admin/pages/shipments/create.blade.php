@extends('admin.layouts.app')
@section('title','Shipment')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item">Shipment</li>
<li class="breadcrumb-item active">Add Shipment</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.shipment.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="control-label">Load Type</label>
                        <select class="form-control" name="load_type">
                            <option value="part_load">Part Load</option>
                            <option value="full_load">Full Load</option>
                        </select>

                    </div>

                    <div class="form-group">
                        <label class="control-label">Transport Media</label>
                        <select class="form-control" name="transport_media_id">
                            @foreach($transport_medias as $media)
                            <option value="{{$media->id}}">{{$media->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Origin Country</label>
                        <select class="form-control" name="origin_country_id">
                            @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Local Origin</label>
                        <input type="text" class="form-control" name="local_origin">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Destination Country</label>
                        <select class="form-control" name="destination_country_id">
                            @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Local Destination</label>
                        <input type="text" class="form-control" name="local_destination">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Product Category</label>
                        <select class="form-control" name="product_category_id">
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Terminal Point</label>
                        <select class="form-control" name="terminal_point_id">
                            @foreach($terminals as $terminal)
                            <option value="{{$terminal->id}}">{{$terminal->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Sensitivity </label>
                        <select class="form-control" name="sensitivity_id">
                            @foreach($sensitivities as $sensitivity)
                            <option value="{{$sensitivity->id}}">{{$sensitivity->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Total Bill Amount</label>
                        <input type="number" step=".01" class="form-control" name="total_bill_amount">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="submit">
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
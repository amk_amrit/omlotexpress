<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                
                <div class="form-group">
                    <label class="control-label"  >Country</label>
                    <input type="number" class="form-control" name="country_id"  value="{{ isset($rate) ? $rate->country_id : old('country_id') }}">
                    @if ($errors->has('country_id')) <p style="color:red;">{{ $errors->first('country_id') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label" >Vehicle Dimension</label>
                    <select class="form-control" name="vehicle_dimension_id" >
                    <option value="" selected disabled>--select an option--</option>
                        @foreach($vehicleDimensions as $vehicleDimension)
                            <option value="{{$vehicleDimension->id}}"
                                {{$vehicleDimension->id == old('vehicle_dimension_id') ? 'selected' : ''}}
                                <?php if(isset($rate) && $vehicleDimension->id == $rate->vehicle_dimension_id ) echo 'selected'; ?>
                                >
                                {{$vehicleDimension->title}}
                            </option>
                        @endforeach
                
                    </select>
                    @if ($errors->has('vehicle_dimension_id')) <p style="color:red;">{{ $errors->first('vehicle_dimension_id') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label"  >Per Km Charge</label>
                    <input type="number" step="0.01" class="form-control" name="per_km_charge"  value="{{ isset($rate) ? $rate->per_km_charge : old('per_km_charge') }}">
                    @if ($errors->has('per_km_charge')) <p style="color:red;">{{ $errors->first('per_km_charge') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label"  >Minimum Charge</label>
                    <input type="number" step ="0.01" class="form-control" name="minimum_charge"  value="{{ isset($rate) ? $rate->min_charge : old('minimum_charge') }}">
                    @if ($errors->has('minimum_charge')) <p style="color:red;">{{ $errors->first('minimum_charge') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label">Select</label>
                    <div class="custom-control custom-radio p-l-0">
                        <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{ isset($rate) && $rate->status == 1 || (!is_null(old('status')) && old('status') == 1 ) ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultChecked">Active</label>
                    </div> 
                    <div class="custom-control custom-radio p-l-0">
                        <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{ isset($rate) && $rate->status == 0 || (!is_null(old('status')) && old('status') == 0 ) ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                    </div>
                </div>
                @if ($errors->has('status')) <p style="color:red;">{{ $errors->first('status') }}</p> @endif <br>
                
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
                </div>

            </div>
        </div>
    </div>
</div>



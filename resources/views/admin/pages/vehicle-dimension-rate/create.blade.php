@extends('admin.layouts.app')
@section('title','Vehicle Dimension Rates')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicles.rates.index')}}">Vehicle Dimension Rates</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')
@include('error-messages.message')
<form action="{{route('admin.vehicles.rates.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.pages.vehicle-dimension-rate.form', [$formMode="Publish"])
</form>
@endsection

@extends('admin.layouts.app')
@section('title','Vehicle Dimension')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.vehicles.rates.index')}}">Vehicle Dimension Rates</a></li>
<li class="breadcrumb-item active">Show</li>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.vehicles.rates.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                        <table class="table">
                           
                            <tr>
                                <th>Country</th>
                                <td>{{$rate->country->name}}</td>
                            </tr>
    
                            <tr>
                                <th>Vehicle Dimension </th>
                                <td>{{$rate->vehicleDimension->title}}</td>
                            </tr>
        
                            <tr>
                                <th>Currency</th>
                                <td>{{$rate->country->currency}}</td>
                            </tr>
        
                            <tr>
                                <th>Per Km Charge</th>
                                <td>{{$rate->per_km_charge}}</td>
                            </tr>
        
                            <tr>
                                <th>Minimum Charge</th>
                                <td>{{$rate->min_charge}}</td>
                            </tr>
        
                            <tr>
                                <th>Status</th>
                                <td>
                                @if($rate->status == 1)
                                    <span style ="color:red"> Activated</span>
                                @else
                                    <span style ="color:blue"> Deactivated</span>
                                @endif
                                </td>
                            </tr>
                        </table>
                    </div> -->
                <div class="country-div-list">
                    <ul class="weight-gain-ul">
                        <li>
                            <h4>Country : </h4>
                            <h6>{{$rate->country->name}}</h6>
                        </li>
                        <li>
                            <h4>Vehicle Dimension : </h4>
                            <h6>{{$rate->vehicleDimension->title}}</h6>
                        </li>
                        <li>
                            <h4>Currency : </h4>
                            <h6>{{$rate->country->currency}}</h6>
                        </li>
                        <li>
                            <h4>Per Km Charge : </h4>
                            <h6>{{$rate->per_km_charge}}</h6>
                        </li>
                        <li>
                            <h4>Minimum Charge : </h4>
                            <h6>{{$rate->min_charge}}</h6>
                        </li>
                        <li>
                            <h4>Status : </h4>
                            <h6>
                                @if($rate->status == 1)
                                <span style="color:red"> Activated</span>
                                @else
                                <span style="color:blue"> Deactivated</span>
                                @endif
                            </h6>
                        </li>
                    </ul>
                </div>
                <div class="form-btn">
                    <a href="{{ route('admin.vehicles.rates.edit', $rate->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('admin.vehicles.rates.destroy', $rate->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($rate->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
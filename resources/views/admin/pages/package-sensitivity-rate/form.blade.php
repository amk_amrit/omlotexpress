<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                
                <div class="form-group">
                    <label class="control-label"  >Country</label>
                    <input type="number" class="form-control" name="country_id"  value="{{ isset($rate) ? $rate->country_id : old('country_id') }}">
                    @if ($errors->has('country_id')) <p style="color:red;">{{ $errors->first('country_id') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label" >Package Sensitivity Type</label>
                    <select class="form-control" name="package_sensitivity_id" >
                    <option value="" selected disabled>--select an option--</option>
                    @foreach($packageSensitivities as $packageSensitivity)
                            <option value="{{$packageSensitivity->id}}"
                                {{$packageSensitivity->id == old('package_sensitivity_id') ? 'selected' : ''}}
                                <?php if(isset($rate) && $packageSensitivity->id == $rate->package_sensitivity_id ) echo 'selected'; ?>
                                >
                             {{$packageSensitivity->title}}
                            </option>
                    @endforeach
                
                    </select>
                    @if ($errors->has('package_sensitivity_id')) <p style="color:red;">{{ $errors->first('package_sensitivity_id') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label"  >Handling Rate</label>
                    <input type="number" step="0.01" class="form-control" name="handling_rate"  value="{{ isset($rate) ? $rate->handling_rate : old('handling_rate') }}">
                    @if ($errors->has('handling_rate')) <p style="color:red;">{{ $errors->first('handling_rate') }}</p> @endif <br>
                </div>
                
              
                <label class="control-label">Select</label>

                <div class="form-group">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" checked>
                        <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                    </div> 
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{ isset($rate) && $rate->status == 1 || (!is_null(old('status')) && old('status') == 1 ) ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultChecked">Active</label>
                    </div>
                </div>
               
                @if ($errors->has('status')) <p style="color:red;">{{ $errors->first('status') }}</p> @endif <br>
                
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
                </div>

            </div>
        </div>
    </div>
</div>



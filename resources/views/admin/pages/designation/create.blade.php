@extends('admin.layouts.app')
@section('title','Designations')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.designations.index')}}">Designations</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')
    
    <form action="{{route('admin.designations.store')}}" method="POST">
        {{ csrf_field() }}
        @include('admin.pages.designation.form', [$formMode="Publish"])
    </form>

@endsection

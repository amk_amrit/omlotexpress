@extends('admin.layouts.app')
@section('title','Designations')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.designations.index')}}">Designations</a></li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.designations.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col"> ID </th>
                            <th scope="col"> Name </th>
                            <th scope="col"> Description </th>
                            <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        @foreach($designations as $designation)
                            <tbody>
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$designation->title}}</td>
                                <td>{{$designation->description}}</td>
                                <td class="table-flex">
                                    <a href="{{route('admin.designations.show', $designation->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                    <a href="{{route('admin.designations.edit', $designation->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
                                    <form action="{{ route('admin.designations.destroy', $designation->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        @if($designation->status==0)

                                        <button type="submit" class="btn btn-danger">Activate</button>
                                        @else
                                            <button type="submit" class="btn btn-danger">Deactivate</button>
                                        @endif
                                    </form>
                                </td>

                            </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
    

    
@endsection

@extends('admin.layouts.app')
@section('title','Designations')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.designations.index')}}">Designations</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')

    <form action="{{route('admin.designations.update', $designation->id)}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        @include('admin.pages.designation.form', [$formMode="Edit"])
    </form>
        
@endsection

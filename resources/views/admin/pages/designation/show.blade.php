@extends('admin.layouts.app')
@section('title','Designations')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.designations.index')}}">Designations</a></li>
<li class="breadcrumb-item active">{{$designation->title}}</li>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.designations.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>{{$designation->title}}</td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td>{{$designation->description}}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                @if($designation->status == 1)
                                <span> Activated</span>
                                @else
                                <span> Deactivated</span>
                                @endif
                            </td>
                        </tr>

                    </table>
                </div> -->
                <div class="country-div-list">
                    <ul>
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$designation->title}}</h6>
                        </li>
                        <li>
                            <h4>Description : </h4>
                            <h6>{{$designation->description}}</h6>
                        </li>
                        <li>
                            <h4>Status : </h4>
                            <h6>
                                @if($designation->status == 1)
                                <span> Activated</span>
                                @else
                                <span> Deactivated</span>
                                @endif
                            </h6>
                        </li> 
                    </ul>
                </div>

                <div class="form-btn">
                    <a href="{{ route('admin.designations.edit', $designation->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('admin.designations.destroy', $designation->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($designation->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
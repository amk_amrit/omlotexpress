<form >
        <div class="form-group">
            <label  class="control-label" >Name</label>
            <input type="text" class="form-control" name="name" value="{{ isset($unit) ? $unit->name : '' }}">
            @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label  class="control-label" >Unit Category</label>
            <select class="form-control" name="unit_category_id">
            <option value="{{isset($unit) ? $unit->category->id : ''}}" >{{isset($unit) ? $unit->category->name : ''}}</option>
            <option value="" disabled selected>--Select Unit Category--</option>
            @foreach($categoryunit as $categoryunits)
            <option value="{{$categoryunits->id}}"
            {{$categoryunits->id == old('unit_category_id') ? 'selected' : ''}}
                        <?php if(isset($unit) && $unit->category->id == $categoryunits->id ) echo 'selected'; ?>
                        >
            {{$categoryunits->name}}</option>
            @endforeach

            </select>
            @if ($errors->has('unit_category_id')) <p style="color:red;">{{ $errors->first('unit_category_id') }}</p> @endif <br>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
        </div>
</form>
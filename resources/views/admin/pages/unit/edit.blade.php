@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.units.index')}}"> Units</a></li>
<li class="breadcrumb-item active">Edit</li>
@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.units.update', [$unit->id])}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    @include('admin.pages.unit.form', [$formMode="Edit"])
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title','Transport Vehicle')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicles.index')}}">Transport Vehicle</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')
<form action="{{route('admin.vehicles.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.pages.vehicle.form', [$formMode="Publish"])
</form>
@endsection

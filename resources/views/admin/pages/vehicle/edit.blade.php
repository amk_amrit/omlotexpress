@extends('admin.layouts.app')
@section('title','Transport Vehicle')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicles.index')}}">Transport Vehicle</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
<form action="{{route('admin.vehicles.update', $transportVehicle->id)}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            @include('admin.pages.vehicle.form', [$formMode="Edit"])
</form>
@endsection

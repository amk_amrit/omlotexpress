@extends('admin.layouts.app')
@section('title','Transport Vehicle')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicles.index')}}">Transport Vehicle</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.vehicles.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table">
                        <thead>
                            <tr>
                                <th scope="col"> ID </th>
                                <th scope="col"> Name </th>
                                <th scope="col"> Slug </th>
                                <th scope="col"> Transport Media</th>
                                <th scope="col"> Description</th>
                                <th scope="col"> Action </th>
                            </tr>
                        </thead>
                        @foreach($transportVehicles as $transportVehicle)
                        <tbody>
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$transportVehicle->name}}</td>
                                <td>{{$transportVehicle->slug}}</td>
                                <td>{{$transportVehicle->transportMedia->name}}</td>
                                <td>{{$transportVehicle->description}}</td>
                
                
                                <td class="table-flex">
                                    <a href="{{route('admin.vehicles.show', $transportVehicle->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                    <a href="{{route('admin.vehicles.edit', $transportVehicle->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>

                                    <form action="{{ route('admin.vehicles.destroy', $transportVehicle->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        @if($transportVehicle->status==0)
                                            <button type="submit" class="btn btn-success">Activate</button>
                                        @else
                                            <button type="submit" class="btn btn-danger">Deactivate</button>
                                        @endif

                                    </form>
                                </td>
                                
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>   
@endsection

@extends('admin.layouts.app')
@section('title','Transport Vehicle')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.vehicles.index')}}">Transport Vehicle</a></li>
<li class="breadcrumb-item active">{{$transportVehicle->name}}</li>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.vehicles.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <td>{{$transportVehicle->name}}</td>
                            </tr>
                            <tr>
                                <th>Transport Media</th>
                                <td>{{$transportVehicle->transportMedia->name}}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{{$transportVehicle->description}}</td>
                            </tr>
                            
                            <tr>
                                <th>Status</th>
                                <td>
                                @if($transportVehicle->status == 1)
                                    <span> Activated</span>
                                @else
                                    <span> Deactivated</span>
                                @endif
                                </td>
                            </tr>
                            
                        </table>
                    </div> -->
                <div class="country-div-list">
                    <ul class="weight-gain-ul">
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$transportVehicle->name}}</h6>
                        </li>
                        <li>
                            <h4>Transport Media : </h4>
                            <h6>{{$transportVehicle->transportMedia->name}}</h6>
                        </li>
                        <li>
                            <h4>Description : </h4>
                            <h6>{{$transportVehicle->description}}</h6>
                        </li>
                        <li>
                            <h4>Status : </h4>
                            <h6>
                                @if($transportVehicle->status == 1)
                                <span> Activated</span>
                                @else
                                <span> Deactivated</span>
                                @endif
                            </h6>
                        </li>
                    </ul>
                </div>
                <div class="form-btn">
                    <a href="{{ route('admin.vehicles.edit', $transportVehicle->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('admin.vehicles.destroy', $transportVehicle->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($transportVehicle->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.partload')}}">Part Loads</a></li>
<li class="breadcrumb-item active">View</li>
@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Shipment Id</th>
                                <th scope="col">Package Type Id</th>
                                <th scope="col">Num of Items</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        @foreach($partload as $partload)
                        <tbody>
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$partload->shipment->load_type}}</td>

                                <td>{{$partload->packageType->type_name}}</td>

                                <td>{{$partload->num_of_items}}</td>
                                <td>
                                    <a href="{{ URL::to('admin/partloadshow/' . $partload->id) }}" class="btn btn-info">View</a>

                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
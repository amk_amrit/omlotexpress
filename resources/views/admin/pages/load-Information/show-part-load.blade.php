@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.partload')}}">Part Loads</a></li>
    <li class="breadcrumb-item active">{{$partloadshows->shipment->load_type}}</li>
@endsection

    <div class="row">
            <table style="width:100%">
                    <tr>
                        <th>ID:</th>
                        <td>{{$partloadshows->id}}</td>
                    </tr>
                    <tr>
                        <th>Shipment Type </th>
                        <td>{{$partloadshows->shipment->load_type}}</td>
                    </tr>
                    <tr>
                        <th>Package Type </th>
                        <td>{{$partloadshows->packageType->type_name}}</td>
                    </tr>
                    <tr>
                        <th>Number Of Items</th>
                        <td>{{$partloadshows->num_of_items}}</td>
                    </tr>
                    <tr>
                        <th>Height</th>
                        <td>{{$partloadshows->height}}</td>
                    </tr>
                    <tr>
                        <th>Width</th>
                        <td>{{$partloadshows->width}}</td>
                    </tr>
                    <tr>
                        <th>Length</th>
                        <td>{{$partloadshows->length}}</td>
                    </tr>
                    <tr>
                        <th>Dimension Unit</th>
                        <td>{{$partloadshows->unit->name}}</td>
                    </tr>
                    <tr>
                        <th>Total Weight</th>
                        <td>{{$partloadshows->total_weight}}</td>
                    </tr>
                    <tr>
                        <th>Weight Unit</th>
                        <td>{{$partloadshows->unit->name}}</td>
                    </tr>
        </table>
    </div>
               
@endsection
@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.fullload')}}">Full Loads</a></li>
<li class="breadcrumb-item active">View</li>
@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Load Type</th>
                                <th scope="col">Vehicle Dimension Title</th>
                                <th scope="col">Number of Vehicles </th>
                            </tr>
                        </thead>
                        @foreach($fullload as $fullloads)
                        <tbody>
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$fullloads->shipment->load_type}}</td>

                                <td>{{$fullloads->vehicleDimension->title}}</td>

                                <td>{{$fullloads->no_of_vehicles}}</td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{ $fullload->links() }}


@endsection
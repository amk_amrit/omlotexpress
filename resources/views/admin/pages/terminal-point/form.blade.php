
    <div class="form-group">
        <label class="control-label" >Source Country</label>
        <select class="form-control" name="country_one_id" >
            <option value=""  disabled selected >---Select Country---</option>
            @foreach($countrys as $country)
                <option value="{{$country->id}}"
                {{$country->id == old('country_one_id') ? 'selected' : ''}}
                    <?php if(isset($terminalPoints) && $terminalPoints->country_one_id == $country->id ) echo 'selected'; ?>
                    >
                {{$country->name}}
                </option>
            @endforeach

        </select>
        @if ($errors->has('country_one_id')) <p style="color:red;">{{ $errors->first('country_one_id') }}</p> @endif <br>
    </div>
    <div class="form-group">
        <label class="control-label" >Destination Country</label>
        <select class="form-control" name="country_two_id" >
        <option value="{{isset($terminalPoints) ? $terminalPoints->country_two_id : ''}}">{{isset($terminalPoints) ? $terminalPoints->country_two_id : ''}}</option>
            <option value="" disabled selected >--Select Option---</option>
            @foreach($countrys as $country)
                <option value="{{$country->id}}"
                {{$country->id == old('country_one_id') ? 'selected' : ''}}
                    <?php if(isset($terminalPoints) && $terminalPoints->country_one_id == $country->id ) echo 'selected'; ?>
                    >
                
                {{$country->name}}</option>
            @endforeach

        </select>
        @if ($errors->has('country_two_id')) <p style="color:red;">{{ $errors->first('country_two_id') }}</p> @endif <br>
    </div>
    <div class="form-group">
        <label  class="control-label" >Name</label>
        <input type="text" class="form-control" name="name" value="{{ isset($terminalPoints) ? $terminalPoints->name : '' }}">
        @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
    </div>
    <div class="form-group">
        <label  class="control-label" >Latitude</label>
        <input type="text" class="form-control" name="latitude" value="{{ isset($terminalPoints) ? $terminalPoints->latitude : '' }}">
        @if ($errors->has('latitude')) <p style="color:red;">{{ $errors->first('latitude') }}</p> @endif <br>
    </div>
    <div class="form-group">
        <label  class="control-label" >Logitude</label>
        <input type="text" class="form-control" name="longitude" value="{{ isset($terminalPoints) ? $terminalPoints->longitude : '' }}">
        @if ($errors->has('longitude')) <p style="color:red;">{{ $errors->first('longitude') }}</p> @endif <br>
    </div>
    <div class="form-group">
        <input type="hidden" class="form-control" name="status" value="1">
    </div>
    <div class="form-group">
    <input type="submit" class="btn btn-success"
    value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
    </div>

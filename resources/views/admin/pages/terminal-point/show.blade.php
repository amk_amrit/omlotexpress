@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.terminalpoints.index')}}"> Terminal Points</a></li>
<li class="breadcrumb-item active">{{$termianPoints->name}}</li>
@endsection
<div class="row">
    <div class="col-12">
        <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
            <a href="{{route('admin.terminalpoints.create')}}" class="btn btn-success">Create</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                    <table style="width:100%">
                        <tr>
                            <th>ID:</th>
                            <td>{{$termianPoints->id}}</td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td>{{$termianPoints->name}}</td>
                        </tr>
                        <tr>
                            <th>Source Country</th>
                            <td>
                                @foreach($country as $countrys)
                                @if($termianPoints->country_one_id==$countrys->id)
                                {{$countrys->name}}
                                @endif
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Destination Country</th>
                            <td>
                                @foreach($country as $countrys)
                                @if($termianPoints->country_two_id==$countrys->id)
                                {{$countrys->name}}
                                @endif
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Latitude</th>
                            <td>{{$termianPoints->latitude}}</td>
                        </tr>
                        <tr>
                            <th>Longitude</th>
                            <td>{{$termianPoints->longitude}}</td>
                        </tr>
                    </table>
                </div> -->
                <div class="country-div-list">
                    <ul class="weight-gain-ul">
                        <li>
                            <h4>ID : </h4>
                            <h6>{{$termianPoints->id}}</h6>
                        </li>
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$termianPoints->name}}</h6>
                        </li>
                        <li>
                            <h4>Source Country : </h4>
                            <h6>
                                @foreach($country as $countrys)
                                @if($termianPoints->country_one_id==$countrys->id)
                                {{$countrys->name}}
                                @endif
                                @endforeach
                            </h6>
                        </li>
                        <li>
                            <h4>Destination Country : </h4>
                            <h6>
                                @foreach($country as $countrys)
                                @if($termianPoints->country_two_id==$countrys->id)
                                {{$countrys->name}}
                                @endif
                                @endforeach
                            </h6>
                        </li>
                        <li>
                            <h4>Latitude : </h4>
                            <h6>{{$termianPoints->latitude}}</h6>
                        </li>
                        <li>
                            <h4>Longitude : </h4>
                            <h6>{{$termianPoints->longitude}}</h6>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="d-flex">
            <a href="{{route('admin.terminalpoints.edit' , $termianPoints->id) }}" class="btn btn-warning m-r-10">Edit</a>
            @if($termianPoints->status==0)
            <form action="{{route('admin.terminalpoints.destroy', $termianPoints->id)}}" method="post">
                {{ csrf_field() }}
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Activate</button>
            </form>
            @else
            <form action="{{route('admin.terminalpoints.destroy', $termianPoints->id)}}" method="post">
                {{ csrf_field() }}
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Deactivate</button>
            </form>

            @endif
        </div>
    </div>
</div>

@endsection
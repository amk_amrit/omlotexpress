@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.terminalpoints.index')}}"> Terminal Points</a></li>
<li class="breadcrumb-item active">View</li>
@endsection

@include('error-messages.message')
<div class="row">
    <div class="col-12">
        <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
            <a href="{{route('admin.terminalpoints.create')}}" class="btn btn-success">Create</a>
        </div>
    </div> 
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Source Country</th>
                                <th scope="col">Destination Country</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        @foreach($termianPoints as $termianPoint)
                        <tbody>
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$termianPoint->name}}</td>

                                <td>
                                    @foreach($country as $countrys)
                                    @if($termianPoint->country_one_id==$countrys->id)
                                    {{$countrys->name}}
                                    @endif
                                    @endforeach
                                </td>

                                <td>
                                    @foreach($country as $countrys)
                                    @if($termianPoint->country_two_id==$countrys->id)
                                    {{$countrys->name}}
                                    @endif
                                    @endforeach
                                </td>
                                <td class="dis-flex-term">

                                    <a href="{{route('admin.terminalpoints.show' , $termianPoint->id) }}" class="btn btn-info">View</a>
                                    <a href="{{route('admin.terminalpoints.edit' , $termianPoint->id) }}" class="btn btn-warning">Edit</a>
                                    @if($termianPoint->status==0)
                                    <form action="{{route('admin.terminalpoints.destroy', $termianPoint->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Activate</button>
                                    </form>
                                    @else
                                    <form action="{{route('admin.terminalpoints.destroy', $termianPoint->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Deactivate</button>
                                    </form>

                                    @endif
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{ $termianPoints->links() }}


@endsection
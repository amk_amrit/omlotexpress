@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.terminalpoints.index')}}"> Terminal Points</a></li>
<li class="breadcrumb-item active">Create</li>
@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.terminalpoints.store')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include('admin.pages.terminal-point.form', [$formMode="Publish"])
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
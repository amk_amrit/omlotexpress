@extends('admin.layouts.app')
@section('title','Courier')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="#">Courier</a></li>
<li class="breadcrumb-item active">Create</li>
@endsection
@section('content')


<!-- Row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="mb-0 text-white">Add Courier</h4>
            </div>
            <div class="card-body">
                <form action="#">
                    <div class="form-body">
                        <!-- <h3 class="card-title">Person Info</h3>
                        <hr> -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <ul class="nav nav-pills mb-4">
                                            <li class=" nav-item">
                                                <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">
                                                    National
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">
                                                    International
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content br-n pn">
                                            <div id="navpills-1" class="tab-pane active">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Source</label>
                                                            <input type="text" id="firstName" class="form-control" placeholder="Enter Source point">
                                                            <small class="form-control-feedback"> pickup point </small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Destination</label>
                                                            <input type="text" id="firstName" class="form-control" placeholder="Enter Destination point">
                                                            <small class="form-control-feedback"> Delivery point </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Select Sensitivity</label>
                                                            <select class="form-control custom-select">
                                                                <option value="">choose</option>
                                                                <option value="">Normal</option>
                                                                <option value="">Sensitivity</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Select Package Type</label>
                                                            <select class="form-control custom-select">
                                                                <option value="">Choose</option>
                                                                <option value="">Box</option>
                                                                <option value="">Sack</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">No. of package</label>
                                                            <input type="text" id="firstName" class="form-control" placeholder="Enter Number of package">
                                                            <small class="form-control-feedback"> Total number of package </small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Package Weight</label>
                                                            <input type="text" id="firstName" class="form-control" placeholder="Enter Weight of package">
                                                            <small class="form-control-feedback"> Total Weight of package </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="demo-radio-button">
                                                            <input name="group1" type="radio" class="with-gap" id="radio_1" checked />
                                                            <label for="radio_1">Air</label>
                                                            <input name="group1" type="radio" class="with-gap" id="radio_2" />
                                                            <label for="radio_2">Road</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="demo-checkbox">
                                                            <input type="checkbox" id="basic_checkbox_1" class="filled-in">
                                                            <label for="basic_checkbox_1">Insurance</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row pt-3">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Sender Name:</label>
                                                            <input type="text" id="senderName" class="form-control" placeholder="Enter Sender Name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Sender Phone:</label>
                                                            <input type="text" id="number" class="form-control" placeholder="Enter Sender Phone Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Sender Email</label>
                                                            <input type="email" id="email" class="form-control" placeholder="Enter Sender Email Address">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Receiver Name:</label>
                                                            <input type="text" id="recieverName" class="form-control" placeholder="Enter Receiver Name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Receiver Phone:</label>
                                                            <input type="text" id="number" class="form-control" placeholder="Enter Receiver Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Receiver Email</label>
                                                            <input type="email" id="email" class="form-control" placeholder="Enter Receiver Email Address">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="navpills-2" class="tab-pane">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Source</label>
                                                            <input type="text" id="firstName" class="form-control" placeholder="Enter Source point">
                                                            <small class="form-control-feedback"> pickup point </small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Destination Country</label>
                                                            <input type="text" id="firstName" class="form-control" placeholder="Enter Destination Country">
                                                            <small class="form-control-feedback"> Delivery Country </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Destination place</label>
                                                            <input type="text" id="firstName" class="form-control" placeholder="Enter Destination Country">
                                                            <small class="form-control-feedback"> Delivery Place </small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Select Sensitivity</label>
                                                            <select class="form-control custom-select">
                                                                <option value="">choose</option>
                                                                <option value="">Normal</option>
                                                                <option value="">Sensitivity</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Select Package Type</label>
                                                            <select class="form-control custom-select">
                                                                <option value="">Choose</option>
                                                                <option value="">Box</option>
                                                                <option value="">Sack</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">No. of package</label>
                                                            <input type="text" id="firstName" class="form-control" placeholder="Enter Number of package">
                                                            <small class="form-control-feedback"> Total number of package </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Package Weight</label>
                                                            <input type="text" id="firstName" class="form-control" placeholder="Enter Weight of package">
                                                            <small class="form-control-feedback"> Total Weight of package </small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="demo-checkbox">
                                                            <label class="control-label d-block">Package Weight</label>
                                                            <input type="checkbox" id="basic_checkbox_1" class="filled-in">
                                                            <label for="basic_checkbox_1">Insurance</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row pt-3">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Sender Name:</label>
                                                            <input type="text" id="senderName" class="form-control" placeholder="Enter Sender Name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Sender Phone:</label>
                                                            <input type="text" id="number" class="form-control" placeholder="Enter Sender Phone Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Sender Email</label>
                                                            <input type="email" id="email" class="form-control" placeholder="Enter Sender Email Address">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Receiver Name:</label>
                                                            <input type="text" id="recieverName" class="form-control" placeholder="Enter Receiver Name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Receiver Phone:</label>
                                                            <input type="text" id="number" class="form-control" placeholder="Enter Receiver Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Receiver Email</label>
                                                            <input type="email" id="email" class="form-control" placeholder="Enter Receiver Email Address">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>










                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">
                                Submit
                            </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Row -->





<!-- 
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <form action="#" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="control-label">Name</label>
                        <input type="text" class="form-control" name="name">

                    </div>

                    <div class="form-group">
                        <label class="control-label">Description</label>
                        <textarea class="form-control" rows="5" cols="10" name="description"></textarea>

                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Submit">
                    </div>

                </form>
            </div>
        </div>
    </div>
</div> -->

@endsection
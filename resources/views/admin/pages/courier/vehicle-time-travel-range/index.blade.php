@extends('admin.layouts.app')
@section('title','Vehicle Time Range ')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicle-time-range.index')}}">Vehicle Time Range </a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.vehicle-time-range.create')}}" class="btn btn-success m-b-10">Edit</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                            <tr>
                                <th scope="col"> S.N. </th>
                                <th scope="col"> Country </th>
                                <th scope="col"> Min Time </th>
                                <th scope="col"> Max Time </th>
               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($vehicleTimeRanges as $vehicleTimeRange)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$vehicleTimeRange->country->name}}</td>
                                    <td>{{$vehicleTimeRange->min_time}}</td>
                                    <td>{{$vehicleTimeRange->max_time}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

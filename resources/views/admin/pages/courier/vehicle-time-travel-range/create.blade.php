@extends('admin.layouts.app')
@section('title','Vehicle Time Range')
@section('content')

@include('error-messages.message')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicle-time-range.index')}}"> Vehicle Time Range</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3>Create Vehicle Time Travel Range for Your Country</h3>
                <form action="{{route('admin.vehicle-time-range.store')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @foreach($vehicleRanges as $vehicleRange)
                        <table class="table" >
                        <tr> 
                            <td><input type="hidden" class="form-control min-weight" value="{{$vehicleRange->id}}"  name="delete_id[]"  ></td> 
                            <td><input type="number" class="form-control min-time" value="{{$vehicleRange->min_time}}" placeholder="Enter  minimum time"  name="min_time[]"  ></td>
                            <td><input type="number" class="form-control max-time" value="{{$vehicleRange->max_time}}" placeholder="Enter  maxmium time"  name="max_time[]" ></td>
                        </tr>
                        </table>
                        @endforeach
                        <table class="table" id="dynamic_field">
                            
                        </table>
                            
                        <button style="margin-left: 350px;" type="button" name="add" id="add" class="btn btn-success">Add More</button>

                        <div class="form-group">
                        <input type="submit" class="btn btn-success"
                        value="Publish">
                        </div>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    @include('admin.pages.courier.vehicle-time-travel-range.script');
@endpush


@extends('admin.layouts.app')
@section('title','Airport Links')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.airport-links.index')}}">Airport Links</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')
    <div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.airport-links.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                        <th scope="col"> ID </th>
                        <th scope="col"> Linked Airport Name  </th>
                        <th scope="col"> Link Type </th>
                        <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        @foreach($airportLinks as $airportLink)
                        <td>{{$loop->iteration}}</td>
                        <td>{{ucfirst(trans($airportLink->airportOne->airportOneName))}}--{{ucfirst(trans($airportLink->airportTwo->airportTwoName))}}</td>
                         <td>{{ucfirst(trans($airportLink->air_link_type))}}</td>
                                <td class="table-flex">
                                <a href="{{route('admin.airport-charges.edit', $airportLink->id)}}" class="btn btn-primary m-r-10">View charge</a>
                                <a href="{{route('admin.airport-charges.show', $airportLink->id)}}" class="btn btn-info m-r-10">Create charge</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
    

    
@endsection






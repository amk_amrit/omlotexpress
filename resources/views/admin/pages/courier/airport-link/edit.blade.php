@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.airport-links.index')}}"> Airport List</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
<form action="{{route('admin.airport-links.update', $airportLinks->id )}}" method="POST" enctype="multipart/form-data">
<input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label"  > Country</label>
            <input type="hidden" class="form-control" name="country_id" readonly="readonly" value="{{$airportLinks->country->id}}" >
            <p>{{$airportLinks->country->name}}</p>
            @if ($errors->has('country_id')) <p style="color:red;">{{ $errors->first('country_id') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label"> Source Airport</label>
            <input type="hidden" class="form-control" name="source_airport_id" readonly="readonly" value="{{$airportLinks->source_airport_id}}" >
            <p>{{$airportLinks->airport->name}}</p>
            @if ($errors->has('country_id')) <p style="color:red;">{{ $errors->first('country_id') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label"> Destination Airport</label>
            <input type="hidden" class="form-control" name="destination_airport_id" readonly="readonly" value="{{$airportLinks->destination_airport_id}}" >
            <p>{{$airportLinks->airport->name}}</p>
            @if ($errors->has('country_id')) <p style="color:red;">{{ $errors->first('country_id') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label"> Quantity Group</label>
            <input type="hidden" class="form-control" name="quantity_group_id" readonly="readonly" value="{{$airportLinks->quantity_group_id}}" >
            <p>{{$airportLinks->quantity_group_id}}</p>
            @if ($errors->has('country_id')) <p style="color:red;">{{ $errors->first('country_id') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label"> Rate</label>
            <input type="text" class="form-control" name="rate" value="{{$airportLinks->rate}}" >
            @if ($errors->has('rate')) <p style="color:red;">{{ $errors->first('rate') }}</p> @endif <br>
        </div>

        <div class="form-group">
            <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{$airportLinks->status == 1 ? 'checked' : '' }}>
            <label class="custom-control-label" for="defaultChecked">Active</label>
        </div>
        </div>
        <div class="form-group">
        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{$airportLinks->status == 0 ? 'checked' : '' }}>
            <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection

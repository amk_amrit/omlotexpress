<script>
    var airportInfo = {
        airports2: [],
        selectedAirports2: []
    };
    var i = 1;

    var j = 0;
    $(document).ready(function() {

        $('#add').click(function() {
            i++;

            var addAirports2 = airportInfo.airports2;

            if (j === addAirports2.length) {
                alert('Cannot Add More than the available number of airports')
                $(this).attr('disabled', true);
                return false;
            }
            j++;

            var addAirports2 = airportInfo.airports2;
            // console.log(airports2,data)

            var addAirport2Html = `
                 <tr id=row${i}>
                 <td>
                 <select id="addairport" class="form-control" name="airport_two_id[]">
                 <option disable selected value=""  >--Select Aiport--</option>
                 `;



            $.each(addAirports2, function(index, airport) {
                addAirport2Html += `
                <option value=${airport.id}>${airport.name}</option>
                       `;
            });
            addAirport2Html += `</select>
                 </td>
                 <br>
                 <td>
                 <button type="button" name="remove" id=${i} class="btn btn-danger btn_remove">X</button>
                 </td>
                 </tr>
                 `;

            console.log('add j', j, addAirports2.length);
            $('#dynamic_field').append(addAirport2Html);
        });
        $(document).on('click', '.btn_remove', function() {
            var selectedAirports2 = airportInfo.selectedAirports2;
            var button_id = $(this).attr("id");
            var nearestOption = $(this).closest('tr').find('#addairport').val();
            if (nearestOption != '') {
                nearestOptionIndex = selectedAirports.indexOf(nearestOption);
                console.log('index', nearestOptionIndex);
                selectedAirports.splice(nearestOptionIndex, 1);
                console.log('after removal', selectedAirports);
                // j--;
                // $('#row'+button_id+'').remove(); 
                $('#add').attr('disabled', false);

            }
            j = j - 1;
            console.log('remove j', j)

            $('#row' + button_id + '').remove();
        });

    });

    function getDataCountryOne(airportLinkOne) {

        $.ajax({
            type: 'GET',
            url: "{{route('admin.airport-links.getdatacountryone')}}",
            data: airportLinkOne,
            datatype: "json",
        }).done(function(data) {
            $("#dynamic_content").empty().html(data);
        }).fail(function(customError) {
            console.log(customError);
        });
    }

    $('#scope_one').on('click', function(e) {
        e.preventDefault();
        let airportLinkOne = $('#airportlinks').serialize();
        getDataCountryOne(airportLinkOne);
    });

    function getDataCountryTwo(airportLinkTwo) {

        $.ajax({
            type: 'GET',
            url: "{{route('admin.airport-links.getdatacountrytwo')}}",
            data: airportLinkTwo,
            datatype: "json",
        }).done(function(data) {

            var airports2 = airportInfo.airports2 = data;
            //console.log(airports2,data)

            var airport2Html = `
                 <label class="control-label">Airport2</label>
    <select class="form-control" name="airport_two_id[]" id="airport_two_id">
        <option selected disabled value="">--Select Airport--</option>
                 `;



            $.each(airports2, function(index, airport) {
                airport2Html += `
                <option value=${airport.id}>${airport.name}</option>
                       `;
            });

            // airport2Html += '</select>'



            $("#dynamic_form").empty().html(airport2Html);
        }).fail(function(customError) {
            console.log(customError);
        });
    }

    $(document).on('change', '#airport_two_id', function(e) {
        e.preventDefault();
        console.log(airport);
        var airport = $(this).val();
        selectedAirports = airportInfo.selectedAirports2;
        var airportIndex = selectedAirports.indexOf(airport);

        if (airportIndex == -1) {
            if (!selectedAirports.includes(airport)) {
                selectedAirports[0] = airport;
                j = 1;
            } else {
                selectedAirports[airportindex] = airport;
            }
        } else {
          //  e.preventDefault();

            $(this).find(":selected").prop("selected", false);

            //     selectedAirports.splice(airportIndex,1);
            // j --;
            alert('You have already selected this airport');

            //   selectedIndex = $(this).find(":selected").index(); 
            //  $(this).prop('selectedIndex',0);

        }



        console.log('after adding', selectedAirports)
    });


    $(document).on('change', '#addairport', function(e) {

        e.preventDefault();
        //console.log(airport);
        var airport = $(this).val();

        selectedAirports = airportInfo.selectedAirports2;
        if (selectedAirports.includes(airport)) {

            //$(this).val($.data(this, 'val')); //set back
        
          // $(this).find(":selected").prop("selected", false);
            alert('You have already selected this airport');
        } else {
            selectedAirports.push(airport);
        }
        console.log(selectedAirports)
    });

    $('#scope_two').on('click', function(e) {
        e.preventDefault();
        let airportLinkTwo = $('#airportlinks').serialize();
        getDataCountryTwo(airportLinkTwo);
    });

    //Store Data
    function storeData(airportLinkStore) {
        $.ajax({
            type: 'POST',
            url: "{{route('admin.airport-links.store')}}",
            data: airportLinkStore,
            datatype: "json",
        }).done(function(data) {
            console.log(data);
           // window.location.href = "{{route('admin.airport-links.index')}}";
        }).fail(function(data) {
            if (data.status == 500) {
                $('#errorMessage').show().html(data.responseJSON.error);
            }
            console.log(data.responseJSON.error);
            if (data.status == 422) {
                var errorString = "<ol type='1'>";
                for (error in data.responseJSON.error) {
                    errorString += "<li>" + data.responseJSON.error[error] + "</li>";
                }
                errorString += "</ol>";

                $('#errorMessage').show().html(errorString);
            }
        });
    }

    $('#submitted').on('click', function(e) {
        e.preventDefault();
        let airportLinkStore = $('#airportlinks').serialize();
        storeData(airportLinkStore);
    });
    $('#errorMessage').hide();
</script>
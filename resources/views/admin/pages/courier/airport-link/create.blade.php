@extends('admin.layouts.app')
@section('title','Airport Link')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.airport-links.index')}}">Airport Links</a></li>
<li class="breadcrumb-item active">Create Links</li>
@endsection
@include('error-messages.message')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-danger" role="alert" id="errorMessage">

                </div>
                <form action="" method="" id="airportlinks" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card card-country-cust">
                        <div class="card-header">COUNTRY - A</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Country</label>
                                        <select class="form-control" name="country_id_one">
                                            <option selected disabled value="">--Select Country--</option>
                                            @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label class="control-label">Airport Type</label>
                                        <select class="form-control" name="scope_one" id="scope_one">
                                            <option selected disabled value="">--Select Airport--</option>
                                            <option value="domestic">Domestic</option>
                                            <option value="international">International</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div id="dynamic_content"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-country-cust">
                        <div class="card-header">COUNTRY - B</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Country</label>
                                        <select class="form-control" name="country_id_two">
                                            <option selected disabled value="">--Select Country--</option>
                                            @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Airport Type</label>
                                        <select class="form-control" name="scope_two" id="scope_two">
                                            <option selected disabled value="">--Select Airport--</option>
                                            <option value="domestic">Domestic</option>
                                            <option value="international">International</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div id="dynamic_form"></div>
                                    </div>
                                    <table class="table" id="dynamic_field">
                                    </table>
                                </div>
                                <div class="col-lg-12">
                                    <button style="float:right;" type="button" name="add" id="add" class="btn btn-success">Add More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" id="submitted" class="btn btn-success" value="Publish">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@include('admin.pages.courier.airport-link.airportLinkScript');
@endpush
@extends('admin.layouts.app')
@section('title','Agency Class Charge')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.agency-links.index')}}">Agency Charge List</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
@include('error-messages.message')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.agency-links.update', $agency_id)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                   <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Agency name</th>
                                <th scope="col">Disatence</th>
                                </tr>
                            </thead>
                            <tbody>
                            <input type="hidden" class="form-control" name="agency_id" value="{{$agency_id}}">
                                @foreach($getAgencyData as $getAgencyDatas)
                                <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>
                                    <select class="form-control" name="link_id[]">
                                        <option  disabled value="">--Select Agency --</option>
                                        @foreach($agencies as $agency)
                                            <option value="{{$agency->id}}" {{ ($agency->id == old('link_id')) || $agency->id == $getAgencyDatas->id ? 'selected': '' }}">{{$agency->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                
                                    <input type="hidden" class="form-control" name="link_agency_id[]" value="{{$getAgencyDatas->id}}">

                                
                                <td>
                                    <input type="text" class="form-control" name="distance[]" value="{{$getAgencyDatas->distance}}">
                                </td> 
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                            <div class="form-group">
                            <input type="submit" class="btn btn-success"
                                    value="Update">
                                    </div>
                   </form>
            </div>
        </div>
    </div>
</div>

@endsection

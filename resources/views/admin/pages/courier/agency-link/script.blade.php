<script>
    function getData(transitclasscharge){

    $.ajax(
                {
                    type: 'GET',
                    url: "{{route('admin.agency-link-data')}}",
                    data:transitclasscharge,
                    datatype: "json",
                }).done(function (data) {
                    $("#dynamic_content").empty().html(data).trigger("click");
            }).fail(function (customError) {
                console.log(customError);
                //console.log("data: " + data.responseJSON);
                // console.log( 'error',customError.responseJSON.message);
                // Display an error toast, with a title
                // toastr.error(customError.responseJSON.message);
            });
        }

        $('#agency_id').on('click',function(e){
            e.preventDefault();
            let transitclasscharge=$('#agencylinks').serialize();
            getData(transitclasscharge);
        })
    </script>
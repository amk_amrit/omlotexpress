@extends('admin.layouts.app')
@section('title','Agency Link')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.agency-links.index')}}">Agency Links</a></li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.agency-links.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                            <th scope="col">S.N</th>
                            <th scope="col"> Agency Name </th>
                            <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($agencyLinks as $agencyLink)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$agencyLink->agency->name}}</td>
                                <td class="table-flex">
                                    <a href="{{route('admin.agency-links.show', $agencyLink->agency_id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

    
@endsection

<form action="{{route('admin.agency-links.store' )}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
<table id="myTable" class="table">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Agency</th>
      <th scope="col">Distance</th>
    </tr>
  </thead>
  <input type="hidden" name="agency_id" value="{{$agency_id}}">
  @foreach($getAgencyData as $getAgencyDatas)

  <tbody>
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{ $getAgencyDatas->name}}</td>
      <input type="hidden" value="{{$getAgencyDatas->id}}" name="link_id[]">
      <td><input type="text" class="form-control distance" placeholder="Enter  Distance"  name="distance[]" value="{{old('distance')}}"></td>
    </tr>
  </tbody>
  @endforeach        

</table>

        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>

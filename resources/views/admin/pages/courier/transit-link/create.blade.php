@extends('admin.layouts.app')
@section('title','Transit Link')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-links.index')}}">Agency Links</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')
@include('error-messages.message')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <form action="" method="" id="transitlinks" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="control-label" >Agency </label>
                                    <select class="form-control" name="transit_id" id="transit_id" > 
                                        <option disabled selected value="" >-- Select Transit --</option>
                                            @foreach($transits as $transit)
                                                <option value="{{$transit->id}}" {{$transit->id == old('transit_id') ? 'selected' : ''}} >{{$transit->name}}</option>
                                            @endforeach
                                    </select>
                                    </div>
                                    <!-- <div class="form-group">
                                            <input type="button" id="publish" class="btn btn-success"
                                    value="Publish">
                                    </div> -->
                                                </form>
                                                <div id="dynamic_content"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
@endsection
@push('scripts')
@include('admin.pages.courier.transit-link.script');
@endpush

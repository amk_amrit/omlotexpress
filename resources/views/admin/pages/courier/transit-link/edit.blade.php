@extends('admin.layouts.app')
@section('title','Transit Link Charge')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-links.index')}}">Transit Link List</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
@include('error-messages.message')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.transit-links.update', $transit_id)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                   <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Transit name</th>
                                <th scope="col">Disatence</th>
                                </tr>
                            </thead>
                            <tbody>
                            <input type="hidden" class="form-control" name="transit_id" value="{{$transit_id}}">
                                @foreach($transitLinkDatas as $transitLinkData)
                                <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>
                                    <select class="form-control" name="link_id[]">
                                        <option  disabled value="">--Select Transit --</option>
                                        @foreach($transits as $transit)
                                            <option value="{{$transit->id}}" {{ ($transit->id == old('link_id')) || $transit->id == $transitLinkData->id ? 'selected': '' }}">{{$transit->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                
                                    <input type="hidden" class="form-control" name="link_transit_id[]" value="{{$transitLinkData->id}}">

                                
                                <td>
                                    <input type="text" class="form-control" name="distance[]" value="{{$transitLinkData->distance}}">
                                </td> 
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                            <div class="form-group">
                            <input type="submit" class="btn btn-success"
                                    value="Update">
                                    </div>
                   </form>
            </div>
        </div>
    </div>
</div>

@endsection

<script>
    function getData(transitclasscharge){

    $.ajax(
                {
                    type: 'GET',
                    url: "{{route('admin.transit-link-data')}}",
                    data:transitclasscharge,
                    datatype: "json",
                }).done(function (data) {
                    $("#dynamic_content").empty().html(data);
            }).fail(function (customError) {
                console.log(customError);
                //console.log("data: " + data.responseJSON);
                // console.log( 'error',customError.responseJSON.message);
                // Display an error toast, with a title
                // toastr.error(customError.responseJSON.message);
            });
        }

        $('#transit_id').on('click',function(e){
            e.preventDefault();
            let transitclasscharge=$('#transitlinks').serialize();
            getData(transitclasscharge).trigger("click");
        });
    </script>
<form action="{{route('admin.transit-links.store' )}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
<table id="myTable" class="table">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Transit</th>
      <th scope="col">Distance</th>
    </tr>
  </thead>
  <input type="hidden" name="transit_id" value="{{$transit_id}}">
  @foreach($getTransitData as $getTransitDatas)

  <tbody>
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{ $getTransitDatas->name}}</td>
      <input type="hidden" value="{{$getTransitDatas->id}}" name="link_id[]">
      <td><input type="text" class="form-control distance" placeholder="Enter  Distance"  name="distance[]"></td>
    </tr>
  </tbody>
  @endforeach        

</table>

        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>

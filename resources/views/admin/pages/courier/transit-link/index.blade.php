@extends('admin.layouts.app')
@section('title','Transit Link')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-links.index')}}">Transit Links</a></li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.transit-links.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                            <th scope="col">S.N</th>
                            <th scope="col"> Transit Name </th>
                            <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transitLinkData as $transitLinkDatas)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$transitLinkDatas->transits->name}}</td>
                                <td class="table-flex">
                                    <a href="{{route('admin.transit-links.show', $transitLinkDatas->transit_id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

    
@endsection

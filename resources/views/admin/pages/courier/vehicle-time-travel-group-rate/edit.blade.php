@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.time-travel-group-rates.index')}}"> Time Travel Group Rate</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
<form action="{{route('admin.time-travel-group-rates.update', $vehicleTimeTravelGroupRates->id )}}" method="POST" enctype="multipart/form-data">
<input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label"  > Country</label>
            <input type="hidden" class="form-control" name="country_id" readonly="readonly" value="{{$vehicleTimeTravelGroupRates->country->id}}" >
            <p>{{$vehicleTimeTravelGroupRates->country->name}}</p>
            @if ($errors->has('country_id')) <p style="color:red;">{{ $errors->first('country_id') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" > Time Travel Group</label>
            <input type="hidden" class="form-control" name="time_travel_group_id" readonly="readonly" value="{{$vehicleTimeTravelGroupRates->vTimeTravelGroup->id}}" >
            <p>{{$vehicleTimeTravelGroupRates->vTimeTravelGroup->name}}</p>
            @if ($errors->has('time_travel_group_id')) <p style="color:red;">{{ $errors->first('time_travel_group_id') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label"  > Currency</label>
            <input type="hidden" class="form-control" name="currency" readonly="readonly" value="{{$vehicleTimeTravelGroupRates->currency}}"  >
            <p>{{$vehicleTimeTravelGroupRates->currency}}</p>
            @if ($errors->has('currency')) <p style="color:red;">{{ $errors->first('currency') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" > Rate</label>
            <input type="text" class="form-control" name="rate" value="{{$vehicleTimeTravelGroupRates->rate}}" >
            @if ($errors->has('rate')) <p style="color:red;">{{ $errors->first('rate') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{$vehicleTimeTravelGroupRates->status == 1 ? 'checked' : '' }}>
            <label class="custom-control-label" for="defaultChecked">Active</label>
        </div>
        </div>
        <div class="form-group">
        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{$vehicleTimeTravelGroupRates->status == 0 ? 'checked' : '' }}>
            <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection

@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.time-travel-group-rates.index')}}"> Vehicle Time Travel Group Rate</a></li>
    <li class="breadcrumb-item active">Select Country</li>
@endsection
<form action="{{route('admin.create-time-travel-group-rate' )}}" method="PUT" enctype="multipart/form-data">
            {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label" > Country</label>
            <select class="form-control" name="country">
                @foreach($countries as $country)
                    <option value="{{$country->id}}">{{$country->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('country')) <p style="color:red;">{{ $errors->first('country') }}</p> @endif <br>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection

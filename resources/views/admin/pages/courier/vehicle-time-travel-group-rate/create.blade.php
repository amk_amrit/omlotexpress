@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.time-travel-group-rates.index')}}"> Vehicle Time Travel Group Rate</a></li>
    <li class="breadcrumb-item active">Select Country</li>
@endsection
@include('error-messages.message')
<form action="{{route('admin.time-travel-group-rates.store' )}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Time Travel Group</th>
      <th scope="col">Rate</th>
    </tr>
  </thead>
  <input type="hidden" name="country_id" value="{{$country_id}}">
  @foreach($vehicleTimeTravelGroupRates as $vehicleTimeTravelGroupRate)

  <tbody>
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{ $vehicleTimeTravelGroupRate->name }}</td>
      <input type="hidden" value="{{$vehicleTimeTravelGroupRate->id}}" name="time_travel_group_id[]">
      <td><input type="text" class="form-control rates" placeholder="Enter  rate"  name="rate[]" ></td>
    </tr>
  </tbody>
  @endforeach        

</table>

        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection

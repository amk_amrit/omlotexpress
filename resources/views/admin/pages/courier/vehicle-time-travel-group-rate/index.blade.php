@extends('admin.layouts.app')
@section('title','Dashboard')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.time-travel-group-rates.index')}}">Time Travel Group Rate</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
        <a href="{{route('admin.time-travel-group-rates.create')}}" class="btn btn-success">Create</a>
    </div>
</div>

<div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> ID </th>
                <th scope="col"> Country  </th>
                <th scope="col"> Time Travel Group </th>
                <th scope="col"> Rate  </th>
                <th scope="col">Currency</th>
                <th scope="col"> Action </th>
            </tr>
        </thead>
        @foreach($vehicleTimeTravelGroupRates as $vehicleTimeTravelGroupRate)
        <tbody>
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$vehicleTimeTravelGroupRate->country->name}}</td>
                <td>{{$vehicleTimeTravelGroupRate->vTimeTravelGroup->name}}</td>
                <td>{{$vehicleTimeTravelGroupRate->rate}}</td>
                <td>{{$vehicleTimeTravelGroupRate->currency}}</td>
                <td>
                    <a href="#" class="btn btn-info">View</a>
                    <a href="{{route('admin.time-travel-group-rates.edit', $vehicleTimeTravelGroupRate->id)}}" class="btn btn-warning">Edit</a>
                    @if($vehicleTimeTravelGroupRate->status==0)
                    <form action="{{ route('admin.time-travel-group-rates.destroy', $vehicleTimeTravelGroupRate->id)}}" method="post">
                    {{ csrf_field() }}
                    @method('DELETE')
                        <button type="submit" class="btn btn-danger" style="margin-top: 10px; margin-left:10px;">Activate</button>
                        </form>
                    @else
                    <form action="{{ route('admin.time-travel-group-rates.destroy', $vehicleTimeTravelGroupRate->id)}}" method="post">
                    {{ csrf_field() }}
                    @method('DELETE')
                        <button type="submit" class="btn btn-danger" style="margin-top: 10px; margin-left:10px;">Deactivate</button>
                        </form>
                    
                    @endif
               
                </td>
                
            </tr>
        </tbody>
        @endforeach
    </table>
    </div>
    
    {{ $vehicleTimeTravelGroupRates->links() }}

    
@endsection

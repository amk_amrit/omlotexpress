@extends('admin.layouts.app')
@section('title','Agency Transit Vehicle Charge')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.agency-offices.index')}}">Agency Transit Links</a></li>
<li class="breadcrumb-item active">Create Vehicle Charge</li>
@endsection
@include('error-messages.message')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
      <div class="alert alert-danger"  role="alert" id="errorMessage">
  
      </div>
        <form action="" method="" id="agencyTransitVehicleCharge" enctype="multipart/form-data">
                {{ csrf_field() }}
              <div class="form-group">
                <label class="control-label">Agency Transit Linked Name</label>
                <select class="form-control" name="agency_transit_connection_id" >
                  <option selected disabled value="">-- Select Agency Transit Linked --</option>
                  @foreach($agencyTransitLinkNames as $agencyTransitLinkName)
                  <option value="{{$agencyTransitLinkName->agenct_transit_connection_id}}">{{$agencyTransitLinkName->agencyName}}--{{$agencyTransitLinkName->transitName}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label class="control-label">Quantity Group</label>
                <select class="form-control" name="quantity_group_id" id="quantity_group_id" >
                  <option selected disabled value="">--Select Quantity Group--</option>
                  @foreach($getquanityGroupNames as $getquanityGroupName)
                  <option value="{{$getquanityGroupName->id}}">{{$getquanityGroupName->group_name}}</option>
                  @endforeach
                </select>
              </div>
              
              <div class="form-group">
                <div id="dynamic_content"></div>
              </div>
          <div class="form-group">
        <input type="submit" id="submitted" class="btn btn-success"
        value="Publish">
        </div>
            </form>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
@include('admin.pages.courier.agency-transit-vehicle-charge.agencyTransitVehicleChargeScript');
@endpush
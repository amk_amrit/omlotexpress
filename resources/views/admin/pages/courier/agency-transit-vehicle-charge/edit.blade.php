@extends('admin.layouts.app')
@section('title','Agency Transit Vehicle Charge')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admin-charges.index')}}">Agency Transit Conncetion</a></li>
    <li class="breadcrumb-item active">@foreach($agencyTransitLinkNames as $agencyTransitLinkName)
        {{$agencyTransitLinkName->agencyName}}-{{$agencyTransitLinkName->transitName}}
        @endforeach</li>
@endsection

@section('content')

@include('error-messages.message')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                @if(count($quantityGroupRanges) > 0) 
                    <form method="POST" action="{{route('admin.agency-transit-vehicle-charges.update', $agencyTransitConnectionId)}}" enctype="multipart/form-data" >
                    <input type="hidden" name="_method" value="PUT">
                    {{ csrf_field() }}
                    <table  class="table">
                        <thead>
                        <tr>
                        <th scope="col"> ID </th>
                        <th scope="col"> Quantity Group Name  </th>
                        <th scope="col"> Rate </th>
                        <th scope="col"> Rate Type </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                       
                                        
                        @foreach($quantityGroupRanges as $quantityGroupRange)
                        <td>{{$loop->iteration}}</td>
                        <td>{{$quantityGroupRange->group_name}}({{$quantityGroupRange->min_weight}}-{{$quantityGroupRange->max_weight}})</td>
                        <input type="hidden" name="id[]" class="form-control" value="{{$quantityGroupRange->id}}">
                        <input type="hidden" name="quantity_group_range_id[]" class="form-control" value="{{$quantityGroupRange->quantity_group_range_id}}">

                        <td>
                            <input type="number" name="rate[]" class="form-control" value="{{$quantityGroupRange->rate}}">
                        </td>
                        <td>
                            <select name="rate_type[]" class="form-control">
                                @if($quantityGroupRange->rate_type=='fix'){
                                    <option value="fix">Fix</option>
                                    <option value="per_kg">Per Kg</option>
                                }
                                @else{
                                    <option value="per_kg">Per Kg</option>
                                    <option value="fix">Fix</option>
                                }
                                @endif
                               
                               
                            </select>
                            </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="form-group">
                    <input type="submit"  class="btn btn-success"
                     value="Update">
                        
                    </form>
                    @else
                        <p>There is No data !! please Create charge to click button </p>
                            <a href="{{route('admin.admin-charges.create', $adminId)}}" class="btn btn-info m-r-10">Create charge</a>
                    
                     @endif
                </div>
            </div>
        </div>
    </div>


</div>
    

    
@endsection






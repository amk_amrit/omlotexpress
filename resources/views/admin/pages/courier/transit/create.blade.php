@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transits.index')}}">All Transit</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@include('error-messages.message')
<form action="{{route('admin.transits.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label" >Country</label>
            <select class="form-control" name="country_id">
                <option selected disabled value="">--Select Country--</option>
                @foreach($countries as $country)
                <option value="{{$country->id}}">{{$country->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('country_id')) <p style="color:red;">{{ $errors->first('country_id') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" >Transit Class</label>
            <select class="form-control" name="transit_class_id">
                <option selected disabled value="">--Select Transit Class--</option>
                @foreach($transitclass as $transitclasses)
                <option value="{{$transitclasses->id}}">{{$transitclasses->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('transit_class_id')) <p style="color:red;">{{ $errors->first('transit_class_id') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" >Office</label>
            <select class="form-control" name="office_id">
                <option selected disabled value="">--Select Office--</option>
                @foreach($offices as $office)
                <option value="{{$office->id}}">{{$office->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('office_id')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" >Name</label>
            <input type="text" class="form-control" name="name" value="{{old('name')}}" >
            @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" >Email</label>
            <input type="text" class="form-control" name="email" value="{{old('email')}}" >
            @if ($errors->has('email')) <p style="color:red;">{{ $errors->first('email') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" >Phone</label>
            <input type="text" class="form-control" name="phone" value="{{old('phone')}}" >
            @if ($errors->has('phone')) <p style="color:red;">{{ $errors->first('phone') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" >Description</label>
            <textarea class="form-control" name="description">

            </textarea>
            @if ($errors->has('description')) <p style="color:red;">{{ $errors->first('description') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" >Address</label>
            <input type="text" class="form-control" name="address" id="address" value="{{old('address')}}"  >
            @if ($errors->has('address')) <p style="color:red;">{{ $errors->first('address') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" >Latitude</label>
            <input class="form-control" name="latitude" id="latitude" placeholder="Enter Longitude" value="{{old('latitude')}}">
        </div>
        @if ($errors->has('latitude')) <p style="color:red;">{{ $errors->first('latitude') }}</p> @endif <br>
        <div class="form-group">
            <label class="control-label" >Longitude</label>
            <input class="form-control" name="longitude" id="longitude" placeholder="Enter Longitude" value="{{old('longitude')}}">
        </div>
        @if ($errors->has('longitude')) <p style="color:red;">{{ $errors->first('longitude') }}</p> @endif <br>
        <div class="form-group">
            <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" checked>
            <label class="custom-control-label" for="defaultChecked">Active</label>
        </div>
        </div>
        <div class="form-group">
        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0">
            <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection
@push('scripts')
@include('admin.pages.courier.transit.transit-map-scripts');
@endpush

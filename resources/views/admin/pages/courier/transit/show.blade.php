
@extends('admin.layouts.app')
@section('title','Transit')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transits.index')}}">Agency</a></li>
    <li class="breadcrumb-item active">{{$transits->name}}</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 text-right">
            <a href="{{route('admin.transits.create')}}" class="btn btn-success m-b-10">Create</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                        <tr>
                        <th>Country</th>
                        <td>{{$transits->country->name}}</td>
                    </tr>
                    <tr>
                        <th>Transit Class</th>
                        <td>{{$transits->transitClass->name}}</td>
                    </tr>
                    <tr>
                        <th>Office</th>
                        <td>{{$transits->office->name}}</td>
                    </tr>
                    <tr>
                        <th>Office</th>
                        <td>{{$transits->name}}</td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td>{{$transits->address}}</td>
                    </tr>
                    <tr>
                        <th>Phone</th>
                        <td>{{$transits->phone}}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{$transits->email}}</td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td>{{$transits->description}}</td>
                    </tr>
                    <tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($transits->status == 1)
                                        <span> Activated</span>
                                    @else
                                        <span> Deactivated</span>
                                    @endif
                                </td>
                            </tr>

                        </table>
                    </div>

                    <div class="form-btn" >
                        <a href="{{ route('admin.transits.edit', $transits->id) }}" class="btn btn-warning">Edit</a>
                        <form action="{{ route('admin.transits.destroy', $transits->id)}}" method="post">
                            {{ csrf_field() }}
                            @method('DELETE')
                            @if($transits->status==0)
                                <button type="submit" class="btn btn-success">Activate</button>
                            @else
                                <button type="submit" class="btn btn-danger">Deactivate</button>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
               
@endsection
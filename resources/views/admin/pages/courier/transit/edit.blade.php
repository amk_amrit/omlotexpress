@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transits.index')}}"> All Transit</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
<form action="{{route('admin.transits.update', $transits->id )}}" method="POST" enctype="multipart/form-data">
<input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
            
        <div class="form-group">
            <label class="control-label" >country</label>
            <select class="form-control" name="country_id" >
            <option selected disabled value="">--select an  Country--</option>
                @foreach($countries as $country)
                    <option value="{{$country->id}}"
                        {{$country->id == old('country_id') ? 'selected' : ''}}
                        <?php if(isset($countries) && $transits->country->id == $country->id ) echo 'selected'; ?>
                        >
                        {{$country->name}}
                    </option>

                @endforeach

            </select>
            @if ($errors->has('country_id')) <p style="color:red;">{{ $errors->first('country_id') }}</p> @endif <br>

        </div>
        <div class="form-group">
            <label class="control-label" >Transit Class</label>
            <select class="form-control" name="transit_class_id" >
            <option selected  disabled value="">--select an  Transit Class--</option>
                @foreach($transitclass as $transitclasses)
                    <option value="{{$transitclasses->id}}"
                        {{$country->id == old('transit_class_id') ? 'selected' : ''}}
                        <?php if(isset($transitclasses) && $transits->transitClass->id == $transitclasses->id ) echo 'selected'; ?>
                        >
                        {{$transitclasses->name}}
                    </option>

                @endforeach

            </select>
            @if ($errors->has('transit_class_id')) <p style="color:red;">{{ $errors->first('transit_class_id') }}</p> @endif <br>

        </div>
        <div class="form-group">
            <label class="control-label" >Office</label>
            <select class="form-control" name="office_id" >
            <option selected disabled value="">--select an  Office--</option>
                @foreach($offices as $office)
                    <option value="{{$office->id}}"
                        {{$office->id == old('transit_class_id') ? 'selected' : ''}}
                        <?php if(isset($office) && $transits->office->id == $office->id ) echo 'selected'; ?>
                        >
                        {{$office->name}}
                    </option>

                @endforeach

            </select>
            @if ($errors->has('office_id')) <p style="color:red;">{{ $errors->first('office_id') }}</p> @endif <br>

        </div>
        <div class="form-group">
            <label class="control-label" >Name</label>
            <input class="form-control" name="name" value="{{$transits->name}}">
            @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>

        </div>
        <div class="form-group">
            <label class="control-label" >Address</label>
            <input class="form-control" name="address" id="address" value="{{$transits->address}}">
            @if ($errors->has('address')) <p style="color:red;">{{ $errors->first('address') }}</p> @endif <br>

        </div>
        <div class="form-group">
            <label class="control-label" >Phone</label>
            <input class="form-control" name="phone" value="{{$transits->phone}}">
            @if ($errors->has('phone')) <p style="color:red;">{{ $errors->first('phone') }}</p> @endif <br>

        </div>
        <div class="form-group">
            <label class="control-label" >Email</label>
            <input class="form-control" name="email" value="{{$transits->email}}">
            @if ($errors->has('email')) <p style="color:red;">{{ $errors->first('email') }}</p> @endif <br>

        </div>
        <div class="form-group">
            <label class="control-label" >Longitude</label>
            <input class="form-control" name="longitude" id="longitude" value="{{$transits->longitude}}">
            @if ($errors->has('longitude')) <p style="color:red;">{{ $errors->first('longitude') }}</p> @endif <br>

        </div>
        <div class="form-group">
            <label class="control-label" >Latitude</label>
            <input class="form-control" name="latitude" id="latitude" value="{{$transits->latitude}}">
            @if ($errors->has('latitude')) <p style="color:red;">{{ $errors->first('latitude') }}</p> @endif <br>

        </div>
        <div class="form-group">
            <label class="control-label" >Description</label>
            <textarea name="description" class="form-control">{{$transits->description}}</textarea>
        </div>
        <div class="form-group">
            <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{$transits->status == 1 ? 'checked' : '' }}>
            <label class="custom-control-label" for="defaultChecked">Active</label>
        </div>
        </div>
        <div class="form-group">
        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{$transits->status == 0 ? 'checked' : '' }}>
            <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection
@push('scripts')
@include('admin.pages.courier.transit.transit-map-scripts');
@endpush

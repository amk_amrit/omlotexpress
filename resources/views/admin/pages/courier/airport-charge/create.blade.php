@extends('admin.layouts.app')
@section('title','Airport Charge')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.airport-links.index')}}">Airport Links</a></li>
<li class="breadcrumb-item active">Create Charge</li>
@endsection
@include('error-messages.message')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
      <div class="alert alert-danger"  role="alert" id="errorMessage">
  
      </div>
        <form action="" method="" id="airportcharge" enctype="multipart/form-data">
                {{ csrf_field() }}
              <div class="form-group">
                <label class="control-label">Airport Link</label>
                <select class="form-control" name="airport_link_id" id="airport_link_id" >
                  <option selected disabled value="">-- Select Airportlink --</option>
                  @foreach($airportlinks as $airportlink)
                  <option value="{{$airportlink->id}}">{{$airportlink->airportOne->airportOneName}}--{{$airportlink->airportTwo->airportTwoName}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label class="control-label">Quantity Group</label>
                <select class="form-control" name="quantity_group_id" id="quantity_group_id" >
                  <option selected disabled value="">--Select Quantity Group--</option>
                  @foreach($quantitygroups as $quantitygroup)
                  <option value="{{$quantitygroup->id}}">{{$quantitygroup->group_name}}</option>
                  @endforeach
                </select>
              </div>
              
              <div class="form-group">
                <div id="dynamic_content"></div>
              </div>
          <div class="form-group">
        <input type="submit" id="submitted" class="btn btn-success"
        value="Publish">
        </div>
            </form>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
@include('admin.pages.courier.airport-charge.airportChargeScript');
@endpush
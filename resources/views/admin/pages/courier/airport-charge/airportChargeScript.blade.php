<script>  
 function getData(airportCharges){

$.ajax(
            {
                type: 'POST',
                url: "{{route('admin.airport-charges.store')}}",
                data:airportCharges,
                datatype: "json",
            }).done(function (data) {
                window.location.href = "{{route('admin.airport-links.index')}}";
        }).fail(function (data) {
            if(data.status==500){
                $('#errorMessage').show().html(data.responseJSON.error);
            }
            console.log(data.responseJSON.error);
            if(data.status==422){
                var errorString = "<ol type='1'>";
            for (error in data.responseJSON.error) {
                errorString += "<li>" + data.responseJSON.error[error] + "</li>";
            }
            errorString += "</ol>";
        
            $('#errorMessage').show().html(errorString);
            }
        });
    }

    $('#submitted').on('click',function(e){
        e.preventDefault();
        let airportCharges=$('#airportcharge').serialize();
        getData(airportCharges);
    });
    $('#errorMessage').hide();


function getQuantityGroupRange(getQuantityGroupRangedata){

$.ajax(
            {
                type: 'GET',
                url: "{{route('admin.airport-charges.getQuantityGroupRange')}}",
                data:getQuantityGroupRangedata,
                datatype: "json",
            }).done(function (data) {
                $("#dynamic_content").empty().html(data);
        }).fail(function (customError) {
            console.log(customError);
        });
    }

    $('#quantity_group_id').on('click',function(e){
        e.preventDefault();
        let getQuantityGroupRangedata=$('#airportcharge').serialize();
        getQuantityGroupRange(getQuantityGroupRangedata);
    });
 </script>
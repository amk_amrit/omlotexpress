@extends('admin.layouts.app')
@section('title','Airport Links')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.airport-links.index')}}">Airport Links</a></li>
    <li class="breadcrumb-item active">@foreach($getAirportLinkNames as $getAirportLinkName)
        {{$getAirportLinkName->airportOneName}}-{{ $getAirportLinkName->airportTwoName }}
        @endforeach</li>
@endsection

@section('content')

@include('error-messages.message')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                @if(count($getAirportChargeDatas) > 0) 
                    <form method="POST" action="{{route('admin.airport-charges.update', $airportLinkId)}}" enctype="multipart/form-data" >
                    <input type="hidden" name="_method" value="PUT">
                    {{ csrf_field() }}
                    <table  class="table">
                        <thead>
                        <tr>
                        <th scope="col"> ID </th>
                        <th scope="col"> Quantity Group Name  </th>
                        <th scope="col"> Rate </th>
                        <th scope="col"> Rate Type </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                       
                                        
                        @foreach($getAirportChargeDatas as $getAirportChargeData)
                        <td>{{$loop->iteration}}</td>
                        <td>{{$getAirportChargeData->groupName}}({{$getAirportChargeData->min_weight}}-{{$getAirportChargeData->max_weight}})</td>
                        <input type="hidden" name="id[]" class="form-control" value="{{$getAirportChargeData->id}}">
                        <input type="hidden" name="quantity_group_range_id[]" class="form-control" value="{{$getAirportChargeData->quantityGroupId}}">

                        <td>
                            <input type="number" name="rate[]" class="form-control" value="{{$getAirportChargeData->rate}}">
                        </td>
                        <td>
                            <select name="rate_type[]" class="form-control">
                                @if($getAirportChargeData->rate_type=='fix'){
                                    <option value="fix">Fix</option>
                                    <option value="per_kg">Per Kg</option>
                                }
                                @else{
                                    <option value="per_kg">Per Kg</option>
                                    <option value="fix">Fix</option>
                                }
                                @endif
                               
                               
                            </select>
                            </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="form-group">
                    <input type="submit"  class="btn btn-success"
                     value="Update">
                        
                    </form>
                    @else
                        <p>There is No data !! please Create charge to click button </p>
                            <a href="{{route('admin.airport-charges.show', $airportLinkId)}}" class="btn btn-info m-r-10">Create charge</a>
                    
                     @endif
                </div>
            </div>
        </div>
    </div>


</div>
    

    
@endsection






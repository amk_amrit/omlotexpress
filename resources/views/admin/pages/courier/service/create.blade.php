@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.services.index')}}">Services</a></li>
<li class="breadcrumb-item active">Create</li>
@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.services.store')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="control-label">Name</label>
                        <input type="text" class="form-control" name="name">
                        @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-radio p-l-0">
                            <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" checked>
                            <label class="custom-control-label" for="defaultChecked">Active</label>
                        </div> 
                        <div class="custom-control custom-radio p-l-0">
                            <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0">
                            <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Publish">
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>
                @endsection
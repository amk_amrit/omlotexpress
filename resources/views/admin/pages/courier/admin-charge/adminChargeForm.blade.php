<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
                <table id="myTable" class="table">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Quantity Group Range</th>
                    <th scope="col">Rate</th>
                    </tr>
                </thead>
                @foreach($quantityGroupRange as $quantityGroupRanges)

                <tbody>
                    <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>({{$quantityGroupRanges->min_weight }}--{{$quantityGroupRanges->max_weight }})</td>
                    <input type="hidden" value="{{$quantityGroupRanges->id}}" name="quantity_group_range_id[]">
                    <td><input type="text" class="form-control rates" placeholder="Enter  rate"  name="rate[]"></td>
                    <td>
                      <select class="form-control rate_type" name="rate_type[]">
                      <option value="">--Select Rate Type--</option>
                      <option value="fix" selected >Fix</option>
                      <option value="per_kg" {{ $quantityGroupRanges->rate_type == "per_kg" ? 'selected' : '' }}>Per Kg</option>
                      </select>
                    </td>
                    </tr>
                </tbody>
                @endforeach        

                </table>
      </div>
    </div>
  </div>
</div>
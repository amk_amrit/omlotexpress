<script>  
 function getData(adminCharge){

$.ajax(
            {
                type: 'POST',
                url: "{{route('admin.admin-charges.store')}}",
                data:adminCharge,
                datatype: "json",
            }).done(function (data) {
                //window.location.href = "{{route('admin.admin-charges.index')}}";
                $('#showFlashMessage').removeClass().addClass('alert alert-success').show().empty().html('Data Created Successfully');
        }).fail(function (data) {
            if(data.status==500){
                $('#errorMessage').show().html(data.responseJSON.error);
            }
            console.log(data.responseJSON.error);
            if(data.status==422){
                var errorString = "<ol type='1'>";
            for (error in data.responseJSON.error) {
                errorString += "<li>" + data.responseJSON.error[error] + "</li>";
            }
            errorString += "</ol>";
        
            $('#errorMessage').show().html(errorString);
            }
        });
    }

    $('#submitted').on('click',function(e){
        e.preventDefault();
        let adminCharge=$('#admincharges').serialize();
        getData(adminCharge);
    });
    $('#errorMessage').hide();
    $('#showFlashMessage').hide();


function getQuantityGroupRange(getQuantityGroupRangedata){

$.ajax(
            {
                type: 'GET',
                url: "{{route('admin.admin-charges.getQuantityGroupRange')}}",
                data:getQuantityGroupRangedata,
                datatype: "json",
            }).done(function (data) {
                $("#dynamic_content").empty().html(data);
        }).fail(function (customError) {
            console.log(customError);
        });
    }

    $('#quantity_group_id').on('click',function(e){
        e.preventDefault();
        let getQuantityGroupRangedata=$('#admincharges').serialize();
        getQuantityGroupRange(getQuantityGroupRangedata);
    });
 </script>
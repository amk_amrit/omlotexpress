@extends('admin.layouts.app')
@section('title','Admin charge')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admin-charges.index')}}">Admin Charge</a></li>
    <li class="breadcrumb-item active">@foreach($adminName as $adminNames)
        {{$adminNames->name}}
        @endforeach</li>
@endsection

@section('content')

@include('error-messages.message')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                @if(count($adminCharges) > 0) 
                    <form method="POST" action="{{route('admin.admin-charges.update', $adminId)}}" enctype="multipart/form-data" >
                    <input type="hidden" name="_method" value="PUT">
                    {{ csrf_field() }}
                    <table  class="table">
                        <thead>
                        <tr>
                        <th scope="col"> ID </th>
                        <th scope="col"> Quantity Group Name  </th>
                        <th scope="col"> Rate </th>
                        <th scope="col"> Rate Type </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                       
                                        
                        @foreach($adminCharges as $adminCharge)
                        <td>{{$loop->iteration}}</td>
                        <td>{{$adminCharge->groupName}}({{$adminCharge->min_weight}}-{{$adminCharge->max_weight}})</td>
                        <input type="hidden" name="id[]" class="form-control" value="{{$adminCharge->id}}">
                        <input type="hidden" name="quantity_group_range_id[]" class="form-control" value="{{$adminCharge->quantityGroupId}}">

                        <td>
                            <input type="number" name="rate[]" class="form-control" value="{{$adminCharge->rate}}">
                        </td>
                        <td>
                            <select name="rate_type[]" class="form-control">
                                @if($adminCharge->rate_type=='fix'){
                                    <option value="fix">Fix</option>
                                    <option value="per_kg">Per Kg</option>
                                }
                                @else{
                                    <option value="per_kg">Per Kg</option>
                                    <option value="fix">Fix</option>
                                }
                                @endif
                               
                               
                            </select>
                            </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="form-group">
                    <input type="submit"  class="btn btn-success"
                     value="Update">
                        
                    </form>
                    @else
                        <p>There is No data !! please Create charge to click button </p>
                            <a href="{{route('admin.admin-charges.create', $adminId)}}" class="btn btn-info m-r-10">Create charge</a>
                    
                     @endif
                </div>
            </div>
        </div>
    </div>


</div>
    

    
@endsection






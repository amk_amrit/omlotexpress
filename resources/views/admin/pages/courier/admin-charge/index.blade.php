@extends('admin.layouts.app')
@section('title','Admin Charge')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admin-charges.index')}}">Admin Charge</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')
    <div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.admin-charges.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                        <th scope="col"> ID </th>
                        <th scope="col">User</th>
                        <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($admincharges as $admincharge) 
                        <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>
                            {{$admincharge->office->name}}
                        </td>
                            <td class="table-flex">
                                    <a href="{{route('admin.admin-charges.edit', $admincharge->office_id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
                                    <!-- <form action="{{ route('admin.admin-charges.destroy', $admincharge->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        @if($admincharge->status==0)
                                        <button type="submit" class="btn btn-success">Activate</button>
                                        @else
                                            <button type="submit" class="btn btn-danger">Deactivate</button>
                                        @endif
                                    </form> -->
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
    

    
@endsection




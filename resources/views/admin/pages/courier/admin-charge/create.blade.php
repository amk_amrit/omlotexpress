@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admin-charges.index')}}"> Admin Charge</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="alert alert-danger"  role="alert" id="errorMessage"></div>
        <div class="alert alert-success"  role="alert" id="showFlashMessage"></div>
        <form action="" method="" id="admincharges" enctype="multipart/form-data">
                    {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label" >Office</label>
                    <select class="form-control" name="office_id">
                    <option disabled selected value="">--Select office--</option>
                        @foreach($offices as $office)
                    <option value="{{$office->id}}">{{$office->name}}</option>
                    @endforeach
                    </select>
                    
                </div>
                <div class="form-group">
                    <label class="control-label" >Quantity Group Name</label>
                    <select class="form-control" name="quantity_group_id" id="quantity_group_id">
                        @foreach($quantityGroupNameData as $quantityGroupNameData)
                    <option value="{{$quantityGroupNameData->id}}">{{$quantityGroupNameData->group_name}}</option>
                    @endforeach
                    </select>
                    
                </div>
                <div class="form-group">
                        <div id="dynamic_content"></div>
                    </div>
                    <div class="form-group">
                <input type="submit" id="submitted" class="btn btn-success"
                value="Publish">
                </div>

        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
@include('admin.pages.courier.admin-charge.adminChargeScript');
@endpush

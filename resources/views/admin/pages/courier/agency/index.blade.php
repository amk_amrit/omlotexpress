@extends('admin.layouts.app')
@section('title','Agency')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.agencies.index')}}">Agencies</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<!-- <div class="row">
    <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
        <a href="{{route('admin.agencies.create')}}" class="btn btn-success">Create</a>
    </div>
</div> -->

<!-- <div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> ID </th>
                <th scope="col"> Name  </th>
                <th scope="col"> Type </th>
                <th scope="col"> Action </th>
            </tr>
        </thead>
        @foreach($agencies as $agency)
        <tbody>
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$agency->name}}</td>
                <td>
                    @if($agency->type==1)
                    Source Agency
                    @else
                    Destination Agency
                    @endif
                </td>
                <td>
                    <a href="{{route('admin.agencies.show', $agency->id )}}" class="btn btn-info">View</a>
                    <a href="{{route('admin.agencies.edit', $agency->id)}}" class="btn btn-warning">Edit</a>
                    @if($agency->status==0)
                    <form action="{{ route('admin.agencies.destroy', $agency->id)}}" method="post">
                    {{ csrf_field() }}
                    @method('DELETE')
                        <button type="submit" class="btn btn-danger" style="margin-top: 10px; margin-left:10px;">Activate</button>
                        </form>
                    @else
                    <form action="{{ route('admin.agencies.destroy', $agency->id)}}" method="post">
                    {{ csrf_field() }}
                    @method('DELETE')
                        <button type="submit" class="btn btn-danger" style="margin-top: 10px; margin-left:10px;">Deactivate</button>
                        </form>
                    
                    @endif
               
                </td>
                
            </tr>
        </tbody>
        @endforeach
    </table>
    </div> -->




    <div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.agencies.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                            <th scope="col">S.N</th>
                            <th scope="col"> Name </th>
                            <th scope="col"> Type </th>
                            <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($agencies as $agency)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$agency->name}}</td>
                                <td>{{$agency->sourceAgencyClass->name}}/{{$agency->destinationAgencyClass->name}}</td>
                                <td class="table-flex">
                                    <a href="{{route('admin.agencies.show', $agency->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                    <a href="{{route('admin.agencies.edit', $agency->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
                                    <form action="{{ route('admin.agencies.destroy', $agency->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        @if($agency->status==0)
                                        <button type="submit" class="btn btn-danger">Activate</button>
                                        @else
                                            <button type="submit" class="btn btn-danger">Deactivate</button>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
    

    
@endsection

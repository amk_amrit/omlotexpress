
@extends('admin.layouts.app')
@section('title','Agency')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.agencies.index')}}">Agency</a></li>
    <li class="breadcrumb-item active">{{$agencies->name}}</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 text-right">
            <a href="{{route('admin.agencies.create')}}" class="btn btn-success m-b-10">Create</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <td>{{$agencies->name}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$agencies->email}}</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>{{$agencies->phone}}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>{{$agencies->address}}</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>{{$agencies->country->name}}</td>
                            </tr>
                            <tr>
                                <th>Source Class</th>
                                <td>{{$agencies->sourceAgencyClass->name}}</td>
                            </tr>
                            <tr>
                                <th>Destination class</th>
                                <td>{{$agencies->destinationAgencyClass->name}}</td>
                            </tr>
                            <tr>
                                <th>Latitude</th>
                                <td>{{$agencies->latitude}}</td>
                            </tr>
                            <tr>
                                <th>Longitude</th>
                                <td>{{$agencies->longitude}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($agencies->status == 1)
                                        <span> Activated</span>
                                    @else
                                        <span> Deactivated</span>
                                    @endif
                                </td>
                            </tr>

                        </table>
                    </div>

                    <div class="form-btn" >
                        <a href="{{ route('admin.agencies.edit', $agencies->id) }}" class="btn btn-warning">Edit</a>

                        <form action="{{ route('admin.agencies.destroy', $agencies->id)}}" method="post">
                            {{ csrf_field() }}
                            @method('DELETE')
                            @if($agencies->status==0)
                                <button type="submit" class="btn btn-success">Activate</button>
                            @else
                                <button type="submit" class="btn btn-danger">Deactivate</button>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
               
@endsection
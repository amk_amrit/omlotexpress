@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.agencies.index')}}"> Agencies</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@include('error-messages.message')
<form action="{{route('admin.agencies.update', $agencies->id)}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
            <label class="control-label" >Country</label>
            <select class="form-control" name="country_id" readonly>
                <option value="{{$agencies->country_id}}">{{$agencies->country->name}}</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label" >Source Class</label>
           <select class="form-control" name="orgin_class_id">       
            <option selected disabled value="">--Select Destinaton Class--</option>
                @foreach($agencyClass as $agencyClasses)
                @if($agencyClasses->type==1)

                <option value="{{$agencies->orgin_class_id}}"
                        {{$agencyClasses->id == old('orgin_class_id') ? 'selected' : ''}}
                        <?php if(isset($agencies) && $agencies->sourceAgencyClass->id == $agencyClasses->id ) echo 'selected'; ?>
                        >
                        {{$agencyClasses->name}}
                    </option>                    
                @endif
                @endforeach
           </select>
        </div>
        <div class="form-group">
            <label class="control-label" >Destination Class</label>
           <select class="form-control" name="destination_class_id">
           <option selected disabled value="">--Select Destinaton Class--</option>
                @foreach($agencyClass as $agencyClasses)
                @if($agencyClasses->type==2)
                <option value="{{$agencies->destination_class_id}}"
                        {{$agencyClasses->id == old('destination_class_id') ? 'selected' : ''}}
                        <?php if(isset($agencies) && $agencies->destinationAgencyClass->id == $agencyClasses->id ) echo 'selected'; ?>
                        >
                        {{$agencyClasses->name}}
                    </option>   
                    @endif
                @endforeach
           </select>
        </div>
        <div class="form-group">
            <label class="control-label" >Parent Transit</label>
           <select class="form-control" name="transit_link_id">
           <option selected disabled value="">--Select Parent Transit--</option>
                @foreach($transits as $transit)
                <option value="{{$transit->id}}"
                        {{$transit->id == old('transit_link_id') ? 'selected' : ''}}
                        <?php if(isset($agencies) && $agencies->transit->id == $transit->id ) echo 'selected'; ?>
                        >
                        {{$transit->name}}
                    </option>   
                @endforeach
           </select>
        </div>
        <div class="form-group">
            <label class="control-label" >Name</label>
            <input class="form-control" name="name" placeholder="Enter Name" value="{{$agencies->name}}">
        </div>
        @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>

        <div class="form-group">
            <label class="control-label" >Email</label>
            <input class="form-control" type="text" name="email" placeholder="Enter Email" value="{{$agencies->email}}">
        </div>
        @if ($errors->has('email')) <p style="color:red;">{{ $errors->first('email') }}</p> @endif <br>

        <div class="form-group">
            <label class="control-label" >Phone</label>
            <input class="form-control" name="phone" placeholder="Enter Phone Number" value="{{$agencies->phone}}">
        </div>
        @if ($errors->has('phone')) <p style="color:red;">{{ $errors->first('phone') }}</p> @endif <br>

        <div class="form-group">
            <label class="control-label" >Address</label>
            <input class="form-control" name="address" id="address" placeholder="Enter Address" value="{{$agencies->address}}">
        </div>
        @if ($errors->has('address')) <p style="color:red;">{{ $errors->first('address') }}</p> @endif <br>

        <div class="form-group">
            <label class="control-label" >Latitude</label>
            <input class="form-control" name="latitude" id="latitude" placeholder="Enter Longitude" value="{{$agencies->latitude}}">
        </div>
        @if ($errors->has('latitude')) <p style="color:red;">{{ $errors->first('latitude') }}</p> @endif <br>

        <div class="form-group">
            <label class="control-label" >Longitude</label>
            <input class="form-control" name="longitude" id="longitude" placeholder="Enter Longitude" value="{{$agencies->longitude}}">
        </div>
        @if ($errors->has('longitude')) <p style="color:red;">{{ $errors->first('longitude') }}</p> @endif <br>

        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection
@push('scripts')
@include('admin.pages.courier.agency.agency-map-scripts');
@endpush

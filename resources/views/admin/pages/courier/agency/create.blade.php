@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.agencies.index')}}"> Agencies</a></li>
<li class="breadcrumb-item active">Create</li>
@endsection
@include('error-messages.message')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.agencies.store')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="control-label">Country</label>
                        <select class="form-control" name="country_id">
                            <option selected disabled value="">--Select Country--</option>
                            @foreach($countries as $country)
                            <option value="{{ $country->id }}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Source Class</label>
                        <select class="form-control" name="orgin_class_id">
                            <option selected disabled value="">--Select Source class--</option>
                            @foreach($agencyClass as $agencyClasses)
                            @if($agencyClasses->type==1)
                            <option value="{{$agencyClasses->id}}">{{$agencyClasses->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Destination Class</label>
                        <select class="form-control" name="destination_class_id">
                            <option selected disabled value="">--Select Destinaton Class--</option>
                            @foreach($agencyClass as $agencyClasses)
                            @if($agencyClasses->type==2)
                            <option value="{{$agencyClasses->id}}">{{$agencyClasses->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Parent Transit</label>
                        <select class="form-control" name="transit_link_id">
                            <option selected disabled value="">--Select Parent Transit--</option>
                            @foreach($tranit as $tranits)
                            <option value="{{$tranits->id}}">{{$tranits->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Name</label>
                        <input class="form-control" name="name" placeholder="Enter Name" value="{{old('name')}}">
                    </div>
                    @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>

                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input type="text" class="form-control" name="email" placeholder="Enter Email" value="{{old('email')}}">
                    </div>
                    @if ($errors->has('email')) <p style="color:red;">{{ $errors->first('email') }}</p> @endif <br>
                    <div class="form-group">
                        <label class="control-label">Phone</label>
                        <input class="form-control" name="phone" placeholder="Enter Phone Number" value="{{old('phone')}}">
                    </div>
                    @if ($errors->has('phone')) <p style="color:red;">{{ $errors->first('phone') }}</p> @endif <br>

                    <div class="form-group">
                        <label class="control-label">Address</label>
                        <input class="form-control" name="address" id="address" placeholder="Enter Address" value="{{old('address')}}">
                    </div>
                    @if ($errors->has('address')) <p style="color:red;">{{ $errors->first('address') }}</p> @endif <br>

                    <div class="form-group">
                        <label class="control-label">Latitude</label>
                        <input class="form-control" name="latitude" id="latitude" placeholder="Enter Longitude" value="{{old('latitude')}}">
                    </div>
                    @if ($errors->has('latitude')) <p style="color:red;">{{ $errors->first('latitude') }}</p> @endif <br>
                    <div class="form-group">
                        <label class="control-label">Longitude</label>
                        <input class="form-control" name="longitude" id="longitude" placeholder="Enter Longitude" value="{{old('longitude')}}">
                    </div>
                    @if ($errors->has('longitude')) <p style="color:red;">{{ $errors->first('longitude') }}</p> @endif <br>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Publish">
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
@include('admin.pages.courier.agency.agency-map-scripts');
@endpush
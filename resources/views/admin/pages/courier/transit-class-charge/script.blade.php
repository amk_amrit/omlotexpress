<script>

    function changeAction(){
        var selectedGroup = $('#quantityGroup option:selected').val();
        var selectedTransitClass = $('#transitClass option:selected').val();

        if(!(selectedTransitClass == '' || selectedTransitClass == undefined || selectedGroup == '' || selectedGroup == undefined)){
            $('#showFlashMessage').empty().hide();
            $.ajax({
            type : 'GET',
            url : "{{route('admin.class.charge.get-transit-quantity-group-range')}}",
            data : {
                'quantity_group_id' : selectedGroup,
                'courier_transit_class_id' : selectedTransitClass,
                },
            }).done(function (data){
                $("#dynamic_content").empty().html(data);
            });
        }
    }
        
    $(document).on('click','#publish',function(e){
        e.preventDefault();
        let transitclasscharge=$('#transitclasscharge').serialize();
        submitData(transitclasscharge);
    });
    $('#showFlashMessage').hide();


    function submitData(transitclasscharge){
        $.ajax({
            type: 'POST',
            url: "{{route('admin.class.charge.transits.store')}}",
            data:transitclasscharge,
            datatype: "json",
            }).done(function (data) {
                $('#showFlashMessage').removeClass().addClass('alert alert-success').show().empty().html('Data Created Successfully');
                //window.location.href = "{{route('admin.class.charge.transits.index')}}";
            }).fail(function (data) {
                if(data.status==500){
                    $('#showFlashMessage').removeClass().addClass('alert alert-danger').show().empty().html(data.responseJSON.error);
                }
                console.log(data.responseJSON.error);
                if(data.status==422){
                    var errorString = "<ol type='1'>";
                for (error in data.responseJSON.error) {
                    errorString += "<li>" + data.responseJSON.error[error] + "</li>";
                }
                errorString += "</ol>";
            
                $('#showFlashMessage').removeClass().addClass('alert alert-danger').show().empty().html(errorString);
            }
        });
    }

    </script>
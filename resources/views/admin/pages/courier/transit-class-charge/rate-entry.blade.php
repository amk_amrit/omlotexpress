@extends('admin.layouts.app')
@section('title','Transit Class Charge')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.class.charge.transits.index')}}">Transit Class Charges</a></li>
    <li class="breadcrumb-item"> {{ $transitClass->name }}</li>
    <li class="breadcrumb-item"> {{ $quantityGroup->group_name }}</li>


@endsection

@section('content')

@include('error-messages.message')

<h3> Rate Entry Of {{ $transitClass->name }} for {{ $quantityGroup->group_name }} </h3>

<div role="alert"  id="showFlashMessage">
  
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                
                <form id="transitclasscharge" enctype="multipart/form-data" >
                    <table class="table">
                        <tr>
                          <th scope="col">Minimum Weight </th>
                          <th scope="col">Maximum Weight </th>
                          <th scope="col">Rate</th>
                          <th scope="col">Rate Type</th>
                          
                        </tr>
                      </thead>
                      
                      <tbody>
                        {{ csrf_field() }}
                        @foreach($quantityGroupWithRates as $groupRange)
                            <tr>
                            <td >{{$groupRange->min_weight}}</td>
                            <td >{{$groupRange->max_weight}}</td>
                            <td>
                                <input type="hidden" name="quantity_group_range_id[]" value=" {{ $groupRange->id}} " >
                                <input type="number" step =".001" class="form-control rates" placeholder="Enter rate" value= "{{ isset($groupRange) ? $groupRange->rate : 0  }}" name="rate[]">
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control" name = "rate_type[]">
                                    <option value="fix" selected >Fix</option>
                                    <option value="per_kg" {{ $groupRange->rate_type == "per_kg" ? 'selected' : '' }}>Per Kg</option>
                                </select>
                                </div>
                                
                            </td>
                            </tr>
                        @endforeach        
                      </tbody>
                    </table>
                    
                    <input type="hidden" name="quantity_group_id" value="{{ $quantityGroup->id }}">
                    <input type="hidden" name="courier_transit_class_id" value="{{ $transitClass->id }}">
                    
                    <div class="form-group">
                      <input type="button"  id="publish" class="btn btn-success" value="Publish">                                       
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    @include('admin.pages.courier.transit-class-charge.script');
@endpush

@extends('admin.layouts.app')
@section('title','Transit Class Charge')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.class.charge.transits.index')}}">Transit Class Charges</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')


<div role="alert"  id="showFlashMessage">
  
</div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <form action="" method="" id="transitclasscharge" enctype="multipart/form-data">
                        {{ csrf_field() }}       
                        <div class="form-group">
                            <label class="control-label" >Transit Class</label>
                            <select class="form-control" name="courier_transit_class_id" id="transitClass" required onchange="changeAction()" > 
                                <option disabled selected value="" >--Select Transit Class--</option>
                                    @foreach($transitClasses as $transitClass)
                                        <option value="{{$transitClass->id}}" {{$transitClass->id == old('transit_class_id') ? 'selected' : ''}} >{{$transitClass->name}}</option>
                                    @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label" >Quantity Group</label>
                            <select class="form-control" name="quantity_group_id" id="quantityGroup" required onchange="changeAction()" > 
                                <option disabled selected value="" >--Select Quantity Group --</option>
                                    @foreach($quantityGroups as $quantityGroup)
                                        <option value="{{$quantityGroup->id}}" {{$quantityGroup->id == old('transit_class_id') ? 'selected' : ''}} >
                                            {{$quantityGroup->group_name}}
                                        </option>
                                    @endforeach
                            </select>
                        </div>

                      
                        <div id="dynamic_content">
                        </div>

                        
                    </form>
                                                
                </div>
            </div>
        </div>
    </div>
    

@endsection
@push('scripts')
@include('admin.pages.courier.transit-class-charge.script');
@endpush

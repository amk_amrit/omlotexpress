@extends('admin.layouts.app')
@section('title','Transit Class Charge')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.class.charge.transits.index')}}">Transit Class Charges</a></li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.class.charge.transits.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                            <th scope="col">S.N</th>
                            <th scope="col"> Country </th>
                            <th scope="col"> Transit Class </th>
                            <th scope="col"> Quantity Group Range </th>
                            <th scope="col"> Rate </th>
                            <th scope="col"> Currency </th>
    
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transitClassCharges as $transitClassCharge)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td> {{ $transitClassCharge->country->name }} </td>
                                <td>{{$transitClassCharge->transitClass->name}}</td>
                                <td>
                                    {{ $transitClassCharge->quantityGroupRange->quantityGroup->group_name }}
                                    {{$transitClassCharge->quantityGroupRange->min_weight}} - {{ $transitClassCharge->quantityGroupRange->max_weight }} KG
                                 
                                </td>
                                <td> {{ $transitClassCharge->rate }} ({{  $transitClassCharge->rate_type }}) </td>
                                <td> {{ $transitClassCharge->country->currency }} </td>
                                



                                <td>{{$transitClassCharge->group_name}}</td>
                             
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

    
@endsection

<script>
    $(document).ready(function() {
        var i = 1;
        $('#add').click(function() {
            i++;
            $('#dynamic_field').append('<tr id="row' + i + '"><td><input type="text" name="name[]" placeholder="Enter courier Class Name" class="form-control name"/></td><td><textarea rows="5" class="form-control" name="description[]" placeholder="Description"></textarea></td><br><td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
        });
        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

    });

    function getData(courierclass) {

        $.ajax({
            type: 'POST',
            url: "{{route('admin.courier-classes.store')}}",
            data: courierclass,
            datatype: "json",
        }).done(function(data) {
            window.location.href = "{{route('admin.courier-classes.index')}}";
        }).fail(function(data) {
            console.log(data.responseJSON.error);

            var errorString = "<ol type='1'>";
            for (error in data.responseJSON.error) {
                errorString += "<li>" + data.responseJSON.error[error] + "</li>";
            }
            errorString += "</ol>";
        
            $('#errorMessage').show().html(errorString);
        });
    }

    $('#submitted').on('click', function(e) {
        e.preventDefault();
        let courierclass = $('#courierclasses').serialize();
        getData(courierclass);
    });
    $('#errorMessage').hide();

</script>
@extends('admin.layouts.app')
@section('title','Courier Class')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.courier-classes.index')}}"> Courier Classes</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                    <div class="alert alert-danger" role="alert" id="errorMessage">
                    </div>
                    <form action="" method="" id="courierclasses" enctype="multipart/form-data">
                                {{ csrf_field() }} 
                            <div class="form-group">
                                <label class="control-label" >Courier Class Types</label>
                                <select class="form-control for_office_type" id="office_type" name="for_office_type" >
                                <option selected disabled value="">--Select Class Type--</option>
                                    <option value="agency">Agency</option>
                                    <option value="transit">Transit</option> 
                                </select>
                            </div>
                            
                            <table class="table" id="dynamic_field">
                                <tr>
                                    <td><input type="text"  class="form-control name" placeholder="Enter courier Class name" id="name"  name="name[]"  ></td>
                                    <td><textarea rows="5" class="form-control" name="description[]" placeholder="Description"></textarea></td>
                                </tr>
                            </table>
                            
                            <button style="margin-left: 350px;" type="button" name="add" id="add" class="btn btn-success">Add More</button>
                            
                            <div class="form-group">
                            <input type="submit" id="submitted" class="btn btn-success"
                            value="Publish">
                            </div>

                    </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    @include('admin.pages.courier.courier-class.courierClassScript');
@endpush

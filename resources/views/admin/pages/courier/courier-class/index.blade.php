
@extends('admin.layouts.app')
@section('title','Transit Class')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.courier-classes.index')}}">All Courier Classes</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')
    <div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.courier-classes.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                        <th scope="col"> ID </th>
                        <th scope="col"> Name  </th>
                        <th scope="col"> Type </th>
                        <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courierClasses as $courierClass)
                            <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{ucfirst(trans($courierClass->name))}}</td>
                            <td>{{ucfirst(trans($courierClass->for_office_type))}}</td>
                                <td class="table-flex">
                                    <a href="{{route('admin.courier-classes.edit', $courierClass->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
                                    <form action="{{ route('admin.courier-classes.destroy', $courierClass->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        @if($courierClass->status==0)
                                        <button type="submit" class="btn btn-success">Activate</button>
                                        @else
                                            <button type="submit" class="btn btn-danger">Deactivate</button>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
    

    
@endsection




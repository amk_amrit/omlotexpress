@extends('admin.layouts.app')
@section('title','Courier Class')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.courier-classes.index')}}"> Courier Class</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@include('error-messages.message')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.courier-classes.update',$courierClasses->id )}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" >Courier Class Types</label>
                        <input readonly type="text" value="{{$courierClasses->for_office_type}}" class="form-control" name="for_office_type">
                    </div>

                    <div class="form-group">
                        <label class="control-label" >Name</label>
                        <input type="text" class="form-control" name="name" value="{{$courierClasses->name}}" >
                        @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                    </div>
                    <div class="form-group">
                        <label class="control-label" >Description</label>
                        <textarea rows="5" class="form-control" name="description">{{$courierClasses->description}}</textarea>
                        @if ($errors->has('description')) <p style="color:red;">{{ $errors->first('description') }}</p> @endif <br>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-radio p-l-0">
                            <input type="radio" class="custom-control-input" id="defaultChecked" name="status"  value="1" {{$courierClasses->status == 1 ? 'checked' : '' }} >
                            <label class="custom-control-label" for="defaultChecked">Active</label>
                        </div>
                        
                        <div class="custom-control custom-radio p-l-0">
                            <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{$courierClasses->status == 0 ? 'checked' : '' }} >
                            <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success"    value="Publish">
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

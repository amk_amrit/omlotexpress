@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.quantity.countries.index')}}"> Quantity Group Country</a></li>
    <li class="breadcrumb-item active">{{$quantityGroupCountrys->quantityGroup->group_name}}</li>
@endsection
    <div class="row">
        <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
        <a href="{{route('admin.quantity.countries.create')}}" class="btn btn-success" >Create</a>
        </div>
    </div> 
    <div class="row">
            <table style="width:100%">
                    <tr>
                        <th>Country</th>
                        <td>{{$quantityGroupCountrys->Country->name}}</td>
                    </tr>
                    <tr>
                        <th>Quantity Group</th>
                        <td>{{$quantityGroupCountrys->quantityGroup->group_name}}</td>
                    </tr>
                    @foreach($quantityGroupCountryAlls as $quantityGroupCountryAll)
                    <tr>
                        <th>Maxmium Weight</th>
                        <td>{{$quantityGroupCountryAll->max_weight}}</td>
                    </tr>
                    <tr>
                        <th>Minimum Weight</th>
                        <td>{{$quantityGroupCountryAll->min_weight}}</td>
                    </tr>
                    <tr>
                        <th>Rate</th>
                        <td>{{$quantityGroupCountryAll->rate}}</td>
                    </tr>
                    @endforeach
        </table>
    </div>
    <div class="row" >
    </div>
               
@endsection
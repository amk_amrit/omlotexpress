@extends('admin.layouts.app')
@section('title','Quantity Group Range')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.quantity.ranges.index')}}">Quantity Group Ranges</a></li>
    <li class="breadcrumb-item active">Index</li>
@endsection

@section('content')

@include('error-messages.message')
    <div class="row">
    <div class="col-lg-12 text-right">
        <!-- <a href="{{route('admin.quantity.ranges.create')}}" class="btn btn-success m-b-10">Create</a> -->
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                        <!-- <th scope="col"> ID </th> -->
                        <th scope="col"> Quantity Group </th>
                        <th scope="col"> Quantity Group Ranges </th>
                        <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($quantityGroupRanges as $quantityGroupRange)
                            <tr>
                            <!-- <td>{{$loop->iteration}}</td> -->
                            <td>{{$quantityGroupRange->group_name}}</td>
                            <td>
                            @php
                            $count = $quantityGroupRange->count_quantity_group_ranges;                                      
                            $more = (($count - 4) > 0) ? ' & '.($count - 4).' more Ranges' : '';
                            @endphp
                            {{$quantityGroupRange->quantiry_group_ranges}} {{$more}}
                            </td>
                                <td class="table-flex">
                                <a href="{{route('admin.quantity.ranges.edit', $quantityGroupRange->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        
                        @foreach($quantityGroups as $quantityGroup)
                            <tr>
                            <!-- <td>{{$loop->iteration}}</td> -->
                            <td>{{$quantityGroup->group_name}}</td>
                            <td>
                                <p>No datas</p>
                            </td>
                                <td class="table-flex">
                                <a href="{{route('admin.quantity.ranges.edit', $quantityGroup->id)}}" class="btn btn-warning m-r-10"><i class="fa fa-plus"></i></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
    

    
@endsection




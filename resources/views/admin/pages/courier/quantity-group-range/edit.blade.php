@extends('admin.layouts.app')
@section('title','Quantity Group Range')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.quantity.ranges.index')}}"> Quantity Group Ranges </a></li>
    <li class="breadcrumb-item active">Edit</li>
    @include('error-messages.message')
@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <div class="alert alert-danger" role="alert" id="errorMessage">
            </div>
                <form action="" method="" id="quantityGroupRanges" enctype="multipart/form-data">
                            {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label" >Quantity Group</label>
                            <select readonly type="text" name="quantity group id" class="form-control">
                                <option readonly value="{{$quantityGroupNameID->id}}">{{$quantityGroupNameID->group_name}}</option>
                            </select>
                        </div>
                        @foreach($quantityGroupRange as $quantityGroupRanges)
                        <table class="table" >
                        <tr> 
                            <td class="d-none"><input type="hidden" class="form-control min-weight" value="{{$quantityGroupRanges->id}}"  name="id[]"  ></td> 
                            <td><input type="number" class="form-control min-weight" value="{{$quantityGroupRanges->min_weight}}" placeholder="Enter  minimum weight"  name="min_weight[]"  ></td>
                            <td><input type="number" class="form-control max-weight" value="{{$quantityGroupRanges->max_weight}}" placeholder="Enter  maxmium weight"  name="max_weight[]" ></td>
                        </tr>
                        </table>
                        @endforeach
                        <table class="table" id="dynamic_field">
                        <tr>
                            
                        </tr>
                        </table>
                        
                        <button style="margin-left: 350px;" type="button" name="add" id="add" class="btn btn-success">Add More</button>
                        
                        <div class="form-group">
                        <input type="submit" id="submitted"  class="btn btn-success" value="Update">
                        </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    @include('admin.pages.courier.quantity-group-range.quantityGroupRangeScript');
@endpush

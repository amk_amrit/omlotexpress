@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.quantity.ranges.index')}}"> Quantity Group Countries </a></li>
    <li class="breadcrumb-item active">Create</li>
   
@endsection
@include('error-messages.message')
@if(isset($error))
{{ $error }}
@endif
<div  role="alert" id="errorMessage">
  
</div>
<form action="" method=""  id="quantityGroupRanges" enctype="multipart/form-data" >
            {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label" >Quantity Group</label>
            <select class="form-control" name="quantity_group_id" id="quantity_group_id" >
            <option value="">--select an option--</option>
                @foreach($quantityGroups as $quantityGroup)
                    <option value="{{$quantityGroup->id}}"
                    {{$quantityGroup->id == old('quantity_group_id') ? 'selected' : ''}}
                        >
                        {{$quantityGroup->group_name}}
                    </option>
                    
                @endforeach

            </select>
        </div>
        <table class="table" id="dynamic_field">
        <tr>
            <td><input type="text"  class="form-control min-weight" placeholder="Enter  minimum weight" id="min_weight"  name="min_weight[]"  ></td>
            <td><input type="text"  class="form-control max-weight" placeholder="Enter  maxmium weight" id="max_weight"  name="max_weight[]" ></td>
        </tr>
        </table>
        
        <button style="margin-left: 350px;" type="button" name="add" id="add" class="btn btn-success">Add More</button>
        
        <div class="form-group">
        <input type="submit" id="submitted" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection
@push('scripts')
    @include('admin.pages.courier.quantity-group-range.quantityGroupCountryScript');
@endpush

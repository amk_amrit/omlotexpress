<script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="number" name="min_weight[]" placeholder="Enter  minimum weight" class="form-control min-weight"/></td><td><input type="number" name="max_weight[]" placeholder="Enter maxmium weight" class="form-control max-weight"/></td><br><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
    
 });  
 


 function getData(quantityGroupRange){

    $.ajax({
            type: 'POST',
            url: "{{route('admin.quantity.ranges.store')}}",
            data:quantityGroupRange,
            datatype: "json",
            }).done(function (data) {
                window.location.href = "{{route('admin.quantity.ranges.index')}}";
            }).fail(function (data) {
                if(data.status==500){
                    $('#errorMessage').show().html(data.responseJSON.error);
                }
                console.log(data.responseJSON.error);
                if(data.status==422){
                    var errorString = "<ol type='1'>";
                for (error in data.responseJSON.error) {
                    errorString += "<li>" + data.responseJSON.error[error] + "</li>";
                }
                errorString += "</ol>";
            
                $('#errorMessage').show().html(errorString);
            }
    });
}

    $('#submitted').on('click',function(e){
        e.preventDefault();
        let quantityGroupRange=$('#quantityGroupRanges').serialize();
        getData(quantityGroupRange);
    });
    $('#errorMessage').hide();
 </script>
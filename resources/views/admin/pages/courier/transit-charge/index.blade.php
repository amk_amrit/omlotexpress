@extends('admin.layouts.app')
@section('title','Dashboard')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-charges.index')}}">Transits Charge</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
        <a href="{{route('admin.transit-charges.create')}}" class="btn btn-success">Create</a>
    </div>
</div>

<div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> ID </th>
                <th scope="col"> Transit class </th>
                <th scope="col"> Quantity Group  </th>
                <th scope="col">Rate</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        @foreach($transitCharges as $transitCharge)
        <tbody>
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$transitCharge->name}}</td>
                <td>{{$transitCharge->group_name}}</td>
                <td>{{$transitCharge->rate}}</td>
                <td>
                    <a href="{{route('admin.transit-charges.show', $transitCharge->id)}}" class="btn btn-info">View</a>
                    <a href="{{route('admin.transit-charges.edit',$transitCharge->id)}}" class="btn btn-warning">Edit</a>
               
                </td>
                
            </tr>
        </tbody>
        @endforeach
    </table>
    </div>


    
@endsection

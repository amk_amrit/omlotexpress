@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-charges.index')}}">All Transit</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
<form action="{{route('admin.transit-charges.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label" >Transit</label>
            <select class="form-control" name="transit_id">
                @foreach($tranits as $tranit)
                <option value="{{$tranit->id}}">{{$tranit->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection

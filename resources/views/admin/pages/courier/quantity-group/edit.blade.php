@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.quantity.groups.index')}}"> Quantity Groups</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.quantity.groups.update', $quantityGroup->id )}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" >Group Name</label>
                        <input type="text" class="form-control" name="group_name" value="{{$quantityGroup->group_name}}" >
                        @if ($errors->has('group_name')) <p style="color:red;">{{ $errors->first('group_name') }}</p> @endif <br>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-radio p-l-0">
                            <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{$quantityGroup->status == 1 ? 'checked' : '' }}>
                            <label class="custom-control-label" for="defaultChecked">Active</label>
                        </div>
                        <div class="custom-control custom-radio p-l-0">
                            <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{$quantityGroup->status == 0 ? 'checked' : '' }}>
                            <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Publish">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

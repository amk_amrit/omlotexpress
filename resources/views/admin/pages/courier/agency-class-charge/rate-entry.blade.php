@extends('admin.layouts.app')
@section('title','Agency Class Charge')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.class.charge.agencies.index')}}">Agency Class Charges</a></li>
    <li class="breadcrumb-item"> {{ $agencyClass->name }}</li>
    <li class="breadcrumb-item"> {{ $quantityGroup->group_name }}</li>


@endsection

@section('content')

@include('error-messages.message')

<h3> Rate Entry Of {{ $agencyClass->name }} for {{ $quantityGroup->group_name }} </h3>

<div role="alert"  id="showFlashMessage">
  
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                
                <form id="agencyclasscharge" enctype="multipart/form-data" >
                    <table class="table">
                        <tr>
                          <th scope="col">Minimum Weight </th>
                          <th scope="col">Maximum Weight </th>
                          <th scope="col">Origin Rate</th>
                          <th scope="col">Destination Rate</th>
                          <th scope="col">Rate Type</th>
                        </tr>
                      </thead>
                      
                      <tbody>
                        {{ csrf_field() }}
                        @foreach($quantityGroupWithRates as $groupRange)
                            <tr>
                            <td >{{$groupRange->min_weight}}</td>
                            <td >{{$groupRange->max_weight}}</td>
                            <td>
                                <input type="hidden" name="quantity_group_range_id[]" value=" {{ $groupRange->id}} " >
                                <input type="number" step =".001" class="form-control rates" placeholder="Enter Origin rate" value= "{{ $groupRange->origin_rate}}" name="origin_rate[]">
                            </td>
                            <td>
                                <input type="number" step =".001" class="form-control rates" placeholder="Enter Destination Rate" value= "{{ $groupRange->destination_rate }}" name="destination_rate[]">
                            </td>
                            <td>
                                <div class="form-group">
                                <select class="form-control" name = "rate_type[]">
                                    <option value="fix" selected >Fix</option>
                                    <option value="per_kg" {{ $groupRange->rate_type == "per_kg" ? 'selected' : '' }}>Per Kg</option>
                                </select>
                                </div>
                                
                            </td>
                            </tr>
                        @endforeach        
                      </tbody>
                    </table>
                    
                    <input type="hidden" name="quantity_group_id" value="{{ $quantityGroup->id }}">
                    <input type="hidden" name="courier_agency_class_id" value="{{ $agencyClass->id }}">
                    
                    <div class="form-group">
                      <input type="button"  id="publish" class="btn btn-success" value="Publish">                                       
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    @include('admin.pages.courier.agency-class-charge.script');
@endpush

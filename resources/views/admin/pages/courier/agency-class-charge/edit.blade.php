@extends('admin.layouts.app')
@section('title','Agency Class Charge')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.class.charge.agencies.index')}}">Agency Charge List</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
@include('error-messages.message')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.class.charge.agencies.update', $country_id)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="country_id" value="{{$country_id}}">
                   <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Agency Class</th>
                                <th scope="col">Quantity Group</th>
                                <th scope="col">Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($agencyClassCharges as $agencyClassCharge)
                                <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>
                                    <select class="form-control" name="agency_class_id[]">
                                        <option  disabled value="">--Select Agency Class--</option>
                                        @foreach($agencyClasses as $agencyClass)
                                            <option value="{{$agencyClass->id}}" {{ ($agencyClass->id == old('agency_class_id')) || $agencyClass->id == $agencyClassCharge->id ? 'selected': '' }}">{{$agencyClass->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" name="quantity_group_id[]">
                                    <option  disabled value="">--Select Quantity Group--</option>
                                        @foreach($quantityGroups as $quantityGroup)
                                            <option value="{{$quantityGroup->id}}" {{ ($quantityGroup->id == old('quantity_group_id')) || $quantityGroup->id == $agencyClassCharge->id ? 'selected': '' }}">{{$quantityGroup->quantityGroup->group_name}}({{ $quantityGroup->min_weight }} -- {{ $quantityGroup->max_weight }})</option>
                                        @endforeach
                                    </select>
                                </td> 
                                <td><input type="hidden" value="{{$agencyClassCharge->id}}" name="agencyChargeId[]"></td>
                                <td><input type="text" value="{{ $agencyClassCharge->rate }}" name="rate[]"></td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                            <div class="form-group">
                            <input type="submit" class="btn btn-success"
                                    value="Update">
                                    </div>
                   </form>
            </div>
        </div>
    </div>
</div>

@endsection

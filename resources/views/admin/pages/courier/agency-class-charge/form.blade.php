
<label class="control-label"> Enter Rates </label>
<table class="table">
    <tr>
      <th scope="col">Minimum Weight </th>
      <th scope="col">Maximum Weight </th>
      <th scope="col">Origin Rate</th>
      <th scope="col">Destination Rate</th>
      <th scope="col">Rate Type</th>
      
    </tr>
  </thead>
  
  <tbody>
  @foreach($quantityGroupWithRates as $groupRange)
    <tr>
      <input type="hidden" name="quantity_group_range_id[]" value=" {{ $groupRange->id}} " >
      <td >{{$groupRange->min_weight}}</td>
      <td >{{$groupRange->max_weight}}</td>
      <td>
        <input type="number" step =".001" class="form-control rates" placeholder="Enter Origin Rate" value= "{{ $groupRange->origin_rate }}" name="origin_rate[]">
      </td>
      <td>
        <input type="number" step =".001" class="form-control rates" placeholder="Enter Destination Rate" value= "{{ $groupRange->origin_rate }}" name="destination_rate[]">
      </td>
      <td>
        <div class="form-group">
          <select class="form-control" name = "rate_type[]">
            <option value="fix" selected >Fix</option>
            <option value="per_kg" {{ $groupRange->rate_type == "per_kg" ? 'selected' : '' }}>Per Kg</option>
          </select>
        </div>
        
      </td>
    </tr>
  @endforeach        
  </tbody>
</table>

<div class="form-group">
  <input type="button" id="publish" class="btn btn-success" value="Publish">                                       
</div>



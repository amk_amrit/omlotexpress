@extends('admin.layouts.app')
@section('title','agency Class Charge')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.class.charge.agencies.index')}}">Agency Class Charges</a></li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.class.charge.agencies.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                            <th scope="col"> S.N </th>
                            <th scope="col"> Country </th>
                            <th scope="col"> agency Class </th>
                            <th scope="col"> Quantity Group </th>
                            <th scope="col"> Rates </th>
                            
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach($agencyClasses as $agencyClass)
                            @foreach ($quantityGroups as $quantityGroup)
                                <tr>
                                <td> {{ $i++ }} </td>
                                    <td> {{ $agencyClass->country->name }} </td>
                                    <td>{{$agencyClass->name}}</td>
                                    <td>{{ $quantityGroup->group_name }}</td>
                                    <td class="table-flex" > 
                                        <a href=" {{ route('admin.class.charge.get-agency-quantity-group-range',['quantity_group_id' =>$quantityGroup->id, 'courier_agency_class_id' => $agencyClass->id ]) }} " class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
                                    </td>
                                </tr> 
                            @endforeach
                            
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

    
@endsection

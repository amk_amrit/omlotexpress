<script>

    function changeAction(){
        var selectedGroup = $('#quantityGroup option:selected').val();
        var selectedAgencyClass = $('#agencyClass option:selected').val();

        if(!(selectedAgencyClass == '' || selectedAgencyClass == undefined || selectedGroup == '' || selectedGroup == undefined)){
            $('#showFlashMessage').empty().hide();
            $.ajax({
            type : 'GET',
            url : "{{route('admin.class.charge.get-agency-quantity-group-range')}}",
            data : {
                'quantity_group_id' : selectedGroup,
                'courier_agency_class_id' : selectedAgencyClass,
                },
            }).done(function (data){
                $("#dynamic_content").empty().html(data);
            });
        }
    }
        
    $(document).on('click','#publish',function(e){
        e.preventDefault();
        let agencyclasscharge=$('#agencyclasscharge').serialize();
        submitData(agencyclasscharge);
    });
    $('#showFlashMessage').hide();


    function submitData(agencyclasscharge){
        $.ajax({
            type: 'POST',
            url: "{{route('admin.class.charge.agencies.store')}}",
            data:agencyclasscharge,
            datatype: "json",
            }).done(function (data) {
                $('#showFlashMessage').removeClass().addClass('alert alert-success').show().empty().html('Data Created Successfully');
            }).fail(function (data) {
                if(data.status==500){
                    $('#showFlashMessage').removeClass().addClass('alert alert-danger').show().empty().html(data.responseJSON.error);
                }
                console.log(data.responseJSON.error);
                if(data.status==422){
                    var errorString = "<ol type='1'>";
                for (error in data.responseJSON.error) {
                    errorString += "<li>" + data.responseJSON.error[error] + "</li>";
                }
                errorString += "</ol>";
            
                $('#showFlashMessage').removeClass().addClass('alert alert-danger').show().empty().html(errorString);
            }
        });
    }

    </script>
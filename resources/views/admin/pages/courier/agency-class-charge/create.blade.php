@extends('admin.layouts.app')
@section('title','Agency Class Charge')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.class.charge.agencies.index')}}">Agency Class Charges</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')


<div role="alert"  id="showFlashMessage">
  
</div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <form action="" method="" id="agencyclasscharge" enctype="multipart/form-data">
                        {{ csrf_field() }}       
                        <div class="form-group">
                            <label class="control-label" >agency Class</label>
                            <select class="form-control" name="courier_agency_class_id" id="agencyClass" required onchange="changeAction()" > 
                                <option disabled selected value="" >--Select agency Class--</option>
                                    @foreach($agencyClasses as $agencyClass)
                                        <option value="{{$agencyClass->id}}" {{$agencyClass->id == old('agency_class_id') ? 'selected' : ''}} >{{$agencyClass->name}}</option>
                                    @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label" >Quantity Group</label>
                            <select class="form-control" name="quantity_group_id" id="quantityGroup" required onchange="changeAction()" > 
                                <option disabled selected value="" >--Select Quantity Group --</option>
                                    @foreach($quantityGroups as $quantityGroup)
                                        <option value="{{$quantityGroup->id}}" {{$quantityGroup->id == old('agency_class_id') ? 'selected' : ''}} >
                                            {{$quantityGroup->group_name}}
                                        </option>
                                    @endforeach
                            </select>
                        </div>

                      
                        <div id="dynamic_content">
                        </div>

                        
                    </form>
                                                
                </div>
            </div>
        </div>
    </div>
    

@endsection
@push('scripts')
@include('admin.pages.courier.agency-class-charge.script');
@endpush

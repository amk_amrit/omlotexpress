@extends('admin.layouts.app')
@section('title','Agency Class')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.class.agencies.index')}}">Agency</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')
    <div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.class.agencies.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                        <th scope="col"> ID </th>
                        <th scope="col"> Name  </th>
                        <th scope="col"> Scope </th>
                        <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($agencyClasses as $agencyClass)
                            <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$agencyClass->name}}</td>
                            <td>
                                    @if($agencyClass->type==1)
                                    Source Agency
                                    @else
                                    Destination Agency
                                    @endif
                                </td>
                                <td class="table-flex">
                                    <!-- <a href="{{route('admin.class.agencies.edit', $agencyClass->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a> -->
                                    <form action="{{ route('admin.class.agencies.destroy', $agencyClass->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
    

    
@endsection



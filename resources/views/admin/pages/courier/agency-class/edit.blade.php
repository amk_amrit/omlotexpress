@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.class.agencies.index')}}"> Agency Class</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
<form action="{{route('admin.class.agencies.update',$agencyClasses->id )}}" method="POST" enctype="multipart/form-data">
<input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label" >Scope</label>
            <select class="form-control" name="type">
                @if($agencyClasses->type==1)
                <option value="1">Pickup</option>
                <option value="2">Drop </option>
                @else
                <option value="2">Drop </option>
                <option value="1">Pickup</option>
                @endif
            </select>
        </div>

        <div class="form-group">
            <label class="control-label" >Name</label>
            <input type="text" class="form-control" name="name" value="{{$agencyClasses->name}}" >
            @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <label class="control-label" >Description</label>
            <input type="text" class="form-control" name="description" value="{{$agencyClasses->description}}" >
            @if ($errors->has('description')) <p style="color:red;">{{ $errors->first('description') }}</p> @endif <br>
        </div>
        <div class="form-group">
            <div class="custom-control custom-radio p-l-0">
                <input type="radio" class="custom-control-input" id="defaultChecked" name="status"  value="1" {{$agencyClasses->status == 1 ? 'checked' : '' }} >
                <label class="custom-control-label" for="defaultChecked">Active</label>
            </div> 
            <div class="custom-control custom-radio p-l-0">
                <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{$agencyClasses->status == 0 ? 'checked' : '' }} >
                <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
            </div>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection

@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicle-agent-charges.index')}}">Vehicle Agent Charge</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
<form action="{{route('admin.vehicle-agent-charge-get-datas')}}" method="GET" enctype="multipart/form-data">        
        <div class="form-group">
            <label class="control-label" >Country</label>
            <select class="form-control" name="country_id">
            @foreach($countries as $country)
                <option value="{{$country->id}}">{{$country->name}}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="control-label" >Vehicle</label>
            <select class="form-control" name="vehicle_id">
                @foreach($vehicles as $vehicle)
                <option value="{{$vehicle->id}}">{{ $vehicle->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="control-label" >Time Travel Group</label>
            <select class="form-control" name="time_trave_group_id">
                @foreach($timeTravelGroupes as $timeTravelGroup)
                <option value="{{$timeTravelGroup->id}}">{{$timeTravelGroup->name}}</option>
                @endforeach
           </select>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Select">
        </div>

</form>
@endsection

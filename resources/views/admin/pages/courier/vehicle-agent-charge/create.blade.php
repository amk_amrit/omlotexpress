@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicle-agent-charges.index')}}"> Vehicle Agent</a></li>
    <li class="breadcrumb-item active">Store</li>
@endsection
@include('error-messages.message')
<form action="{{route('admin.vehicle-agent-charges.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
  <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Quanity Groups</th>
      <th scope="col">Rate</th>
    </tr>
  </thead>
  <input type="hidden" name="country_id" value="{{$country_id}}">
  <input type="hidden" name="vehicle_id" value="{{$vehicle_id}}">
  <input type="hidden" name="type" value="{{$type}}">
  @foreach($vehicleDatas as $vehicleData)

  <tbody>
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{ $vehicleData->quantityGroup->group_name }}({{ $vehicleData->min_weight }} -- {{ $vehicleData->max_weight }})</td>
      <input type="hidden" value="{{$vehicleData->id}}" name="quantity_group_id[]">
      <td><input type="text" class="form-control rates" placeholder="Enter  rate"  name="rate[]" ></td>
      @if ($errors->has('rate')) <p style="color:red;">{{ $errors->first('rate') }}</p> @endif <br>

    </tr>
  </tbody>
  @endforeach        

</table>

        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection

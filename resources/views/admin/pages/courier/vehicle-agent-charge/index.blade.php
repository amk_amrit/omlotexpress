@extends('admin.layouts.app')
@section('title','Dashboard')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicle-agent-charges.index')}}">Vehicle Agents</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
        <a href="{{route('admin.vehicle-agent-charge-create-datas')}}" class="btn btn-success">Create</a>
    </div>
</div>

<div class="row">
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col"> ID </th>
                <th scope="col"> Name  </th>
                <th scope="col"> rate  </th>
                <th scope="col"> Action </th>
            </tr>
        </thead>
        @foreach($vehicleAgenctCharges as $vehicleAgenctCharge)
        <tbody>
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$vehicleAgenctCharge->vehicleName}}</td>
                <td>{{$vehicleAgenctCharge->rate}}</td>
                <td>
                    <a href="{{route('admin.vehicle-agent-charges.show', $vehicleAgenctCharge->id)}}" class="btn btn-info">View</a>
                    <a href="{{route('admin.vehicle-agent-charges.edit', $vehicleAgenctCharge->id)}}" class="btn btn-warning">Edit</a>
                    @if($vehicleAgenctCharge->status==0)
                    <form action="{{ route('admin.vehicle-agent-charges.destroy', $vehicleAgenctCharge->id)}}" method="post">
                    {{ csrf_field() }}
                    @method('DELETE')
                        <button type="submit" class="btn btn-danger" style="margin-top: 10px; margin-left:10px;">Activate</button>
                        </form>
                    @else
                    <form action="{{ route('admin.vehicle-agent-charges.destroy', $vehicleAgenctCharge->id)}}" method="post">
                    {{ csrf_field() }}
                    @method('DELETE')
                        <button type="submit" class="btn btn-danger" style="margin-top: 10px; margin-left:10px;">Deactivate</button>
                        </form>
                    
                    @endif
               
                </td>
                
            </tr>
        </tbody>
        @endforeach
    </table>
    </div>
    
    {{ $vehicleAgenctCharges->links() }}

    
@endsection

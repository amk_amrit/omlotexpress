@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicle-agent-charges.index')}}"> Vehicle Agent charges</a></li>
    <li class="breadcrumb-item active"></li>
@endsection
    <div class="row">
        <div style="display:flex; justify-content:flex-end; width:100%; padding:0; margin-bottom: 10px;">
        <a href="{{route('admin.vehicle-agent-charges.create')}}" class="btn btn-success" >Create</a>
        </div>
    </div> 
    <div class="row">
            <table style="width:100%">
                    <tr>
                        <th>Time Travel Group</th>
                        <td>{{$vehicleAgenctCharges->timeTravelGroupName}}</td>
                    </tr>
                    <tr>
                        <th>Quantity Group</th>
                        <td>{{$vehicleAgenctCharges->group_name}}</td>
                    </tr>
                    <tr>
                        <th>Country </th>
                        <td>{{$vehicleAgenctCharges->countryName}}</td>
                    </tr>
                    <tr>
                        <th>Vehicle</th>
                        <td>{{$vehicleAgenctCharges->vehicleName}}</td>
                    </tr>
                    <tr>
                        <th>Rate</th>
                        <td>{{$vehicleAgenctCharges->rate}}</td>
                    </tr>
        </table>
    </div>
    <div class="row" >
    </div>
               
@endsection
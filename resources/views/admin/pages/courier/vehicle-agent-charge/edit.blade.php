@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicle-agent-charges.index')}}">Vehicle Agent Charge</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
<form action="{{route('admin.vehicle-agent-charges.update', $vehicleAgenctCharge->id)}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label class="control-label" >Country</label>
            <input type="hidden" name="country_id" value="{{$vehicleAgenctCharge->country_id}}">
            <p>{{$vehicleAgenctCharge->countryName}}</p>
        </div>
        <div class="form-group">
            <label class="control-label" >Vehicle</label>
            <input type="hidden" name="vehicle_id" value="{{$vehicleAgenctCharge->vehicle_id}}">
            <p>{{$vehicleAgenctCharge->vehicleName}}</p>
        </div>
        <div class="form-group">
            <label class="control-label" >Time Travel Group</label>
            <input type="hidden" name="type" value="{{$vehicleAgenctCharge->type}}">
            <p>{{$vehicleAgenctCharge->timeTravelGroupName}}</p>
        </div>
        <div class="form-group">
            <label class="control-label" >Quantity Group</label>
            <input type="hidden" name="quantity_group_id" value="{{$vehicleAgenctCharge->quantity_group_id}}">
            <p>{{$vehicleAgenctCharge->group_name}}</p>
        </div>
        <div class="form-group">
            <label class="control-label" >rate</label>
            <input type="text" class="form-control" name="rate" value="{{$vehicleAgenctCharge->rate}}">
        </div>
        @if ($errors->has('rate')) <p style="color:red;">{{ $errors->first('rate') }}</p> @endif <br>

        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="Publish">
        </div>

</form>
@endsection

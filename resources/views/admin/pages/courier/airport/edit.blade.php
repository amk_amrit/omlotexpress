@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.airports.index')}}"> Airports</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@include('error-messages.message')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                    <form action="{{route('admin.airports.update',$airports->id )}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">
                            <div class="form-group">
                                <label class="control-label" >Scope</label>
                            <select class="form-control" name="scope">
                            <option selected disabled value="">--Select Airport Type--</option>
                                <option value="domestic" {{($airports->scope == 'domestic') ? 'selected' : ''}}>Domestic</option>
                                <option value="international" {{($airports->scope == 'international') ? 'selected' : ''}}>International</option>
                            </select>
                                @if ($errors->has('type')) <p style="color:red;">{{ $errors->first('type') }}</p> @endif <br>
                            </div>
                            <div class="form-group">
                                <label class="control-label" >Operate By</label>
                            <select class="form-control" name="operate_by">
                                <option selected disabled value="">--Select Operate Office--</option>
                                @foreach($transits as $transit)
                                <option value="{{$transit->id}}" {{($airports->operate_by == $transit->id) ? 'selected' : ''}}>{{$transit->name}}</option>                                @endforeach
                            </select>
                                @if ($errors->has('operate_by')) <p style="color:red;">{{ $errors->first('operate_by') }}</p> @endif
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label" >Name</label>
                                <input type="text" class="form-control" name="name" value="{{$airports->name}}" >
                                @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label" >Address</label>
                                <input type="text" class="form-control" name="address" id="address" value="{{$airports->address}}" >
                                @if ($errors->has('address')) <p style="color:red;">{{ $errors->first('address') }}</p> @endif
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label" >Latitude</label>
                                <input class="form-control" name="latitude" id="latitude" placeholder="Enter Longitude" value="{{$airports->latitude}}">
                            </div>
                            @if ($errors->has('latitude')) <p style="color:red;">{{ $errors->first('latitude') }}</p> @endif
                            <div class="form-group">
                                <label class="control-label" >Longitude</label>
                                <input class="form-control" name="longitude" id="longitude" placeholder="Enter Longitude" value="{{$airports->longitude}}">
                            </div>
                            @if ($errors->has('longitude')) <p style="color:red;">{{ $errors->first('longitude') }}</p> @endif
                            <div class="form-group">
                                <div class="custom-control custom-radio p-l-0">
                                    <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{ (isset($airports) && $airports->status == 1) || old('status') === 1 ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="defaultChecked">Active</label>
                                </div> 
                                <div class="custom-control custom-radio p-l-0">
                                    <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{ (isset($airports) && $airports->status == 0) || old('status') === 0 ? 'checked' : '' }} >
                                    <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-success" value="Publish">
                            </div> 
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@include('admin.pages.courier.airport.airport-map-scripts');
@endpush

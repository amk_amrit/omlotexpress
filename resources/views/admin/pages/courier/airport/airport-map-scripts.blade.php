<script>

    let autocomplete={};

    let countryRestrict = {'country': 'np'};//for 1st time //ailey use garya xaina

    let pacInput;

    let placeOriginLatitude;
    let placeOriginLongitude;
    let placeDestinationLatitude;
    let placeDestinationLongitude;


    // This example requires the Places library. Include the libraries=places
    function initAutocomplete() {

       pacInput = document.getElementById('address');

        var options = {
            //componentRestrictions: countryRestrict
            //types: ['(cities)'],
            //componentRestrictions: {country: 'fr'}
        };

        autocomplete = new google.maps.places.Autocomplete(pacInput,options);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);


        autocomplete.addListener('place_changed', function() {

            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
                placeOriginLatitude=place.geometry.location.lat();
                placeOriginLongitude=place.geometry.location.lng();

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            //custom function
            bindLatLngToInputField(placeOriginLatitude,placeOriginLongitude);

        });

    }

    // Set the country restriction based on user input.
    // function setAutocompleteCountry(countryType) {
    //     var country = document.getElementById(countryType).value;
    //     if (country) {
    //         autocomplete.setComponentRestrictions({'country': country});
    //     } else {
    //         autocomplete.setComponentRestrictions({'country': []});
    //     }

    // }

    function bindLatLngToInputField(latitude,longitude) {
        $('#latitude').val(latitude);
        $('#longitude').val(longitude);
    }

    // $(document).ready(function () {

    //     let localOriginAddressInput = $('#pac-input');
    //     let localDestinationAddressInput = $('#destination_input');
    //     localOriginAddressInput.prop('disabled', true);
    //     localDestinationAddressInput.prop('disabled', true);
    //     var calculatedDistance=0;


    //     $('#origin_country').on('change',function () {

    //         localOriginAddressInput.val('');
    //         localOriginAddressInput.prop('disabled', false);

    //     });
    //     localOriginAddressInput.on('focus',function () {

    //         autocomplete={};
    //         initAutocomplete('pac-input');//initializing map
    //         setAutocompleteCountry('origin_country');//pass country select id
    //     });

    //     $('#destination_country').on('change',function () {
    //         localDestinationAddressInput.val('');
    //         localDestinationAddressInput.prop('disabled', false);

    //     });
    //     localDestinationAddressInput.on('focus',function () {

    //         autocomplete={};
    //         initAutocomplete('destination_input');//initializing map
    //         setAutocompleteCountry('destination_country');//pass country select id
    //     });


    // });

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrPMYf0pq2cXXte1SxJOdMJR8F9dMxKdg&libraries=places&callback=initAutocomplete"
        async defer></script>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrPMYf0pq2cXXte1SxJOdMJR8F9dMxKdg&libraries=places"
        async defer></script> -->


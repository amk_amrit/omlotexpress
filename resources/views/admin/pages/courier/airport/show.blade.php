@extends('admin.layouts.app')
@section('title','Airports')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.airports.index')}}"> All Airport</a></li>
    <li class="breadcrumb-item active">Show</li>
@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                                    <thead>
                                        <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Address</th>
                                        <th scope="col">Operate By</th>
                                        <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($airports as $airport)
                                        <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ucfirst(trans($airport->name))}}</td>
                                        <td>{{ucfirst(trans($airport->address))}}</td>
                                        <td>{{ucfirst(trans($airport->officeName))}}</td>
                                        
                                    <td>
                                    <a href="{{route('admin.airports.edit', $airport->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>

                                    </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    </table>
            </div>
        </div>
    </div>
</div>
@endsection


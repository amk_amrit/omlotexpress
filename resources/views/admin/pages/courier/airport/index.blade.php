@extends('admin.layouts.app')
@section('title','Airport')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.airports.index')}}">Airports</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')
    <div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.airports.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                        <th scope="col"> ID </th>
                        <th scope="col"> Scope  </th>
                        <th scope="col"> Total Airport </th>
                        <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        @foreach($airports as $airport)
                        <td>{{$loop->iteration}}</td>
                        <td>{{ucfirst(trans($airport->scope))}}</td>
                        <td>
                            @php
                            $count = $airport->count_country_airports;                                      
                            $more = (($count - 3) > 0) ? ' & '.($count - 3).' more Airports' : '';
                            @endphp
                            {{$airport->country_airports}} {{$more}}
                        </td>
                                <td class="table-flex">
                                    <a href="{{route('admin.airports.show', $airport->scope)}}" class="btn btn-warning m-r-10"><i class="fa fa-eye"></i></a>
                                    <!-- <form action="{{ route('admin.airports.destroy', $airport->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        @if($airport->status==0)
                                        <button type="submit" class="btn btn-danger">Activate</button>
                                        @else
                                            <button type="submit" class="btn btn-danger">Deactivate</button>
                                        @endif
                                    </form> -->
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
    

    
@endsection





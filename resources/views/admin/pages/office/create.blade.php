@extends('admin.layouts.app')
@section('title','Offices')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.offices.index')}}">Offices</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')
@include('error-messages.message')

    <form action="{{route('admin.offices.store')}}" method="POST">
        {{ csrf_field() }}
        @include('admin.pages.office.form', [$formMode="Publish"])
    </form>
           
@endsection

@extends('admin.layouts.app')
@section('title','Offices')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.offices.index')}}">Offices</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
@include('error-messages.message')
   
    <form action="{{route('admin.offices.update', $office->id)}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        @include('admin.pages.office.form', [$formMode="Edit"])
    </form>
            
@endsection

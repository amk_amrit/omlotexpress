@extends('admin.layouts.app')
@section('title','Offices')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.offices.index')}}">Offices</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.offices.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col"> S.N </th>
                            <th scope="col"> Name </th>
                            <th scope="col">Country</th>
                            <th scope="col">Type</th>
                            <th scope="col"> Address</th>
                            <th scope="col"> Action </th>
                        </tr>
                        </thead>
                        @foreach($offices as $office)
                            <tbody>
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$office->name}}</td>
                                <td>{{$office->country->name}}</td>
                                <td>{{$office->office_type}}</td>
                                <td>{{$office->address}}</td>
                                <td class="table-flex">
                                    <a href="{{route('admin.offices.show', $office->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                    <a href="{{route('admin.offices.edit', $office->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>

                                    <form action="{{ route('admin.offices.destroy',$office->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        @if($office->status==0)
                                            <button type="submit" class="btn btn-success">Activate</button>
                                        @else
                                            <button type="submit" class="btn btn-danger">Deactivate</button>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    

    
@endsection

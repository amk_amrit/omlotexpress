@extends('admin.layouts.app')
@section('title',$office->name . 'Connections')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.agency-offices.index')}}">Agency Offices</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.agency-offices.show',$office->id)}}">{{$office->name}}</a></li>
    <li class="breadcrumb-item">Connections</li>
@endsection

@section('content')

    @include('error-messages.message')

    <div class="row">
        <div class="col-lg-12 text-right">
            <a id="add-connection" class="btn btn-success m-b-10"
               data-toggle="collapse" href="#add-connection-form"  aria-expanded="false" aria-controls="add-connection-form">Add Connection</a>
        </div>
    </div>

    <!-- add connection form -->
    @include('admin.pages.office.agent-office.agency-transit-link.transit-connection-form')
    <!-- /add connection form-->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h3>Transit Connections of {{$office->name}}</h3>
                    <div class="table-responsive mt-4">
                        <table id="myTable" class="table">
                            <thead>
                            <tr>
                                <th scope="col"> S.N </th>
                                <th scope="col"> Name </th>
                                <th scope="col">Country</th>
                                <th scope="col">Distance</th>
                                <th scope="col">Vehicle Travel Time</th>
                                <th scope="col"> Action </th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($agencyTransitConnections as $transitOffice)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$transitOffice->office->name}}</td>
                                    <td>{{$transitOffice->office->country->name}}</td>
                                    <td>{{$transitOffice->pivot->distance}}</td>
                                    <td>{{$transitOffice->pivot->vehicle_travel_time}}</td>
                                    <td class="table-flex">

                                        <a href="{{route('admin.agency-transit.connections.edit',
                                        ['agencyOfficeId'=>$office->id,'transitOfficeId'=>$transitOffice->office->id])}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
                                        <a href="{{route('admin.agency-transit-vehicle-charges.show',$transitOffice->pivot->id)}}" class="btn btn-primary m-r-10">Create Charge</a>
                                        <a href="{{route('admin.agency-transit-vehicle-charges.edit',$transitOffice->pivot->id)}}" class="btn btn-primary m-r-10">Edit Charge</a>


                                        <form action="{{route('admin.agency-transit.connections.toggleStatus',
                                        ['agencyOfficeId'=>$office->id,'transitOfficeId'=>$transitOffice->office->id])}}" method="post">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            @if($transitOffice->pivot->status == 0)
                                                <button type="submit" class="btn btn-success m-r-10">Activate</button>
                                            @else
                                                <button type="submit" class="btn btn-danger m-r-10">Deactivate</button>
                                            @endif
                                        </form>

                                        <form action="{{route('admin.agency-transit.connections.destroy',
                                        ['agencyOfficeId'=>$office->id,'transitOfficeId'=>$transitOffice->office->id])}}" method="post"
                                              onclick="return confirm('Are you sure you want to delete this connection?');">
                                            {{ csrf_field() }}
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger">Delete</button>

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


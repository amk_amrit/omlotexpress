@extends('admin.layouts.app')
@section('title',$agencyOffice->name . 'Connections')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.agency-offices.index')}}">Agency Offices</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.agency-offices.show',$agencyOffice->id)}}">{{$agencyOffice->name}}</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.agency-transit.connections', $agencyOffice->id) }}">Connections</a> </li>
@endsection

@section('content')

    @include('error-messages.message')

    <form id="add-connection-form" action="{{route('admin.agency-transit.connections.update',['agencyOfficeId'=>$agencyOffice->id,'transitOfficeId'=>$transitOffice->id])}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >Transit Office</label>
                                    <select class="form-control" name="transit_connection" readonly>

                                        <option>{{$transitOffice->name}}</option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >Distance</label>
                                    <input type="number" step="any" class="form-control" name="distance" placeholder="In Kilometer" value="{{ $agencyTransitConnection->distance }}">
                                    @if ($errors->has('distance')) <p style="color:red;">{{ $errors->first('distance') }}</p> @endif <br>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >Travel Time</label>
                                    <input type="number" step="any" class="form-control" name="travel_time" value="{{ $agencyTransitConnection->vehicle_travel_time }}" placeholder="In hour">
                                    @if ($errors->has('travel_time')) <p style="color:red;">{{ $errors->first('travel_time') }}</p> @endif <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-success"
                                   value="Update Connection">
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </form>





@endsection



<form id="add-connection-form" class="collapse" action="{{route('admin.agency-transit.connections.store',$office->id)}}" method="POST">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label" >Transit Office</label>
                                <select class="form-control select2" name="transit_connection" >
                                    <option selected disabled >--select an option --</option>

                                    @foreach($transitOffices as $office)
                                        <option value="{{$office->transit->id}}" {{old('transit_connection') == $office->transit->id ? 'selected' : ''}}>{{$office->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('transit_connection')) <p style="color:red;">{{ $errors->first('transit_connection') }}</p> @endif <br>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label" >Distance</label>
                                <input type="number" step="any" class="form-control" name="distance" placeholder="In Kilometer" value="{{ old('distance') }}">
                                @if ($errors->has('distance')) <p style="color:red;">{{ $errors->first('distance') }}</p> @endif <br>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label" >Travel Time</label>
                                <input type="number" step="any" class="form-control" name="travel_time" value="{{ old('travel_time') }}" placeholder="In hour">
                                @if ($errors->has('travel_time')) <p style="color:red;">{{ $errors->first('travel_time') }}</p> @endif <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <input type="submit" class="btn btn-success"
                               value="Add Connection">
                    </div>
                </div>

            </div>
        </div>
    </div>


</form>


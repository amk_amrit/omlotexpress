@extends('admin.layouts.app')
@section('title','Transit Offices')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.transit-offices.index')}}">Transit Offices</a></li>
<li class="breadcrumb-item active">{{$office->name}}</li>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.transit-offices.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                    <table class="table">

                        <tr>
                            <th>Name</th>
                            <td>{{$office->name}}</td>
                        </tr>
                        <tr>
                            <th>Country</th>
                            <td>{{$office->country->name}}</td>
                        </tr>
                        <tr>
                            <th>Office Type</th>
                            <td>{{$office->office_type}}</td>
                        </tr>

                        <tr>
                            <th>Class</th>
                            <td>
                                <a href="{{route('admin.courier-classes.show',$office->transit->transitClass->id)}}">
                                    {{$office->transit->transitClass->name}}
                                </a>

                            </td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>{{$office->address}}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                @if($office->status == 1)
                                <span style="color:red"> Activated</span>
                                @else
                                <span style="color:blue"> Deactivated</span>
                                @endif
                            </td>
                        </tr>

                        <tr>
                            <th>Created By</th>
                            <td>{{$office->createdBy->user->name}}</td>
                        </tr>

                        <tr>
                            <th>Updated By</th>
                            <td>{{$office->updatedBy->user->name}}</td>
                        </tr>
                    </table>
                </div> -->

                <div class="country-div-list">
                    <ul>
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$office->name}}</h6>
                        </li>
                        <li>
                            <h4>Country : </h4>
                            <h6>{{$office->country->name}}</h6>
                        </li>
                        <li>
                            <h4>Office Type : </h4>
                            <h6>{{$office->office_type}}</h6>
                        </li>
                        <li>
                            <h4>Class : </h4>
                            <h6>
                                <a href="{{route('admin.courier-classes.show',$office->transit->transitClass->id)}}">
                                    {{$office->transit->transitClass->name}}
                                </a>
                            </h6>
                        </li>
                        <li>
                            <h4>Address : </h4>
                            <h6>{{$office->address}}</h6>
                        </li>
                        <li>
                            <h4>Status : </h4>
                            <h6>
                                @if($office->status == 1)
                                <span style="color:red"> Activated</span>
                                @else
                                <span style="color:blue"> Deactivated</span>
                                @endif
                            </h6>
                        </li>
                        <li>
                            <h4>Created by : </h4>
                            <h6>{{$office->createdBy->user->name}}</h6>
                        </li>
                        <li>
                            <h4>Updated by : </h4>
                            <h6>{{$office->updatedBy->user->name}}</h6>
                        </li>
                    </ul>
                </div>

                <div class="form-btn">
                    <a href="{{ route('admin.transit-offices.edit', $office->id) }}" class="btn btn-warning">Edit</a>

                    <form action="{{ route('admin.transit-offices.destroy', $office->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($office->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
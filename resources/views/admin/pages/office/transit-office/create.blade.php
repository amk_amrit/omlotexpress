@extends('admin.layouts.app')
@section('title','Transit Offices')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.index')}}">Transit Offices</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')
    @include('error-messages.message')

    <form action="{{route('admin.transit-offices.store')}}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <div class="form-group">
                            <label class="control-label"  >Name</label>
                            <input type="text" class="form-control" name="name" value="{{old('name') }}">
                            @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                        </div>


                        <div class="form-group">
                            <label class="control-label"  >Address</label>
                            <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                            @if ($errors->has('address')) <p style="color:red;">{{ $errors->first('address') }}</p> @endif <br>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label"  >Phone</label>
                                    <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                                    @if ($errors->has('phone')) <p style="color:red;">{{ $errors->first('phone') }}</p> @endif <br>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label"  >Email</label>
                                    <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email')) <p style="color:red;">{{ $errors->first('email') }}</p> @endif <br>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label"  >Latitude</label>
                                    <input type="number" step="any" class="form-control" name="latitude" value="{{ old('latitude') }}">
                                    @if ($errors->has('latitude')) <p style="color:red;">{{ $errors->first('latitude') }}</p> @endif <br>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label"  >Longitude</label>
                                    <input type="number" step="any" class="form-control" name="longitude" value="{{ old('longitude') }}">
                                    @if ($errors->has('longitude')) <p style="color:red;">{{ $errors->first('longitude') }}</p> @endif <br>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" >Transit Class</label>
                            <select class="form-control select2" name="transit_class" >
                                <option selected disabled >--select an option --</option>

                                @foreach($transitTypeCourierClasses as $courierClass)
                                    <option value="{{$courierClass->slug}}" {{old('transit_class') == $courierClass->slug ? 'selected' : ''}}>{{$courierClass->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('transit_class')) <p style="color:red;">{{ $errors->first('transit_class') }}</p> @endif <br>
                        </div>

                        <div class="form-group">
                            <label class="control-label" >Transit Connection</label>
                            <select class="form-control select2" name="transit_connection" >
                                <option selected disabled >--select an option --</option>

                                @foreach($transitOffices as $office)
                                    <option value="{{$office->transit->id}}" {{old('transit_connection') == $office->transit->id ? 'selected' : ''}}>{{$office->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('transit_connection')) <p style="color:red;">{{ $errors->first('transit_connection') }}</p> @endif <br>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label class="control-label" >Travel Medium</label>
                                    <select class="form-control select2" name="travel_medium" >
                                        <option selected disabled >--select an option --</option>

                                        @foreach($travelMedium as $mediumKey => $mediumValue)
                                            <option value="{{$mediumValue}}" {{old('travel_medium') ==$mediumValue ? 'selected' : ''}}>{{$mediumKey}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('travel_medium')) <p style="color:red;">{{ $errors->first('travel_medium') }}</p> @endif <br>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label class="control-label" >Distance</label>
                                    <input type="number" step="any" class="form-control" name="distance" placeholder="In Kilometer" value="{{ old('distance') }}">
                                    @if ($errors->has('distance')) <p style="color:red;">{{ $errors->first('distance') }}</p> @endif <br>
                                </div>
                            </div>



                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label class="control-label" >Travel Time</label>
                                    <input type="number" step="any" class="form-control" name="travel_time" value="{{ old('travel_time') }}" placeholder="In hour">
                                    @if ($errors->has('travel_time')) <p style="color:red;">{{ $errors->first('travel_time') }}</p> @endif <br>
                                </div>
                            </div>
                        </div>




                        <div class="form-group">
                            <label class="control-label">Select</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1"
                                        {{ old('status') == 1 ? 'checked' : '' }}>
                                <label class="custom-control-label" for="defaultChecked">Active</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0"
                                        {{ (!is_null(old('status')) && old('status') == 0)  ? 'checked' : '' }}>
                                <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success"
                                   value="Submit">
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </form>

@endsection

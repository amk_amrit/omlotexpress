@extends('admin.layouts.app')
@section('title','Transit Offices')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.index')}}">Transit Offices</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
    @include('error-messages.message')

    <form action="{{route('admin.transit-offices.update',$office->id)}}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <div class="form-group">
                            <label class="control-label"  >Name</label>
                            <input type="text" class="form-control" name="name" value="{{$office->name }}">
                            @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                        </div>


                        <div class="form-group">
                            <label class="control-label"  >Address</label>
                            <input type="text" class="form-control" name="address" value="{{ $office->address }}">
                            @if ($errors->has('address')) <p style="color:red;">{{ $errors->first('address') }}</p> @endif <br>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label"  >Phone</label>
                                    <input type="text" class="form-control" name="phone" value="{{ $office->phone }}">
                                    @if ($errors->has('phone')) <p style="color:red;">{{ $errors->first('phone') }}</p> @endif <br>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label"  >Email</label>
                                    <input type="text" class="form-control" name="email" value="{{$office->email }}">
                                    @if ($errors->has('email')) <p style="color:red;">{{ $errors->first('email') }}</p> @endif <br>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label"  >Latitude</label>
                                    <input type="number" step="any" class="form-control" name="latitude" value="{{ $office->latitude }}">
                                    @if ($errors->has('latitude')) <p style="color:red;">{{ $errors->first('latitude') }}</p> @endif <br>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label"  >Longitude</label>
                                    <input type="number" step="any" class="form-control" name="longitude" value="{{ $office->longitude }}">
                                    @if ($errors->has('longitude')) <p style="color:red;">{{ $errors->first('longitude') }}</p> @endif <br>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" >Transit Class</label>
                            <select class="form-control select2" name="transit_class" >
                                <option selected disabled >--select an option --</option>

                                @foreach($transitTypeCourierClasses as $courierClass)
                                    <option value="{{$courierClass->slug}}"
                                            {{$office->transit->transitClass->slug == $courierClass->slug ? 'selected' : ''}}>
                                        {{$courierClass->name}}
                                    </option>
                                @endforeach
                            </select>
                            @if ($errors->has('transit_class')) <p style="color:red;">{{ $errors->first('transit_class') }}</p> @endif <br>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Select</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1"
                                        {{ $office->status == 1 ? 'checked' : '' }}>
                                <label class="custom-control-label" for="defaultChecked">Active</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0"
                                        {{ (!is_null($office->status) && $office->status == 0)  ? 'checked' : '' }}>
                                <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success"
                                   value="Update">
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </form>

@endsection

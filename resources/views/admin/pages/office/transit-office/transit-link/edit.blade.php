@extends('admin.layouts.app')
@section('title',$office->name . 'Connections')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.index')}}">Transit Offices</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.show',$office->id)}}">{{$office->name}}</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.connections', $office->id) }}">Connections</a> </li>
@endsection

@section('content')

    @include('error-messages.message')

    <form id="add-connection-form" action="{{route('admin.transit-offices.connections.update', [$office->id,
                                        $transitLink->id,'connected-office-slug'=>$connectedOffice->slug])}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >Transit Office</label>
                                    <select class="form-control" name="transit_connection" readonly>

                                        <option>{{$connectedOffice->name}}</option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >Distance</label>
                                    <input type="number" step="any" class="form-control" name="distance" placeholder="In Kilometer" value="{{ $transitLink->distance }}">
                                    @if ($errors->has('distance')) <p style="color:red;">{{ $errors->first('distance') }}</p> @endif <br>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="control-label" >Travel Time</label>
                                    <input type="number" step="any" class="form-control" name="travel_time" value="{{ $transitLink->travel_time }}" placeholder="In hour">
                                    @if ($errors->has('travel_time')) <p style="color:red;">{{ $errors->first('travel_time') }}</p> @endif <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-success"
                                   value="Update Connection">
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </form>





@endsection


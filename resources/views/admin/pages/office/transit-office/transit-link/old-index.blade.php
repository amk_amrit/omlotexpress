@extends('admin.layouts.app')
@section('title',$office->name . 'Connections')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.index')}}">Transit Offices</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.show',$office->id)}}">{{$office->name}}</a></li>
    <li class="breadcrumb-item">Connections</li>
@endsection

@section('content')

    @include('error-messages.message')

    <div class="row">
        <div class="col-lg-12 text-right">
            <a id="add-connection" class="btn btn-success m-b-10"
               data-toggle="collapse" href="#add-connection-form"  aria-expanded="false" aria-controls="add-connection-form">Add Connection</a>
        </div>
    </div>

    <!-- add connection form -->
    @include('admin.pages.office.transit-office.transit-link.transit-connection-form')
    <!-- /add connection form-->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h3>Connections of {{$office->name}}</h3>
                    <div class="table-responsive mt-4">
                        <table id="myTable" class="table">
                            <thead>
                            <tr>
                                <th scope="col"> S.N </th>
                                <th scope="col"> Name </th>
                                <th scope="col">Country</th>
                                <th scope="col">Distance</th>
                                <th scope="col">Travel Time</th>
                                <th scope="col">Medium</th>
                                <th scope="col"> Action </th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($transitConnections as $transitOffice)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$transitOffice->office->name}}</td>
                                    <td>{{$transitOffice->office->country->name}}</td>
                                    <td>{{$transitOffice->pivot->distance}}</td>
                                    <td>{{$transitOffice->pivot->travel_time}}</td>
                                    <td>{{$transitOffice->pivot->travel_medium}}</td>
                                    <td class="table-flex">

                                        <a href="{{route('admin.transit-offices.connections.edit', [$office->id,
                                        $transitOffice->pivot->id,'connected-office-slug'=>$transitOffice->office->slug])}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>

                                        <form action="{{route('admin.transit-offices.connections.toggleStatus', [$office->id,
                                        $transitOffice->pivot->id,'connected-office-slug'=>$transitOffice->office->slug])}}" method="post">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            @if($transitOffice->pivot->status==0)
                                                <button type="submit" class="btn btn-success">Activate</button>
                                            @else
                                                <button type="submit" class="btn btn-danger">Deactivate</button>
                                            @endif
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
@include('admin.pages.office.transit-office.transit-link.transit-link-scripts')
@endpush

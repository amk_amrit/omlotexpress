@extends('admin.layouts.app')
@section('title',$office->name . 'Connections')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.index')}}">Transit Offices</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.show',$office->id)}}">{{$office->name}}</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.connections', $office->id) }}">Connections</a> </li>
@endsection

@section('content')

    @include('error-messages.message')

    <div class="row">
        <div class="col-lg-12 text-right">
            <a id="add-connection" class="btn btn-success m-b-10"
               data-toggle="collapse" href="#add-connection-form"  aria-expanded="false" aria-controls="add-connection-form">Add Connection</a>
        </div>
    </div>

    <!-- add connection form -->
    @include('admin.pages.office.transit-office.transit-link.transit-connection-form')
    <!-- /add connection form-->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h3>Connections of {{$office->name}}</h3>
                    <div class="table-responsive mt-4">
                        <table id="myTable" class="table">
                            <thead>
                            <tr>
                                <th scope="col"> S.N </th>
                                <th scope="col"> Name </th>
                                <th scope="col">Country</th>
                                <th scope="col">Distance</th>
                                <th scope="col">Travel Time</th>
                                <th scope="col">Medium</th>
                                <th scope="col"> Action </th>
                            </tr>
                            </thead>

                                <tbody>
                                @foreach($transitConnections as $transitLink)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$transitLink->my_connected_office_name}}</td>
                                    <td>{{$transitLink->my_connected_office_country}}</td>
                                    <td>{{$transitLink->distance}}</td>
                                    <td>{{$transitLink->travel_time}}</td>
                                    <td>{{$transitLink->travel_medium}}</td>
                                    <td class="table-flex">

                                        <a href="{{route('admin.transit-offices.connections.edit', [$office->id,
                                        $transitLink->id,'connected-office-slug'=>$transitLink->my_connected_office_slug])}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>

                                        <form action="{{route('admin.transit-offices.connections.toggleStatus', [$office->id,
                                        $transitLink->id,'connected-office-slug'=>$transitLink->my_connected_office_slug])}}" method="post">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            @if($transitLink->status==0)
                                                <button type="submit" class="btn btn-success m-r-10">Activate</button>
                                            @else
                                                <button type="submit" class="btn btn-danger m-r-10">Deactivate</button>
                                            @endif
                                        </form>
                                        <form action="{{route('admin.transit-offices.connections.destroy', [$office->id,
                                        $transitLink->id,'connected-office-slug'=>$transitLink->my_connected_office_slug])}}" method="post"
                                              onclick="return confirm('Are you sure you want to delete this connection?');">
                                            {{ csrf_field() }}
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger">Delete</button>

                                        </form>

                                    </td>
                                </tr>
                                @endforeach
                                </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
@include('admin.pages.office.transit-office.transit-link.transit-link-scripts')
@endpush

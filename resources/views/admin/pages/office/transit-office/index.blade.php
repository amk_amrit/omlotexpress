@extends('admin.layouts.app')
@section('title','Transit Offices')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.transit-offices.index')}}">Transit Offices</a></li>
@endsection

@section('content')

    @include('error-messages.message')

    <div class="row">
        <div class="col-lg-12 text-right">
            <a href="{{route('admin.transit-offices.create')}}" class="btn btn-success m-b-10">Create</a>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive mt-4">
                        <table id="myTable" class="table">
                            <thead>
                            <tr>
                                <th scope="col"> S.N </th>
                                <th scope="col"> Name </th>
                                <th scope="col">Country</th>
                                <th scope="col">Type</th>
                                <th scope="col"> Address</th>
                                <th scope="col"> Action </th>
                            </tr>
                            </thead>
                                <tbody>
                                @foreach($transitOffices as $office)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$office->name}}</td>
                                    <td>{{$office->country->name}}</td>
                                    <td>{{$office->office_type}}</td>
                                    <td>{{$office->address}}</td>
                                    <td class="table-flex">
                                        <a href="{{route('admin.transit-offices.show', $office->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                        <a href="{{route('admin.transit-offices.connections', $office->id) }}" class="btn btn-info m-r-10"><i class="fa fa-link" title="connections"></i></a>
                                        <a href="{{route('admin.transit-offices.edit', $office->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>

                                        <form action="{{ route('admin.transit-offices.destroy',$office->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            @if($office->status==0)
                                                <button type="submit" class="btn btn-success">Activate</button>
                                            @else
                                                <button type="submit" class="btn btn-danger">Deactivate</button>
                                            @endif
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

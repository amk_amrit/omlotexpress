<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="form-group">
                    <label class="control-label"  >Name</label>
                    <input type="text" class="form-control" name="name" value="{{ isset($office) ? $office->name : old('name') }}">
                    @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                </div>
        
                @if(auth()->user()->employee->isSuperAdmin())
                    <div class="form-group">
                        <label class="control-label" >Country</label>
                        <select class="form-control" name="country_id" >
                            
                            <option {{ !isset($office) ? 'selected' : ''}} disabled >-- Select Country --</option>
                            {{-- <option value="{{isset($office) ? $office->country_id : ''}}" {{ !isset($office) ? 'disabled selected' : '' }} >{{isset($office) ? $office->country->name : '--select an option--'}}</option> --}}
                            @foreach($countries as $country)
                                <option value="{{$country->id}}"  {{ (isset($office) && $country->id == $office->country_id) || $country->id == old('country_id') ? 'selected' : ''}} >{{$country->name}}</option>
                            @endforeach
                        
                        </select>
        
                        
                        @if ($errors->has('country_id')) <p style="color:red;">{{ $errors->first('country_id') }}</p> @endif <br>
                    </div>
                @endif
        
                <div class="form-group">
                    <label class="control-label" >Office Type</label>
                    <select class="form-control" name="office_type" >
                        <option selected disabled >--select an option --</option>
                        {{-- <option value="super_head" {{(isset($office) && $office->office_type == 'super_head') || old('office_type') == 'super_head' ? 'selected' : '' }}>Super Head</option> --}}
                        @if(auth()->user()->employee->isSuperAdmin())
                            <option value="country_head" {{(isset($office) && $office->office_type == 'country_head') || old('office_type') == 'country_head'  ? 'selected' : '' }}>Country Head</option>
                        @endif
                        @if(auth()->user()->employee->isCountryHead())
                            <option value="region_head" {{(isset($office) && $office->office_type == 'region_head') || old('region_head') == 'region_head' ? 'selected' : '' }}>Region Head</option>
                        @endif
                        @if(auth()->user()->employee->isRegionHead())
                            <option value="transit" {{(isset($office) && $office->office_type == 'transit') || old('transit') == 'transit' ? 'selected' : '' }}>Transit Office</option>
                        @endif
                    </select>
                    @if ($errors->has('office_type')) <p style="color:red;">{{ $errors->first('office_type') }}</p> @endif <br>
                </div>
        
                <div class="form-group">
                    <label class="control-label"  >Address</label>
                    <input type="text" class="form-control" name="address" value="{{ isset($office) ? $office->address : old('address') }}">
                    @if ($errors->has('address')) <p style="color:red;">{{ $errors->first('address') }}</p> @endif <br>
                </div>
        
                <div class="form-group">
                    <label class="control-label">Select</label>
                    <div class="custom-control custom-radio p-l-0">
                        <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1"
                                {{ old('status') == 1 || (isset($office) && $office->status == 1)  ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultChecked">Active</label>
                    </div>
                    <div class="custom-control custom-radio p-l-0">
                        <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0"
                                {{ (!is_null(old('status')) && old('status') == 0) || (isset($office) && $office->status == 0)  ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                    </div>
                </div>
                
                <div class="form-group">
                <input type="submit" class="btn btn-success"
                value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
                </div>
                
            </div>
        </div>
    </div>
</div>
       

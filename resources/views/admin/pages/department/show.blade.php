@extends('admin.layouts.app')
@section('title','Departments')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('admin') }}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.departments.index')}}">Departments</a></li>
<li class="breadcrumb-item active">{{$department->name}}</li>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.departments.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive mt-4">
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>{{$department->name}}</td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td>{{$department->description}}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                @if($department->status == 1)
                                <span> Activated</span>
                                @else
                                <span> Deactivated</span>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div> -->
                <div class="country-div-list">
                    <ul>
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$department->name}}</h6>
                        </li>
                        <li>
                            <h4>Description : </h4>
                            <h6>{{$department->description}}</h6>
                        </li>
                        <li>
                            <h4>Status : </h4>
                            <h6>{{$department->name}}</h6>
                        </li>
                        <li>
                            <h4>Name : </h4>
                            <h6>
                                @if($department->status == 1)
                                <span> Activated</span>
                                @else
                                <span> Deactivated</span>
                                @endif
                            </h6>
                        </li>
                    </ul>
                </div>
                <div class="form-btn">
                    <a href="{{ route('admin.departments.edit', $department->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('admin.departments.destroy', $department->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($department->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
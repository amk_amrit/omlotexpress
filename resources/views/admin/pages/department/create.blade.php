@extends('admin.layouts.app')
@section('title','Departments')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.departments.index')}}">Departments</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')
    
    <form action="{{route('admin.departments.store')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('admin.pages.department.form', [$formMode="Publish"])
    </form>
               
@endsection

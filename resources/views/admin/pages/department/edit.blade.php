@extends('admin.layouts.app')
@section('title','Departments')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.departments.index')}}">Departments</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')

    <form action="{{route('admin.departments.update', $department->id)}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        @include('admin.pages.department.form', [$formMode="Edit"])
    </form>
               
@endsection

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label class="control-label"  >Name</label>
                    <input type="text" class="form-control" name="name" value="{{ isset($department) ? $department->name : old('name') }}">
                    @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                </div>
            
                <div class="form-group">
                    <label class="control-label" >Description</label>
                    <textarea class="form-control" rows="5" cols="10" name="description">{{ isset($department) ? $department->description : old('description') }}</textarea>
                    @if ($errors->has('description')) <p style="color:red;">{{ $errors->first('description') }}</p> @endif <br>
                </div>
            
                <div class="form-group">
                    <label class="control-label">Select</label>
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{ (isset($department) && $department->status == 1) || old('status') === 1 ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultChecked">Active</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{ (isset($department) && $department->status == 0) || old('status') === 0 ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                    </div>
                </div>
            
                <div class="form-group">
                <input type="submit" class="btn btn-success"
                value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
                </div>
                
            </div>
        </div>
    </div>
</div>
    

@extends('admin.layouts.app')
@section('title','Employees')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.employees.index')}}">Employees</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
@include('error-messages.message')
    <form action="{{route('admin.employees.update', $employee->id )}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label class="control-label">Office</label>
                            <select class="form-control" id= "officeSelect" name="office_id">

                                <option value="" disabled selected>--Select Office--</option>
                                @foreach($offices as $office)
                                    <option value="{{$office->id}}" {{ ($office->id == old('office_id')) || $office->id == $employee->office->id ? 'selected': '' }} > {{$office->name}} </option>
                                @endforeach

                            </select>
                            @if ($errors->has('office_id')) <p style="color:red;">{{ $errors->first('office_id') }}</p> @endif
                            <br>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Designation</label>
                            <select class="form-control" name="designation_id">

                                <option value="" disabled selected>--Select Designation--</option>
                                @foreach($designations as $designation)
                                    <option value="{{$designation->id}}" {{ $designation->id == old('designation_id')  || $designation->id == $employee->designation->id ? 'selected': '' }} > {{$designation->title}}</option>
                                @endforeach

                            </select>
                            @if ($errors->has('designation_id')) <p style="color:red;">{{ $errors->first('designation_id') }}</p> @endif
                            <br>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Employee Type</label>
                            <select class="form-control" id = "selectEmployee" name="employee_type">
                                <option value="" disabled selected>--Select Employee Type--</option>
                                <option value="employee" selected > Employee </option>
                                <option value="office_head" {{ old('employee_type') == 'office_head' || $employee->is_head == 1 ? 'selected' : ''}}> Office Head </option>
                                <option value="department_head" {{ old('employee_type') == 'department_head' || $employee->is_department_head == 1 ? 'selected' : ''}}> Department Head </option>
                                
                            </select>
                            @if ($errors->has('employee_type')) <p style="color:red;">{{ $errors->first('employee_type') }}</p> @endif
                            <br>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Role</label>
                            <select class="form-control select2" name="role_id[]" multiple >

                                <option value="" disabled>--Select Role--</option>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}" {{ in_array($role->id, $employee->roleIds()) || (is_array(old('role_id')) && in_array($role->id, old('role_id'))) ? 'selected' : ''}} >{{$role->name}}</option>
                                @endforeach

                            </select>
                            @if ($errors->has('role_id')) <p style="color:red;">{{ $errors->first('role_id') }}</p> @endif
                            <br>
                        </div>

                        <div class="form-group" id = "department">
                            <label class="control-label">Department</label>
                            <select class="form-control" id="selectDepartment" name="department_id">

                                <option value="" disabled selected>--Select department--</option>
                                @foreach($departments as $department)
                                    <option value="{{$department->id}}" {{ $department->id == old('department_id') ? 'selected' : '' }}
                                        <?php if(!empty($employee->departmentFirst()))
                                                    if($employee->departmentFirst()->id == $department->id)
                                                        echo 'selected';
                                            
                                        ?>
                                    > {{$department->name}}</option>
                                @endforeach

                                {{-- $department->id == old('department_id') --}}

                            </select>
                            @if ($errors->has('department_id')) <p style="color:red;">{{ $errors->first('department_id') }}</p> @endif
                            <br>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Name</label>
                            <input type="text" class="form-control" name="name" value="{{isset($employee) ? $employee->user->name : old('name')}}">
                            @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="text" class="form-control" name="email" value="{{isset($employee) ? $employee->user->email : old('email')}}">
                            @if ($errors->has('email')) <p style="color:red;">{{ $errors->first('email') }}</p> @endif <br>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Address</label>
                            <input type="text" class="form-control" name="address" value="{{isset($employee) ? $employee->user->address : old('address')}}">
                            @if ($errors->has('address')) <p style="color:red;">{{ $errors->first('address') }}</p> @endif <br>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Mobile</label>
                            <input type="number" class="form-control" name="mobile" value="{{isset($employee) ? $employee->user->mobile : old('mobile')}}">
                            @if ($errors->has('mobile')) <p style="color:red;">{{ $errors->first('mobile') }}</p> @endif <br>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="publish">
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    @include('admin.pages.employee.employee-scripts');
@endpush

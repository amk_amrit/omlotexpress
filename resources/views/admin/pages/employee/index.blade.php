@extends('admin.layouts.app')
@section('title','Employees')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.employees.index')}}">Employees</a></li>
<li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.employees.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id="myTable" class="table table-responsive">
                        <thead>
                            <tr>
                                <th scope="col"> ID </th>
                                <th scope="col"> Name </th>
                                <th scope="col"> Email </th>
                                <th scope="col"> Status </th>
                                <th scope="col"> Address </th>
                                <th scope="col"> Mobile </th>
                                <th scope="col"> Office </th>
                                <th scope="col"> {{tr('office_type')}}</th>
                                <th scope="col"> Designation </th>
                                <th scope="col"> Action </th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)

                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$employee->user->name}}</td>
                                <td>{{$employee->user->email}}</td>
                                <td>{{$employee->user->status}}</td>
                                <td>{{$employee->user->address}}</td>
                                <td>{{$employee->user->mobile}}</td>
                                <td>{{$employee->office->name}}</td>
                                <td> {{ tr($employee->office->office_type) }}</td>
                                <td>
                                    @if($employee->departmentFirst())
                                    {{$employee->departmentFirst()->name. ' '. $employee->designation->title}}
                                    @else
                                    {{ $employee->designation->title }}
                                    @endif

                                </td>
                                <td class="table-flex">
                                    <a href="{{route('admin.employees.show', $employee->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                    <a href="{{route('admin.employees.edit', $employee->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>

                                    <form action="{{ route('admin.employees.destroy', $employee->id)}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        @if($employee->user->status=='unverified')
                                        <button type="submit" class="btn btn-success">Activate</button>
                                        @else
                                        <button type="submit" class="btn btn-danger">Deactivate</button>
                                        @endif

                                    </form>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                    {{ $employees->links() }}
                </div>
            </div>
        </div>
    </div>

</div>



@endsection
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                 <div class="form-group">
            <label class="control-label"  >Name</label>
            <input type="text" class="form-control" name="name" value="{{ isset($transportVehicle) ? $transportVehicle->name : '' }}">
            @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
        </div>

        <div class="form-group">
            <label class="control-label" >Transport Media</label>
            <select class="form-control" name="transport_media_id" >
            <option value="{{isset($transportVehicle) ? $transportVehicle->transport_media_id : ''}}">{{isset($transportVehicle) ? $transportVehicle->transportMedia->name : '--select an option--'}}</option>
                <!-- <option value="" disabled selected >Select Country</option> -->
                @foreach($transportMedias as $transportMedia)
                    <option value="{{$transportMedia->id}}">{{$transportMedia->name}}</option>
                @endforeach

            </select>
            @if ($errors->has('transport_media_id')) <p style="color:red;">{{ $errors->first('transport_media_id') }}</p> @endif <br>
        </div>

        <div class="form-group">
            <label class="control-label"  >Description</label>
           
            <textarea class="form-control"  rows="5" cols="5" name="description">{{ isset($transportVehicle) ? $transportVehicle->description : '' }}</textarea>
            @if ($errors->has('description')) <p style="color:red;">{{ $errors->first('description') }}</p> @endif <br>
        </div>

        <div class="form-group">
            <label class="control-label">Select</label>
            <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{ isset($transportVehicle) && $transportVehicle->status == 1 ? 'checked' : '' }}>
                <label class="custom-control-label" for="defaultChecked">Active</label>
            </div>
        </div>
        <div class="form-group">
            <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{ isset($transportVehicle) && $transportVehicle->status == 0 ? 'checked' : '' }}>
                <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
            </div>
        </div>
        
        <div class="form-group">
        <input type="submit" class="btn btn-success"
        value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
        </div>
            </div>
        </div>
    </div>
</div>
       

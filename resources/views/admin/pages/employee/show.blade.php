@extends('admin.layouts.app')
@section('title','Employees')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.employees.index')}}">Employees</a></li>
<li class="breadcrumb-item active">{{$employee->user->name}}</li>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.employees.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>{{$employee->user->name}}</td>
                        </tr>

                        <tr>
                            <th>Email</th>
                            <td>{{$employee->user->email}}</td>
                        </tr>

                        <tr>
                            <th>Mobile</th>
                            <td>{{$employee->user->mobile}}</td>
                        </tr>

                        <tr>
                            <th>Office Name</th>
                            <td>{{$employee->office->name}}</td>
                        </tr>

                        <tr>
                            <th>Designation</th>
                            <td>{{$employee->designation->title}}</td>
                        </tr>

                        <tr>
                            <th>Employee Type</th>
                            <td>
                                @if($employee->is_head)
                                Head of Office
                                @elseif($employee->is_department_head)
                                Head of {{ $employee->departmentFirst()->title }}
                                @else
                                Employee
                                @endif
                            </td>
                        </tr>


                        <tr>
                            <th>Status</th>
                            <td>
                                @if($employee->status == 1)
                                <span style="color:red"> Activated</span>
                                @else
                                <span style="color:blue"> Deactivated</span>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div> -->
                <div class="country-div-list">
                    <ul>
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$employee->user->name}}</h6>
                        </li>
                        <li>
                            <h4>Email : </h4>
                            <h6>{{$employee->user->email}}</h6>
                        </li>
                        <li>
                            <h4>Mobile : </h4>
                            <h6>{{$employee->user->mobile}}</h6>
                        </li>
                        <li>
                            <h4>Office Name : </h4>
                            <h6>{{$employee->office->name}}</h6>
                        </li>
                        <li>
                            <h4>Designation : </h4>
                            <h6>{{$employee->designation->title}}</h6>
                        </li>
                        <li>
                            <h4>Status : </h4>
                            <h6>
                                @if($employee->status == 1)
                                <span style="color:red"> Activated</span>
                                @else
                                <span style="color:blue"> Deactivated</span>
                                @endif
                            </h6>
                        </li>

                    </ul>
                </div>
                <h6>Roles</h6>
                <ul>
                    @foreach($employee->roles as $role)
                    <li><a href=" {{ route('admin.roles.show', $role->id) }} "> {{ $role->name }} </a></li>
                    @endforeach
                </ul>
                <div class="form-btn">
                    <a href="{{ route('admin.employees.edit', $employee->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('admin.employees.destroy', $employee->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($employee->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>


@endsection
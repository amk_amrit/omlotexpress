@extends('admin.layouts.app')
@section('title','Countries')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.countries.index')}}"> Countries</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
<form action="{{route('admin.countries.update', [$country->id])}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            @include('admin.pages.country.form', [$formMode="Edit"])
</form>
@endsection

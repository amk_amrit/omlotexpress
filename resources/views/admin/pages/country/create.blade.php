@extends('admin.layouts.app')
@section('title','Countries')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.countries.index')}}"> Countries</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
<form action="{{route('admin.countries.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.pages.country.form', [$formMode="Publish"])
</form>
@endsection

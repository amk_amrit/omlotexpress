<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                
                <div class="form-group">
                    <label class="control-label" >ISO</label>
                    <input type="text" class="form-control" name="iso" value="{{ isset($country) ? $country->iso : old('iso') }}">
                    @if ($errors->has('iso')) <p style="color:red;">{{ $errors->first('iso') }}</p> @endif <br>
                </div>
                <div class="form-group">
                    <label class="control-label"  >Name</label>
                    <input type="text" class="form-control" name="name" value="{{ isset($country) ? $country->name : old('name') }}">
                    @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                </div>
                <div class="form-group">
                    <label class="control-label"  >Nice Name</label>
                    <input type="text" class="form-control" name="nicename" value="{{ isset($country) ? $country->nicename : old('nicename') }}">
                    @if ($errors->has('nicename')) <p style="color:red;">{{ $errors->first('nicename') }}</p> @endif <br>
                </div>
                <div class="form-group">
                    <label class="control-label"  >IOS3</label>
                    <input type="text" class="form-control" name="iso3" value="{{ isset($country) ? $country->iso3 : old('iso3') }}">
                    @if ($errors->has('iso3')) <p style="color:red;">{{ $errors->first('iso3') }}</p> @endif <br>
                </div>
                <div class="form-group">
                    <label class="control-label"  >Number Code</label>
                    <input type="text" class="form-control" name="numcode" value="{{ isset($country) ? $country->numcode : old('numcode') }}">
                    @if ($errors->has('numcode')) <p style="color:red;">{{ $errors->first('numcode') }}</p> @endif <br>
                </div>
        
                <div class="form-group">
                    <label class="control-label"  >Currency</label>
                    <input type="text" class="form-control" name="currency" value="{{ isset($country) ? $country->currency : old('currency') }}">
                    @if ($errors->has('currency')) <p style="color:red;">{{ $errors->first('currency') }}</p> @endif <br>
                </div>
        
                 <div class="form-group">
                    <label class="control-label"  >Currency Symbol</label>
                    <input type="text" class="form-control" name="currency_symbol" value="{{ isset($country) ? $country->currency_symbol : old('currency_symbol') }}">
                    @if ($errors->has('currency_symbol')) <p style="color:red;">{{ $errors->first('currency_symbol') }}</p> @endif <br>
                </div>
        
                <div class="form-group">
                    <label  class="control-label" >Phone Code</label>
                    <input type="text" class="form-control" name="phonecode" value="{{ isset($country) ? $country->phonecode : old('phonecode') }}">
                    @if ($errors->has('phonecode')) <p style="color:red;">{{ $errors->first('phonecode') }}</p> @endif <br>
                </div>
                <div class="form-group">
                <input type="submit" class="btn btn-success"
                value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
                </div>

            </div>
        </div>
    </div>
</div>
        

@extends('admin.layouts.app')
@section('title','Countries')
@section('content')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.countries.index')}}"> Countries</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.countries.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table">
                        <thead>
                            <tr>
                                <th scope="col">S.N</th>
                                <th scope="col">ISO</th>
                                <th scope="col">Name</th>
                                <th scope="col">Phone Code</th>
                                <th scope="col">Currency</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($countries as $country)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$country->iso}}</td>
                                    <td>{{$country->name}}</td>
                                    <td>+{{$country->phonecode}}</td>
                                    <td>{{$country->currency}} ({{ $country->currency_symbol }})</td>
                                    
                                    <td class="table-flex">
                                        <a href="{{route('admin.countries.show', $country->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                        <a href="{{route('admin.countries.edit', $country->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>

                                        <form action="{{ route('admin.countries.destroy', $country->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            @if($country->status==0)
                                                <button type="submit" class="btn btn-success">Activate</button>
                                            @else
                                                <button type="submit" class="btn btn-danger">Deactivate</button>
                                            @endif

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>   
</div>
   
@endsection

@extends('admin.layouts.app')
@section('title','Countries')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.countries.index')}}"> Countries</a></li>
<li class="breadcrumb-item active">{{$country->name}}</li>
@endsection
<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.countries.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>{{$country->name}}</td>
                        </tr>
                        <tr>
                            <th>Nice Name</th>
                            <td>{{$country->nicename}}</td>
                        </tr>
                        <tr>
                            <th>IOS</th>
                            <td>{{$country->iso}}</td>
                        </tr>
                        <tr>
                            <th>IOS3</th>
                            <td>{{$country->iso3}}</td>
                        </tr>
                        <tr>
                            <th>Number Code</th>
                            <td>{{$country->numcode}}</td>
                        </tr>
                        <tr>
                            <th>Phone Code</th>
                            <td>+{{$country->phonecode}}</td>
                        </tr>
                        <tr>
                            <th>Currency</th>
                            <td>{{$country->currency}}({{ $country->currency_symbol }})</td>
                        </tr>
                    </table>
                </div> -->
                
                <div class="country-div-list">
                    <ul>
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$country->name}}</h6>
                        </li>
                        <li>
                            <h4>Nice Name : </h4>
                            <h6>{{$country->nicename}}</h6>
                        </li>
                        <li>
                            <h4>IOS : </h4>
                            <h6>{{$country->iso}}</h6>
                        </li>
                        <li>
                            <h4>IOS3 : </h4>
                            <h6>{{$country->iso3}}</h6>
                        </li>
                        <li>
                            <h4>Number Code : </h4>
                            <h6>{{$country->numcode}}</h6>
                        </li>
                        <li>
                            <h4>Phone Code : </h4>
                            <h6>+{{$country->phonecode}}</h6>
                        </li>
                        <li>
                            <h4>Currency : </h4>
                            <h6>{{$country->currency}}({{ $country->currency_symbol }})</h6>
                        </li> 
                    </ul>
                </div>
                <div class="form-btn">
                    <a href="{{ route('admin.countries.edit', $country->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('admin.countries.destroy', $country->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($country->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
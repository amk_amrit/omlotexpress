@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.package.types.index')}}"> Package Types</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
<form action="{{route('admin.package.types.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.pages.package-type.form', [$formMode="Publish"])
</form>
@endsection

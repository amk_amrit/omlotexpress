@extends('admin.layouts.app')
@section('title','Dashboard')

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.package.types.index')}}"> Package Types</a></li>
<li class="breadcrumb-item active">{{$packageTypes->type_name}}</li>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.package.types.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive mt-4">
                    <table class="table">
                        <tr>
                            <th>ID:</th>
                            <td>{{$packageTypes->id}}</td>
                        </tr>
                        <tr>
                            <th>Package Name</th>
                            <td>{{$packageTypes->type_name}}</td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td>{{$packageTypes->description}}</td>
                        </tr>
                    </table>
                </div> -->
                <div class="country-div-list">
                    <ul class="weight-gain-ul">
                        <li>
                            <h4>ID : </h4>
                            <h6>{{$packageTypes->id}}</h6>
                        </li>
                        <li>
                            <h4>Package Name : </h4>
                            <h6>{{$packageTypes->type_name}}</h6>
                        </li>
                        <li>
                            <h4>Description : </h4>
                            <h6> {{$packageTypes->description}} </h6>
                        </li> 
                    </ul>
                </div>
                <div class="form-btn">
                    <a href="{{ route('admin.package.types.edit', $packageTypes->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('admin.package.types.destroy', $packageTypes->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($packageTypes->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.package.types.index')}}"> Package Types</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
<form action="{{route('admin.package.types.update', [$packageType->id])}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            @include('admin.pages.package-type.form', [$formMode="Edit"])
</form>
@endsection

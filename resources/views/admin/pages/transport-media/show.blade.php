@extends('admin.layouts.app')
@section('title','Transport Medium')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.medias.index')}}">Transport Medium</a></li>
<li class="breadcrumb-item active">{{$transportMedia->name}}</li>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.medias.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <td>{{$transportMedia->name}}</td>
                            </tr>
                            <tr>
                                <th>Slug</th>
                                <td>{{$transportMedia->slug}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                @if($transportMedia->status == 1)
                                    <span> Activated</span>
                                @else
                                    <span> Deactivated</span>
                                @endif
                                </td>
                            </tr>
                            
                        </table>
                    </div> -->

                <div class="country-div-list">
                    <ul>
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$transportMedia->name}}</h6>
                        </li>
                        <li>
                            <h4>Slug : </h4>
                            <h6>{{$transportMedia->slug}}</h6>
                        </li>
                        <li>
                            <h4>Status : </h4>
                            <h6>
                                @if($transportMedia->status == 1)
                                <span> Activated</span>
                                @else
                                <span> Deactivated</span>
                                @endif
                            </h6>
                        </li>
                    </ul>
                </div>


                <div class="form-btn">
                    <a href="{{ route('admin.medias.edit', $transportMedia->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('admin.medias.destroy', $transportMedia->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($transportMedia->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@extends('admin.layouts.app')
@section('title','Transport Medium')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.medias.index')}}">Transport Medium</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')

<form action="{{route('admin.medias.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.pages.transport-media.form', [$formMode="Publish"])
</form>
@endsection

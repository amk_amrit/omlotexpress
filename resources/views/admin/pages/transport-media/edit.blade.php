@extends('admin.layouts.app')
@section('title','Transport Medium')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.medias.index')}}">Transport Medium</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
<form action="{{route('admin.medias.update', $transportMedia->id)}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            @include('admin.pages.transport-media.form', [$formMode="Edit"])
</form>
@endsection

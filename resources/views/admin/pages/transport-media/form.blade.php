<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body"> 
                <div class="form-group">
                    <label class="control-label"  >Name</label>
                    <input type="text" class="form-control" name="name" value="{{ isset($transportMedia) ? $transportMedia->name : old('name') }}">
                    @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                </div>
        
                <div class="form-group">
                    <label class="control-label">Select</label>
                    <div class="custom-control custom-radio p-l-0">
                        <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{ isset($transportMedia) && $transportMedia->status == 1 ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultChecked">Active</label>
                    </div>
                 
                    <div class="custom-control custom-radio p-l-0">
                        <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{ isset($transportMedia) && $transportMedia->status == 0 ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                    </div>
                </div>
        
                <div class="form-group">
                <input type="submit" class="btn btn-success"
                value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
                </div>

            </div>
        </div>
    </div>
</div>
        

@extends('admin.layouts.app')
@section('title','Roles')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.offices.index')}}">{{auth()->user()->employee->office->name}}</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.roles.index')}}">Roles</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.roles.create')}}" class="btn btn-success  m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                <table id="myTable" class="table">
                        <thead>
                            <tr>
                                <th scope="col"> S.N </th>
                                <th scope="col"> Name </th>
                                <th scope="col"> Description </th>
                                <th scope="col"> Updated At </th>
                                <th scope="col"> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->description}}</td>
                                    <td>{{ $role->updated_at}}</td>
                                    <td class="table-flex">
                                        <a href="{{route('admin.roles.show', $role->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
                                        <a href="#" class="btn btn-danger m-r-10"><i class="fas fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
   
</div>
    

    
@endsection

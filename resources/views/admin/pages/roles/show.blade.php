@extends('admin.layouts.app')
@section('title','Roles')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('admin') }}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.offices.index')}}">{{auth()->user()->employee->office->name}}</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.departments.index')}}">Roles</a></li>
<li class="breadcrumb-item active">{{$role->name}}</li>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.roles.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>{{$role->name}}</td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td>{{$role->description}}</td>
                        </tr>
                    </table>

                    <h4>Permissions</h4>
                    <ul>
                        @foreach ($role->permissions as $permission)
                        <li> {{ $permission->display_name }} </li>
                        @endforeach
                    </ul>

                </div> -->

                <div class="country-div-list">
                    <ul>
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$role->name}}</h6>
                        </li>
                        <li>
                            <h4>Description : </h4>
                            <h6>{{$role->description}}</h6>
                        </li>
                        <li class="d-flex align-items-start">
                            <h4>Permissions : </h4>
                            <ul>
                                @foreach ($role->permissions as $permission)
                                <li class=""> {{ $permission->display_name }} </li>
                                @endforeach
                            </ul>
                        </li> 
                    </ul>
                </div>

                <a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-warning">Edit</a>

            </div>
        </div>
    </div>

</div>


@endsection
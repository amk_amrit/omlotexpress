@extends('admin.layouts.app')
@section('title','Roles')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.offices.index')}}">{{authEmployee()->office->name}}</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.roles.index')}}">Roles</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('admin.roles.store', authEmployee()->office->id ) }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label"  >Name</label>
                        <input type="text" class="form-control" name="name" value="{{ isset($role) ? $role->name : old('name') }}">
                        @if ($errors->has('name')) <p style="color:red;">{{ $errors->first('name') }}</p> @endif <br>
                    </div>
                
                    <div class="form-group">
                        <label class="control-label" >Description</label>
                        <textarea class="form-control" rows="1" cols="10" name="description">{{ isset($role) ? $role->description : old('description') }}</textarea>
                        @if ($errors->has('description')) <p style="color:red;">{{ $errors->first('description') }}</p> @endif <br>
                    </div>
                    <div align="right">
                        <input type="checkbox" id="checkAll" >
                        <label for="checkAll" ><strong> Check ALL Permissions </strong> </label>
                    </div>
                    <hr>

                    <div class="form-group">
                        
                        @foreach ($permissionGroups as $permissionGroup => $permissions)
                            <input type="checkbox"  id="{{ $permissionGroup }}" class="filled-in all-permissions" >
                            <label for="{{ $permissionGroup }}"><strong>{{ ucwords($permissionGroup) }}</strong> </label>
                            <div class="row">
                                
                                @foreach ($permissions as $permission)
                                   
                                    <div class="col-2 permission-col">
                                        <input type="checkbox" id="{{ $permission->name}}" name = "permission_id[]" value="{{ $permission->id }}" 
                                        class=" {{ $permissionGroup }} all-permissions" 
                                        {{ is_array(old('permission_id')) && in_array($permission->id, old('permission_id')) ? 'checked' : '' }} />
                                        <label for = "{{ $permission->name}}">{{ $permission->display_name}}</label>
                                    </div>

                                @endforeach
                            </div>
                            <hr>
                        @endforeach

                        @if ($errors->has('permission_id')) <p style="color:red;">{{ $errors->first('permission_id') }}</p> @endif  
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Publish">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    @include('admin.pages.roles.roles-script')
@endpush
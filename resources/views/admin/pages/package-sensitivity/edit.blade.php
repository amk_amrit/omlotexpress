@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.package.sensitivities.index')}}"> Package Sensitivities</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
<form action="{{route('admin.package.sensitivities.update', [$packageSensitivites->id])}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            @include('admin.pages.package-sensitivity.form', [$formMode="Edit"])
</form>
@endsection

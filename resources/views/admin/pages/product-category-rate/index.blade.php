@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.rates.index')}}"> Product Category Rates</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection
@include('error-messages.message')
<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.rates.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-4">
                    <table id= "myTable" class="table">
        
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Country</th>
                                <th scope="col">Product Category</th>
                                <th scope="col">Rates</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categoryRate as $categoryRates)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$categoryRates->Country->name}}</td>
                                    <td>{{$categoryRates->ProductCategory->name}}</td>
                                    <td>
                                    <a href="{{ route('admin.getProductCategoryRateView', $categoryRates->product_category_id )}}" class="btn btn-info">View</a>
                                    <a href="{{ route('admin.getProductCategoryRate', $categoryRates->product_category_id) }}" class="btn btn-warning">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    </div>
@endsection

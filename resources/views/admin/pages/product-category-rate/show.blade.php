@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.rates.index')}}"> Product Category rates</a></li>
<li class="breadcrumb-item active">{{$categoryRates[0]->Country->name}}</li>
@endsection

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.rates.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Country</th>
                                <td>{{$categoryRates[0]->Country->name}}</td>
                            </tr>
                            <tr>
                                <th>Product Category</th>
                                <td>{{$categoryRates[0]->ProductCategory->name}}</td>
                            </tr>
                            @foreach($categoryRates as $categoryRate)
                            <tr>
                                <th>Maxmium Weight</th>
                                <td>{{$categoryRate->max_weight}}</td>
                            </tr>
                            <tr>
                                <th>Minimum Weight</th>
                                <td>{{$categoryRate->min_weight}}</td>
                            </tr>
                            <tr>
                                <th>Rate</th>
                                <td>{{$categoryRate->rate}}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div> -->
                <div class="country-div-list">
                    <ul class="weight-gain-ul">
                        <li>
                            <h4>Country : </h4>
                            <h6>{{$categoryRates[0]->Country->name}}</h6>
                        </li>
                        <li>
                            <h4>Product Category : </h4>
                            <h6>{{$categoryRates[0]->ProductCategory->name}}</h6>
                        </li>
                        @foreach($categoryRates as $categoryRate)
                        <li>
                            <h4>Maxmium Weight : </h4>
                            <h6>{{$categoryRate->max_weight}}</h6>
                        </li>
                        <li>
                            <h4>Minimum Weight : </h4>
                            <h6>{{$categoryRate->min_weight}}</h6>
                        </li>
                        <li>
                            <h4>Rate : </h4>
                            <h6>{{$categoryRate->rate}}</h6>
                        </li>
                        @endforeach 
                    </ul>
                </div>
                <div class="form-btn">
                    <a href="{{ route('admin.rates.edit', $categoryRate->id) }}" class="btn btn-warning">Edit</a>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
</div>

@endsection
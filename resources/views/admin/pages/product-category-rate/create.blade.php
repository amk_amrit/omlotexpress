@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.rates.index')}}"> Product Category Rates</a></li>
    <li class="breadcrumb-item active">Create</li>
   
@endsection
@include('error-messages.message')
<form action="{{route('admin.rates.store')}}" method="POST"  enctype="multipart/form-data" id="add_name" >
            {{ csrf_field() }}
            @include('admin.pages.product-category-rate.form', [$formMode="Publish"])
</form>
@endsection
@push('scripts')
    @include('admin.pages.product-category-rate.categoryScript');
@endpush

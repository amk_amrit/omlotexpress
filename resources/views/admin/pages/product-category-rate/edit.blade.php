@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.rates.index')}}"> Product Category Rates</a></li>
    <li class="breadcrumb-item active">Edit</li>
    @include('error-messages.message')
@endsection
<form action="{{route('admin.rates.update', $categoryRates[0]->Country->id)}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            @include('admin.pages.product-category-rate.editForm', [$formMode="Edit"])
</form>
@endsection
@push('scripts')
    @include('admin.pages.product-category-rate.categoryScript');
@endpush

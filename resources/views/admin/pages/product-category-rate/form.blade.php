
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label class="control-label" >Category</label>
                        <select class="form-control" name="product_category_id" >
                        <option value="">--select an option--</option>
                            @foreach($category as $categorys)
                                <option value="{{$categorys->id}}"
                                {{$categorys->id == old('product_category_id') ? 'selected' : ''}}
                                    <?php if(isset($categoryRate) && $categoryRate->ProductCategory->id == $categorys->id ) echo 'selected'; ?>
                                    >
                                    {{$categorys->name}}
                                </option>
                                
                            @endforeach
            
                        </select>
                    </div>
                    <table class="table" id="dynamic_field">
                    <tr>
                        <td><input type="text" class="form-control min-weight" placeholder="Enter  minimum weight"  name="min_weight[]"  ></td>
                        <td><input type="text" class="form-control max-weight" placeholder="Enter  maxmium weight"  name="max_weight[]" ></td>
                        <td><input type="text" class="form-control rates" placeholder="Enter  rate"  name="rate[]" ></td>
                    </tr>
                    </table>
                    
                    <button style="margin-left: 350px;" type="button" name="add" id="add" class="btn btn-success">Add More</button>
                    
                    <div class="form-group">
                    <input type="submit" id="submit" name="submit" class="btn btn-success"
                    value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
       

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body"> 
                <div class="form-group">
                    <label class="control-label" >Category</label>
                    <select class="form-control" name="product_category_id" >
                    <option value="">--select an option--</option>
                        @foreach($category as $categorys)
                        <option value="{{$categorys->id}}"<?php if(isset($categoryRates[0]) && $categoryRates[0]->ProductCategory->id == $categorys->id ) echo 'selected'; ?>
                    >{{$categorys->name}}</option>
                        @endforeach
        
                    </select>
                </div>
                @foreach($categoryRates as $categoryRate)
                <table class="table" >
                <tr> 
                    <td class="d-none"><input type="hidden" class="form-control min-weight" value="{{$categoryRate->id}}"  name="deleteId[]"  ></td> 
                    <td class="pad-cus-pro-cat"><input type="text" class="form-control min-weight" value="{{$categoryRate->min_weight}}" placeholder="Enter  minimum weight"  name="min_weight[]"  ></td>
                    <td class="pad-cus-pro-cat"><input type="text" class="form-control max-weight" value="{{$categoryRate->max_weight}}" placeholder="Enter  maxmium weight"  name="max_weight[]" ></td>
                    <td class="pad-cus-pro-cat"><input type="text" class="form-control rates" value="{{$categoryRate->rate}}" placeholder="Enter  rate"  name="rate[]" ></td>
                </tr>
                </table>
                @endforeach
                <table class="table" id="dynamic_field">
                <tr>
                    
                </tr>
                </table>
                
                <button type="button" name="add" id="add" class="btn btn-success btn-center"><i class="fa fa-plus"></i>   Add More</button>
                
                <div class="form-group">
                    <input type="submit" id="submit" name="submit" class="btn btn-success" value="Update">
                </div>
            </div>
        </div>
    </div>
</div>
        

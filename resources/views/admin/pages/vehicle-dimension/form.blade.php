<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="form-group">
                    <label class="control-label"  >Title</label>
                    <input type="text" class="form-control" name="title" value="{{ isset($vehicleDimension) ? $vehicleDimension->title : old('title') }}">
                    @if ($errors->has('title')) <p style="color:red;">{{ $errors->first('title') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label" >Vehicle</label>
                    <select class="form-control" name="vehicle_id" >
                    <option value="">--select an option--</option>
                        @foreach($vehicles as $vehicle)
                            <option value="{{$vehicle->id}}"
                                {{$vehicle->id == old('vehicle_id') ? 'selected' : ''}}
                                <?php if(isset($vehicleDimension) && $vehicle->id == $vehicleDimension->vehicle_id ) echo 'selected'; ?>
                                >
                                {{$vehicle->name}}
                            </option>
                        @endforeach
                
                    </select>
                    @if ($errors->has('vehicle_id')) <p style="color:red;">{{ $errors->first('vehicle_id') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label"  >Dimension</label>
                    <input type="text" class="form-control" name="dimension" placeholder ="Length m * Width m * Height m" value="{{ isset($vehicleDimension) ? $vehicleDimension->dimension : old('dimension') }}">
                    @if ($errors->has('dimension')) <p style="color:red;">{{ $errors->first('dimension') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label"  >Capacity</label>
                    <input type="text" class="form-control" name="capacity" value="{{ isset($vehicleDimension) ? $vehicleDimension->capacity :  old('capacity')}}">
                    @if ($errors->has('capacity')) <p style="color:red;">{{ $errors->first('capacity') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label"  >Description</label>
                    <textarea class="form-control" row ="5" col= "5" name="description">{{ isset($vehicleDimension) ? $vehicleDimension->description :  old('description') }}</textarea>
                    @if ($errors->has('description')) <p style="color:red;">{{ $errors->first('description') }}</p> @endif <br>
                </div>
                
                <div class="form-group">
                    <label class="control-label">Select</label>
                    <div class="custom-control custom-radio p-l-0">
                        <input type="radio" class="custom-control-input" id="defaultChecked" name="status" value="1" {{ isset($vehicleDimension) && $vehicleDimension->status == 1 ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultChecked">Active</label>
                    </div> 
                    <div class="custom-control custom-radio p-l-0">
                        <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{ isset($vehicleDimension) && $vehicleDimension->status == 0 ? 'checked' : '' }}>
                        <label class="custom-control-label" for="defaultUnchecked">Deactive</label>
                    </div>
                </div>
                @if ($errors->has('status')) <p style="color:red;">{{ $errors->first('status') }}</p> @endif <br>
                
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="{{ $formMode == 'Publish' ? 'Publish' : 'Update'}}">
                </div>
                
            </div>
        </div>
    </div>
</div>



@extends('admin.layouts.app')
@section('title','Vehicle Dimension')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicles.dimensions.index')}}">Vehicle Dimensions</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')
<form action="{{route('admin.vehicles.dimensions.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.pages.vehicle-dimension.form', [$formMode="Publish"])
</form>
@endsection

@extends('admin.layouts.app')
@section('title','Vehicle Dimension')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicles.dimensions.index')}}">Vehicle Dimension</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
<form action="{{route('admin.vehicles.dimensions.update', $vehicleDimension->id)}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            @include('admin.pages.vehicle-dimension.form', [$formMode="Edit"])
</form>
@endsection

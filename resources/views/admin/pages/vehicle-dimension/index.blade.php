@extends('admin.layouts.app')
@section('title','Vehicle Dimension')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.vehicles.dimensions.index')}}">Vehicle Dimension</a></li>
    <li class="breadcrumb-item active">View</li>
@endsection

@section('content')

@include('error-messages.message')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.vehicles.dimensions.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table">
                        <thead>
                            <tr>
                                <th scope="col"> S.N </th>
                                <th scope="col"> Title </th>
                                <th scope="col"> Vehicle </th>
                                <th scope="col"> Dimension </th>
                                <th scope="col"> Capacity </th>
                                <th scope="col"> Description </th>
                                <th scope="col"> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($vehicleDimensions as $vehicleDimension)
                            
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$vehicleDimension->title}}</td>
                                    <td>{{$vehicleDimension->vehicle->name}}</td>
                                    <td>{{$vehicleDimension->dimension}}</td>
                                    <td>{{$vehicleDimension->capacity}}</td>
                                    <td>{{$vehicleDimension->description}}</td>
                                    <td class="table-flex">
                                        <a href="{{route('admin.vehicles.dimensions.show', $vehicleDimension->id) }}" class="btn btn-info m-r-10"><i class="fa fa-eye"></i></a>
                                        <a href="{{route('admin.vehicles.dimensions.edit', $vehicleDimension->id)}}" class="btn btn-warning m-r-10"><i class="far fa-edit"></i></a>
    
                                        <form action="{{ route('admin.vehicles.dimensions.destroy', $vehicleDimension->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            @if($vehicleDimension->status==0)
                                                <button type="submit" class="btn btn-success">Activate</button>
                                            @else
                                                <button type="submit" class="btn btn-danger">Deactivate</button>
                                            @endif
    
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>   
</div>
@endsection

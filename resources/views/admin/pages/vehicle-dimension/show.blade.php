@extends('admin.layouts.app')
@section('title','Vehicle Dimension')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.vehicles.dimensions.index')}}">Vehicle Dimension</a></li>
<li class="breadcrumb-item active">{{$vehicleDimension->name}}</li>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 text-right">
        <a href="{{route('admin.vehicles.dimensions.create')}}" class="btn btn-success m-b-10">Create</a>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- <div class="table-responsive">
                        <table class="table"> 
                            <tr>
                                <th>Name</th>
                                <td>{{$vehicleDimension->title}}</td>
                            </tr>
    
                            <tr>
                                <th>Vehicle</th>
                                <td>{{$vehicleDimension->vehicle->name}}</td>
                            </tr>
        
                            <tr>
                                <th>Dimension</th>
                                <td>{{$vehicleDimension->dimension}}</td>
                            </tr>
        
                            <tr>
                                <th>Capacity</th>
                                <td>{{$vehicleDimension->capacity}}</td>
                            </tr>
        
                            <tr>
                                <th>Description</th>
                                <td>{{$vehicleDimension->description}}</td>
                            </tr>
        
                            <tr>
                                <th>Status</th>
                                <td>
                                @if($vehicleDimension->status == 1)
                                    <span style ="color:red"> Activated</span>
                                @else
                                    <span style ="color:blue"> Deactivated</span>
                                @endif
                                </td>
                            </tr>
                        </table>
                    </div> -->
                <div class="country-div-list">
                    <ul class="weight-gain-ul">
                        <li>
                            <h4>Name : </h4>
                            <h6>{{$vehicleDimension->title}}</h6>
                        </li>
                        <li>
                            <h4>Vehicle : </h4>
                            <h6>{{$vehicleDimension->vehicle->name}}</h6>
                        </li>
                        <li>
                            <h4>Dimension : </h4>
                            <h6>{{$vehicleDimension->dimension}}</h6>
                        </li>
                        <li>
                            <h4>Capacity : </h4>
                            <h6>{{$vehicleDimension->capacity}}</h6>
                        </li>
                        <li>
                            <h4>Description : </h4>
                            <h6>{{$vehicleDimension->description}}</h6>
                        </li>
                        <li>
                            <h4>Status : </h4>
                            <h6>
                                @if($transportVehicle->status == 1)
                                <span> Activated</span>
                                @else
                                <span> Deactivated</span>
                                @endif
                            </h6>
                        </li>
                    </ul>
                </div>
                <div class="form-btn">
                    <a href="{{ route('admin.vehicles.dimensions.edit', $vehicleDimension->id) }}" class="btn btn-warning">Edit</a>
                    <form action="{{ route('admin.vehicles.dimensions.destroy', $vehicleDimension->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                        @if($vehicleDimension->status==0)
                        <button type="submit" class="btn btn-success">Activate</button>
                        @else
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
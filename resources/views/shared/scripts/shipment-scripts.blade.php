<script>
    $(document).ready(function () {

        var hasTerminalPoints =false;

        let originToTerminalDistance;
        let terminalToDestinationDistance;
        let origin={lat: null, lng: null};;
        let destination={lat: null, lng: null};;
        let terminalLat;
        let terminalLong;
        let terminalLatLong={lat:null,lng:null};

        let originToDestinationDistance;

        function convertToFloatNumber(number) {
            let newNumber= parseFloat(number.split(',').join(''));
            return newNumber;
        }

        /* countries and its terminals */
        function updateOptionsOfTerminalPoints(terminalPoints) {
            let terminalPoint = $("#terminal_point");
            terminalPoint.empty();

            let disabledOption= "<option value='' selected disabled>Select Terminal Points</option>";

            terminalPoint.append(disabledOption);

            if(terminalPoints.length >= 1){
                hasTerminalPoints=true;

                terminalPoints.forEach(function (option) {
                    terminalPoint.append("<option value='" + option['id'] + "' data-lat='"+option['latitude']+"' data-long='"+option['longitude']+"'>" + option['name'] + "</option>");
                });
            }
            else{
                hasTerminalPoints=false;
                terminalPoint.append("<option value=''>No Terminal Points</option>");
                // Display a warning toast, with title
                toastr.warning('The selected country has no terminal points.','No terminal Points');

            }

        }

        function getCountryTerminalByIso(countryIsoRoute){
            //let gd = {{route('country.iso')}}+"/"+countryIso;
            //countryIsoRoute = countryIsoRoute.replace(':iso', countryIso);

            $.ajax(
                {
                    type: 'GET',
                    url: countryIsoRoute,
                    datatype: "json",
                }).done(function (data) {
                updateOptionsOfTerminalPoints(data);
            }).fail(function (customError) {
                //console.log("data: " + data.responseJSON);
                // console.log( 'error',customError.responseJSON.message);
                // Display an error toast, with a title
                toastr.error(customError.responseJSON.message);
            });
        }

        $('#destination_country').on('change',function () {
            let countryIso=$("option:selected" ,this).data('url');
            //let countryIso=$("#destination_country option:selected" ).val();
            if(countryIso){
                getCountryTerminalByIso(countryIso);
            }
        });


        /* media and its vehicles */
        function updateOptionsOfVehicle(data) {
            let vehicles = $("#vehicle_type");
            vehicles.empty();

            let disabledOption= "<option value='' selected disabled>Choose Vehicle</option>";

            vehicles.append(disabledOption);

            if(data.length > 0){
                data.forEach(function (option) {
                    //vehicles.append("<option value='" + option['id'] + "'>" + option['name'] + "</option>");
                    //vehicles.append("<optgroup label='"+option['name']+ "'>"+"</optgroup>");
                    var optGroup ="<optgroup label="+option['name']+ ">";
                    //vehicles.append(optGroup);


                    option['vehicle_dimensions'].forEach(function (dimension) {
                        var optGroupOptions =("<option value="+dimension['slug']+ ">"+dimension['title']+"</option>");
                        //optGroup.concat(optGroupOptions);
                        optGroup +=optGroupOptions;
                    });

                    optGroup +="</optgroup>";

                    vehicles.append(optGroup);
                });
            }
            else{
                vehicles.append("<option value=''>No Vehicles For The Media</option>");
                // Display a warning toast, with title
                toastr.warning('The selected media has no vehicles.','No vehicles')

            }

        }

        function getMediaVehicles(mediaUrl){
            $.ajax(
                {
                    type: 'GET',
                    url: mediaUrl,
                    datatype: "json",
                }).done(function (data) {
                updateOptionsOfVehicle(data);
            }).fail(function (customError) {
                //console.log("data: " + data.responseJSON);
                // console.log( 'error',customError.responseJSON.message);
                // Display an error toast, with a title
                toastr.error(customError.responseJSON.message);
            });
        }

        $('#transport_media').on('change',function () {
            let mediaUrl=$("option:selected" ,this).data('url');
            if(mediaUrl){
                getMediaVehicles(mediaUrl);
            }
        });

        function calculateDistance(origin,destination){

            if(Object.values(origin).some(checkEmptyProperties) && Object.values(destination).some(checkEmptyProperties))
            {
                return new Promise(function(resolve, reject) {
                    var service = new google.maps.DistanceMatrixService;

                    service.getDistanceMatrix({
                        origins: [origin],
                        destinations: [destination],
                        travelMode: 'DRIVING',
                        unitSystem: google.maps.UnitSystem.METRIC,//in KM
                        avoidHighways: false,
                        avoidTolls: false
                    }, function(response, status) {
                        if (status !== 'OK') {
                            alert('Error was: ' + status);
                            reject(status);
                        } else {
                            var originList = response.originAddresses;
                            var destinationList = response.destinationAddresses;

                            for (var i = 0; i < originList.length; i++) {
                                var results = response.rows[i].elements;
                                for (var j = 0; j < results.length; j++) {
                                    var element = results[j];
                                    var distance = element.distance.text;
                                    var duration = element.duration.text;
                                    var from = originList[i];
                                    var to = destinationList[j];

                                    //console.log(from,'to',to,":",distance);
                                    calculatedDistance = convertToFloatNumber(distance);

                                }
                            }
                            resolve(calculatedDistance);
                        }
                    });

                });
            }

            else{
                return new Promise(function(resolve, reject) {
                    reject('Please Fill Both Addresses.');
                });

            }


        }

        function checkEmptyProperties(obj){
            if (obj === null || obj === "" || obj===undefined ){
                return false;
            }

            return  true;
        }

        $('#calculate_btn').on('click',function (e) {
            e.preventDefault();
            console.log(hasTerminalPoints);
            if(hasTerminalPoints){

                origin = {lat: placeOriginLatitude, lng: placeOriginLongitude};
                destination = {lat: placeDestinationLatitude, lng: placeDestinationLongitude};

                if(Object.values(origin).some(checkEmptyProperties)
                    && Object.values(destination).some(checkEmptyProperties)
                    && Object.values(terminalLatLong).some(checkEmptyProperties)
                ){
                    originToTerminalDistance= calculateDistance(origin,destination);
                    terminalToDestinationDistance = calculateDistance(terminalLatLong,destination);

                    const distances = Promise.all([
                        originToTerminalDistance,
                        terminalToDestinationDistance,
                    ])
                        .then(function([originToTerminalDistanceArg, terminalToDestinationDistanceArg]) {

                            let totalDistance = originToTerminalDistanceArg+terminalToDestinationDistanceArg;
                            console.log(`origin to terminal ${originToTerminalDistanceArg}! 🍔`)
                            console.log(`terminal to desti ${terminalToDestinationDistanceArg}! 🍟`)
                            console.log('Total Distance',totalDistance);

                        });
                }
                else{
                    toastr.error('Please Fill Both Addresses & select terminal point');
                }

            }
            else{

                origin = {lat: placeOriginLatitude, lng: placeOriginLongitude};
                destination = {lat: placeDestinationLatitude, lng: placeDestinationLongitude};

                calculateDistance(origin,destination).then(function(distance) {
                    console.log('Total Distance',distance);
                }).catch(function (message) {
                    toastr.error(message);
                });
            }

        });

        $('#terminal_point').on('change',function () {

            // origin = {lat: placeOriginLatitude, lng: placeOriginLongitude};
            // destination = {lat: placeDestinationLatitude, lng: placeDestinationLongitude};

            terminalLat=$("option:selected" ,this).data('lat');
            terminalLong=$("option:selected" ,this).data('long');
            terminalLatLong ={lat:Number(terminalLat),lng:Number(terminalLong)};

            // originToTerminalDistance= calculateDistance(origin,terminalLatLong);
            // terminalToDestinationDistance = calculateDistance(terminalLatLong,destination);


        });

    });
</script>

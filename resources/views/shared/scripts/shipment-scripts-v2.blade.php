<script>
    $(document).ready(function () {

        let selectOriginCountry = $("#origin_country");
        let selectDestinationCountry =$("#destination_country");
        let isOriginCountrySelected =false;
        let isDestinationCountrySelected =false;
        let hasTerminalPoints =false;

        let originToTerminalDistance;
        let terminalToDestinationDistance;
        let origin={lat: null, lng: null};
        let destination={lat: null, lng: null};
        let terminalLat;
        let terminalLong;
        let terminalLatLong={lat:null,lng:null};

        let originToDestinationDistance;
        let oldQuotationFormData={};


        function convertToFloatNumber(number) {
            let newNumber= parseFloat(number.split(',').join(''));
            return newNumber;
        }

        /* countries and its terminals */
        function updateOptionsOfTerminalPoints(terminalPoints) {
            let terminalPoint = $("#terminal_point");
            terminalPoint.empty();

            let disabledOption= "<option value='' selected disabled>Select Terminal Points</option>";

            terminalPoint.append(disabledOption);

            if(terminalPoints.length >= 1){
                hasTerminalPoints=true;

                terminalPoints.forEach(function (option) {
                    terminalPoint.append("<option value='" + option['id'] + "' data-lat='"+option['latitude']+"' data-long='"+option['longitude']+"'>" + option['name'] + "</option>");
                });
            }
            else{
                hasTerminalPoints=false;
                terminalPoint.append("<option value=''>No Terminal Points</option>");
                // Display a warning toast, with title
                toastr.warning('The selected countryies has no terminal points.','No terminal Points');

            }

        }

        function getCountriesTerminalsByIso(countryOneIso,countryTwoIso){
            //let gd = {{route('country.iso')}}+"/"+countryIso;
            let countryIsoRoute = "countries/terminal-points/"+countryOneIso+"/"+countryTwoIso;

            $.ajax(
                {
                    type: 'GET',
                    url: countryIsoRoute,
                    datatype: "json",
                }).done(function (data) {
                updateOptionsOfTerminalPoints(data);
            }).fail(function (customError) {
                //console.log("data: " + data.responseJSON);
                // console.log( 'error',customError.responseJSON.message);
                // Display an error toast, with a title
                toastr.error(customError.responseJSON.message);
            });
        }

        function setCountriesIso(){
            let countryIso1=$("option:selected" ,selectOriginCountry).val();
            let countryIso2=$("option:selected",selectDestinationCountry ).val();

            if(countryIso1 && countryIso2 && countryIso1!== countryIso2){
                getCountriesTerminalsByIso(countryIso1,countryIso2);
            }
            else{
                toastr.error('Please select different countries.');
            }
        }

        selectDestinationCountry.on('change',function () {

            isDestinationCountrySelected = true;

            let originCountryValue=$("#origin_country option:selected" ).val();
            $(this).children('[value="'+ originCountryValue +'"]').attr('disabled',true);

            if(isOriginCountrySelected){

               setCountriesIso();
            }
        });

        selectOriginCountry.on('change',function () {

            isOriginCountrySelected = true;
            let destinationCountryValue=$("#destination_country option:selected" ).val();
            $(this).siblings('[value="'+ destinationCountryValue +'"]').attr('disabled',true);
            //let countryIso2=$("#origin_country option:selected" ).val();
            if(isDestinationCountrySelected){

               setCountriesIso();
            }
        });


        /* media and its vehicles */
        function updateOptionsOfVehicle(data) {
            let vehicles = $("#vehicle_type");
            vehicles.empty();

            let disabledOption= "<option value='' selected disabled>Choose Vehicle</option>";

            vehicles.append(disabledOption);

            if(data.length > 0){
                data.forEach(function (option) {
                    //vehicles.append("<option value='" + option['id'] + "'>" + option['name'] + "</option>");
                    //vehicles.append("<optgroup label='"+option['name']+ "'>"+"</optgroup>");
                    var optGroup ="<optgroup label="+option['name']+ ">";
                    //vehicles.append(optGroup);


                    option['vehicle_dimensions'].forEach(function (dimension) {
                        var optGroupOptions =("<option value="+dimension['slug']+ ">"+dimension['title']+"</option>");
                        //optGroup.concat(optGroupOptions);
                        optGroup +=optGroupOptions;
                    });

                    optGroup +="</optgroup>";

                    vehicles.append(optGroup);
                });
            }
            else{
                vehicles.append("<option value=''>No Vehicles For The Media</option>");
                // Display a warning toast, with title
                toastr.warning('The selected media has no vehicles.','No vehicles')

            }

        }

        function getMediaVehicles(mediaUrl){
            $.ajax(
                {
                    type: 'GET',
                    url: mediaUrl,
                    datatype: "json",
                }).done(function (data) {
                updateOptionsOfVehicle(data);
            }).fail(function (customError) {
                //console.log("data: " + data.responseJSON);
                // console.log( 'error',customError.responseJSON.message);
                // Display an error toast, with a title
                toastr.error(customError.responseJSON.message);
            });
        }

        $('#transport_media').on('change',function () {
            let mediaUrl=$("option:selected" ,this).data('url');
            if(mediaUrl){
                getMediaVehicles(mediaUrl);
            }
        }).trigger('change');

        function calculateDistance(origin,destination){

            if(!isEmptyProperties(origin)&& !isEmptyProperties(destination))
            {
                return new Promise(function(resolve, reject) {
                    var service = new google.maps.DistanceMatrixService;

                    service.getDistanceMatrix({
                        origins: [origin],
                        destinations: [destination],
                        travelMode: 'DRIVING',
                        unitSystem: google.maps.UnitSystem.METRIC,//in KM
                        avoidHighways: false,
                        avoidTolls: false
                    }, function(response, status) {
                        if (status !== 'OK') {
                            alert('Error was: ' + status);
                            reject(status);
                        } else {
                            var originList = response.originAddresses;
                            var destinationList = response.destinationAddresses;

                            for (var i = 0; i < originList.length; i++) {
                                var results = response.rows[i].elements;
                                for (var j = 0; j < results.length; j++) {
                                    var element = results[j];
                                    var distance = element.distance.text;
                                    var duration = element.duration.text;
                                    var from = originList[i];
                                    var to = destinationList[j];

                                    //console.log(from,'to',to,":",distance);
                                    calculatedDistance = convertToFloatNumber(distance);

                                }
                            }
                            resolve(calculatedDistance);
                        }
                    });

                });
            }

            else{
                return new Promise(function(resolve, reject) {
                    reject('Please Fill Both Addresses.');
                });

            }


        }

        //only for object
        function isEmptyProperties(obj){

            return Object.keys(obj).length === 0 && obj.constructor=== Object? true : false;

            //returns true if object is empty

        }

        function isEqualObjects(obj1,obj2) {
            return JSON.stringify(obj1) === JSON.stringify(obj2);
        }



        function calculateTotalAmountAjax(formData) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax(
                {
                    type: 'POST',
                    url: 'quotation/store',
                    datatype: "html",
                    data:formData,
                }).done(function (data) {
                $('#error-message-hidden').hide().html('');
                $('.error_message-hidden').text('');
                    //console.log('done');
                toastr.success('Calculated Successfully !');
                setTimeout( function(){
                    let billModal =$('#generateBillModal');
                    billModal.find('.modal-body').html(data);
                    billModal.modal('show')
                }, 2000);

            }).fail(function (customErrors) {
                console.log(customErrors);
                $('#error-message-hidden').html('');
                $('.error_message-hidden').text('');
                $.each( customErrors.responseJSON.errors, function( key, value ) {
                    //$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
                    //console.log(key);
                    let errorSpan=$('#error_'+key);
                    if(key && errorSpan.length){
                       errorSpan.show().text(value);
                    }
                    else{
                        let errorMessageHidden=$('#error-message-hidden');
                        errorMessageHidden.show();
                        errorMessageHidden.append(value+'<br>');
                    }
                });

            });
        }

      $('#submit').on('click',function (e) {
            e.preventDefault();
            let quotationFormData =$( '#quotation-form' ).serialize(); //form data,key:value
            calculateTotalAmountAjax(quotationFormData);
        });

        function serializeFormDataToObject(arr){
            var o = {};
            var a = arr;
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        }

        function checkIfNewFormData(quotationFormData) {

            if(!isEmptyProperties(oldQuotationFormData) && !isEmptyProperties(quotationFormData)){

                return !isEqualObjects(oldQuotationFormData,quotationFormData);
            }
            else{
                oldQuotationFormData=quotationFormData;
                return true;
            }
        }

        $('#calculate_btn').on('click',function (e) {
            e.preventDefault();
            let quotationFormData =$( '#quotation-form' ).serializeArray(); //form data
            quotationFormData = serializeFormDataToObject(quotationFormData);
            //console.log(quotationFormData);
            if(hasTerminalPoints){

                origin = {lat: placeOriginLatitude, lng: placeOriginLongitude};
                destination = {lat: placeDestinationLatitude, lng: placeDestinationLongitude};

                if(!isEmptyProperties(origin) && !isEmptyProperties(destination) && !isEmptyProperties(terminalLatLong)){

                    originToTerminalDistance= calculateDistance(origin,terminalLatLong);
                    terminalToDestinationDistance = calculateDistance(terminalLatLong,destination);

                    const distances = Promise.all([
                        originToTerminalDistance,
                        terminalToDestinationDistance,
                    ])
                        .then(function([originToTerminalDistanceArg, terminalToDestinationDistanceArg]) {

                            let totalDistance = originToTerminalDistanceArg+terminalToDestinationDistanceArg;
                            console.log(`origin to terminal ${originToTerminalDistanceArg}! 🍔`)
                            console.log(`terminal to desti ${terminalToDestinationDistanceArg}! 🍟`)
                            console.log('Total Distance',totalDistance);
                            quotationFormData.total_distance = totalDistance;
                            //quotationFormData = quotationFormData+ "&total_distance=" + totalDistance;
                            $('#total_distance').val(totalDistance);
                            if(checkIfNewFormData(quotationFormData)){
                               calculateTotalAmountAjax(quotationFormData);
                            }
                            else{
                                toastr.warning('Duplicate Data Insertion !');
                            }

                            //$( "#quotation-form" ).submit();
                        });
                }
                else{
                    toastr.error('Please Fill Both Addresses & select terminal point');
                }

            }
            else{

               alert('Please fill the inputs.');
            }

        });

        $('#terminal_point').on('change',function () {

            // origin = {lat: placeOriginLatitude, lng: placeOriginLongitude};
            // destination = {lat: placeDestinationLatitude, lng: placeDestinationLongitude};

            terminalLat=$("option:selected" ,this).data('lat');
            terminalLong=$("option:selected" ,this).data('long');
            terminalLatLong ={lat:Number(terminalLat),lng:Number(terminalLong)};

            // originToTerminalDistance= calculateDistance(origin,terminalLatLong);
            // terminalToDestinationDistance = calculateDistance(terminalLatLong,destination);


        });
        
        function resetFormElements(formId) {
            $('#'+formId).find(':input').each(function() {
                switch(this.type) {
                   /* case 'password':
                    case 'text':
                    case 'textarea':
                        $(this).val('');
                        break;
                    case 'checkbox':
                    case 'radio':
                        this.checked = false;
                        break;*/
                    case 'select-multiple':
                    case 'select-one':
                    case 'select':
                        $(this).val('').trigger('change');
                }
            });
            document.getElementById(formId).reset();
        }

        $("#reset-btn").click(function(e){
            e.preventDefault();
            resetFormElements('quotation-form');
            //document.getElementById("quotation-form").reset();
           // $('#package-type').find($('option')).attr('selected',false);
            //$('#package-type').val(null).trigger('change');
        });

    });
</script>

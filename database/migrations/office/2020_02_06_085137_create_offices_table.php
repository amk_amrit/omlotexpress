<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('parent_office_id')->nullable()->comment('default null for parent offices');
            $table->enum('office_type',['super_head','country_head','region_head','transit','agency']);
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->boolean('status')->default(0);
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by');
            $table->timestamps();


//            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
//            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
//            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
//            $table->foreign('parent_office_id')->references('id')->on('offices')->onDelete('no action');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
}

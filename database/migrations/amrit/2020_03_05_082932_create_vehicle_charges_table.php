<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_charges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('quantity_group_range_id');
            $table->unsignedBigInteger('time_travel_range_id');
            $table->unsignedBigInteger('class1_id');
            $table->unsignedBigInteger('class2_id');
            $table->timestamps();

          //  $table->foreign('class1_id')->references('id')->on('agency_classes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_charges');
    }
}

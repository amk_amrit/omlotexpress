<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleAgentChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_agent_charges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('type')->unsigned()->nullable();
            $table->bigInteger('quantity_group_id')->unsigned()->nullable();
            $table->bigInteger('country_id')->unsigned()->nullable();
            $table->bigInteger('vehicle_id')->unsigned()->nullable();
            $table->string('name')->unique();
            $table->decimal('rate', 18,9);
            $table->string('slug')->unique();
            $table->string('currency')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_agent_charges');
    }
}

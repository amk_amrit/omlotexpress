<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysPart3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('countries')){
            Schema::table('airports', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action ');
            });
            Schema::table('v_time_travel_group_rates', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
            Schema::table('admin_charges', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
            Schema::table('quantity_group_countries', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
            Schema::table('agencies', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
            Schema::table('transits', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
            Schema::table('transit_class_countries', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
            Schema::table('agency_class_countries', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
            Schema::table('vehicle_agent_charges', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
            Schema::table('airport_links', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
            Schema::table('agency_class_charges', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
            Schema::table('transit_class_charges', function (Blueprint $table) {
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            });
        }
        
        if(Schema::hasTable('airports')){
            Schema::table('airport_links' , function (Blueprint $table){
                $table->foreign('source_airport_id')->references('id')->on('airports')->onDelete('no action');
                $table->foreign('destination_airport_id')->references('id')->on('airports')->onDelete('no action');
            });
        }
        if(Schema::hasTable('airport_links')){
            Schema::table('airline_charges' , function (Blueprint $table){
                $table->foreign('airline_link_id')->references('id')->on('airport_links')->onDelete('no action');
            });
        }
        if(Schema::hasTable('v_time_trave_groups')){
            Schema::table('v_time_trave_group_rates' , function (Blueprint $table){
                $table->foreign('v_time_travel_group_id')->references('id')->on('v_time_trave_groups')->onDelete('no action');
            });

        }
        if(Schema::hasTable('v_time_trave_group_rates')){
            Schema::table(' vehicle_agent_charges' , function (Blueprint $table){
                $table->foreign('type')->references('id')->on('v_time_trave_group_rates')->onDelete('no action');
            });
            
        }
        if(Schema::hasTable('transport_media')){
            Schema::table('admin_charges' , function (Blueprint $table){
                $table->foreign('transport_meida_id')->references('id')->on('transport_media')->onDelete('no action');
            });
            Schema::table('quantity_groups' , function (Blueprint $table){
                $table->foreign('transport_media_id')->references('id')->on('transport_media')->onDelete('no action');
            });
        }
        if(Schema::hasTable('quantity_group_countries')){
            Schema::table('admin_charges' , function (Blueprint $table){
                $table->foreign('quantity_group_id')->references('id')->on('quantity_group_countries')->onDelete('no action');
            });
             Schema::table('airline_charges' , function (Blueprint $table){
                 $table->foreign('quantity_group_id')->references('id')->on('quantity_group_countries')->onDelete('no action');
             });
             Schema::table('vehicle_agent_charges' , function (Blueprint $table){
                $table->foreign('quantity_group_id')->references('id')->on('quantity_group_countries')->onDelete('no action');
            });
            Schema::table('agency_class_charges' , function (Blueprint $table){
                $table->foreign('quantity_group_id')->references('id')->on('quantity_group_countries')->onDelete('no action');
            });
            Schema::table('transit_class_charges' , function (Blueprint $table){
                $table->foreign('quantity_group_id')->references('id')->on('quantity_group_countries')->onDelete('no action');
            });
            Schema::table('airport_links' , function (Blueprint $table){
                $table->foreign('quantity_group_id')->references('id')->on('quantity_group_countries')->onDelete('no action');
            });
            

        }
        
        if(Schema::hasTable('package_types')){
            Schema::table('quantity_groups' , function (Blueprint $table){
                $table->foreign('package_type_id')->references('id')->on('package_types')->onDelete('no action');
            });
        }
        if(Schema::hasTable('quantity_groups')){
            Schema::table('quantity_group_countries' , function (Blueprint $table){
                $table->foreign('quantity_group_id')->references('id')->on('quantity_groups')->onDelete('no action');
            });
        }
        if(Schema::hasTable('time_travel_groups')){
            Schema::table('quantity_group_countries' , function (Blueprint $table){
                $table->foreign('type_id')->references('id')->on('time_travel_groups')->onDelete('no action');
            });  
        }
        if(Schema::hasTable('agency_classes')){
            Schema::table('agency_class_charges' , function (Blueprint $table){
                $table->foreign('agency_class_id')->references('id')->on('agency_classes')->onDelete('no action');
            });
            Schema::table('agencies' , function (Blueprint $table){
                $table->foreign('orgin_class_id')->references('id')->on('agency_classes')->onDelete('no action');
            });
            Schema::table('agencies' , function (Blueprint $table){
                $table->foreign('destination_class_id')->references('id')->on('agency_classes')->onDelete('no action');
            });
            Schema::table('agency_class_countries' , function (Blueprint $table){
                $table->foreign('agency_class_id')->references('id')->on('agency_classes')->onDelete('no action');
            }); 
        }
        if(Schema::hasTable('agencies')){
            Schema::table('agency_links' , function (Blueprint $table){
                $table->foreign('agency_id')->references('id')->on('agencies')->onDelete('no action');
                $table->foreign('link_id')->references('id')->on('agencies')->onDelete('no action');

            });
        }
        if(Schema::hasTable('transit_classes')){
            Schema::table('transit_class_countries' , function (Blueprint $table){
                $table->foreign('transit_class_id')->references('id')->on('transit_classes')->onDelete('no action');
            });
            Schema::table('transit_class_charges' , function (Blueprint $table){
                $table->foreign('transit_class_id')->references('id')->on('transit_classes')->onDelete('no action');
            });
        }
        if(Schema::hasTable('transits')){
            Schema::table('transit_links' , function (Blueprint $table){
                $table->foreign('transit_id')->references('id')->on('transits')->onDelete('no action');
                $table->foreign('link_id')->references('id')->on('transits')->onDelete('no action');

            });
            Schema::table('agencies' , function (Blueprint $table){
                $table->foreign('transit_link_id')->references('id')->on('transits')->onDelete('no action');
            });
            Schema::table('airports' , function (Blueprint $table){
                $table->foreign('operate_by')->references('id')->on('transits')->onDelete('no action');
            });

            
        }
        if(Schema::hasTable('transport_vehicles')){
            Schema::table('vehicle_agent_charges' , function (Blueprint $table){
                $table->foreign('vehicle_id')->references('id')->on('transport_vehicles')->onDelete('no action');
            });
        }
        if(Schema::hasTable('offices')){
            Schema::table('transits', function(Blueprint $table){
                $table->foreign('office_id')->references('id')->on('offices')->onDelete('no action');
            });
        }
        if(Schema::hasTable('v_time_travel_groups')){
            Schema::table('vehicle_agent_charges', function(Blueprint $table){
                $table->foreign('type')->references('id')->on('v_time_travel_groups')->onDelete('no action');
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('foreign_keys_part3', function (Blueprint $table) {
            //
        });
    }
}

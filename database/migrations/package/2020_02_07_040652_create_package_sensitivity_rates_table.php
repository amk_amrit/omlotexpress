<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageSensitivityRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_sensitivity_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('package_sensitivity_id');
            $table->unsignedBigInteger('country_id');
            $table->string('currency');
            $table->decimal('handling_rate',18,6);
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->unique(['package_sensitivity_id','country_id'], 'uq_columns');

            // $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            // $table->foreign('package_sensitivity_id')->references('id')->on('package_sensitivities')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_sensitivity_rates');
    }
}

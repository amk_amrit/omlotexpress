<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageTypeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_type_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('package_type_id');
            $table->unsignedBigInteger('country_id');
            $table->string('currency');
            $table->boolean('status')->default(0);
            $table->decimal('handling_rate',18,6);
            $table->timestamps();

            $table->unique(['package_type_id','country_id']);
            // $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            // $table->foreign('package_type_id')->references('id')->on('package_types')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_type_rates');
    }
}

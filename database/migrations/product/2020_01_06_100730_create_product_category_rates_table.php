<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoryRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_category_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('country_id')->unsigned()->nullable();
            $table->bigInteger('product_category_id')->unsigned()->nullable();
            $table->bigInteger('min_weight')->comment('in kg');
            $table->bigInteger('max_weight')->default(999999999999)->comment('in kg');
            $table->string('currency');
            $table->decimal('rate',18,6);
            $table->timestamps();

            //$table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
           // $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category_rates');
    }
}

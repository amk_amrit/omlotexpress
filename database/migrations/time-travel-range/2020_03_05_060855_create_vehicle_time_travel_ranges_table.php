<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleTimeTravelRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_time_travel_ranges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('country_id');
            $table->bigInteger('min_time')->comment('in hour');
            $table->bigInteger('max_time')->comment('in hour');
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();

            $table->unique(['country_id','min_time','max_time'], 'uq_columns');

            //$table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            //$table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
           //$table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_time_travel_ranges');
    }
}

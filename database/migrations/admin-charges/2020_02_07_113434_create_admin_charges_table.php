<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_charges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('office_id')->unsigned();
            $table->bigInteger('quantity_group_range_id')->unsigned();
            $table->double('rate');
            $table->string('currency')->comment('office country currency');
            $table->enum('rate_type',['fix','per_kg']);
            $table->boolean('status')->default(0);
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();


            $table->unique(['office_id','quantity_group_range_id'], 'uq_columns');


//            $table->foreign('office_id')->references('id')->on('offices')->onDelete('cascade');
//            $table->foreign('quantity_group_range_id','qgr_id_foreign')->references('id')->on('quantity_group_ranges')->onDelete('cascade');
//            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
//            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_charges');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationShipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportQuotations_shipments', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('quotation_id');
            $table->unsignedBigInteger('shipment_id');

            //$table->foreign('quotation_id')->references('id')->on('transport_quotations')->onDelete('cascade');

            //$table->foreign('shipment_id')->references('id')->on('shipments')->onDelete('cascade');

            $table->primary(['quotation_id', 'shipment_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transportQuotations_shipments', function (Blueprint $table) {
            //
        });
    }
}

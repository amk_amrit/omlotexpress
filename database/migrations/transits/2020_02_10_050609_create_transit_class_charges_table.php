<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransitClassChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transit_class_charges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('courier_transit_class_id')->unsigned();
            $table->bigInteger('quantity_group_range_id')->unsigned();
            $table->bigInteger('country_id')->unsigned();
            $table->double('rate');
            $table->enum('rate_type',['fix','per_kg']);
            $table->string('currency')->comment('country currency');
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();
            $table->unique(['courier_transit_class_id','quantity_group_range_id', 'country_id'])->comment('unique_columns');


//            $table->foreign('courier_transit_class_id','ctc_id_foreign')->references('id')->on('courier_classes')->onDelete('cascade');
//            $table->foreign('quantity_group_range_id','qgr_id_foreign')->references('id')->on('quantity_group_ranges')->onDelete('no action');
//            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
//            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
//            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transit_class_charges');
    }
}

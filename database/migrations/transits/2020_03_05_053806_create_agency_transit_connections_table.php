<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyTransitConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_transit_connections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('agency_id');
            $table->unsignedBigInteger('transit_id');
            $table->double('distance')->comment('in km')->nullable();
            $table->double('vehicle_travel_time')->comment('in hour')->nullable();
            $table->boolean('status')->default(1);
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();

            $table->unique(['agency_id','transit_id']);

            //$table->foreign('agency_id')->references('id')->on('agencies')->onDelete('cascade');
            //$table->foreign('transit_id')->references('id')->on('transits')->onDelete('cascade');
            //$table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            //$table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_transit_connections');
    }
}

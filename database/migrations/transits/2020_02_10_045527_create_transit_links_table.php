<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransitLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transit_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('transit_one_id')->unsigned();
            $table->bigInteger('transit_two_id')->unsigned();
            $table->double('distance')->comment('in km')->nullable();
            $table->double('travel_time')->comment('in hour')->nullable();
            $table->enum('travel_medium',['air','road','train','ship']);
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->unique(['transit_one_id','transit_two_id','travel_medium']);

           // $table->foreign('transit_one_id')->references('id')->on('transits')->onDelete('cascade');
            //$table->foreign('transit_two_id')->references('id')->on('transits')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transit_links');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->boolean('is_closed')->default(0)->comment('closed for modification');
            $table->unsignedBigInteger('office_id')->comment('role created by office');
            $table->mediumText('description')->nullable();
            $table->string('guard_name')->nullable();
            $table->timestamps();

            //$table->foreign('office_id')->references('id')->on('offices')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourierClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courier_classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug');
            $table->enum('for_office_type',['agency','transit']);
            $table->string('description')->nullable();
            $table->bigInteger('country_id')->unsigned();
            $table->boolean('status')->default(0);
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();

            $table->unique(['name','for_office_type','country_id'], 'uq_columns');

        //$table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        //$table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
        //$table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courier_classes');
    }
}

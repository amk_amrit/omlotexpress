<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuantityGroupRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quantity_group_ranges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('quantity_group_id')->unsigned();
            $table->bigInteger('country_id')->unsigned();
            $table->bigInteger('min_weight')->comment('in kg');
            $table->bigInteger('max_weight')->comment('in kg');
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();

            $table->unique(['quantity_group_id','country_id','min_weight','max_weight'], 'uq_columns');

            //$table->foreign('quantity_group_id')->references('id')->on('quantity_groups')->onDelete('cascade');
           // $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
           //$table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
           //$table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quantity_group_ranges');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyTransitVehicleChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_transit_vehicle_charges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('agency_transit_connection_id')->unsigned();
            $table->bigInteger('quantity_group_range_id')->unsigned();
            $table->double('rate');
            $table->enum('rate_type',['per_kg','fix']);
            $table->string('currency');
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();

//            $table->foreign('agency_transit_connection_id','atc_id_foreign')->references('id')->on('agency_transit_connections')->onDelete('cascade');
//            $table->foreign('quantity_group_range_id','qgr_id_foreign')->references('id')->on('quantity_group_ranges')->onDelete('no action');
//            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
//            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_transit_vehicle_charges');
    }
}

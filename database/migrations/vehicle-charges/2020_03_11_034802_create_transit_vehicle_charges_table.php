<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransitVehicleChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transit_vehicle_charges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('courier_transit_one_class_id')->unsigned();
            $table->bigInteger('courier_transit_two_class_id')->unsigned();
            $table->bigInteger('time_travel_range_id')->unsigned();
            $table->bigInteger('quantity_group_range_id')->unsigned();
            $table->double('rate');
            $table->enum('rate_type',['per_kg', 'fix']);
            $table->string('currency');
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();

            //$table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
           //$table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
           //$table->foreign('courier_transit_one_class_id')->references('id')->on('courier_classes')->onDelete('cascade');
           //$table->foreign('courier_transit_two_class_id')->references('id')->on('courier_classes')->onDelete('cascade');
           //$table->foreign('time_travel_range_id')->references('id')->on('vehicle_time_travel_ranges')->onDelete('cascade');
           //$table->foreign('quantity_group_range_id')->references('id')->on('quantity_group_ranges')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transit_vehicle_charges');
    }
}

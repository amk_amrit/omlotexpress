<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //order according to migration folders order

        //folder admin_charges
        Schema::table('admin_charges', function (Blueprint $table) {
            $table->foreign('office_id')->references('id')->on('offices')->onDelete('cascade');
            $table->foreign('quantity_group_range_id','ac_qgr_id_foreign')->references('id')->on('quantity_group_ranges')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        //folder agencies
        Schema::table('agency_class_charges', function (Blueprint $table) {
            $table->foreign('courier_agency_class_id','acc_cgc_id_foreign')->references('id')->on('courier_classes')->onDelete('cascade');
            $table->foreign('quantity_group_range_id','acc_qgr_id_foreign')->references('id')->on('quantity_group_ranges')->onDelete('no action');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        Schema::table('agencies', function (Blueprint $table) {
            $table->foreign('office_id')->references('id')->on('offices')->onDelete('cascade');
            $table->foreign('courier_agency_class_id','a_cac_id_foreign')->references('id')->on('courier_classes')->onDelete('no action');
        });

        //folder airlines
        Schema::table('airports', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            $table->foreign('operate_by')->references('id')->on('transits')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        Schema::table('airport_links', function (Blueprint $table) {
            $table->foreign('airport_one_id')->references('id')->on('airports')->onDelete('cascade');
            $table->foreign('airport_two_id')->references('id')->on('airports')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        Schema::table('airport_charges', function (Blueprint $table) {
            $table->foreign('airport_link_id')->references('id')->on('airport_links')->onDelete('cascade');
            $table->foreign('quantity_group_range_id','airport_qgr_id_foreign')->references('id')->on('quantity_group_ranges')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        //folder countries
        Schema::table('terminal_points', function (Blueprint $table) {
            $table->foreign('country_one_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('country_two_id')->references('id')->on('countries')->onDelete('cascade');
        });

        //folder courier-classes
        Schema::table('courier_classes', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        //folder department
        Schema::table('departments', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        //folder designation
        Schema::table('designations', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        //folder employee
        Schema::table('employees', function (Blueprint $table) {

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('office_id')->references('id')->on('offices')->onDelete('no action');
            $table->foreign('designation_id')->references('id')->on('designations')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');

        });

        Schema::table('employee_department', function (Blueprint $table) {
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        });

        Schema::table('employee_role', function (Blueprint $table) {
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });


        //folder office
        Schema::table('offices', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('parent_office_id')->references('id')->on('offices')->onDelete('no action');
           

        });

        //folder package
        Schema::table('package_sensitivity_rates', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            $table->foreign('package_sensitivity_id')->references('id')->on('package_sensitivities')->onDelete('cascade');
        });

        Schema::table('package_type_rates', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            $table->foreign('package_type_id')->references('id')->on('package_types')->onDelete('cascade');
        });

        //folder product
        Schema::table('product_category_rates', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('cascade');
        });



        //folder quantity-groups
        Schema::table('quantity_groups', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        Schema::table('quantity_group_ranges', function (Blueprint $table) {
            $table->foreign('quantity_group_id')->references('id')->on('quantity_groups')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });


        //folder role-permission
        Schema::table('roles', function (Blueprint $table) {
            $table->foreign('office_id')->references('id')->on('offices')->onDelete('no action');
        });

        Schema::table('role_permission', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
        });

        //folder services
        Schema::table('services', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        //folder shipment
        Schema::table('shipments', function (Blueprint $table) {
            $table->foreign('transport_media_id')->references('id')->on('transport_media')->onDelete('no action');
            $table->foreign('origin_country_id')->references('id')->on('countries')->onDelete('no action');
            $table->foreign('destination_country_id')->references('id')->on('countries')->onDelete('no action');
            $table->foreign('product_category_rate_id','pcr_id_foreign')->references('id')->on('product_category_rates')->onDelete('no action');
            $table->foreign('terminal_point_id')->references('id')->on('terminal_points')->onDelete('no action');
            $table->foreign('sensitivity_rate_id')->references('id')->on('package_sensitivity_rates')->onDelete('no action');
        });

        Schema::table('part_load_information', function (Blueprint $table) {
            $table->foreign('shipment_id')->references('id')->on('shipments')->onDelete('cascade');
            $table->foreign('package_type_rate_id','pli_ptr_id_foreign')->references('id')->on('package_type_rates')->onDelete('no action');
            $table->foreign('dimension_unit')->references('id')->on('units')->onDelete('no action');
            $table->foreign('weight_unit')->references('id')->on('units')->onDelete('no action');

        });

        Schema::table('full_load_information', function (Blueprint $table) {
            $table->foreign('shipment_id')->references('id')->on('shipments')->onDelete('cascade');
            $table->foreign('vehicle_dimension_rate_id','fli_vdr_id_foreign')->references('id')->on('vehicle_dimension_rates')->onDelete('no action');

        });

        //folder time-travel-range
        Schema::table('vehicle_time_travel_ranges', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        //folder transits
        Schema::table('transits', function (Blueprint $table) {
            $table->foreign('office_id')->references('id')->on('offices')->onDelete('cascade');
            $table->foreign('courier_transit_class_id','transit_ctc_id_foreign')->references('id')->on('courier_classes')->onDelete('no action');
        });

        Schema::table('transit_links', function (Blueprint $table) {

            $table->foreign('transit_one_id')->references('id')->on('transits')->onDelete('cascade');
            $table->foreign('transit_two_id')->references('id')->on('transits')->onDelete('cascade');
        });

        Schema::table('transit_class_charges', function (Blueprint $table) {

            $table->foreign('courier_transit_class_id','tcc_ctc_id_foreign')->references('id')->on('courier_classes')->onDelete('cascade');
            $table->foreign('quantity_group_range_id','tcc_qgr_id_foreign')->references('id')->on('quantity_group_ranges')->onDelete('no action');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        Schema::table('agency_transit_connections', function (Blueprint $table) {
            $table->foreign('agency_id')->references('id')->on('agencies')->onDelete('cascade');
            $table->foreign('transit_id')->references('id')->on('transits')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        //folder transport
        Schema::table('transport_vehicles', function (Blueprint $table) {
            $table->foreign('transport_media_id')->references('id')->on('transport_media')->onDelete('no action');
        });

        Schema::table('transport_vehicle_dimensions', function (Blueprint $table) {
            $table->foreign('vehicle_id')->references('id')->on('transport_vehicles')->onDelete('no action');
        });

        Schema::table('vehicle_dimension_rates', function (Blueprint $table) {
            $table->foreign('vehicle_dimension_id')->references('id')->on('transport_vehicle_dimensions')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
        });

        //folder transport-quotation
        Schema::table('transportQuotations_shipments', function (Blueprint $table) {
            $table->foreign('quotation_id')->references('id')->on('transport_quotations')->onDelete('cascade');
            $table->foreign('shipment_id')->references('id')->on('shipments')->onDelete('cascade');
        });

        //folder unit
        Schema::table('units', function (Blueprint $table) {
            $table->foreign('unit_category_id')->references('id')->on('unit_categories')->onDelete('no action');
        });

        //folder vehicle-charges
        Schema::table('agency_transit_vehicle_charges', function (Blueprint $table) {
            $table->foreign('agency_transit_connection_id','atvc_atc_id_foreign')->references('id')->on('agency_transit_connections')->onDelete('cascade');
            $table->foreign('quantity_group_range_id','atvc_qgr_id_foreign')->references('id')->on('quantity_group_ranges')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });

        Schema::table('transit_vehicle_charges', function (Blueprint $table) {

            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
            $table->foreign('courier_transit_one_class_id','tvc_ctoc_id_foreign')->references('id')->on('courier_classes')->onDelete('cascade');
            $table->foreign('courier_transit_two_class_id','tvc_cttc_id_foreign')->references('id')->on('courier_classes')->onDelete('cascade');
            $table->foreign('time_travel_range_id','tvc_ttr_id_foreign')->references('id')->on('vehicle_time_travel_ranges')->onDelete('cascade');
            $table->foreign('quantity_group_range_id','tvc_qgr_id_foreign')->references('id')->on('quantity_group_ranges')->onDelete('cascade');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

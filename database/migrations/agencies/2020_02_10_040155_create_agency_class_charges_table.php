<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencyClassChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_class_charges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('courier_agency_class_id')->unsigned();
            $table->bigInteger('quantity_group_range_id')->unsigned();
            $table->bigInteger('country_id')->unsigned();
            $table->double('origin_rate');
            $table->double('destination_rate');
            $table->enum('rate_type',['fix','per_kg']);
            $table->string('currency');
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();
//
//            $table->foreign('courier_agency_class_id','cgc_id_foreign')->references('id')->on('courier_classes')->onDelete('cascade');
//            $table->foreign('quantity_group_range_id','qgr_id_foreign')->references('id')->on('quantity_group_ranges')->onDelete('no action');
//            $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
//            $table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
//            $table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_class_charges');
    }
}

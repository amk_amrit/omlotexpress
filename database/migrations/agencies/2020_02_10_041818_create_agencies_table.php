<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('office_id');
            $table->unsignedBigInteger('courier_agency_class_id');
            $table->enum('working_as',['source','destination','all']);
            $table->timestamps();

            //$table->foreign('office_id')->references('id')->on('offices')->onDelete('cascade');
            //$table->foreign('courier_agency_class_id','cac_id_foreign')->references('id')->on('courier_classes')->onDelete('no action');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
    }
}

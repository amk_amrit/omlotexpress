<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleDimensionRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_dimension_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vehicle_dimension_id');
            $table->unsignedBigInteger('country_id');
            $table->string('currency');
            $table->decimal('per_km_charge',18,6);
            $table->decimal('min_charge',18,6);
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->unique(['vehicle_dimension_id','country_id'], 'uq_columns');
            
            // $table->foreign('vehicle_dimension_id')->references('id')->on('transport_vehicle_dimensions')->onDelete('cascade');
            // $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_dimension_rates');
    }
}

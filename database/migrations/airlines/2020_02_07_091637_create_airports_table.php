<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->string('address');
            $table->bigInteger('country_id')->unsigned();
            $table->enum('scope',['domestic','international']);
            $table->bigInteger('operate_by')->unsigned()->nullable()->comment('transit office id');
            $table->boolean('status')->default(0);
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();

           // $table->foreign('country_id')->references('id')->on('countries')->onDelete('no action');
            //$table->foreign('operate_by')->references('id')->on('transits')->onDelete('no action');
            //$table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            //$table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airports');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirportLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airport_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('airport_one_id')->unsigned();
            $table->bigInteger('airport_two_id')->unsigned();
            $table->enum('air_link_type',['domestic','international']);
            $table->double('distance')->comment('in km');
            $table->double('travel_time')->comment("in hr");
            $table->boolean('status')->default(0);
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();

            $table->unique(['airport_one_id','airport_two_id','air_link_type'], 'uq_columns');


            //$table->foreign('airport_one_id')->references('id')->on('airports')->onDelete('cascade');
            //$table->foreign('airport_two_id')->references('id')->on('airports')->onDelete('cascade');
             //$table->foreign('created_by')->references('id')->on('employees')->onDelete('no action');
            //$table->foreign('updated_by')->references('id')->on('employees')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airport_links');
    }
}

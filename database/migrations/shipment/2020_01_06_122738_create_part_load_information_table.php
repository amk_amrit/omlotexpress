<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartLoadInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('part_load_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shipment_id');
            $table->unsignedBigInteger('package_type_rate_id')->nullable();
            $table->string('num_of_items');
            $table->decimal('height',18,4)->nullable();
            $table->decimal('width',18,4)->nullable();
            $table->decimal('length',18,4)->nullable();
            $table->unsignedBigInteger('dimension_unit')->nullable();
            $table->decimal('total_weight',18,4);
            $table->unsignedBigInteger('weight_unit')->nullable();
            $table->timestamps();

            //$table->foreign('shipment_id')->references('id')->on('shipments')->onDelete('cascade');
            //$table->foreign('package_type_rate_id','ptr_id_foreign')->references('id')->on('package_type_rates')->onDelete('no action');
            //$table->foreign('dimension_unit')->references('id')->on('units')->onDelete('no action');
            //$table->foreign('weight_unit')->references('id')->on('units')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('part_load_information');
    }
}

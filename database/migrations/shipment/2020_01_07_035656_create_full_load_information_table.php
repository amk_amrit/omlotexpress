<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFullLoadInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('full_load_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shipment_id');
            $table->unsignedBigInteger('vehicle_dimension_rate_id')->nullable();
            $table->integer('no_of_vehicles');
            $table->timestamps();

            //$table->foreign('shipment_id')->references('id')->on('shipments')->onDelete('cascade');
            //$table->foreign('vehicle_dimension_rate_id','vdr_id_foreign')->references('id')->on('vehicle_dimension_rates')->onDelete('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('full_load_information');
    }
}

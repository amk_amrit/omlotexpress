<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('load_type',['part_load','full_load']);
            $table->unsignedBigInteger('transport_media_id');
            $table->unsignedBigInteger('origin_country_id');
            $table->string('local_origin');
            $table->unsignedBigInteger('destination_country_id');
            $table->string('local_destination');
            $table->unsignedBigInteger('product_category_rate_id');
            $table->unsignedBigInteger('terminal_point_id');
            $table->unsignedBigInteger('sensitivity_rate_id');
            $table->decimal('user_bill_amount',18,6)->nullable();
            $table->decimal('total_bill_amount',18,6)->nullable();
            $table->timestamps();

//            $table->foreign('transport_media_id')->references('id')->on('transport_media')->onDelete('no action');
//            $table->foreign('origin_country_id')->references('id')->on('countries')->onDelete('no action');
//            $table->foreign('destination_country_id')->references('id')->on('countries')->onDelete('no action');
//            $table->foreign('product_category_rate_id','pcr_id_foreign')->references('id')->on('product_category_rates')->onDelete('no action');
//            $table->foreign('terminal_point_id')->references('id')->on('terminal_points')->onDelete('no action');
//            $table->foreign('sensitivity_rate_id')->references('id')->on('package_sensitivity_rates')->onDelete('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}

<?php

use Illuminate\Database\Seeder;

class TransitOfficeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /*$transitOffices=[
            [
                'name' => 'Omlot Transit',
                'slug' => 'omlot-transit',
                'country_id' => 149,
                'parent_office_id' => 3,
                'office_type' => 'transit',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ];
        DB::table('offices')->insert($transitOffices);*/


        $transits=[
            [
                'office_id' => 4,
                'courier_transit_class_id' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ];
        DB::table('transits')->insert($transits);
    }
}

<?php

use Illuminate\Database\Seeder;

class OfficeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('offices')->truncate();
        //DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $offices=[
            [
                'name' => 'Omlot Express',
                'slug' => 'omlot-express',
                'country_id' => 149,
                'office_type' => 'super_head',
                'parent_office_id' => null,
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Omlot Nepal',
                'slug' => 'omlot-nepal',
                'country_id' => 149,
                'parent_office_id' => 1,
                'office_type' => 'country_head',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Omlot Regional',
                'slug' => 'omlot-regional',
                'country_id' => 149,
                'parent_office_id' => 2,
                'office_type' => 'region_head',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Omlot Transit',
                'slug' => 'omlot-transit',
                'country_id' => 149,
                'parent_office_id' => 3,
                'office_type' => 'transit',
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ];
        DB::table('offices')->insert($offices);
    }
}

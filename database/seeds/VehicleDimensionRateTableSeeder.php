<?php

use Illuminate\Database\Seeder;

class VehicleDimensionRateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('vehicle_dimension_rates')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $rates=[
            [
                'country_id' => 149,
                'vehicle_dimension_id' => '1',
                'currency' => 'NPR',
                'per_km_charge' => 45.23,
                'min_charge' => 20.45,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [

                'country_id' => 99,
                'vehicle_dimension_id' => '2',
                'currency' => 'INR',
                'per_km_charge' => 45.23,
                'min_charge' => 20.45,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ];
        DB::table('vehicle_dimension_rates')->insert($rates);
    }
}

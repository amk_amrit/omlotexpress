<?php

use App\Models\Department;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(CountriesTableSeeder::class);
        $this->call(PackageSensitivityTableSeeder::class);
        $this->call(TransportMediaTableSeeder::class);
        $this->call(TerminalPointsTableSeeder::class);
        $this->call(TransportVehiclesTableSeeder::class);
        $this->call(TransportVehicleDimensionsTableSeeder::class);
        $this->call(VehicleDimensionRateTableSeeder::class);
        $this->call(PackageTypeTableSeeder::class);
        $this->call(UnitTableSeeder::class);
        $this->call(ProductCategoryTableSeeder::class);
        $this->call(ProductCategoryRatesTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(EmployeeTableSeeder::class);
        $this->call(DesignationSeeder::class);
        $this->call(OfficeSeeder::class);
        $this->call(CourierClassSeeder::class);
        $this->call(TransitOfficeSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(EmployeeRoleTableSeeder::class);
    }
}

<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::table('roles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        $roles = [
            'name' => 'Super Admin',
            'slug' => 'super-admin',
            'is_closed' => 1,
            'office_id' => 1,
            'description' => 'this is the role for Super Admin',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ];

        DB::table('roles')->insert($roles);

    }
}

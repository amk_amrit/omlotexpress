<?php

use Illuminate\Database\Seeder;

class ProductCategoryRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('product_category_rates')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $rates=[
            [
                'country_id' =>149,
                'product_category_id' => 1,
                'min_weight' =>0,
                'max_weight' =>10,
                'currency' => 'NPR',
                'rate' => 100,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'country_id' =>149,
                'product_category_id' => 1,
                'min_weight' =>11,
                'max_weight' =>20,
                'currency' => 'NPR',
                'rate' => 500,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'country_id' => 149,
                'product_category_id' => 2,
                'min_weight' =>0,
                'max_weight' =>20,
                'currency' => 'NPR',
                'rate' => 500,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ];
        DB::table('product_category_rates')->insert($rates);
    }
}

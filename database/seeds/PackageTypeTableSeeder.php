<?php

use Illuminate\Database\Seeder;

class PackageTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('package_types')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $packages=[
            [
                'type_name' => 'Box',
                'slug' => 'box',
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'type_name' => 'Sack',
                'slug' => 'sack',
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ];
        DB::table('package_types')->insert($packages);
    }
}

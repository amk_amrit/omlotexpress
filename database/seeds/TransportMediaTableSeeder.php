<?php

use Illuminate\Database\Seeder;

class TransportMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('transport_media')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $medias=[
            [
                'name' => 'By Air',
                'slug' => 'by-air',
                'status' => 1,
            ],
            [
                'name' => 'By Road',
                'slug' => 'by-road',
                'status' => 1,
            ]

        ];
        DB::table('transport_media')->insert($medias);

    }
}

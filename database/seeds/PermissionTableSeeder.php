<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        //just for demo
        $permissionsDemo = [

            /*Role Permissions*/

            [
                'name' => 'add-role',
                'route'=>'role.create',
                'display_name' => 'Add Role',
                'description' => 'can add role',
                'category' => 'role-category'
            ],
            [
                'name' => 'update-role',
                'route'=>'role.edit',
                'display_name' => 'Update Role',
                'description' => 'can update role detail',
                'category' => 'role-category'
            ],

        ];

        $permissionService = new \App\Services\PermissionService();
        $permissions = $permissionService->getAllModelPermissions();

        foreach ($permissions as $key => $value) {
            \App\Models\Permission::updateOrCreate($value);
        }
    }
}

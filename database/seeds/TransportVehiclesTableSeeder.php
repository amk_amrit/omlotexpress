<?php

use Illuminate\Database\Seeder;

class TransportVehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('transport_vehicles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $vehicles=[
            [
                'name' => 'Truck',
                'slug' =>'truck',
                'status' => 1,
                'transport_media_id'=>2,
                'description' => 'It is a truck.',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Jet',
                'slug' => 'jet',
                'status' => 1,
                'transport_media_id'=>2,
                'description' => 'It is a mini truck.',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ];
        DB::table('transport_vehicles')->insert($vehicles);
    }
}

<?php

use Illuminate\Database\Seeder;

class PackageSensitivityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('package_sensitivities')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $sensitivities=[
            [
                'title' => 'Normal',
                'slug' => 'normal',
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Sensitive',
                'slug' => 'sensitive',
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],

        ];
        DB::table('package_sensitivities')->insert($sensitivities);
    }
}

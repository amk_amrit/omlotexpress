<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('users')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    
    
            $users=[
                [
                    'name' => 'Super Admin',
                    'email' => 'super.admin@gmail.com',
                    'password' => bcrypt('admin123'),
                    'status' => 'verified',
                    'address' => 'Kathmandu,Nepal',
                    'mobile' => 9841123456,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],

                [
                    'name' => 'Country Admin',
                    'email' => 'country.admin@gmail.com',
                    'password' => bcrypt('admin123'),
                    'status' => 'verified',
                    'address' => 'Kathmandu,Nepal',
                    'mobile' => 9841123456,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],


            ];
            DB::table('users')->insert($users);
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class TransportVehicleDimensionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('transport_vehicle_dimensions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $vehicles=[
            [
                'title' =>'Eighteen Wheeler 90 (EURO)',
                'slug' => 'Eighteen-Wheeler-90-(EURO)',
                'vehicle_id' =>1,
                'dimension' => 'Length 13.6 m x 2.45 m Width x Height 2.70 m',
                'capacity' => '22',
                'status' => 1,
                'description' => 'It is a truck.',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [

                'title' =>'Eighteen Wheeler 86 (EURO)',
                'slug' => 'Eighteen-Wheeler-86-(EURO)',
                'vehicle_id' =>1,
                'dimension' => 'Length 13.6 m x Width 2.45 m x Height 2.60 m',
                'capacity' => '22',
                'status' => 1,
                'description' => 'It is a truck.',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ];
        DB::table('transport_vehicle_dimensions')->insert($vehicles);
    }
}

<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
         DB::table('employees')->truncate();
       // DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        $employees = [
            [
                'user_id' => 1,
                'office_id' => 1,
                'designation_id' => 1,
                'is_head' => 1,
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => 2,
                'office_id' => 2,
                'designation_id' => 1,
                'is_head' => 1,
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]


           
        ];

        DB::table('employees')->insert($employees);
    }
}

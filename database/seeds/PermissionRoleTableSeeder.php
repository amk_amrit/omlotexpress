<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::table('role_permission')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        $permissions = Permission::all();
        $rolePermission = [];
        foreach($permissions as $permission){
            $data = ['role_id' => 1, 'permission_id' => $permission->id];
            array_push($rolePermission, $data);
        }
        
        DB::table('role_permission')->insert($rolePermission);


    }
}

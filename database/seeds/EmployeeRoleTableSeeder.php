<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::table('employee_role')->truncate();

        $employeeRole = [
          ['employee_id' => 1, 'role_id' =>1],
        ];

        DB::table('employee_role')->insert($employeeRole);
    }
}

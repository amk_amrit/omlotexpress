<?php

use Illuminate\Database\Seeder;

class CourierClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $courierClasses=[
            [
                'name' => 'Transit Class A',
                'slug' => 'transit-class-a',
                'for_office_type' => 'transit',
                'country_id' => 149,
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ];
        DB::table('courier_classes')->insert($courierClasses);
    }
}

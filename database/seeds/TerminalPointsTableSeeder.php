<?php

use Illuminate\Database\Seeder;

class TerminalPointsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('terminal_points')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $tPoints=[
            [
                'name' => 'Birgunj',
                'country_one_id' => 149,
                'country_two_id' => 99,
                'latitude' =>27.0449,
                'longitude' =>84.8672,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Sunauli',
                'country_one_id' => 149,
                'country_two_id' => 99,
                'latitude' =>27.4713,
                'longitude' =>83.4657,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Raxaul',
                'country_one_id' => 44,
                'country_two_id' => 149,
                'latitude' =>26.9798,
                'longitude' =>84.8516,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ];
        DB::table('terminal_points')->insert($tPoints);
    }
}

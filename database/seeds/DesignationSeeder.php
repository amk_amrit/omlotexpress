<?php

use Illuminate\Database\Seeder;

class DesignationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('designations')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $designations=[
            [
                'title' => 'CEO',
                'slug' => 'ceo',
                'description' => 'this is seeded designation of CEO',
                'status' => 1,
                'is_closed' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Branch Manager',
                'slug' => 'branch-manager',
                'description' => 'this seeded designation of branch manager',
                'status' => 1,
                'is_closed' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'title' => 'Department Manager',
                'slug' => 'department-manager',
                'description' => 'this seeded designation of department manager',
                'status' => 1,
                'is_closed' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],


        ];
        DB::table('designations')->insert($designations);
    }
}

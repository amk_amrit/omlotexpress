<?php

namespace App\Http\Requests;

use App\Country;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class QuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $countryIsoArray = Country::getArrayOfIso();
       //dd($countryIsoArray);

        return [
            'name' => 'required',
            'phone' => 'required',
            'email' =>'required|email',
            'origin_country' => ['required',Rule::in($countryIsoArray)],
        ];
    }

}

<?php

namespace App\Http\Resources\user;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                => $this->id,
            // "fb_id"             => $this->fb_id,
            // "google_id"         => $this->google_id,
            // "provider_id"       => $this->provider_id,
            // "provider"          => $this->provider,
            "name"              => $this->name,
            "email"             => $this->email,
            "email_verified_at" => $this->email_verified_at != null ? $this->email_verified_at->format('Y-m-d H:i:s') : $this->email_verified_at,
            "address"           => $this->address,
            // "phone"             => $this->phone,
            "mobile"            => $this->mobile,
            "user_image"        => photoToUrl($this->user_image, '/common/images/'),
            // "facebook"          => $this->facebook,
            // "instagram"         => $this->instagram,
            // "twitter"           => $this->twitter,
            // "linkedin"          => $this->linkedin,
            "status"            => $this->status,
            "created_at"        => $this->created_at->format('Y-m-d H:i:s'),
            "updated_at"        => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function with($request)
    {
        return [
            'error' => false,
            'code' => 200
        ];
    }
}

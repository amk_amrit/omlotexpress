<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\Api\V1\AuthRepository;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * @var AuthRepository
     */
    private $authRepo;

    public function __construct(AuthRepository $authRepo)
    {
        $this->authRepo = $authRepo;
    }

    /**
     * User Login.
     *
     * @return response
     */

    public function login()
    {
        return $this->authRepo->login();
    }

    /**
     * Refresh Token 
     *
     * @return response
     */

    public function refreshToken()
    {
        return $this->authRepo->refreshToken();
    }

    /**
     * Get User
     *
     * @return response
     */

    public function getUser()
    {
        return $this->authRepo->getUser();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\Role;
// use App\Repositories\Employee\EmployeeInterface;
use App\Repositories\Employee\EmployeeRepository;
use App\Services\EmployeeService;
use Illuminate\Http\Request;
use App\ValidationRules\EmployeeRule;
use Exception;
use Illuminate\Support\Facades\Validator;


class EmployeeController extends Controller
{
    protected $employeeRepo, $employeeService;

    public function __construct(EmployeeRepository $employeeRepo, EmployeeService $employeeService)
    {
        $this->employeeRepo = $employeeRepo;
        $this->employeeService = $employeeService;
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        try {
            // $employees = $this->employeeRepo->all();
            $employees = $this->employeeService->getEmployeesByEmployee(auth()->user()->employee);
            return view('admin.pages.employee.index')->with('employees', $employees);
        } catch (Exception $e) {
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        try{
            $data = [];
            $data['offices'] = $this->employeeService->getOfficesByEmployee(auth()->user()->employee);
            $data['designations'] = Designation::getActiveDesignations();
            $data['departments'] = Department::getActiveDepartments();
            $data['roles'] = Role::findByoffice(authEmployee()->office);

            return view('admin.pages.employee.create', $data);

        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), (new EmployeeRule($request))->getEmployeeValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
 
        try{
            $this->employeeService->storeEmployee($request);
            return redirect()->route('admin.employees.index')->with('flash_message_success', 'Employee Created Successfully');

        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput();
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     */
    public function show($id)
    {
        try{
            $employee = Employee::find($id);
            return view('admin.pages.employee.show')->with('employee', $employee);
        }catch(Exception $e){
            return redirect()->back()->with('flass_message_error', $e->getMessage());
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     */
    public function edit($id)
    {
        try{
            $data = [];
            $data['offices'] = $this->employeeService->getOfficesByEmployee(auth()->user()->employee);
            $data['designations'] = Designation::getActiveDesignations();
            $data['departments'] = Department::getActiveDepartments();
            $data['employee'] = Employee::find($id);
            $data['roles'] = Role::findByoffice(authEmployee()->office);

            
            return view('admin.pages.employee.edit', $data);

        }catch(Exception $e){
            return redirect()->back()->with('flass_message_error', $e->getMessage()); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     */
    public function update(Request $request, Employee $employee)
    {
     
        $validator = Validator::make($request->all(), (new EmployeeRule($request))->getEmployeeValidationRules('update', $employee->user->id));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        try{
            $this->employeeService->updateEmployee($request, $employee);
            return redirect()->route('admin.employees.index')->with('flash_message_success', 'Employee Updated Successfully');

        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput();
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     */
    public function destroy(Employee $employee)
    {
        try{
            $this->employeeService->destroyEmployee($employee);
            return redirect()->route('admin.employees.index')->with('flash_message_success', 'Operation Completed Successfully');

        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
          
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\ImageService;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\Storage;
use App\ValidationRules\CategoryRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class ProductCategoryController extends Controller
{
    use ImageService;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorys=ProductCategory::paginate(10);
        return view('admin.pages.product-category.index')->with('category', $categorys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.product-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(),(new CategoryRule($request))->getCategoryValidationRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
            $category= new ProductCategory();
            $category->name=$request->name;
            $slugs=str_slug($request->name, '_');
            $category->slug=$slugs;
            $category->hsn_code=$request->hsn_code;
            $category->status=1;
                
            if ($request->hasFile('image')) {
                $fileNameToStore=$this->storeImageInServer($request->file('image'),$category->getImagePath());
                $category->image=$fileNameToStore;
            }
            $category->save();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollback();
            //dd($exception->getMessage());
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }


        return redirect()->route('admin.categories.index')->with('message', 'Data Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $categorys=ProductCategory::find($id);
        return view('admin.pages.product-category.show')->with('category' , $categorys);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categorys=ProductCategory::find($id);
        return view('admin.pages.product-category.edit')->with('category', $categorys);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $category=ProductCategory::find($id);
            $this->validate($request,[
                'name' => 'required|unique:product_categories,name,'.$category->id,
                'hsn_code' => 'required|unique:product_categories,hsn_code,'.$category->id
            ]);

        DB::beginTransaction();
            try{

                $category->name=$request->name;
                $slugs=str_slug($request->name, '_');
                $category->slug=$slugs;
                $category->hsn_code=$request->hsn_code;
                $category->status=1;
                //dd($category);
                if ($request->hasFile('image')) {
                    $fileNameToStore=$this->storeImageInServer($request->file('image'),$category->getImagePath());
                    $imageTobeDeleted = $category->image;
                    $category->image=$fileNameToStore;

                    $this->deleteImageFromServer($category->getImagePath(),$imageTobeDeleted);
                }
                $category->save();
                DB::commit();

            }catch (\Exception $exception){
                DB::rollback();
                //dd(1);
                return redirect()->back()
                    ->withInput()
                    ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
            }

            return redirect()->route('admin.categories.index')->with('message', 'Data Created Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category=ProductCategory::find($id);
        if($category->status==0){
           $category->status=1;
           $category->save();
        }
        else{
            $category->status=0;
           $category->save();
        }
        return redirect()->route('admin.categories.index')->with('message',  'Operation Successfuly!');
    }
}

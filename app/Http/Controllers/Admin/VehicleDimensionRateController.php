<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TransportVehicleDimension;
use App\Models\VehicleDimensionRate;
use App\Services\VehicleDimensionRateService;
use App\ValidationRules\VehicleDimensionRateRule;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VehicleDimensionRateController extends Controller
{
    protected $rateService;
    public function __construct(VehicleDimensionRateService $rateService)
    {
        $this->rateService = $rateService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $rates = $this->rateService->index();
            return view('admin.pages.vehicle-dimension-rate.index')->with('rates', $rates);

        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       try{  
        $data['vehicleDimensions'] = TransportVehicleDimension::activeVehicleDimensions();
            return view('admin.pages.vehicle-dimension-rate.create', $data);

       }catch(Exception $e){
           return $e;
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $rule =  (new VehicleDimensionRateRule($request))->getDimensionRateValidationRules('create');
        $validator = Validator::make($request->all(),$rule[0],$rule[1]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }    
        try {
            $VehicleDimension = $this->rateService->save($request);
            if ($VehicleDimension) {
                return redirect()->route('admin.vehicles.rates.index')->with('flash_message_success', 'Data Created Successfuly!');
            } else {
                return redirect()->route('admin.vehicles.rates.index')->with('flash_message_error', 'Error!!');
            }
        } catch (Exception $e) {
            // $sqlState = $e->errorInfo[0];
            // $errorCode = $e->errorInfo[1];
            // if($sqlState == 23000 && $errorCode == 1062){
            //     return redirect()->back()->with('flash_message_error', "Duplicate Enrty .P lease check.")->withInput();
            // }
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleDimensionRate $rate)
    {
        try{
            return view('admin.pages.vehicle-dimension-rate.show')->with('rate', $rate);
        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(VehicleDimensionRate $rate)
    {
        try{
            $data['vehicleDimensions'] = TransportVehicleDimension::activeVehicleDimensions();
            return view('admin.pages.vehicle-dimension-rate.edit', $data)->with('rate', $rate);

       }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleDimensionRate $rate)
    {
        $rule =  (new VehicleDimensionRateRule($request))->getDimensionRateValidationRules('update', $rate->id);
        $validator = Validator::make($request->all(),$rule[0],$rule[1]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $VehicleDimension = $this->rateService->update($request, $rate);
            if ($VehicleDimension) {
                return redirect()->route('admin.vehicles.rates.index')->with('flash_message_success', 'Data Updated Successfuly!');
            } else {
                return redirect()->route('admin.vehicles.rates.index')->with('flash_message_error', 'Error!!');
            }
        } catch (Exception $e) {
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehicleDimensionRate $rate)
    {
        $this->rateService->destroyRate($rate);
        return redirect()->back()->with('flash_message_success', 'Operation Completed Successfully');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductCategoryRate;
use App\Models\ProductCategory;
use App\Models\Country;

use App\ValidationRules\CategoryRateRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Repositories\ProductCategoryRate\CategoryRateInterface;
use App\Traits\WeightRangeService;
use App\Services\CompareWeightRangeService;
use Exception;

class ProductCategoryRateController extends Controller
{
    use WeightRangeService;

    private $catergoryRateRepository;

    public function __construct(CategoryRateInterface $catergoryRateRepository){
            $this->catergoryRateRepository=$catergoryRateRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryRates=$this->catergoryRateRepository->all();
        return view('admin.pages.product-category-rate.index')->with('categoryRate', $categoryRates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categorys=$this->catergoryRateRepository->findByFlag();
        return view('admin.pages.product-category-rate.create')

        ->with('category', $categorys);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $validator = Validator::make($request->all(),(new CategoryRateRule($request))->getCategoryRateValidationRules('create'));

        if ($validator->fails()) {
           // session()->flash('flash_message_error','Please enter  Integer only !!');
            return redirect()->back()
                 ->withErrors($validator)
                 ->withInput();
        }
        $data=$request->except('_token');
        $min = $data['min_weight'];
        $max =$data['max_weight'];

        try{

            $max=$this->testWeightRange($min,$max);
            $categoryRate=$this->catergoryRateRepository->save($data, $min, $max);
        }catch (\Exception $exception){
            session()->flash('flash_message_error',$exception->getMessage());
            return redirect()->back();
        }
        
        if($categoryRate){
            //return redirect()->route('admin.rate.index')->with('message', 'Data Create Successfuly!');
                session()->flash('flash_message_success','Data Store Successfuly!!');
                return redirect()->route('admin.rates.index');
        }
        else{
                session()->flash('flash_message_error','Error!!');
                return redirect()->route('admin.rates.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($productCategoryId)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($caId, $coId )
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new CategoryRateRule($request))->getCategoryRateValidationRules('update'));

        if ($validator->fails()) {
            //session()->flash('flash_message_error','Please enter  Integer only !!');
            return redirect()->back()
                 ->withErrors($validator)
                 ->withInput();
        }
        $data=$request->except('_token');
        $min = $data['min_weight'];
        $max =$data['max_weight'];

        try{
            $max=$this->testWeightRange($min,$max);
            $this->catergoryRateRepository->update($data, $min, $max, $id);
            $this->catergoryRateRepository->deletePerviousData($data);
                session()->flash('flash_message_success','Data Update Successfuly!!');
                return redirect()->route('admin.rates.index');
        }catch (\Exception $exception){
            session()->flash('flash_message_error',$exception->getMessage());
            return redirect()->back()
                 ->withErrors($validator)
                 ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getProductCategoryRateEdit($productCategoryId){
        $categoryRates=$this->catergoryRateRepository->findByProductCategoryId($productCategoryId);
        $categorys=$this->catergoryRateRepository->findByFlag();
  
        return view('admin.pages.product-category-rate.edit')
     
        ->with('category', $categorys)
        ->with('categoryRates', $categoryRates);
    }
    public function getProductCategoryRateShow($productCategoryId){
        $categoryRates=$this->catergoryRateRepository->findByProductCategoryId($productCategoryId);
        // $categoryRateSingle=$this->catergoryRateRepository->findByCountryIdFirst($coId, $caId);
        return view('admin.pages.product-category-rate.show')
                ->with('categoryRates', $categoryRates);
    }
}

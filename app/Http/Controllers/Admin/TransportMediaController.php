<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\TransportMedia\TransportMediaInterface;
use App\ValidationRules\TransportMediaRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;



class TransportMediaController extends Controller
{
   
    protected $transRepo;
    //Initialize the Controller with TransportMediaRepository
    public function __construct(TransportMediaInterface $transRepo){
        $this->transRepo = $transRepo;
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $transportMedias = $this->transRepo->all();
        return view('admin.pages.transport-media.index')->with('transportMedias', $transportMedias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.transport-media.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new TransportMediaRule($request))->getTransportMediaValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=$request->except('_token');
        $data['slug']= Str::slug($data['name'] , '-');
        $transportMedia=$this->transRepo->save($data);
        if($transportMedia){
            return redirect()->route('admin.medias.index')->with('flash_message_success', 'Data Created Successfuly!');
        }
        else{
            return redirect()->route('admin.medias.index')->with('flash_message_error', 'Error!!');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transportMedia = $this->transRepo->find($id);
        return view('admin.pages.transport-media.show')->with('transportMedia', $transportMedia);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transportMedia = $this->transRepo->find($id);
        return view('admin.pages.transport-media.edit')->with('transportMedia', $transportMedia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new TransportMediaRule($request))->getTransportMediaValidationRules('update', $id));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=$request->except('_token');
        $data['slug']= Str::slug($data['name'] , '-');
        $transportMedia=$this->transRepo->update($data, $id);
        if($transportMedia){
            return redirect()->route('admin.medias.index')->with('flash_message_success', 'Data Updated Successfuly!');
        }
        else{
            return redirect()->route('admin.medias.index')->with('flash_message_error', 'Error!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->transRepo->changeStatus($id);
        return back()->with('flash_message_success', 'Operation Completed Successfully');
    }

    

}

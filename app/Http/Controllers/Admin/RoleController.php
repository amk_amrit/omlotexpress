<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\models\Office;
use App\Models\Role;
use App\Services\RoleService;
use App\ValidationRules\RoleRule;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;



class RoleController extends Controller
{
    protected $roleService;
    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $roles = Role::findByoffice(authEmployee()->office);
            return view('admin.pages.roles.index')->with('roles', $roles);
        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            //$permissions = $this->roleService->getPermissionByEmployee(authEmployee());
            $permissionGroups = $this->roleService->getGroupedPermissionOfEmployee(authEmployee());
            return view('admin.pages.roles.create')->with('permissionGroups', $permissionGroups);
        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new RoleRule($request))->getRoleValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();    
        }

        if(Role::checkSlug($request->name)){
            return redirect()->back()->withErrors(['name'=>'Name is already taken in this office'])->withInput();    
        }

        DB::beginTransaction();
        try{
            $this->roleService->store($request);
            DB::commit();
            return redirect()->route('admin.roles.index')->with('flash_message_success', 'Roles Created Successfully');
        }catch(Exception $e){
            DB::rollback();
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        try{
            return view('admin.pages.roles.show')->with('role', $role);
        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        try{
            $permissions = $this->roleService->getGroupedPermission(authEmployee());
            return view('admin.pages.roles.edit')->with('permissions', $permissions)->with('role', $role);
        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $validator = Validator::make($request->all(),(new RoleRule($request))->getRoleValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();    
        }

        if(Role::checkSlug($request->name, $role->id )){
            return redirect()->back()->withErrors(['name'=>'Name is already taken in this office'])->withInput();    
        }

        DB::beginTransaction();
        try{
            $this->roleService->update($request, $role->id);
            DB::commit();
            return redirect()->route('admin.roles.index')->with('flash_message_success', 'Roles Updated Successfully');
        }catch(Exception $e){
            DB::rollback();
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PackageSensitivity;
use App\Models\PackageSensitivityRate;
use App\Services\PackageSensitivityRateService;
use App\ValidationRules\PackageSensitivityRateRule;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PackageSensitivityRateController extends Controller
{
    protected $rateService;
    public function __construct(PackageSensitivityRateService $rateService)
    {
        $this->rateService = $rateService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $rates = $this->rateService->index();
            return view('admin.pages.package-sensitivity-rate.index')->with('rates', $rates);
        } catch (Exception $e) {
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $data['packageSensitivities'] = PackageSensitivity::getActiveSensitivities();
            return view('admin.pages.package-sensitivity-rate.create', $data);
        } catch (Exception $e) {
            return $e;
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rule = (new PackageSensitivityRateRule($request))->getPackageSensitivityRateValidationRules('create');
        $validator = Validator::make($request->all(), $rule[0], $rule[1]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $packageSensitivity = $this->rateService->save($request);
            if ($packageSensitivity) {
                return redirect()->route('admin.package.sensitivity.rates.index')->with('flash_message_success', 'Data Created Successfully!');
            } else {
                return redirect()->route('admin.package.sensitivity.rates.index')->with('flash_message_error', 'Error!!');
            }
        } catch (Exception $e) {
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PackageSensitivityRate $rate)
    {
        try {
            return view('admin.pages.package-sensitivity-rate.show')->with('rate', $rate);
        } catch (Exception $e) {
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PackageSensitivityRate $rate)
    {
        try {
            $data['packageSensitivities'] = PackageSensitivity::getActiveSensitivities();
            return view('admin.pages.package-sensitivity-rate.edit', $data)->with('rate', $rate);
        } catch (Exception $e) {
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PackageSensitivityRate $rate)
    {
        $rule = (new PackageSensitivityRateRule($request))->getPackageSensitivityRateValidationRules('update', $rate->id);
        $validator = Validator::make($request->all(), $rule[0], $rule[1]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $packageSensitivity = $this->rateService->update($request, $rate);
            if ($packageSensitivity) {
                return redirect()->route('admin.package.sensitivity.rates.index')->with('flash_message_success', 'Data Updated Successfuly!');
            } else {
                return redirect()->route('admin.package.sensitivity.rates.index')->with('flash_message_error', 'Error!!');
            }
        } catch (Exception $e) {
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PackageSensitivityRate $rate)
    {
        $this->rateService->destroyRate($rate);
        return redirect()->back()->with('flash_message_success', 'Operation Completed Successfully');
    }
}

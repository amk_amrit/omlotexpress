<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\PackageTypeService;
use Illuminate\Http\Request;
use App\Repositories\PackageType\PackageTypeInterfaces;

use App\Models\PackageType;

use App\ValidationRules\PackageTypeRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;



class PackageTypeController extends Controller
{
    private $packageTypeService;

    public function __construct(PackageTypeService $packageTypeService)
    {
        $this->packageTypeService = $packageTypeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$packageTypes=$this->packageTypeRepository->all();
        $packageTypes = $this->packageTypeService->paginatedPackageTypes();
        return view('admin.pages.package-type.index', compact('packageTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.package-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), (new PackageTypeRule($request))->getPackageTypeValidationRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data = $request->except('_token');
        $packageTypes = $this->packageTypeService->save($request);
        if ($packageTypes) {
            session()->flash('flash_message_success', 'Data Create Successfuly!!');
            return redirect()->route('admin.package.types.index');
        } else {
            session()->flash('flash_message_error', 'Error!!');
            return redirect()->route('admin.package.types.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $packageType = $this->packageTypeService->findById($id);
        return view('admin.pages.package-type.show')->with('packageTypes', $packageType);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $packageTypes = $this->packageTypeService->findById($id);
        return view('admin.pages.package-type.edit')->with('packageType', $packageTypes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), (new PackageTypeRule($request))->getPackageTypeValidationRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data = $request->except('_token');
        $packageType = $this->packageTypeService->update($request, $id);
        if ($packageType) {
            session()->flash('flash_message_success', 'Data Update Successfuly!!');
            return redirect()->route('admin.package.types.index');
        } else {
            session()->flash('flash_message_error', 'Error!!');
            return redirect()->route('admin.package.types.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $packageType = $this->packageTypeService->delete($id);
        session()->flash('flash_message_success', 'Operation Successfuly!!');
        return redirect()->route('admin.package.types.index');
    }
}

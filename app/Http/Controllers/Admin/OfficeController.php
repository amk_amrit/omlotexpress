<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Repositories\Office\OfficeInterface;
use App\Services\OfficeService;
use Illuminate\Http\Request;
use App\ValidationRules\OfficeRule;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OfficeController extends Controller
{
    protected $officeRepo, $officeService;
    public function __construct(OfficeInterface $officeRepo, OfficeService $officeService){
        $this->officeRepo = $officeRepo;
        $this->officeService = $officeService;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $offices = $this->officeService->getOfficesByEmployee(auth()->user()->employee);
            
            return view('admin.pages.office.index')->with('offices', $offices);
        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            $countries = Country::getActiveCountries();
            return view('admin.pages.office.create')->with('countries', $countries);
        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new OfficeRule($request))->getOfficeValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        try{

            DB::beginTransaction();
            $this->officeService->storeOffice($request);
            DB::commit();
            return redirect()->route('admin.offices.index')->with('flash_message_success', 'Office Created Successfuly!');
            
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->back()->with('flash_message_danger', $e->getMessage())->withInput();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $office = $this->officeRepo->find($id);
            return view('admin.pages.office.show')->with('office', $office);
        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $countries = Country::getActiveCountries();
            $office = $this->officeRepo->find($id);
            return view('admin.pages.office.edit')->with('office', $office)->with('countries', $countries);

        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $validator = Validator::make($request->all(),(new OfficeRule($request))->getOfficeValidationRules('update', $id));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        try{
            DB::beginTransaction();
            $office = $this->officeService->updateOffice($request, $id);
            DB::commit();

        }catch(Exception $e){
            DB::rollBack();
            return redirect()->back()->with('flash_message_error', $e->getMessage());
            
        }
        
        return redirect()->route('admin.offices.index')->with('flash_message_success', 'Office Updated Successfuly!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->officeRepo->changeStatus($id);
            return back()->with('flash_message_success', 'Operation Completed Successfully');
        }catch(Exception $e){
            return back()->with('flash_message_error', $e->getMessage());
        }
        
    }
}

<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\Transit\TransitInterface;
use App\ValidationRules\TransitRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class TransitController extends Controller
{
    private $transitRepository;

    public function __construct(TransitInterface $transitRepository){
            $this->transitRepository=$transitRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $transit=$this->transitRepository->all();
        return view('admin.pages.courier.transit.index')->with('transits' ,$transit);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $transitClass=$this->transitRepository->transitClass();
        $country=$this->transitRepository->country();
        $office=$this->transitRepository->office();
        return \view('admin.pages.courier.transit.create')
        ->with('transitclass', $transitClass)
        ->with('countries', $country)
        ->with('offices', $office);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),(new TransitRule($request))->getTransitRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        //dd($request->all());
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $transit=$this->transitRepository->save($data);
        DB::commit();
        session()->flash('flash_message_success', 'Data Store Successfuly!!');
        return redirect()->route('admin.transits.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $transit=$this->transitRepository->findById($id);
        return view('admin.pages.courier.transit.show')->with('transits', $transit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $transit=$this->transitRepository->findById($id);
        $transitClass=$this->transitRepository->transitClass();
        $country=$this->transitRepository->country();
        $office=$this->transitRepository->office();
        return view('admin.pages.courier.transit.edit')
                    ->with('transits', $transit)
                    ->with('transitclass', $transitClass)
                    ->with('countries', $country)
                    ->with('offices', $office);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new TransitRule($request))->getTransitRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        //return  $request->all();
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $transit=$this->transitRepository->update($data, $id);
        DB::commit();
        session()->flash('flash_message_success', 'Data Update Successfuly!!');
        return redirect()->route('admin.transits.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try{
        $transit=$this->transitRepository->delete($id);
        DB::commit();
        session()->flash('flash_message_success', 'Operation Successfuly!!');
        return redirect()->route('admin.transits.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }
}

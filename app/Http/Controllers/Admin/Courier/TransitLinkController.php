<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\TransitLink\TransitLinkInterface;
use App\ValidationRules\TransitLinkRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class TransitLinkController extends Controller
{
    private $transitLinkRepo;
    public function __construct(TransitLinkInterface $transitLinkRepo){
        $this->transitLinkRepo=$transitLinkRepo;
    }
        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $getTransitLinkData=$this->transitLinkRepo->all();
        return view('admin.pages.courier.transit-link.index')->with('transitLinkData' ,$getTransitLinkData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $getTranst=$this->transitLinkRepo->transit();
        return view('admin.pages.courier.transit-link.create')->with('transits' ,$getTranst);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new TransitLinkRule($request))->getTransitLinkRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $storeTrasnitData=$this->transitLinkRepo->save($data);
        DB::commit();
        session()->flash('flash_message_success', 'Data Store Successfuly!!');
        return redirect()->route('admin.transit-links.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $transit=$this->transitLinkRepo->transit();
        $getTransitLinkData=$this->transitLinkRepo->findById($id);
        return view('admin.pages.courier.transit-link.edit')
        ->with('transit_id', $id)
        ->with('transits', $transit)
        ->with('transitLinkDatas' ,$getTransitLinkData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new TransitLinkRule($request))->getTransitLinkRules('update'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $storeTransitLinkData=$this->transitLinkRepo->update($data);
        DB::commit();
        session()->flash('flash_message_success', 'Data Update Successfuly!!');
        return redirect()->route('admin.transit-links.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function transitLinkData(Request $request){
        //
        $transit_id=$request->transit_id;
        $getTransitData=$this->transitLinkRepo->transitLinkData($transit_id);
        if ($request->ajax()) {
            return view('admin.pages.courier.transit-link.form',compact('getTransitData', 'transit_id' ))->render();
        }

        return response()->json(['getTransitData'=>$getTransitData],201);      
    }

}

<?php

namespace App\Http\Controllers\Admin\Courier;
use App\Repositories\Courier\CourierClass\CourierClassInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ValidationRules\CourierClassRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class CourierClassController extends Controller
{
    private $CourierClassRepository;

    public function __construct(CourierClassInterface $CourierClassRepository){
            $this->CourierClassRepository=$CourierClassRepository;
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $courierClass=$this->CourierClassRepository->all();
       // return $courierClass;
        return view('admin.pages.courier.courier-class.index')->with('courierClasses', $courierClass);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.courier.courier-class.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $rule = (new CourierClassRule($request))->getCourierClassRules('create');
        $validator = Validator::make($request->all(), $rule[0], $rule[1]);
       // $validator = Validator::make($request->all(),(new CourierClassRule($request))->getCourierClassRules('create'));

        if ($validator->fails()) {
            if ($request->ajax()){
                return response()->json(['error'=>$validator->errors()->all()],'422');
                //return response()->json(['errors' => $validator->errors(),'error' => true],'422');

            }
        }
        //
        if($validator->passes()){
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $transitClass=$this->CourierClassRepository->save($data);
        DB::commit();
        session()->flash('flash_message_success', 'Data Store Successfuly!!');
        return response()->json(['Success',200]);   
     }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $courierClass=$this->CourierClassRepository->findById($id);
        return view('admin.pages.courier.courier-class.edit')->with('courierClasses', $courierClass);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //$validator = Validator::make($request->all(),(new CourierClassRule($request))->getCourierClassRules('update', $id));
        $rule = (new CourierClassRule($request))->getCourierClassRules('update', $id);
        $validator = Validator::make($request->all(), $rule[0], $rule[1]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $transitClass=$this->CourierClassRepository->update($data, $id);
        DB::commit();
        session()->flash('flash_message_success', 'Data Update Successfuly!!');
        return redirect()->route('admin.courier-classes.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try{
        $courierClass=$this->CourierClassRepository->delete($id);
        DB::commit();
        session()->flash('flash_message_success', 'Data Update Successfuly!!');
        return redirect()->route('admin.courier-classes.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }
}

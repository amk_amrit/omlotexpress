<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\AgencyLink\AgencyLinkInterface;
use App\ValidationRules\AgencyLinkRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;
class AgencyLinkController extends Controller
{
    private $agencyLinkRepo;
    public function __construct(AgencyLinkInterface $agencyLinkRepo)
    {
        $this->agencyLinkRepo=$agencyLinkRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $agencyLink=$this->agencyLinkRepo->all();
        return view('admin.pages.courier.agency-link.index')->with('agencyLinks' ,$agencyLink);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $agency=$this->agencyLinkRepo->agency();
        return view('admin.pages.courier.agency-link.create')->with('agencies', $agency);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new AgencyLinkRule($request))->getAgencyLinkRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $agencyLinkStoreData=$this->agencyLinkRepo->save($data);
        DB::commit();
        session()->flash('flash_message_success','Data Store Successfuly!!');
        return redirect()->route('admin.agency-links.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $agency=$this->agencyLinkRepo->agency();
        $getAgencyLinkDatas=$this->agencyLinkRepo->findById($id);
        return view('admin.pages.courier.agency-link.edit')
        ->with('agency_id', $id)
        ->with('getAgencyData',$getAgencyLinkDatas)
        ->with('agencies', $agency);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new AgencyLinkRule($request))->getAgencyLinkRules('update'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
       // return $data;
        $updateData=$this->agencyLinkRepo->update($data);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return redirect()->route('admin.agency-links.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function agencyLinkData(Request $request){
         $agency_id=$request->agency_id;
         $getAgencyData=$this->agencyLinkRepo->getAgancyData($agency_id);

         if ($request->ajax()) {
            return view('admin.pages.courier.agency-link.form',compact('getAgencyData', 'agency_id' ))->render();
        }

        return response()->json(['getAgencyData'=>$getAgencyData],201);      }
}

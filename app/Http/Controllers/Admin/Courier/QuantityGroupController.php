<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\QuantityGroup\QuantityGroupInterface;

use App\ValidationRules\QuantityGroupRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;


class QuantityGroupController extends Controller
{
    private $quantityGroupRepository;

    public function __construct(QuantityGroupInterface $quantityGroupRepository){
            $this->quantityGroupRepository=$quantityGroupRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $quantityGroup=$this->quantityGroupRepository->all();
        return view('admin.pages.courier.quantity-group.index')->with('quantitygroups', $quantityGroup);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $packageType=$this->quantityGroupRepository->packageType();
        $transportMedia=$this->quantityGroupRepository->transportMedia();
        return view('admin.pages.courier.quantity-group.create')
        ->with('transportmedias', $transportMedia)
        ->with('packagetypes', $packageType);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new QuantityGroupRule($request))->getQuantityGroupValidationRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        DB::beginTransaction();
        try{
            $data=$request->except('_token');
            $quantityGroup=$this->quantityGroupRepository->save($data);
             DB::commit();
            session()->flash('flash_message_success','Data Store Successfuly!!');
            return redirect()->route('admin.quantity.groups.index');

        }catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }
        
        //
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $packageType=$this->quantityGroupRepository->packageType();
        $transportMedia=$this->quantityGroupRepository->transportMedia();
        $quantityGroups=$this->quantityGroupRepository->findById($id);
        return view('admin.pages.courier.quantity-group.edit')
            ->with('quantityGroup',$quantityGroups)
            ->with('transportmedias', $transportMedia)
            ->with('packagetypes', $packageType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new QuantityGroupRule($request))->getQuantityGroupValidationRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $quantityGroup=$this->quantityGroupRepository->update($data, $id);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return redirect()->route('admin.quantity.groups.index');
        
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $quantityGroup=$this->quantityGroupRepository->delete($id);
        session()->flash('flash_message_success','Operation Successfuly!!');
        return redirect()->route('admin.quantity.groups.index');
    }
}

<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\VehicleAgentCharge\VehicleAgentChargeInterface;
use App\ValidationRules\VehicleAgentChargeRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;
use Redirect;

class VehicleAgentChargeController extends Controller
{
    private $vehicleChargeRepository;

    public function __construct(VehicleAgentChargeInterface $vehicleChargeRepository){
            $this->vehicleChargeRepository=$vehicleChargeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $vehicleAgenctCharge=$this->vehicleChargeRepository->all();
        return view('admin.pages.courier.vehicle-agent-charge.index')->with('vehicleAgenctCharges' , $vehicleAgenctCharge);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $country=$this->vehicleChargeRepository->getCountry();
        $vehicle=$this->vehicleChargeRepository->getVehicle();
        $timeTravelGroup=$this->vehicleChargeRepository->getTimeTravelGroup();
        return view('admin.pages.courier.vehicle-agent-charge.create')
        ->with('countries' , $country)
        ->with('vehicles' ,$vehicle)
        ->with('timeTravelGroupes' , $timeTravelGroup);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new VehicleAgentChargeRule($request))->getVehicleAgentChargeRules('create'));

        if ($validator->fails()) {
            //dd($request->all());
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        
        $data=$request->except('_token');
        $vehicleAgenctCharge=$this->vehicleChargeRepository->save($data);
        session()->flash('flash_message_success', 'Data create Successfuly!!');
        return redirect()->route('admin.vehicle-agent-charges.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $vehicleAgenctCharge=$this->vehicleChargeRepository->findById($id);
        return view('admin.pages.courier.vehicle-agent-charge.show')->with('vehicleAgenctCharges' , $vehicleAgenctCharge);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $vehicleAgenctCharges=$this->vehicleChargeRepository->findById($id);
        return view('admin.pages.courier.vehicle-agent-charge.edit')->with('vehicleAgenctCharge' , $vehicleAgenctCharges);
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new VehicleAgentChargeRule($request))->getVehicleAgentChargeRules('update'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        $data=$request->except('_token');
        $vehicleAgenctCharge=$this->vehicleChargeRepository->update($data, $id);
        session()->flash('flash_message_success', 'Data edit Successfuly!!');
        return redirect()->route('admin.vehicle-agent-charges.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $vehicleAgenctCharge=$this->vehicleChargeRepository->delete($id);
        session()->flash('flash_message_success', 'Operation Successfuly!!');
        return redirect()->route('admin.vehicle-agent-charges.index');
    }
    public function getVehicleAgentCreateData(){
        $country=$this->vehicleChargeRepository->getCountry();
        $vehicle=$this->vehicleChargeRepository->getVehicle();
        $timeTravelGroup=$this->vehicleChargeRepository->getTimeTravelGroup();
        return view('admin.pages.courier.vehicle-agent-charge.create-data')
        ->with('countries' , $country)
        ->with('vehicles' ,$vehicle)
        ->with('timeTravelGroupes' , $timeTravelGroup);
    }
    public function vehicleAgentChargeGetData(Request $request){
         $data=$request->except('_token');
         $vehicleData=$this->vehicleChargeRepository->getVehicleData($data);
         return view('admin.pages.courier.vehicle-agent-charge.create')
         ->with('country_id', $data['country_id'])
         ->with('vehicle_id' , $data['vehicle_id'])
         ->with('type', $data['time_trave_group_id'])
         ->with('vehicleDatas',$vehicleData);
    }
}

<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\AgencyTransitVehicleCharge\AgencyTransitVehicleChargeInterface;

use App\ValidationRules\AgencyTransitVehicleChargeRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class AgencyTransitVehicleChargeController extends Controller
{
    private $agencyTransitVehicleChargeRepositorie;
    public function __construct(AgencyTransitVehicleChargeInterface $agencyTransitVehicleChargeRepositorie)
    {
        $this->agencyTransitVehicleChargeRepositorie=$agencyTransitVehicleChargeRepositorie;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        dd(1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return response()->json($request->all());

        $agencyTransitConnectionId=$request->agency_transit_connection_id;
         $quantityGroupId=$request->quantity_group_id;
         $rule = (new AgencyTransitVehicleChargeRule($request))->getAgencyTransitVehicleChargeRules('create', $agencyTransitConnectionId, $quantityGroupId );
         $validator = Validator::make($request->all(), $rule[0], $rule[1]);
         //return response()->json($request->all());
        
         if ($validator->fails()) {
             if ($request->ajax()){
                 return response()->json(['error'=>$validator->errors()->all()],'422'); 
             }
         }
         
         else{
             
             try {
                 DB::beginTransaction();
                $data = $request->except('_token');
                $airportCharge = $this->agencyTransitVehicleChargeRepositorie->save($data);
                   DB::commit();
               session()->flash('flash_message_success', 'Data Store Successfuly!!');
                return response()->json(['sucess'=>'success'],'200');            
            } catch (\Exception $exception) {
                DB::rollback();
                return response()->json(['error'=>'Invalite Data !! Please Enter proper data !!'],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $agencyTransitLinkName=$this->agencyTransitVehicleChargeRepositorie->agencyTransitLinkName($id);
        $getquanityGroupName=$this->agencyTransitVehicleChargeRepositorie->quantityGroupName();
       // return $agencyTransitLinkName;
        return view('admin.pages.courier.agency-transit-vehicle-charge.create')
                ->with('agencyTransitLinkNames', $agencyTransitLinkName)
                ->with('getquanityGroupNames', $getquanityGroupName);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //$id=1;
        $agencyTransitLinkName=$this->agencyTransitVehicleChargeRepositorie->agencyTransitLinkName($id);
        $quantityGroupRange=$this->agencyTransitVehicleChargeRepositorie->quantityGroupRange($id);
        //return $quantityGroupRange;
        return view('admin.pages.courier.agency-transit-vehicle-charge.edit')
                ->with('agencyTransitConnectionId', $id)
                ->with('agencyTransitLinkNames', $agencyTransitLinkName)
                ->with('quantityGroupRanges', $quantityGroupRange);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $agencyTransitConnectionId=$id;
         $quantityGroupId=$request->quantity_group_id;
         $rule = (new AgencyTransitVehicleChargeRule($request))->getAgencyTransitVehicleChargeRules('update', $agencyTransitConnectionId, $quantityGroupId );
         $validator = Validator::make($request->all(), $rule[0], $rule[1]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } 
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $agencyTransitVehicleCharge=$this->agencyTransitVehicleChargeRepositorie->update($data, $id);
        DB::commit();

        session()->flash('flash_message_success','Data Update Successfuly!!');
            return redirect()->route('admin.agency-offices.index');
        }catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getQuantityGroupRangedata(Request $request){
        //return response()->json($request->all());
        $data=$request->except('_token');
        $quantityGroupRange=$this->agencyTransitVehicleChargeRepositorie->getQuantityGroupRange($data);
        //return response()->json($quantityGroupRange);
         if ($request->ajax()) {
             return view('admin.pages.courier.agency-transit-vehicle-charge.agencyTransitVehicleChargeForm',compact('quantityGroupRange' ))->render();
         }    
    }

}

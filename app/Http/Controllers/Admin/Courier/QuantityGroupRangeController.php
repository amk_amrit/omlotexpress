<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\QuantityGroupRange\QuantityGroupRangeInterface;

use App\ValidationRules\QuantityGroupRangeRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;
use App\Traits\WeightRangeService;
use App\Services\CompareWeightRangeService;
use Illuminate\Database\QueryException;

class QuantityGroupRangeController extends Controller
{
    use WeightRangeService;
    private $quantityGroupRangeRepository;

    public function __construct(QuantityGroupRangeInterface $quantityGroupRangeRepository){
            $this->quantityGroupRangeRepository=$quantityGroupRangeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $quantityGroupRange=$this->quantityGroupRangeRepository->all();
        $quantityGroup=$this->quantityGroupRangeRepository->quanityGroup($quantityGroupRange);
        //return $quantityGroup;
        return view('admin.pages.courier.quantity-group-range.index')
                    ->with('quantityGroups', $quantityGroup)
                    ->with('quantityGroupRanges', $quantityGroupRange);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $quantityGroup=$this->quantityGroupRangeRepository->quanityGroup();
        return view('admin.pages.courier.quantity-group-range.create')
                    ->with('quantityGroups', $quantityGroup);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $qgid=$request->quantity_group_id;
        $rule = (new QuantityGroupRangeRule($request))->getQuantityGroupRangeValidationRules('update', $qgid);
         $validator = Validator::make($request->all(), $rule[0], $rule[1]);

        if ($validator->fails()) {
            if ($request->ajax()){
                return response()->json(['error'=>$validator->errors()->all()],'422');
                //return response()->json(['errors' => $validator->errors(),'error' => true],'422');

            }
        }

        else{
        $data=$request->except('_token');
        $min = $data['min_weight'];
        $max =$data['max_weight'];
            try{
        $max=$this->testWeightRange($min,$max);
                 try{
                    $this->quantityGroupRangeRepository->save($data, $min, $max);
                    
                    session()->flash('flash_message_success', 'Data Store Successfuly!!');
                     return response()->json(['Quantity Group Range Updated',200]);            
                 }catch(\Exception $exception){
                    if($exception instanceof QueryException){
                        $sqlState = $exception->errorInfo[0];
                        $errorCode = $exception->errorInfo[1];
                        if($sqlState === "23000" && $errorCode == 1062){
                            return response()->json(['error'=>'Duplicate Entry Problem'],500);
                        }
                        return response()->json(['error'=>'Something went Wrong !!'],500);
                     }
                    return response()->json(['error'=>$exception->getMessage(),'file' => $exception->getFile(),'line' => $exception->getLine()],500); 
                 }
         }catch (\Exception $exception){
            return response()->json(['error'=>$exception->getMessage()],500);
         }
        


     }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $quantityGroupRanges=$this->quantityGroupRangeRepository->findById($id);
        $quantityGroupNameID=$this->quantityGroupRangeRepository->findSingleById($id);
        //return $quantityGroupNameID;
        return view('admin.pages.courier.quantity-group-range.edit')
                       ->with('quantityGroupNameID', $quantityGroupNameID)
                       ->with('quantityGroupRange', $quantityGroupRanges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        // $validator = Validator::make($request->all(),(new QuantityGroupCountryRule($request))->getQuantityGroupCountryValidationRules('update'));

        // if ($validator->fails()) {
        //     return redirect()->back()
        //         ->withErrors($validator)
        //         ->withInput();
        // }
        // $data=$request->except('_token');
        // $min = $data['min_weight'];
        // $max =$data['max_weight'];

        // try{
        //     $max=$this->testWeightRange($min,$max);
        //     $quantityGroupCountry=$this->quantityGroupRangeRepository->update($data, $min, $max, $id);
        //     $deletePreviousData=$this->quantityGroupRangeRepository->deletePerviousData($data);
        //         session()->flash('flash_message_success','Data Update Successfuly!!');
        //         return redirect()->route('admin.quantity.countries.index');
        // }catch (\Exception $exception){
        //     session()->flash('flash_message_error','Please Enter Proper range !!');
        //     return redirect()->back()
        //          ->withErrors($validator)
        //          ->withInput();
        // }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function quantityGroupCountryShow($coId, $qaId){
        // $quantityGroupCountry=$this->quantityGroupRangeRepository->findById($coId, $qaId);
        // $quantityGroupCountryAll=$this->quantityGroupRangeRepository->findAllById($coId, $qaId);
        // return view('admin.pages.courier.quantity-group-country.show')
        //         ->with('quantityGroupCountrys', $quantityGroupCountry)
        //         ->with('quantityGroupCountryAlls', $quantityGroupCountryAll);
    }
    public function quantityGroupCountryEdit($coId, $qaId){
        // $quantityGroupCountry=$this->quantityGroupRangeRepository->findById($coId, $qaId);
        // $quantityGroupCountryAll=$this->quantityGroupRangeRepository->findAllById($coId, $qaId);
        // $quantityGroup=$this->quantityGroupRangeRepository->quanityGroup();
        // $country=$this->quantityGroupRangeRepository->country();
        // return view('admin.pages.courier.quantity-group-country.edit')
        //         ->with('quantityGroupCountries', $quantityGroupCountry)
        //         ->with('quantityGroupCountryAlls', $quantityGroupCountryAll)
        //         ->with('quantityGroups', $quantityGroup)
        //         ->with('countries', $country);
    }
}

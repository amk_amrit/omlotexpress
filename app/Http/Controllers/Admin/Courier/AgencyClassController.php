<?php

namespace App\Http\Controllers\Admin\Courier;
use App\Repositories\Courier\AgencyClass\AgencyClassInterface;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ValidationRules\AgencyClassRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class AgencyClassController extends Controller
{
    private $agencyClassRepository;

    public function __construct(AgencyClassInterface $agencyClassRepository){
            $this->agencyClassRepository=$agencyClassRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $agencyClass=$this->agencyClassRepository->all();
        return view('admin.pages.courier.agency-class.index')->with('agencyClasses', $agencyClass);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.courier.agency-class.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new AgencyClassRule($request))->getAgencyClassRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $agencyClass=$this->agencyClassRepository->save($data);
        DB::commit();
        session()->flash('flash_message_success','Data Store Successfuly!!');
        return redirect()->route('admin.class.agencies.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $agencyClass=$this->agencyClassRepository->findById($id);
        return view('admin.pages.courier.agency-class.edit')->with('agencyClasses', $agencyClass);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new AgencyClassRule($request))->getAgencyClassRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $agencyClass=$this->agencyClassRepository->update($data , $id);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return redirect()->route('admin.class.agencies.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try{
        $agencyClass=$this->agencyClassRepository->delete($id);
        DB::commit();
        session()->flash('flash_message_success','Operation Successfuly!!');
        return redirect()->route('admin.class.agencies.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }
}

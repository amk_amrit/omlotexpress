<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use App\Repositories\Courier\Agency\AgencyInterface; 
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\Session;

use App\ValidationRules\AgencyRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class AgencyController extends Controller
{
    private $agencyRepository;

    public function __construct(AgencyInterface $agencyRepository){
            $this->agencyRepository=$agencyRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $agency=$this->agencyRepository->all();
       // return $agency;
        return view('admin.pages.courier.agency.index')->with('agencies', $agency);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $country=$this->agencyRepository->country();
        $agencyClass=$this->agencyRepository->agencyClass();
        $transit=$this->agencyRepository->transit();
        return view('admin.pages.courier.agency.create')
        ->with('tranit', $transit)
        ->with('countries', $country)
        ->with('agencyClass', $agencyClass);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new AgencyRule($request))->getAgencyRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $agency=$this->agencyRepository->save($data);
        DB::commit();
        session()->flash('flash_message_success','Data Store Successfuly!!');
        return redirect()->route('admin.agencies.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $agency=$this->agencyRepository->findById($id);
        return view('admin.pages.courier.agency.show')->with('agencies', $agency);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $agency=$this->agencyRepository->findById($id);
        $agencyClass=$this->agencyRepository->agencyClass();
        $transit=$this->agencyRepository->transit();
        return view('admin.pages.courier.agency.edit')
        ->with('transits', $transit)
        ->with('agencies', $agency)
        ->with('agencyClass', $agencyClass);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request->all();
        $validator = Validator::make($request->all(),(new AgencyRule($request))->getAgencyRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $agency=$this->agencyRepository->update($data, $id);
        DB::commit();
        session()->flash('flash_message_success','Data update Successfuly!!');
        return redirect()->route('admin.agencies.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try{
        $agency=$this->agencyRepository->delete($id);
        DB::commit();
        session()->flash('flash_message_success','Data Store Successfuly!!');
        return redirect()->route('admin.agencies.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }
}

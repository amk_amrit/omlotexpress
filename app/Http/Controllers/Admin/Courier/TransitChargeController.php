<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\TransitCharge\TransitChargeInterface;

class TransitChargeController extends Controller
{
    private $transitChargeRepository;

    public function __construct(TransitChargeInterface $transitChargeRepository){
            $this->transitChargeRepository=$transitChargeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $transitCharge=$this->transitChargeRepository->all();
        return view('admin.pages.courier.transit-charge.index')->with('transitCharges', $transitCharge);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //$transitClass=$this->transitChargeRepository->transitClass();
        //$quantityGroupCountry=$this->transitChargeRepository->quantityGroupCountry();
        $transit=$this->transitChargeRepository->transit();
        return view('admin.pages.courier.transit-charge.create')
                    ->with('tranits' , $transit);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data=$request->except('_token');
        $quantityGroupCountry=$this->transitChargeRepository->quantityGroupCountry($data);
        return $quantityGroupCountry;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use App\Repositories\Courier\AdminCharge\AdminChargeInterface;
use Illuminate\Http\Request;

use App\ValidationRules\AdminChargeRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class AdminChargerController extends Controller
{
    private $adminChargeRepository;

    public function __construct(AdminChargeInterface $adminChargeRepository){
            $this->adminChargeRepository=$adminChargeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $adminCharge=$this->adminChargeRepository->all();
        //return $adminCharge;
        return view('admin.pages.courier.admin-charge.index')->with('admincharges', $adminCharge);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
       $office=$this->adminChargeRepository->office();
       $quantityGroupName=$this->adminChargeRepository->quantityGroupName();
         //return $quantityGroupName;
        return view('admin.pages.courier.admin-charge.create')
                    ->with('quantityGroupNameData', $quantityGroupName)
                    ->with('offices', $office);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return response()->json($request->all());

         $adminId=$request->airport_link_id;
         $quantityGroupId=$request->quantity_group_id;
         $rule = (new AdminChargeRule($request))->getAdminChargeRules('update',$adminId, $quantityGroupId );
         $validator = Validator::make($request->all(), $rule[0], $rule[1]);
         //return response()->json($request->all());
        
         if ($validator->fails()) {
             if ($request->ajax()){
                 return response()->json(['error'=>$validator->errors()->all()],'422'); 
             }
         }
         
         else{
             
             try {
                 DB::beginTransaction();
                $data = $request->except('_token');
                $airportCharge = $this->adminChargeRepository->save($data);
                  DB::commit();
               session()->flash('flash_message_success', 'Data Store Successfuly!!');
                return response()->json(['success'],'200');            
            } catch (\Exception $exception) {
                DB::rollback();
                return response()->json(['error'=>'Invalite Data !! Please Enter proper data !!'],500);
            }
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $adminCharge=$this->adminChargeRepository->findById($id);
        $adminName=$this->adminChargeRepository->officeName($id);
        //return $adminCharge; 
        return  view('admin.pages.courier.admin-charge.edit')
                    ->with('adminId', $id)
                    ->with('adminName', $adminName)
                    ->with('adminCharges', $adminCharge);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminId=$id;
        $quantityGroupId=0;
        $rule = (new AdminChargeRule($request))->getAdminChargeRules('create',$adminId, $quantityGroupId );
        $validator = Validator::make($request->all(), $rule[0], $rule[1]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } 
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $airportChargeUpdate=$this->adminChargeRepository->update($data, $id);
        DB::commit();

        session()->flash('flash_message_success','Data Update Successfuly!!');
            return redirect()->route('admin.admin-charges.index');
        }catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // DB::beginTransaction();
        // try{
        // $adminCharge=$this->adminChargeRepository->delete($id);
        // DB::commit();
        session()->flash('flash_message_success','Opeartion Successfuly!!');
        return redirect()->route('admin.admin-charges.index');
    // }catch (\Exception $exception){
    //     DB::rollback();
    //     return redirect()->back()
    //         ->withInput()
    //         ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    // }
    }
    public function adminChargeCreate(Request $request){

    $data=$request->except('_token');
    $createData=$this->adminChargeRepository->adminChargeCreateData($data);
    return view('admin.pages.courier.admin-charge.admin-charge-store')
    ->with('transportmedia_id', $data['transport_meida_id'])
    ->with('country_id', $data['country_id'])
    ->with('user_id', $data['user_id'])
    ->with('createData', $createData);
           
    }
    public function getQuantityGroupRangedata(Request $request){
        $data=$request->except('_token');
        $quantityGroupRange=$this->adminChargeRepository->getQuantityGroupRange($data);
        //return response()->json($quantityGroupRange);
         if ($request->ajax()) {
             return view('admin.pages.courier.airport-charge.quantityGroupRangeForm',compact('quantityGroupRange' ))->render();
         }    
    }
}

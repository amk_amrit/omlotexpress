<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Repositories\Courier\AirportLink\AirportLinkInterface;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ValidationRules\AirportLinkRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class AirportLinkController extends Controller
{
    private $airportLinkRepository;

    public function __construct(AirportLinkInterface $airportLinkRepository)
    {
        $this->airportLinkRepository = $airportLinkRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $airportLink = $this->airportLinkRepository->all();
        //return $airportLink;
        $quanityGroupName = $this->airportLinkRepository->quantiryName();
        return view('admin.pages.courier.airport-link.index')
            ->with('airportLinks', $airportLink);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $airports = $this->airportLinkRepository->airport();
        $country = $this->airportLinkRepository->country();
        // return $countries;
        return view('admin.pages.courier.airport-link.create')
            ->with('countries', $country)
            ->with('airports', $airports);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        // return response()->json($request->all());
         $country_id_one=$request->country_id_one;
         $country_id_two=$request->country_id_two;

        $rule = (new AirportLinkRule($request))->getAirportLinkRules('update',$country_id_one, $country_id_two );
         //return $rule;
         $validator = Validator::make($request->all(), $rule[0], $rule[1]);

        if ($validator->fails()) {
            if ($request->ajax()){
                return response()->json(['error'=>$validator->errors()->all()],'422');
                //return response()->json(['errors' => $validator->errors(),'error' => true],'422');

            }
        }
        //
        else{
             DB::beginTransaction();
             try {
                $data = $request->except('_token');
                $airportLink = $this->airportLinkRepository->save($data);
                  DB::commit();
                session()->flash('flash_message_success', 'Data Store Successfuly!!');
                return response()->json(['Success',200]);            
            } catch (\Exception $exception) {
                return response()->json(['error'=>'Invalite Data !! Please Select proper data !!'],500);
            }
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $airportLink = $this->airportLinkRepository->editData($id);
        return view('admin.pages.courier.airport-link.edit')->with('airportLinks', $airportLink);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), (new AirportLinkRule($request))->getAirportLinkRules('update'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try {
            $data = $request->except('_token');
            $airportLink = $this->airportLinkRepository->update($data, $id);
            DB::commit();
            session()->flash('flash_message_success', 'Data Update Successfuly!!');
            return redirect()->route('admin.airport-links.index');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage() . '.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try {
            $airportLink = $this->airportLinkRepository->delete($id);
            DB::commit();
            session()->flash('flash_message_success', 'Operation Successfuly!!');
            return redirect()->route('admin.airport-links.index');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage() . '.');
        }
    }
    public function getAirpotData(Request $request)
    {
        $data = $request->except('_token');
        $airports = $this->airportLinkRepository->findById($data);
        return view('admin.pages.courier.airport-link.create')
            ->with('country_id', $data['country'])
            ->with('quantitygroup_id', $data['quantity_id'])
            ->with('source_airport_id', $data['airport'])
            ->with('airport', $airports);
    }
    public function getDataCountryOne(Request $request)
    {
        $country_id = $request->country_id_one;
            $scope = $request->scope_one;
            $getDatas = $this->airportLinkRepository->getDataCountryOne($country_id, $scope);
           // return response()->json($getDatas);
            if ($request->ajax()) {
                return view('admin.pages.courier.airport-link.airportDropDownform',compact('getDatas' ))->render();
            }         
        } 
         
    public function getDataCountryTwo(Request $request)
    {
        $country_id = $request->country_id_two;
            $scope = $request->scope_two;
            $airport_one_id=$request->airport_one_id;
            $getDatas = $this->airportLinkRepository->getDataCountryTwo($country_id, $scope, $airport_one_id);
             return response()->json($getDatas);
            //  if ($request->ajax()) {
            //      return view('admin.pages.courier.airport-link.airportlinkDropDownform',compact('getDatas' ))->render();
            //  }        
     } 
         
}

<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\Service\ServiceInterface;

use App\ValidationRules\ServiceRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class ServiceController extends Controller
{
    private $serviceRepository;

    public function __construct(ServiceInterface $serviceRepository){
            $this->serviceRepository=$serviceRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $service=$this->serviceRepository->all();
        return view('admin.pages.courier.service.index')->with('services', $service);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.courier.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new ServiceRule($request))->getServiceRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $service=$this->serviceRepository->save($data);
        DB::commit();
        session()->flash('flash_message_success','Data Store Successfuly!!');
            return redirect()->route('admin.services.index');
        }catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $service=$this->serviceRepository->findById($id);
        return view('admin.pages.courier.service.edit')->with('services', $service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new ServiceRule($request))->getServiceRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $service=$this->serviceRepository->update($data, $id);
        DB::commit();

        session()->flash('flash_message_success','Data Update Successfuly!!');
            return redirect()->route('admin.services.index');
        }catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try{
        $service=$this->serviceRepository->delete($id);
        DB::commit();
        session()->flash('flash_message_success','Operation Successfuly!!');
            return redirect()->route('admin.services.index');
        }catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }
    }
}

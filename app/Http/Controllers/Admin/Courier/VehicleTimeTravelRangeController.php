<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\VehicleTimeTravelRange\VehicleTimeTravelRangeRepository;
use Illuminate\Support\Facades\Validator;

use App\Traits\WeightRangeService;
use App\ValidationRules\VehicleTimeTravelRangeRule;
use Exception;

class VehicleTimeTravelRangeController extends Controller
{
    protected $timeRangeRepo;
    use WeightRangeService;
    public function __construct(VehicleTimeTravelRangeRepository $timeRangeRepo)
    {
        $this->timeRangeRepo = $timeRangeRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicleTimeRanges = $this->timeRangeRepo->all();
        return view('admin.pages.courier.vehicle-time-travel-range.index')->with('vehicleTimeRanges', $vehicleTimeRanges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicleTimeRanges = $this->timeRangeRepo->fetchData();
        return view('admin.pages.courier.vehicle-time-travel-range.create')->with('vehicleRanges', $vehicleTimeRanges);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new VehicleTimeTravelRangeRule($request))->getVehicleTimeTravelRangeRules('create'));

        if ($validator->fails()) {
           // session()->flash('flash_message_error','Please enter  Integer only !!');
            return redirect()->back()
                 ->withErrors($validator)
                 ->withInput();
        }
      
        $data=$request->except('_token');
        $min = $data['min_time'];
        $max =$data['max_time'];
        
        try{
            $max=$this->testWeightRange($min,$max);
            if(isset($data['delete_id'])){
                $this->timeRangeRepo->deletePerviousData($data['delete_id']);
            }
            $this->timeRangeRepo->store($data, $min, $max);
            return redirect()->route('admin.vehicle-time-range.index')->with('flash_message_success', 'Data Created Successfully');

        }catch(Exception $e){
            return redirect()->back()->with('flash_message_danger', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

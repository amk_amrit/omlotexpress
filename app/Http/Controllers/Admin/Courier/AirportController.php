<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\Airport\AirportRateInterface;

use App\ValidationRules\AirportRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class AirportController extends Controller
{
    private $airportRepository;

    public function __construct(AirportRateInterface $airportRepository){
            $this->airportRepository=$airportRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $airport=$this->airportRepository->all();
        //return $airport;
        return view('admin.pages.courier.airport.index')->with('airports',$airport);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        
        //
        $transit=$this->airportRepository->transit();
        //return $transit;
       // $country=$this->airportRepository->country();
        return view('admin.pages.courier.airport.create')
        ->with('transits', $transit);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new AirportRule($request))->getAirportRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
            
        $data=$request->except('_token');
        //dd($data);
        $airport=$this->airportRepository->save($data);
        DB::commit();
         session()->flash('flash_message_success','Data Store Successfuly!!');
         return redirect()->route('admin.airports.index');
        }catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
       // $airport=$this->airportRepository->findById($id);
       $transit=$this->airportRepository->transit();
       $airport=$this->airportRepository->findById($id);
       //return $airport;
       return view('admin.pages.courier.airport.show')
               ->with('transits', $transit)
               ->with('airports', $airport);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $transit=$this->airportRepository->transit();
        $airport=$this->airportRepository->findBySingleId($id);
        //return $transit;
        return view('admin.pages.courier.airport.edit')
                ->with('transits', $transit)
                ->with('airports', $airport);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),(new AirportRule($request))->getAirportRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $airport=$this->airportRepository->update($data, $id);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return redirect()->route('admin.airports.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try{
        $airport=$this->airportRepository->delete($id);
        DB::commit();
        session()->flash('flash_message_success','Operation Successfuly!!');
        return redirect()->route('admin.airports.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }
}

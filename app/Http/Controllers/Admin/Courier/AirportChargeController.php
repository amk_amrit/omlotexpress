<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Courier\AirportCharge\AirportChargeInterface;
use App\ValidationRules\AirportChargeRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class AirportChargeController extends Controller
{
    private $airportChargeRepo;
    public function __construct(AirportChargeInterface $airportChargeRepo)
    {
        $this->airportChargeRepo=$airportChargeRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //return response()->json($request->all());

        $airportLinkId=$request->airport_link_id;
        $quantityGroupId=$request->quantity_group_id;
        $rule = (new AirportChargeRule($request))->getAirportChargeRules('update',$airportLinkId, $quantityGroupId );
        $validator = Validator::make($request->all(), $rule[0], $rule[1]);
        //return response()->json($request->all());
       
        if ($validator->fails()) {
            if ($request->ajax()){
                return response()->json(['error'=>$validator->errors()->all()],'422');
                //return response()->json(['errors' => $validator->errors(),'error' => true],'422');

            }
        }
        
        else{
           
            try {
                DB::beginTransaction();
               $data = $request->except('_token');
               $airportCharge = $this->airportChargeRepo->save($data);
                 DB::commit();
               session()->flash('flash_message_success', 'Data Store Successfuly!!');
               return response()->json([$airportCharge,200]);            
           } catch (\Exception $exception) {
               DB::rollback();
               return response()->json(['error'=>[$exception->getMessage(),$exception->getFile(), $exception->getLine()]],500);
           }
       }
       

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $airportlink=$this->airportChargeRepo->findById($id);
        $quantityGroup=$this->airportChargeRepo->quantityGroup();
        return view('admin.pages.courier.airport-charge.create')
                    ->with('quantitygroups', $quantityGroup)
                    ->with('airportlinks', $airportlink);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $getAirportChargeData=$this->airportChargeRepo->findByIdData($id);
        $getAirportLinkName=$this->airportChargeRepo->airPortLinkName($id);
        //return $getAirportLinkName;
        return view('admin.pages.courier.airport-charge.edit')
                    ->with('getAirportLinkNames', $getAirportLinkName)
                    ->with('airportLinkId', $id)
                    ->with('getAirportChargeDatas', $getAirportChargeData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $airportLinkId=$id;
        $quantityGroupId=0;
        $rule = (new AirportChargeRule($request))->getAirportChargeRules('create',$airportLinkId, $quantityGroupId );
        $validator = Validator::make($request->all(), $rule[0], $rule[1]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $airportChargeUpdate=$this->airportChargeRepo->update($data, $id);
        DB::commit();

        session()->flash('flash_message_success','Data Update Successfuly!!');
            return redirect()->route('admin.airport-links.index');
        }catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().$exception->getLine().$exception->getFile().'.');
        }
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getQuantityGroupRangedata(Request $request){
        $data=$request->except('_token');
        $quantityGroupRange=$this->airportChargeRepo->getQuantityGroupRange($data);
       // return response()->json($quantityGroupRange);
        if ($request->ajax()) {
            return view('admin.pages.courier.airport-charge.quantityGroupRangeForm',compact('quantityGroupRange' ))->render();
        }    
    }
}

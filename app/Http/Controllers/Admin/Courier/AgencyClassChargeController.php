<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use App\Models\CourierClass;
use App\Models\QuantityGroup;
use Illuminate\Http\Request;
use App\Repositories\Courier\AgencyClassCharge\AgencyClassChargeRepository;
use App\ValidationRules\AgencyClassChargeRule;
use Illuminate\Support\Facades\Validator;
use DB;

class AgencyClassChargeController extends Controller
{
    private $agencyClassChargeRepository;

    public function __construct(AgencyClassChargeRepository $agencyClassChargeRepository){
            $this->agencyClassChargeRepository=$agencyClassChargeRepository;
    }   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data['agencyClasses'] = CourierClass::OfagencyType()->OfCountry()->get();
        $data['quantityGroups'] = QuantityGroup::active()->get();
        return view('admin.pages.courier.agency-class-charge.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
        $data['agencyClasses'] = CourierClass::OfagencyType()->OfCountry()->get();
        $data['quantityGroups'] = QuantityGroup::active()->get();

        return view('admin.pages.courier.agency-class-charge.create', $data);
                    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        $rule = (new agencyClassChargeRule($request))->getagencyClassChargeRules('create');
        $validator = Validator::make($request->all(), $rule[0], $rule[1]);
   
        // $validator = Validator::make($request->all(),(new agencyClassChargeRule($request))->getagencyClassChargeRules('create'));
        if ($validator->fails()) {
            if ($request->ajax()){
                return response()->json(['error'=>$validator->errors()->all()],'422');
            }
        }
        DB::beginTransaction();
        try{
        $this->agencyClassChargeRepository->save($request);
        DB::commit();
        if ($request->ajax()){
            return response()->json('Data Updated Successfully',200);
        }
  
    }catch (\Exception $exception){
            DB::rollback();
       
            if ($request->ajax()){
                return response()->json(['error'=>[$exception->getMessage()]],'422');
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function getQuanityGroupRange(Request $request){
    
        $data['quantityGroupWithRates'] = $this->agencyClassChargeRepository->getQuantityGroupRange($request);
        $data['quantityGroup'] = QuantityGroup::find($request->quantity_group_id);
        $data['agencyClass'] = CourierClass::find($request->courier_agency_class_id);

        // dd($data['quantityGroupWithRates']);

        if ($request->ajax()) {
            return view('admin.pages.courier.agency-class-charge.form')->with('quantityGroupWithRates', $data['quantityGroupWithRates'])->render();
        }

        return view('admin.pages.courier.agency-class-charge.rate-entry',$data);    
    }
}

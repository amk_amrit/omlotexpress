<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Repositories\Courier\VehicleTimeTravelGroupRate\VehicleTimeTravelGroupRateInterface;
use DB;
use App\ValidationRules\VehicleTimeTravelGroupRateRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\VTimeTravelGroupRate;
use App\Models\VTimeTravelGroup;

class VehicleTimeTravelGroupRateController extends Controller
{
    private $VehicleTimeTravelGroupRateRepository;

    public function __construct(VehicleTimeTravelGroupRateInterface $VehicleTimeTravelGroupRateRepository){
            $this->VehicleTimeTravelGroupRateRepository=$VehicleTimeTravelGroupRateRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $vehicleTimeTravelGroupRate=$this->VehicleTimeTravelGroupRateRepository->all();
        return view('admin.pages.courier.vehicle-time-travel-group-rate.index')->with('vehicleTimeTravelGroupRates',$vehicleTimeTravelGroupRate);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $country=$this->VehicleTimeTravelGroupRateRepository->country();
        return view('admin.pages.courier.vehicle-time-travel-group-rate.country')->with('countries', $country);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new VehicleTimeTravelGroupRateRule($request))->getVehicleTimeTravelGroupRateRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $vehicleTimeTravelGroupRate=$this->VehicleTimeTravelGroupRateRepository->save($data);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return redirect()->route('admin.time-travel-group-rates.index');
        }
        catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $vehicleTimeTravelGroupRate=$this->VehicleTimeTravelGroupRateRepository->findById($id);
        return view('admin.pages.courier.vehicle-time-travel-group-rate.edit')->with('vehicleTimeTravelGroupRates' ,$vehicleTimeTravelGroupRate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new VehicleTimeTravelGroupRateRule($request))->getVehicleTimeTravelGroupRateRules('update'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        
        $vehicleTimeTravelGroupRate=$this->VehicleTimeTravelGroupRateRepository->update($data, $id);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return redirect()->route('admin.time-travel-group-rate.index');
        }
        catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try{
        $vehicleTimeTravelGroupRate=$this->VehicleTimeTravelGroupRateRepository->delete($id);
        DB::commit();
        session()->flash('flash_message_success','Data Operation Successfuly!!');
        return redirect()->route('admin.time-travel-group-rate.index');
        }
        catch (\Exception $exception){
            DB::rollback();
            return redirect()->back()
                ->withInput()
                ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
        }

    }
    public function timeTravelGroupData(Request $request){
        $data=$request->except('_token');
        $vehicleTimeTravelGroupRate=$this->VehicleTimeTravelGroupRateRepository->createDataAll($data);
        return view('admin.pages.courier.vehicle-time-travel-group-rate.create')
        ->with('country_id', $data['country'])
        ->with('vehicleTimeTravelGroupRates',$vehicleTimeTravelGroupRate);
    }
}

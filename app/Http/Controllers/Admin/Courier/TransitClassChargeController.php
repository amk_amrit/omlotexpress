<?php

namespace App\Http\Controllers\Admin\Courier;

use App\Http\Controllers\Controller;
use App\Models\CourierClass;
use App\Models\QuantityGroup;
use Illuminate\Http\Request;
use App\Repositories\Courier\TransitClassCharge\TransitClassChargeRepository;
use App\ValidationRules\TransitClassChargeRule;
use Illuminate\Support\Facades\Validator;
use DB;

class TransitClassChargeController extends Controller
{
    private $transitClassChargeRepository;

    public function __construct(TransitClassChargeRepository $transitClassChargeRepository){
            $this->transitClassChargeRepository=$transitClassChargeRepository;
    }   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $transitClassCharge=$this->transitClassChargeRepository->all();
        $data['transitClasses'] = CourierClass::OfTransitType()->OfCountry()->get();
        $data['quantityGroups'] = QuantityGroup::active()->get();
        return view('admin.pages.courier.transit-class-charge.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
        $data['transitClasses'] = CourierClass::OfTransitType()->OfCountry()->get();
        $data['quantityGroups'] = QuantityGroup::active()->get();

        return view('admin.pages.courier.transit-class-charge.create', $data);
                    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        $rule = (new TransitClassChargeRule($request))->getTransitClassChargeRules('create');
        $validator = Validator::make($request->all(), $rule[0], $rule[1]);
   
        // $validator = Validator::make($request->all(),(new TransitClassChargeRule($request))->getTransitClassChargeRules('create'));
        if ($validator->fails()) {
            if ($request->ajax()){
                return response()->json(['error'=>$validator->errors()->all()],'422');
            }
        }
        DB::beginTransaction();
        try{
        $this->transitClassChargeRepository->save($request);
        DB::commit();
        if ($request->ajax()){
            return response()->json('Data Updated Successfully',200);
        }
  
    }catch (\Exception $exception){
            DB::rollback();
       
            if ($request->ajax()){
                return response()->json(['error'=>[$exception->getMessage()]],'422');
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function getQuanityGroupRange(Request $request){
    
        $data['quantityGroupWithRates'] = $this->transitClassChargeRepository->getQuantityGroupRange($request);
        $data['quantityGroup'] = QuantityGroup::find($request->quantity_group_id);
        $data['transitClass'] = CourierClass::find($request->courier_transit_class_id);

        // dd($quantityGroupWithRates);

        if ($request->ajax()) {
            // return response()->json(['groupRanges' => $groupRanges]);
            return view('admin.pages.courier.transit-class-charge.form')->with('quantityGroupWithRates', $data['quantityGroupWithRates'])->render();
        }

        return view('admin.pages.courier.transit-class-charge.rate-entry',$data);

             
    }
}

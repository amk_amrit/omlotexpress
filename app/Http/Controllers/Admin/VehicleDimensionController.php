<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\VehicleDimension\VehicleDimensionInterface;
use Illuminate\Support\Facades\Validator;
use App\ValidationRules\VehicleDimensionRule;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class VehicleDimensionController extends Controller
{
    protected $vehicleDimensionRepo;
    public function __construct(VehicleDimensionInterface $vehicleDimensionRepo){
        $this->vehicleDimensionRepo = $vehicleDimensionRepo;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicleDimensions = $this->vehicleDimensionRepo->all();
        return view('admin.pages.vehicle-dimension.index')->with('vehicleDimensions', $vehicleDimensions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->vehicleDimensionRepo->fetchData();
        return view('admin.pages.vehicle-dimension.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new VehicleDimensionRule($request))->getVehicleDimensionValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=$request->except('_token');
        $data['slug']= Str::slug($data['title'] , '-');
        $VehicleDimension=$this->vehicleDimensionRepo->save($data);
        if($VehicleDimension){
            return redirect()->route('admin.vehicles.dimensions.index')->with('flash_message_success', 'Data Created Successfuly!');
        }
        else{
            return redirect()->route('admin.vehicles.dimensions.index')->with('flash_message_error', 'Error!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicleDimension = $this->vehicleDimensionRepo->find($id);
        return view('admin.pages.vehicle-dimension.show')->with('vehicleDimension', $vehicleDimension);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->vehicleDimensionRepo->fetchData();
        $vehicleDimension = $this->vehicleDimensionRepo->find($id);
        return view('admin.pages.vehicle-dimension.edit', $data)->with('vehicleDimension', $vehicleDimension);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new VehicleDimensionRule($request))->getVehicleDimensionValidationRules('update', $id));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        
        $data=$request->except('_token');
        $data['slug']= Str::slug($data['title'] , '-');
        $VehicleDimension=$this->vehicleDimensionRepo->update($data, $id);
        if($VehicleDimension){
            return redirect()->route('admin.vehicles.dimensions.index')->with('flash_message_success', 'Data Updated Successfuly!');
        }
        else{
            return redirect()->route('admin.vehicles.dimensions.index')->with('flash_message_error', 'Error!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->vehicleDimensionRepo->changeStatus($id);
        return back()->with('flash_message_success', 'Operation Completed Successfully');
    }
    
}

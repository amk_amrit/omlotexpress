<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UnitCategory\UnitCategoryInterfaces;
use App\ValidationRules\UnitCategoryRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UnitCategoryController extends Controller
{
    private $unitCategoryRepository;

    public function __construct(UnitCategoryInterfaces $unitCategoryRepository){
            $this->unitCategoryRepository=$unitCategoryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $unitCategoty=$this->unitCategoryRepository->all();
        return view('admin.pages.unit-category.index')->with('unitCategory', $unitCategoty);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.unit-category.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),(new UnitCategoryRule($request))->getUnitCategoryValidationRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data=$request->except('_token');
        $unitCategoty=$this->unitCategoryRepository->save($data);
        if($unitCategoty){
            session()->flash('flash_message_success','Data Store Successfuly!!');
            return redirect()->route('admin.categorys.index');
        }
        else{
            session()->flash('flash_message_error','Error!!');
            return redirect()->route('admin.categorys.index');      
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $unitCategory=$this->unitCategoryRepository->findById($id);
        return view('admin.pages.unit-category.edit')->with('unitCategories', $unitCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|unique:unit_categories,name,'.$id,
        ]);
        //
       $data=$request->except('_token');
       $unitCategory=$this->unitCategoryRepository->update($data, $id);
       session()->flash('flash_message_success','Data Update Successfuly!!');
       return redirect()->route('admin.categorys.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $unitCategory=$this->unitCategoryRepository->delete($id);
        session()->flash('flash_message_success','Operation Successfuly!!');
        return redirect()->route('admin.categorys.index');
    }
}

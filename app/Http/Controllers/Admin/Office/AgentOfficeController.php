<?php

namespace App\Http\Controllers\Admin\Office;

use App\Http\Controllers\Controller;
use App\Models\Agency;
use App\Models\Country;
use App\Services\CourierClassService;
use App\Services\OfficeService;
use App\ValidationRules\AgentOfficeRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DB;

class AgentOfficeController extends Controller
{
    private $officeService;

    public function __construct(OfficeService $officeService){
        $this->officeService = $officeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       $agencyOffices = $this->officeService->getAgentOfficesByEmployee(authEmployee());

       return view('admin.pages.office.agent-office.index',compact('agencyOffices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //to be changed to regional admin
        if(authEmployee()->isSuperAdmin()){
           // $countries = Country::getActiveCountries();
            $agencyWorkingOptions  = (new Agency())->getWorkingAsOptions();

            $transitOffices = $this->officeService->getActiveTransitOffices();
            $agencyTypeCourierClasses =CourierClassService::getAgencyTypeCourierClasses(authEmployee()->getWorkingCountry()->id);

            return view('admin.pages.office.agent-office.create',compact('agencyWorkingOptions',
                'agencyTypeCourierClasses','transitOffices'));
        }
        return redirect()->route('admin.dashboard');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new AgentOfficeRule($request))->getAgentOfficeValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        try{

            DB::beginTransaction();
            $this->officeService->storeAgentOffice($request);
            DB::commit();
            return redirect()->back()->with('flash_message_success', 'Agent Office Created Successfully!');

        }catch(Exception $e){
            DB::rollBack();
            return redirect()->back()->with('flash_message_danger', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $office = $this->officeService->findAgencyOfficeById($id);
        return view('admin.pages.office.agent-office.show')->with('office', $office);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $office = $this->officeService->findAgencyOfficeById($id);

        $agencyWorkingOptions  = (new Agency())->getWorkingAsOptions();

        $agencyTypeCourierClasses =CourierClassService::getAgencyTypeCourierClasses(authEmployee()->getWorkingCountry()->id);

        //dd($office);

        return view('admin.pages.office.agent-office.edit',
            compact('office','agencyWorkingOptions','agencyTypeCourierClasses'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new AgentOfficeRule($request))->getAgentOfficeValidationRules('update',$id));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        try{

            DB::beginTransaction();
            $this->officeService->updateAgentOffice($request,$id);
            DB::commit();
            return redirect()->back()->with('flash_message_success', 'Agent Office Updated Successfully!');

        }catch(Exception $e){
            DB::rollBack();
            return redirect()->back()->with('flash_message_danger', $e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try{
            $this->officeService->changeStatus($id);
            return back()->with('flash_message_success', 'Operation Completed Successfully');
        }catch(Exception $e){
            return back()->with('flash_message_error', $e->getMessage());
        }
    }
}

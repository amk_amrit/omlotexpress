<?php

namespace App\Http\Controllers\Admin\Office;

use App\Http\Controllers\Controller;
use App\Services\OfficeService;
use App\ValidationRules\AgentOfficeRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DB;

class AgencyTransitConnectionController extends Controller
{
    private $officeService;

    public function __construct(OfficeService $officeService){
        $this->officeService = $officeService;
    }

    public function getTransitConnectionsOfAgency($officeId){

        $office = $this->officeService->findAgencyOfficeById($officeId);

        $agencyTransitConnections = $this->officeService->getTransitConnectionsOfAgency($officeId);

        //return($agencyTransitConnections);

        //for add connection form
        $transitOffices = $this->officeService->getActiveTransitOffices($officeId);

        return  view('admin.pages.office.agent-office.agency-transit-link.index',compact('office',
            'agencyTransitConnections','transitOffices'));
    }

    public function storeTransitConnectionOfAgency(Request $request,$officeId){

        $validator = Validator::make($request->all(),(new AgentOfficeRule($request))->getAgentOfficeValidationRules('addConnection',0,$officeId));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        try{

            DB::beginTransaction();
            $this->officeService->storeTransitConnectionOfAgency($request,$officeId);
            DB::commit();
            return redirect()->back()->with('flash_message_success', 'New Connection Added Successfully!');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('flash_message_danger', $e->getMessage())->withInput();
        }
    }

    public function editTransitConnectionOfAgency($agencyOfficeId,$transitOfficeId){

        try{

            $transitOffice = $this->officeService->findTransitOfficeById($transitOfficeId);
            $agencyOffice = $this->officeService->findAgencyOfficeById($agencyOfficeId);

            $agencyTransitConnection = $this->officeService->findAgencyTransitConnection($agencyOffice->agency->id,$transitOffice->transit->id);

            return view('admin.pages.office.agent-office.agency-transit-link.edit',compact('agencyOffice',
                'transitOffice','agencyTransitConnection'));

        }catch(\Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    public function updateTransitConnectionOfAgency(Request $request,$agencyOfficeId,$transitOfficeId){

        $validator = Validator::make($request->all(),(new AgentOfficeRule($request))->getAgentOfficeValidationRules('editConnection'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        try{

            DB::beginTransaction();
            $this->officeService->updateTransitConnectionOfAgency($request,$agencyOfficeId,$transitOfficeId);
            DB::commit();
            return redirect()->back()->with('flash_message_success', 'Connection Updated Successfully!');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('flash_message_danger', $e->getMessage())->withInput();
        }
    }

    public function toggleTransitConnectionStatusOfAgency($agencyOfficeId,$transitOfficeId){

        try{
            $this->officeService->toggleTransitConnectionStatusOfAgency($agencyOfficeId,$transitOfficeId);
            return back()->with('flash_message_success', 'Operation Completed Successfully');
        }catch(\Exception $e){
            return back()->with('flash_message_error', $e->getMessage());
        }
    }

    public function deleteTransitConnectionOfAgency($agencyOfficeId,$transitOfficeId){

        try{
            $this->officeService->deleteTransitConnectionOfAgency($agencyOfficeId,$transitOfficeId);
            return back()->with('flash_message_success', 'Connection Deleted Successfully');
        }catch(\Exception $e){
            return back()->with('flash_message_error', $e->getMessage());
        }
    }
}

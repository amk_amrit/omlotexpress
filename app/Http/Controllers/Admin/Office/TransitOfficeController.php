<?php

namespace App\Http\Controllers\Admin\Office;

use App\Http\Controllers\Controller;
use App\Models\Office;
use App\Models\Transit;
use App\Models\TransitLink;
use App\Services\CourierClassService;
use App\Services\OfficeService;
use App\ValidationRules\TransitOfficeRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DB;

class TransitOfficeController extends Controller
{
    private $officeService;

    public function __construct(OfficeService $officeService){
        $this->officeService = $officeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transitOffices = $this->officeService->getTransitOfficesByEmployee(authEmployee());

        return view('admin.pages.office.transit-office.index',compact('transitOffices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //to be changed to regional admin
        if(authEmployee()->isSuperAdmin()){

            $transitTypeCourierClasses =CourierClassService::getTransitTypeCourierClasses(authEmployee()->getWorkingCountry()->id);

            $transitOffices = $this->officeService->getActiveTransitOffices();

            $travelMedium = (new Transit())->travelMediums();

            return view('admin.pages.office.transit-office.create',compact('transitTypeCourierClasses'
                , 'transitOffices','travelMedium'));
        }
        return redirect()->route('admin.dashboard');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new TransitOfficeRule($request))->getTransitOfficeValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());

        try{

            DB::beginTransaction();
            $this->officeService->storeTransitOffice($request);
            DB::commit();
            return redirect()->route('admin.transit-offices.index')->with('flash_message_success', 'Transit Office Created Successfully!');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('flash_message_danger', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $office = $this->officeService->findTransitOfficeById($id);
        return view('admin.pages.office.transit-office.show')->with('office', $office);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $office = $this->officeService->findTransitOfficeById($id);
        $transitTypeCourierClasses =CourierClassService::getTransitTypeCourierClasses(authEmployee()->getWorkingCountry()->id);

        return view('admin.pages.office.transit-office.edit',
            compact('office','transitTypeCourierClasses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new TransitOfficeRule($request))->getTransitOfficeValidationRules('update',$id));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        try{

            DB::beginTransaction();
            $this->officeService->updateTransitOffice($request,$id);
            DB::commit();
            return redirect()->back()->with('flash_message_success', 'Transit Office Updated Successfully!');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('flash_message_danger', $e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $this->officeService->changeStatus($id);
            return back()->with('flash_message_success', 'Operation Completed Successfully');
        }catch(\Exception $e){
            return back()->with('flash_message_error', $e->getMessage());
        }
    }

    public function getTransitOfficeConnections($officeId){

        try{

            $office = $this->officeService->findTransitOfficeById($officeId);
            $transitConnections = $this->officeService->getTransitOfficeConnectionsByOfficeId($officeId);

           //return($transitConnections);
            //return($this->officeService->getConnectionlessTransitForOffice($officeId));

            //for add connection form
            $transitOffices = $this->officeService->getActiveTransitOffices($officeId);

            $travelMedium = (new Transit())->travelMediums();

            return view('admin.pages.office.transit-office.transit-link.index',compact('office',
                'transitConnections','transitOffices','travelMedium'));

        }catch(\Exception $e){
            return redirect()->route('admin.transit-offices.index')->with('flash_message_error', $e->getMessage());
        }
    }


    public function storeConnectionOfTransit(Request $request,$officeId)
    {


        $validator = Validator::make($request->all(),(new TransitOfficeRule($request))->getTransitOfficeValidationRules('addConnection',0,$officeId));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd(1);
        try{

            DB::beginTransaction();
            $this->officeService->storeTransitConnection($request,$officeId);
            DB::commit();
            return redirect()->back()->with('flash_message_success', 'New Connection Added Successfully!');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('flash_message_danger', $e->getMessage())->withInput();
        }
    }

    public function editConnectionOfTransit(Request $request,$officeId,$transitLinkId){

        try{

            $connectedOffice = $this->officeService->findTransitOfficeSlug($request['connected-office-slug']);
           // dd($connectedOffice);
            $office = $this->officeService->findTransitOfficeById($officeId);
            $transitLink = $this->officeService->getTransitLinkOfOffice($officeId,$transitLinkId);
            $travelMedium = (new Transit())->travelMediums();

            return view('admin.pages.office.transit-office.transit-link.edit',compact('connectedOffice','office',
                'transitLink','travelMedium'));

        }catch(\Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    public function updateConnectionOfTransit(Request $request,$officeId,$transitLinkId){

        $validator = Validator::make($request->all(),(new TransitOfficeRule($request))->getTransitOfficeValidationRules('editConnection'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        try{

            DB::beginTransaction();
            $this->officeService->updateConnectionOfTransit($request,$officeId,$transitLinkId);
            DB::commit();
            return redirect()->back()->with('flash_message_success', 'Connection Detail Updated Successfully!');

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('flash_message_danger', $e->getMessage())->withInput();
        }
    }

    public function toggleConnectionStatusOfTransit(Request $request,$officeId,$transitLinkId){

        try{
            $this->officeService->toggleConnectionStatusOfTransit($request,$officeId,$transitLinkId);
            return back()->with('flash_message_success', 'Operation Completed Successfully');
        }catch(\Exception $e){
            return back()->with('flash_message_error', $e->getMessage());
        }
    }

    public function deleteConnectionOfTransit(Request $request,$officeId,$transitLinkId){
        try{
            $this->officeService->deleteConnectionOfTransit($request,$officeId,$transitLinkId);
            return back()->with('flash_message_success', 'Connection Deleted Successfully');
        }catch(\Exception $e){
            return back()->with('flash_message_error', $e->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PackageType;
use App\Models\PackageTypeRate;
use App\Services\PackageTypeRateService;
use App\ValidationRules\PackageTypeRateRule;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PackageTypeRateController extends Controller
{
    protected $rateService;
    public function __construct(PackageTypeRateService $rateService)
    {
        $this->rateService = $rateService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $rates = $this->rateService->index();
            return view('admin.pages.package-type-rate.index')->with('rates', $rates);

        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       try{  
        $data['packageTypes'] = PackageType::activePackageTypes();
            return view('admin.pages.package-type-rate.create', $data);

       }catch(Exception $e){
           return $e;
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $rule =  (new PackageTypeRateRule($request))->getPackageTypeRateValidationRules('create');
        $validator = Validator::make($request->all(),$rule[0],$rule[1]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }    
        try {
            $packageType = $this->rateService->save($request);
            if ($packageType) {
                return redirect()->route('admin.package.rates.index')->with('flash_message_success', 'Data Created Successfuly!');
            } else {
                return redirect()->route('admin.package.rates.index')->with('flash_message_error', 'Error!!');
            }
        } catch (Exception $e) {
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PackageTypeRate $rate)
    {
        try{
            return view('admin.pages.package-type-rate.show')->with('rate', $rate);
        }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PackageTypeRate $rate)
    {
        try{
            $data['packageTypes'] = PackageType::activePackageTypes();
            return view('admin.pages.package-type-rate.edit', $data)->with('rate', $rate);

       }catch(Exception $e){
            return redirect()->back()->with('flash_message_error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PackageTypeRate $rate)
    {
        $rule =  (new PackageTypeRateRule($request))->getPackageTypeRateValidationRules('update', $rate->id);
        $validator = Validator::make($request->all(),$rule[0],$rule[1]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $packageType = $this->rateService->update($request, $rate);
            if ($packageType) {
                return redirect()->route('admin.package.rates.index')->with('flash_message_success', 'Data Updated Successfuly!');
            } else {
                return redirect()->route('admin.package.rates.index')->with('flash_message_error', 'Error!!');
            }
        } catch (Exception $e) {
            return redirect()->back()->with('flash_message_error', $e->getMessage())->withInput;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PackageTypeRate $rate)
    {
        $this->rateService->destroyRate($rate);
        return redirect()->back()->with('flash_message_success', 'Operation Completed Successfully');
    }
}

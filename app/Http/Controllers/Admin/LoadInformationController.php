<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\LoadInfromation\LoadInformationInterfaces;

use App\Models\FullLoadInformation;
use App\Models\PartLoadInformation;

class LoadInformationController extends Controller
{
    private $loadInformationRepository;

    public function __construct(LoadInformationInterfaces $loadInformationRepository){
            $this->loadInformationRepository=$loadInformationRepository;
    }

    public  function  getPartLoad(){
        $partloads=$this->loadInformationRepository->allPartLoad();
        return view('admin.pages.load-Information.part-load')->with('partload', $partloads);
     }
     public function getPartLoadView($id){
         $partloadshow=$this->loadInformationRepository->PartLoadShow($id);
         return view('admin.pages.load-Information.show-part-load')->with('partloadshows', $partloadshow);
     }
     public function getFillLoad(){
         $fullLoad=$this->loadInformationRepository->allFullLoad();
         return view('admin.pages.load-Information.full-load')->with('fullload',$fullLoad);
     }
}

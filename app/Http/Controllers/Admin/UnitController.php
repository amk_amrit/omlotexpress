<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Units\UnitInterfaces;
use App\ValidationRules\UnitRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UnitController extends Controller
{
    private $unitRepository;

    public function __construct(UnitInterfaces $unitRepository){
            $this->unitRepository=$unitRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $unit=$this->unitRepository->all();
        return view('admin.pages.unit.index')->with('units', $unit);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categoryunits=$this->unitRepository->categoryall();
        return view('admin.pages.unit.create')->with('categoryunit', $categoryunits);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new UnitRule($request))->getUnitValidationRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        $data=$request->except('_token');
        $unit=$this->unitRepository->save($data);
                session()->flash('flash_message_success','Data Store Successfuly!!');
                return redirect()->route('admin.units.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $units=$this->unitRepository->getById($id);
        $categoryunits=$this->unitRepository->categoryall();
        return view('admin.pages.unit.edit')
        ->with('unit', $units)  
        ->with('categoryunit', $categoryunits);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new UnitRule($request))->getUnitValidationRules('update'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        $data=$request->except('_token');
        $unit=$this->unitRepository->update($data, $id);
        session()->flash('flash_message_success','Data Store Successfuly!!');
        return redirect()->route('admin.units.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

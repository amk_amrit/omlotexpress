<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    //
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){

        return view('admin.auth.login');
    }

    public function login(Request $request){

        //validate the form data
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6'
            ]
        );

        if($validator->fails()){
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }

        if($user = User::where('email', $request->get('email'))->first()){
            if($user->status == 'unverified'){
                return redirect()->back()->withErrors(['email'=>'Your Account is not Activated !! Please Contact Administrator']);
            }
            
        }

        //attempt to log the user in
        if(Auth::attempt([
            'email'=> $request->input('email'),
            'password' =>$request->input('password'),
            'status' =>'verified',
        ],$request->remember)){

            //if successfull redirect to intended location
            /* return redirect()->guest(route('fe.home'));*/
            return redirect()->intended(route('admin.dashboard'));
        }
        else{
            return $this->sendFailedLoginResponse($request);
            //if not successfull redirect to login page with the form data
            //return redirect()->back()->withInput($request->only('email','remember'));
        }
    }

    public function logout()
    {
        Auth::logout();

        return  redirect()->route('admin.getLoginForm');
    }


}

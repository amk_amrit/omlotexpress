<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Designation\DesignationInterface;
use Illuminate\Http\Request;
use App\ValidationRules\DesignationRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class DesignationController extends Controller
{
    protected $designationRepo;
    public function __construct(DesignationInterface $designationRepo){
        $this->designationRepo = $designationRepo;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designations = $this->designationRepo->all();
        return view('admin.pages.designation.index')->with('designations', $designations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.designation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new DesignationRule($request))->getDesignationValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=$request->except('_token');
        $data['slug']= Str::slug($data['title'] , '-');
        $designation=$this->designationRepo->save($data);
        if($designation){
            return redirect()->route('admin.designations.index')->with('flash_message_success', 'Designation Created Successfuly!');
        }
        else{
            return redirect()->route('admin.designations.index')->with('flash_message_error', 'Error!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $designation = $this->designationRepo->find($id);
        return view('admin.pages.designation.show')->with('designation', $designation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $designation = $this->designationRepo->find($id);
        return view('admin.pages.designation.edit')->with('designation', $designation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new DesignationRule($request))->getDesignationValidationRules('update'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=$request->except('_token');
        $data['slug']= Str::slug($data['title'] , '-');
        $designation=$this->designationRepo->update($data, $id);
        if($designation){
            return redirect()->route('admin.designations.index')->with('flash_message_success', 'Designation Updated Successfuly!');
        }
        else{
            return redirect()->route('admin.designations.index')->with('flash_message_error', 'Error!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->designationRepo->changeStatus($id);
        return back()->with('flash_message_success', 'Operation Completed Successfully');
    }
}

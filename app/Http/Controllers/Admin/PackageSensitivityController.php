<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PackageSensitivity\PackageSensitivityInterfaces;
use App\ValidationRules\PackageSensitivityRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PackageSensitivityController extends Controller
{
    private $packageSensitivityRepository;

    public function __construct(PackageSensitivityInterfaces $packageSensitivityRepository){
            $this->packageSensitivityRepository=$packageSensitivityRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $packageSemsi=$this->packageSensitivityRepository->all();
         return view('admin.pages.package-sensitivity.index')->with('packageSemsis', $packageSemsi);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.package-sensitivity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new PackageSensitivityRule($request))->getPackageSeniRuleValidationRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        $data=$request->except('_token');
        $packageSemsi=$this->packageSensitivityRepository->save($data);
        if($packageSemsi){
            session()->flash('flash_message_success','Data Store Successfuly!!');
            return redirect()->route('admin.package.sensitivities.index');
        }
        else{
            session()->flash('flash_message_error','Error!!');
            return redirect()->route('admin.package.sensitivities.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $packageSensitivite=$this->packageSensitivityRepository->findById($id);
        return view('admin.pages.package-sensitivity.edit')->with('packageSensitivites', $packageSensitivite);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(),(new PackageSensitivityRule($request))->getPackageSeniRuleValidationRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        $data=$request->except('_token');
        $packageSemsi=$this->packageSensitivityRepository->update($data, $id);
        if($packageSemsi){
            session()->flash('flash_message_success','Data Update Successfuly!!');
            return redirect()->route('admin.package.sensitivities.index');        }
        else{
            session()->flash('flash_message_error','Error!!');
            return redirect()->route('admin.package.sensitivities.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $packageSemsi=$this->packageSensitivityRepository->delete($id);
        session()->flash('flash_message_success','Operation Successfuly!!');
        return redirect()->route('admin.package.sensitivities.index');    }
}

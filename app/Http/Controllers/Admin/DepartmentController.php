<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Department\DepartmentInterface;
use Illuminate\Http\Request;
use App\ValidationRules\DepartmentRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;



class DepartmentController extends Controller
{
    
    protected $departmentRepo;
    public function __construct(DepartmentInterface $departmentRepo){
        $this->departmentRepo = $departmentRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = $this->departmentRepo->all();
        return view('admin.pages.department.index')->with('departments', $departments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $validator = Validator::make($request->all(),(new DepartmentRule($request))->getDepartmentValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=$request->except('_token');
        $data['slug']= Str::slug($data['name'] , '-');
        $department=$this->departmentRepo->save($data);
        if($department){
            return redirect()->route('admin.departments.index')->with('flash_message_success', 'Department Created Successfuly!');
        }
        else{
            return redirect()->route('admin.departments.index')->with('flash_message_error', 'Error!!');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = $this->departmentRepo->find($id);
        return view('admin.pages.department.show')->with('department', $department);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = $this->departmentRepo->find($id);
        return view('admin.pages.department.edit')->with('department', $department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new DepartmentRule($request))->getDepartmentValidationRules('update', $id));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=$request->except('_token');
        $data['slug']= Str::slug($data['name'] , '-');
        $department=$this->departmentRepo->update($data, $id);
        if($department){
            return redirect()->route('admin.departments.index')->with('flash_message_success', 'Department Updated Successfuly!');
        }
        else{
            return redirect()->route('admin.departments.index')->with('flash_message_error', 'Error!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->departmentRepo->changeStatus($id);
        return back()->with('flash_message_success', 'Operation Completed Successfully');
    }
}

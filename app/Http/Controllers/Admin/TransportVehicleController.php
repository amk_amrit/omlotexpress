<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\TransportVehicle\TransportVehicleInterface;
use Illuminate\Support\Facades\Validator;
use App\ValidationRules\TransportVehicleRule;
use Illuminate\Support\Str;




class TransportVehicleController extends Controller
{
    protected $vehicleRepo;
    public function __construct(TransportVehicleInterface $vehicleRepo){
        $this->vehicleRepo = $vehicleRepo;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transportVehicles = $this->vehicleRepo->all();
        return view('admin.pages.vehicle.index')->with('transportVehicles', $transportVehicles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->vehicleRepo->fetchData();
        return view('admin.pages.vehicle.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new TransportVehicleRule($request))->getTransportVehicleValidationRules('create'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=$request->except('_token');
        $data['slug']= Str::slug($data['name'] , '-'); //Add Slug to the data to be saved in Repo
        $transportVehicle=$this->vehicleRepo->save($data);
        if($transportVehicle){
            return redirect()->route('admin.vehicles.index')->with('flash_message_success', 'Data Created Successfuly!');
        }
        else{
            return redirect()->route('admin.vehicles.index')->with('flash_message_error', 'Error!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transportVehicle = $this->vehicleRepo->find($id);
        return view('admin.pages.vehicle.show')->with('transportVehicle', $transportVehicle);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->vehicleRepo->fetchData($id);
        $transportVehicle = $this->vehicleRepo->find($id);
        return view('admin.pages.vehicle.edit', $data)->with('transportVehicle', $transportVehicle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new TransportVehicleRule($request))->getTransportVehicleValidationRules('update', $id));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=$request->except('_token');
        $data['slug']= Str::slug($data['name'] , '-'); //Add Slug to the data to be saved in Repo
        $transportVehicle=$this->vehicleRepo->update($data, $id);
        if($transportVehicle){
            return redirect()->route('admin.vehicles.index')->with('flash_message_success', 'Data Updated Successfuly!');
        }
        else{
            return redirect()->route('admin.vehicles.index')->with('flash_message_error', 'Error!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->vehicleRepo->changeStatus($id);
        return back()->with('flash_message_success', 'Operation Completed Successfully');
    }
    

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\ValidationRules\CountryRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $countries=Country::all();
        return view('admin.pages.country.index')->with('countries', $countries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.country.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new CountryRule($request))->getCountryValidationRules('create'));

    if ($validator->fails()) {
        return redirect()->back()
            ->withErrors($validator)
            ->withInput();
    }
        //
        $country=new Country;
        $country->iso=$request->iso;
        $country->name=$request->name;
        $country->nicename=$request->nicename;
        $country->iso3=$request->iso3;
        $country->numcode=$request->numcode;
        $country->phonecode=$request->phonecode;
        $country->currency=$request->currency;
        $country->currency_symbol=$request->currency_symbol;


        $country->save();
       // return redirect()->route('countries.index')->with('message', 'Data Create Successfuly!');
        session()->flash('flash_message_success', 'Data Create Successfuly!!');
        return redirect()->route('admin.countries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $countrys=Country::find($id);
        return view('admin.pages.country.show')->with('country' , $countrys);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $countrys=Country::find($id);
        return view('admin.pages.country.edit')->with('country', $countrys);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),(new CountryRule($request))->getCountryValidationRules('update'));

    if ($validator->fails()) {
        return redirect()->back()
            ->withErrors($validator)
            ->withInput();
    }
        $country=Country::find($id);
        $country->iso=$request->iso;
        $country->name=$request->name;
        $country->nicename=$request->nicename;
        $country->iso3=$request->iso3;
        $country->numcode=$request->numcode;
        $country->phonecode=$request->phonecode;
        $country->currency=$request->currency;
        $country->currency_symbol=$request->currency_symbol;
        $country->save();

        session()->flash('flash_message_success', 'Data Update Successfuly!!');
        return redirect()->route('admin.countries.index');
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $country=Country::find($id);
        if($country->status==0){
           $country->status=1;
           $country->save(); 
        }
        else{
            $country->status=0;
           $country->save();
        }
        session()->flash('flash_message_success','Operation Completed Successfuly!');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ValidationRules\TerminalPointRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Repositories\TerminalPoint\TerminalPointInterface;

class TerminalPointController extends Controller
{
    private $terminalPointRepository;

    public function __construct(TerminalPointInterface $terminalPointRepository){
            $this->terminalPointRepository=$terminalPointRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $termianPoint=$this->terminalPointRepository->all();
        $country=$this->terminalPointRepository->allcountry();
        return view('admin.pages.terminal-point.index')
                ->with('termianPoints', $termianPoint)
                ->with('country', $country);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $country=$this->terminalPointRepository->allcountry();
        return view('admin.pages.terminal-point.create')->with('countrys', $country);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new TerminalPointRule($request))->getTerminalPointValidationRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        $data=$request->except('_token');
        $terminalPoint=$this->terminalPointRepository->save($data);
        if($terminalPoint){
                session()->flash('flash_message_success','Data Store Successfuly!!');
                return redirect()->route('admin.terminalpoints.index');
        }
        else{
            session()->flash('flash_message_error','Error!!');
            return redirect()->route('admin.terminalpoints.index');        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $termianPoint=$this->terminalPointRepository->findById($id);
        $countrys=$this->terminalPointRepository->allcountry();
        return view('admin.pages.terminal-point.show')
                ->with('termianPoints', $termianPoint)
                ->with('country', $countrys);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $terminalPoint=$this->terminalPointRepository->findById($id);
        $country=$this->terminalPointRepository->allcountry();
        return view('admin.pages.terminal-point.edit')
                    ->with('terminalPoints', $terminalPoint)
                    ->with('countrys', $country);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new TerminalPointRule($request))->getTerminalPointValidationRules('update'));

    if ($validator->fails()) {
        return redirect()->back()
            ->withErrors($validator)
            ->withInput();
    }
        //
        $data=$request->except('_token');
        $terminalPoint=$this->terminalPointRepository->update($data, $id);
        if($terminalPoint){
            session()->flash('flash_message_success','Data Update Successfuly!!');
            return redirect()->route('admin.terminalpoints.index');        }
        else{
            session()->flash('flash_message_error','Error!!');
            return redirect()->route('admin.terminalpoints.index');        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $terminalPoint=$this->terminalPointRepository->delete($id);

        session()->flash('flash_message_success','Operation Successfuly!!');
        return redirect()->route('admin.terminalpoints.index');     }
}

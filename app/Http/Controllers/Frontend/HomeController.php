<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Models\PackageSensitivity;
use App\Models\PackageType;
use App\Models\ProductCategory;
use App\Models\TransportMedia;
use App\Models\Unit;
use App\Services\ClassHelper\ClassHelperService;
use App\Services\CountryService;
use App\Traits\ImageService;
use App\Traits\WeightRangeService;

use Illuminate\Http\Request;

use File;


class HomeController extends Controller
{
    use ImageService;
    use WeightRangeService;

    //
    public function getHome(){

        $cs = new ClassHelperService();
        $countries = CountryService::getCountriesByIso(['NP','IN']);
        $activeTransportMedias = TransportMedia::activeTransportMedias();
        $productCategories = ProductCategory::with(['productRate'])->get();

        $activeSensitivities = PackageSensitivity::getActiveSensitivities();
        $packageTypes =PackageType::activePackageTypes();
        $dimensionUnits = Unit::dimensionUnits();
        $weightUnits = Unit::weightUnits();

        //$activeVehicles = TransportVehicle::ofWithActive()->get();

        //dd($activeVehicles);
        //dd($dimensionUnits);
        return view('frontend.pages.home.home',compact('countries','activeTransportMedias',
            'productCategories','activeSensitivities','packageTypes','dimensionUnits','weightUnits'));
    }

    public function rangeTest(){


        $min = [
            5,11,0,30,
        ];
        $max =[10,15,4,40];

        try{

            $max=$this->testWeightRange($min,$max);
            dd($max);
        }catch (\Exception $exception){

            dd($exception->getMessage());
        }

    }

    public function imageTest(Request $request){

        if($request->hasFile('company_logo')){
           // dd($request);

            $ps = new PackageSensitivity();

            try{

                $fileNameToStore=$this->storeImageInServer($request->file('company_logo'),$ps->getImagePath(),true);
                //$this->deleteImageFromServer($ps->getImagePath(),'blog-2_1580299937.jpg');

               // dd($fileNameToStore);
            }catch (\Exception $exception){

                dd($exception->getMessage());
            }



           /* $oldFileName = $slider->image;

            $slider->image = $fileNameToStore;
            $slider->doSomething();

            ImageService::deleteImage($this->imagePath,$oldFileName);*/

        }
    }

}

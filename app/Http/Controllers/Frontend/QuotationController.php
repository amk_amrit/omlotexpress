<?php

namespace App\Http\Controllers\Frontend;


use Alphametric\Validation\Rules\Decimal;
use App\Http\Controllers\Controller;

use App\Models\Country;
use App\Models\ProductCategory;
use App\Models\TransportMedia;
use App\Services\CalculateShipmentRateService;
use App\Services\CountryService;
use App\Services\PackageTypeService;
use App\Services\ProductCategoryService;
use App\Services\QuotationService;
use App\Services\SensitivityService;
use App\ValidationRules\QuotationRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use DB;

class QuotationController extends Controller
{
    public function store(Request $request){


        $quotationRule = new QuotationRule($request);
        $validator = Validator::make($request->all(),($quotationRule->getAllQuotationRules('create')),
           $quotationRule->getAllQuotationMessages('create'));

        if ($validator->fails()) {
            if ($request->ajax()){
                //return response()->json([$validator->errors()->getMessages()]); //with key and value
                return response()->json(['errors' => $validator->errors(),'error' => true],'422');
            }

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        DB::beginTransaction();
        try{
            $quotationService = new QuotationService();
            $shipment = $quotationService->calculateAndStoreShipment($request);
            DB::commit();
        }catch (\Exception $exception){
            DB::rollback();
            return response()->json(['errors' => [$exception->getMessage()],'error' => true],'500');
        }


        //dd($totalChargeAmount);
        if ($request->ajax()) {
            return view('frontend.pages.home.partials.quotation-bill-modal',compact('shipment'))->render();
        }

        return response()->json(['shipment'=>$shipment],201);


    }

}

<?php

namespace App\Http\Controllers\Shared;

use App\Http\Controllers\Controller;
use App\Services\CountryService;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    //
    public function getCountryByIso($iso='NP'){
        $country = CountryService::getCountryByIso($iso);
        return response()->json(['country'=>$country],201);
    }

    public function getTerminalPointsByCountryIso($iso='NP'){
        $terminalPoints = CountryService::getActiveTerminalPointsByCountryIso($iso);
        return response()->json($terminalPoints,201);
    }

    public function getTerminalPointsByCountriesIso($countryOneIso,$countryTwoIso){
        $terminalPoints = CountryService::getActiveTerminalPointsByCountriesIso($countryOneIso,$countryTwoIso);
        return response()->json($terminalPoints,201);
    }
}

<?php

namespace App\Http\Controllers\Shared;

use App\Http\Controllers\Controller;
use App\Models\TransportMedia;
use App\Models\TransportVehicle;
use Illuminate\Http\Request;

class TransportController extends Controller
{
    //
    public function getVehiclesByMedia($transportMediaSlug){

        $transportMedia = TransportMedia::getActiveMediaBySlug($transportMediaSlug);
        $transportVehicles = TransportVehicle::ofWithActive()->where('transport_media_id',$transportMedia->id)->get();

        return response()->json($transportVehicles,201);
    }
}

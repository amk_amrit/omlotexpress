<?php

namespace App\Http\Middleware;

use App\Services\UserService;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        $employee = auth()->user()->employee;

        if ($employee) {
            if ($employee->isSuperAdmin() || $employee->isCountryHead()) {
                return $next($request);
            }

            $requestedRouteName = $request->route()->getAction()['as']; // route info

            $employeePermissions = $employee->collectPermissions();

            //dd($employeePermissions);

            $employeePermissions = UserService::getUserAssociativePermissions($employeePermissions);

            // dd($employeePermissions);
            if (in_array($requestedRouteName, $employeePermissions)) {

                return $next($request);
            }
        }

        abort(403, 'You Don\'t Have Right Permissions.');
    }
}

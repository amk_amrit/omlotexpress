<?php

namespace App\Providers\Shipment;

use Illuminate\Support\ServiceProvider;

class ShipmentRepoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Shipment\ShipmentInterface', 'App\Repositories\Shipment\ShipmentRepository');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
    }
}

<?php

namespace App\Providers\Designation;

use Illuminate\Support\ServiceProvider;

class DesignationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Repositories\Designation\DesignationInterface', 'App\Repositories\Designation\DesignationRepository');
    }
}

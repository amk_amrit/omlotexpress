<?php

namespace App\Providers\terminalPoint;

use Illuminate\Support\ServiceProvider;
use App\Repositories\TerminalPoint\TerminalPointInterface;
use App\Repositories\TerminalPoint\TerminalPointRepository;

class TerminalPointServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(TerminalPointInterface::class, TerminalPointRepository::class);

    }
}

<?php

namespace App\Providers\PackageSensitivity;

use Illuminate\Support\ServiceProvider;

use App\Repositories\PackageSensitivity\PackageSensitivityInterfaces;
use App\Repositories\PackageSensitivity\PackageSensitivitiyRepository;


class PackageSensitivitiyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(PackageSensitivityInterfaces::class, PackageSensitivitiyRepository::class);

    }
}

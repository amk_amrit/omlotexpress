<?php

namespace App\Providers\LoadInformation;

use Illuminate\Support\ServiceProvider;
use App\Repositories\LoadInfromation\LoadInformationInterfaces;
use App\Repositories\LoadInfromation\LoadInfromationRepository;

class LoadInformationServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(LoadInformationInterfaces::class, LoadInfromationRepository::class);

    }
}

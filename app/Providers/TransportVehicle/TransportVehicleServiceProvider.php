<?php

namespace App\Providers\TransportVehicle;

use Illuminate\Support\ServiceProvider;

class TransportVehicleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Repositories\TransportVehicle\TransportVehicleInterface', 'App\Repositories\TransportVehicle\TransportVehicleRepository');
        
    }
}

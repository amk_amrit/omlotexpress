<?php

namespace App\Providers\UnitCategory;

use Illuminate\Support\ServiceProvider;
use App\Repositories\UnitCategory\UnitCategoryInterfaces;
use App\Repositories\UnitCategory\UnitCategoryRepository;


class UnitCategoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(UnitCategoryInterfaces::class, UnitCategoryRepository::class);

    }
}

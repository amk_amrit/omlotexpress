<?php

namespace App\Providers\Unit;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Units\UnitInterfaces;
use App\Repositories\Units\UnitRepository;


class UnitServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(UnitInterfaces::class, UnitRepository::class);

    }
}

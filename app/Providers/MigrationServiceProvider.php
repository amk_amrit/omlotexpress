<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MigrationServiceProvider extends ServiceProvider
{

    private function attachBaseMigrationPath(array $migrating_folders): array
    {
        array_walk($migrating_folders, function (&$folder, $key) {
            $folder = database_path() . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR . "$folder";
        });

        return $migrating_folders;
    }


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $migration_folders = [

            // 'admin-charges',
            // 'agencies',
            // 'airlines',
            // 'countries',
            // 'courier-classes',
            // 'department',
            // 'designation',
            // 'employee',
            // 'office',
            // 'package',
            // 'product',
            // 'quantity-groups',
            // 'role-permission',
            // 'services',
            // 'shipment',
            // 'time-travel-range',
            // 'transits',
            // 'transport',
            // 'transport-quotation',
            // 'unit',
            // 'user',
            // 'vehicle-charges', 



            'foreign-keys',

            //'safety-migration-tank',
            //'amrit',
            // 'amrit-forgien-key',


        ];

        $db_migration_folders = $this->attachBaseMigrationPath($migration_folders);

        $this->loadMigrationsFrom($db_migration_folders);
    }
}

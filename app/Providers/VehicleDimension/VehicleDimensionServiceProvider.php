<?php

namespace App\Providers\VehicleDimension;

use Illuminate\Support\ServiceProvider;

class VehicleDimensionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Repositories\VehicleDimension\VehicleDimensionInterface', 'App\Repositories\VehicleDimension\VehicleDimensionRepository');
        
    }
}

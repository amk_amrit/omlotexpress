<?php

namespace App\Providers\Courier\Transit;

use App\Models\Transit;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\Transit\TransitInterface;
use App\Repositories\Courier\Transit\TransitRepository;
class TransitServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(TransitInterface::class, TransitRepository::class);
    }
}

<?php

namespace App\Providers\Courier\AgencyTransitVehicleCharge;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\AgencyTransitVehicleCharge\AgencyTransitVehicleChargeInterface;
use App\Repositories\Courier\AgencyTransitVehicleCharge\AgencyTransitVehicleChargeRepository;

class AgencyTransitVehicleChargeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(AgencyTransitVehicleChargeInterface::class, AgencyTransitVehicleChargeRepository::class);
    }
}

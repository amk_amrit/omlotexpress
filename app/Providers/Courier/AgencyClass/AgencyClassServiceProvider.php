<?php

namespace App\Providers\Courier\AgencyClass;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\AgencyClass\AgencyClassInterface;
use App\Repositories\Courier\AgencyClass\AgencyClassRepository;
class AgencyClassServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(AgencyClassInterface::class, AgencyClassRepository::class);
    }
}

<?php

namespace App\Providers\Courier\VehicleTimeTravelGroupRate;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\VehicleTimeTravelGroupRate\VehicleTimeTravelGroupRateInterface;
use App\Repositories\Courier\VehicleTimeTravelGroupRate\VehicleTimeTravelGroupRateRepository;

class VehicleTimeTravelGroupRateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(VehicleTimeTravelGroupRateInterface::class, VehicleTimeTravelGroupRateRepository::class);

    }
}

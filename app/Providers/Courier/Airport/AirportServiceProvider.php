<?php

namespace App\Providers\Courier\Airport;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\Airport\AirportRateInterface;
use App\Repositories\Courier\Airport\AirportRepository;

class AirportServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(AirportRateInterface::class, AirportRepository::class);

    }
}

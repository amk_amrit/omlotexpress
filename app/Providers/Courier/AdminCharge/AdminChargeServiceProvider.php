<?php

namespace App\Providers\Courier\AdminCharge;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\AdminCharge\AdminChargeInterface;
use App\Repositories\Courier\AdminCharge\AdminChargeRepository;

class AdminChargeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(AdminChargeInterface::class, AdminChargeRepository::class);
    }
}

<?php

namespace App\Providers\Courier\AirportCharge;

use App\Repositories\Courier\AirportCharge\AirportChargeInterface;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\AirportCharge\AirportChargeRepository;
class AirportChargeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(AirportChargeInterface::class, AirportChargeRepository::class);
    }
}

<?php

namespace App\Providers\Courier\VehicleTimeTravelGroup;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\VehicleTimeTravelGroup\VehicleTimeTravelGroupInterface;
use App\Repositories\Courier\VehicleTimeTravelGroup\VehicleTimeTravelGroupRepository;

class VehicleTimeTravelGroupServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(VehicleTimeTravelGroupInterface::class, VehicleTimeTravelGroupRepository::class);

    }
}

<?php

namespace App\Providers\Courier\AgencyLink;
use App\Repositories\Courier\AgencyLink\AgencyLinkInterface;
use App\Repositories\Courier\AgencyClass\AgencyClassRepository;
use App\Repositories\Courier\AgencyLink\AgencyLinkRepository;
use Illuminate\Support\ServiceProvider;

class AgencyLinkServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(AgencyLinkInterface::class, AgencyLinkRepository::class);
    }
}

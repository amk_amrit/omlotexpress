<?php

namespace App\Providers\Courier\QuantityGroupRange;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\QuantityGroupRange\QuantityGroupRangeInterface;
use App\Repositories\Courier\QuantityGroupRange\QuantityGroupRangeRepository;

class QuantityGroupRangeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(QuantityGroupRangeInterface::class, QuantityGroupRangeRepository::class);

    }
}

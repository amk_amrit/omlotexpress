<?php

namespace App\Providers\Courier\CourierClass;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\CourierClass\CourierClassInterface;
use App\Repositories\Courier\CourierClass\CourierClassRepository;

class CourierClassServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(CourierClassInterface::class,CourierClassRepository::class);
    }
}

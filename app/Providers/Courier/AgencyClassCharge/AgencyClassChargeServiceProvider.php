<?php

namespace App\Providers\Courier\AgencyClassCharge;
use App\Repositories\Courier\AgencyClassCharge\AgencyClassChargeInterface;
use App\Repositories\Courier\AgencyClassCharge\AgencyClassChargeRepository;

use Illuminate\Support\ServiceProvider;

class AgencyClassChargeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(AgencyClassChargeInterface::class, AgencyClassChargeRepository::class);
    }
}

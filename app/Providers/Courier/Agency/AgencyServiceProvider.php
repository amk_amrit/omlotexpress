<?php

namespace App\Providers\Courier\Agency;
use App\Repositories\Courier\Agency\AgencyInterface;
use App\Repositories\Courier\Agency\AgencyRepository;

use Illuminate\Support\ServiceProvider;

class AgencyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(AgencyInterface::class, AgencyRepository::class);
    }
}

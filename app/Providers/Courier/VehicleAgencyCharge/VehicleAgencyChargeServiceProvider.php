<?php

namespace App\Providers\Courier\VehicleAgencyCharge;
use App\Repositories\Courier\VehicleAgentCharge\VehicleAgentChargeInterface;
use App\Repositories\Courier\VehicleAgentCharge\VehicleAgentChargeRepository;
use Illuminate\Support\ServiceProvider;

class VehicleAgencyChargeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(VehicleAgentChargeInterface::class, VehicleAgentChargeRepository::class);
    }
}

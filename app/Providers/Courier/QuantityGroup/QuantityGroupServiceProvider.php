<?php

namespace App\Providers\Courier\QuantityGroup;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\QuantityGroup\QuantityGroupInterface;
use App\Repositories\Courier\QuantityGroup\QuantityGroupRepository;

class QuantityGroupServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(QuantityGroupInterface::class, QuantityGroupRepository::class);

    }
}

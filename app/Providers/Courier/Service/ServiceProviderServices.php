<?php

namespace App\Providers\Courier\Service;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\Service\ServiceInterface;
use App\Repositories\Courier\Service\ServiceRepository;

class ServiceProviderServices extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(ServiceInterface::class, ServiceRepository::class);
    }
}

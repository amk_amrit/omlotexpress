<?php

namespace App\Providers\Courier\TransitLink;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\TransitLink\TransitLinkInterface;
use App\Repositories\Courier\TransitLink\TransitLinkRepository;

class TransitLinkServieProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(TransitLinkInterface::class, TransitLinkRepository::class);
    }
}

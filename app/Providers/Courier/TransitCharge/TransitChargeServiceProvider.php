<?php

namespace App\Providers\Courier\TransitCharge;
use App\Repositories\Courier\TransitCharge\TransitChargeInterface;
use App\Repositories\Courier\TransitCharge\TransitChargeRepository;

use Illuminate\Support\ServiceProvider;

class TransitChargeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(TransitChargeInterface::class, TransitChargeRepository::class);
    }
}

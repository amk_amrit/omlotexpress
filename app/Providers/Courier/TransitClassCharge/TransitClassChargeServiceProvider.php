<?php

namespace App\Providers\Courier\TransitClassCharge;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\TransitClassCharge\TransitClassChargeInterface;
use App\Repositories\Courier\TransitClassCharge\TransitClassChargeRepository;

class TransitClassChargeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(TransitClassChargeInterface::class, TransitClassChargeRepository::class);
    }
}

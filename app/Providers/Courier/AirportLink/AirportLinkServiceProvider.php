<?php

namespace App\Providers\Courier\AirportLink;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Courier\AirportLink\AirportLinkInterface;
use App\Repositories\Courier\AirportLink\AirportLinkRepository;

class AirportLinkServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(AirportLinkInterface::class, AirportLinkRepository::class);
    }
}

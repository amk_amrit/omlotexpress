<?php

namespace App\Providers\Department;

use App\Repositories\Department\DepartmentInterface;
use App\Repositories\Department\DepartmentRepository;
use Illuminate\Support\ServiceProvider;

class DepartmentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(DepartmentInterface::class, DepartmentRepository::class);
        
    }
}

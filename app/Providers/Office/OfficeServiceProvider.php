<?php

namespace App\Providers\Office;

use Illuminate\Support\ServiceProvider;

class OfficeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Repositories\Office\OfficeInterface', 'App\Repositories\Office\OfficeRepository');
    }
}

<?php

namespace App\Providers\ProductCategoryRate;

use Illuminate\Support\ServiceProvider;
use App\Repositories\ProductCategoryRate\CategoryRateInterface;
use App\Repositories\ProductCategoryRate\CatergoryRateRepository;


class CategoryRateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(CategoryRateInterface::class, CatergoryRateRepository::class);
    }
}

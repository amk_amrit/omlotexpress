<?php

namespace App\Providers\PackageType;

use Illuminate\Support\ServiceProvider;
use App\Repositories\PackageType\PackageTypeInterfaces;
use App\Repositories\PackageType\PackageTypeRepository;


class packageTypeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(PackageTypeInterfaces::class, PackageTypeRepository::class);

    }
}

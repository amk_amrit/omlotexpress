<?php

namespace App\Providers\TransportMedia;

use Illuminate\Support\ServiceProvider;

class TransportMediaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Repositories\TransportMedia\TransportMediaInterface', 'App\Repositories\TransportMedia\TransportMediaRepository');
    }
}

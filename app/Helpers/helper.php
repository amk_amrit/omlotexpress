<?php
function testJ(){
    return 11;
}

function convertToDecimal($number){
    return number_format($number, 4);
}

function compareArraysLength(array $array1,array $array2){

    if(count($array1) === count($array2)){

      return true;
    }
    return false;
}

/**
* Function Name: tr()
*
* Description : Language key file contents and default language
*
*/
function tr($key , $confirmation_content_lang_key = "", $locale = 'en') {
    
    return \Lang::choice('messages.'.$key, 0, Array('confirmation_content_lang_key' => $confirmation_content_lang_key), $locale);
}

function authEmployee(){

    return auth()->user()->employee;
}


if (! function_exists('str_ordinal')) {
    /**
     * Append an ordinal indicator to a numeric value.
     *
     * @param  string|int  $value
     * @param  bool  $superscript
     * @return string
     */
    function str_ordinal($value, $superscript = false)
    {
        $number = abs($value);
 
        $indicators = ['th','st','nd','rd','th','th','th','th','th','th'];
 
        $suffix = $superscript ? '<sup>' . $indicators[$number % 10] . '</sup>' : $indicators[$number % 10];
        if ($number % 100 >= 11 && $number % 100 <= 13) {
            $suffix = $superscript ? '<sup>th</sup>' : 'th';
        }
 
        return number_format($number) . $suffix;
    }
}
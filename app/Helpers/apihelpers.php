<?php

/**
 * Send a response back
 *
 * @param  string $message
 * @param  boolean $error
 * @param  integer $code
 * @param  array $parameters
 * @return Illuminate\Http\JsonResponse
 */
function sendResponse($message, $error = false, $code = 200, array $parameters = null)
{
    $response = [
        'error'   => $error,
        'message' => $message,
        'code'    => $code
    ];

    if (!is_null($parameters)) {
        foreach ($parameters as $key => $value) {
            $response = array_add($response, $key, $value);
        }
    }

    return response()->json($response, $code);
}


function sendMinimalResponse($data, $error = false, $code = 200)
{
    $data = array_add($data, 'error', $error);
    $data = array_add($data, 'code', $code);
    return response()->json($data, $code);
}

/**
 * Convert url to proper path
 *
 * @param $path
 * @return string
 */
function photoToUrl($path, $directory_path)
{
    if (filter_var($path, FILTER_VALIDATE_URL)) {
        $photo = $path;
    } else {
        $photo = $path ? url('/') . $directory_path . $path : '';
    }
    return $photo;
}

/**
 * Convert base64 to jpg
 *
 * @param  $base64_string
 * @param $output_path
 * @return String
 */
function base64_to_jpeg($base64_string, $output_path)
{
    $data      = explode(',', $base64_string);
    $imageData = str_replace(' ', '+', end($data));
    $source    = base64_decode($imageData);

    // ImageService::checkFolderExistance($output_path);

    $filename  = generateFilename($output_path, get_extension_from_base64($imageData));
    file_put_contents($output_path . $filename, $source);

    return $filename;
}

/**
 * Convert base64 to jpg with resize paths
 *
 * @param  $base64_string
 * @param $output_path
 * @param $output_small_path
 * @param $output_new_path
 * @return String
 */
function base64_to_jpeg_with_resize($base64_string, $output_path, $resize)
{
    $data      = explode(',', $base64_string);
    $imageData = str_replace(' ', '+', end($data));
    $source    = base64_decode($imageData);


    $filename  = generateFilename($output_path, get_extension_from_base64($imageData));

    $imagePathWithFileName = $output_path;
    $smallImagePathWithFileName = $output_path . 'small/';
    $mediumImagePathWithFileName = $output_path . 'medium/';


    // Resize Image Code

    // if ($resize) {
    //     ImageService::checkFolderExistance($output_path);
    //     ImageService::checkFolderExistance($output_path . 'small');
    //     ImageService::checkFolderExistance($output_path . 'medium');

    //     Image::make($source)->resize(80, 80)->save($smallImagePathWithFileName . $filename);
    //     Image::make($source)->resize(450, 450)->save($mediumImagePathWithFileName . $filename);
    //     Image::make($source)->resize(1024, 1024)->save($imagePathWithFileName . $filename);
    // } else {
    file_put_contents($output_path . $filename, $source);
    // }


    return $filename;
}

/**
 * Get file extension from base64 string
 *
 * @param $base64_string
 * @return mixed
 */
function get_extension_from_base64($base64_string)
{
    $imageData = base64_decode($base64_string);
    $f = finfo_open();

    $mime_type = finfo_buffer($f, $imageData, FILEINFO_MIME_TYPE);
    $extensions = [
        'image/gif'  => 'gif',
        'image/png'  => 'png',
        'image/jpeg' => 'jpg',
        'image/bmp'  => 'bmp',
        'image/webp' => 'webp'
    ];

    return $extensions[$mime_type];
}

/**
 * Generate an unique filename
 *
 * @param  $path
 * @param $extension
 * @return string
 */
function generateFilename($path, $extension)
{
    $filename = substr(hash('sha256', Illuminate\Support\Str::random(), false), 0, 16) . '.' . $extension;
    if (app('files')->exists($path . $filename)) {
        generateFilename($path, $extension);
    }
    return $filename;
}

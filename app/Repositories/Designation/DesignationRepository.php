<?php

namespace App\Repositories\Designation;

use Illuminate\Http\Request;
use App\Repositories\Designation\DesignationInterface as DesignationInterface;
use App\Models\Designation;



class DesignationRepository implements DesignationInterface
{
   

    public function all()
    {
        return Designation::latest()->get();
    }

    public function find($id)
    {
        return Designation::where('id', $id)->first();
    }


    public function changeStatus($id)
    {
        
        $designation = Designation::find($id);
        if($designation->status == 0)
        {
            $designation->status = 1;
        }else{
            
            $designation->status = 0;
        }
        $designation->save();
        return true;
    }

    public function save($data){
        $data['created_by'] = auth()->user()->employee->id;
        $data['updated_by'] = auth()->user()->employee->id;
        $designation = new Designation;
        $designation->create($data);
        if($designation){
            return true;
        }
        else{
            return false;
        }
    }

    public function update(array $data, $id){
        $designation = Designation::find($id);
        $data['updated_by'] = auth()->user()->employee->id;
        $designation->update($data);
        if($designation){
            return true;
        }
        else{
            return false;
        }
    }
}
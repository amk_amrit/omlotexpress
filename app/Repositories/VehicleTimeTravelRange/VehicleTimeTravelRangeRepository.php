<?php

namespace App\Repositories\VehicleTimeTravelRange;

use App\Models\VehicleTimeTravelRange;
use DB;

class VehicleTimeTravelRangeRepository
{
    public function all()
    {
        if (authEmployee()->isSuperAdmin()) {
            $range = VehicleTimeTravelRange::orderBy('country_id', 'desc');
        } else {
            $range = VehicleTimeTravelRange::where('country_id', authEmployee()->office->country_id)->orderBy('id', 'asc');
        }
        return $range->get();
    }

    public function store($data, $max, $min)
    {
        $countryId = authEmployee()->office->country_id;
        //dd($data);
        // $categoryRate=ProductCategoryRate::find($id);
        foreach ($data['max_time'] as $key => $val) {
            $timeRange = new VehicleTimeTravelRange;
            $timeRange->country_id = $countryId;
            $timeRange->min_time = $max[$key];
            $timeRange->max_time = $min[$key];
            $timeRange->created_by = authEmployee()->id;
            $timeRange->updated_by = authEmployee()->id;
            $timeRange->save();
        }
    }

    public function fetchData()
    {
        return VehicleTimeTravelRange::where('country_id', authEmployee()->office->country_id)->orderBy('id', 'asc')->get();
    }

    public function deletePerviousData($data)
    {
        $numberOfArray = count($data);

        for ($i = 0; $i < $numberOfArray; $i++) {
            DB::table('vehicle_time_travel_ranges')->where('id', $data[$i])->delete();
        }
    }
}

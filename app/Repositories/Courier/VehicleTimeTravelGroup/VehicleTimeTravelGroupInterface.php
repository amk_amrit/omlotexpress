<?php

namespace App\Repositories\Courier\VehicleTimeTravelGroup;

interface VehicleTimeTravelGroupInterface {

    public function all();
    public function save($data);
    public function findById($id);
    public function update($data, $id);
    public function delete($id);
    
}
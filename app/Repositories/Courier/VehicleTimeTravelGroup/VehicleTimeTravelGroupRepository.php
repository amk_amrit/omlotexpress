<?php

namespace App\Repositories\Courier\VehicleTimeTravelGroup;

use Illuminate\Http\Request;
use App\Repositories\Courier\VehicleTimeTravelGroup\VehicleTimeTravelGroupInterface as VehicleTimeTravelGroupInterface;
use DB;
use App\Models\VTimeTravelGroup;


class VehicleTimeTravelGroupRepository implements VehicleTimeTravelGroupInterface
{
    public function all(){
        return $vehiclTimeTravelGroup=VTimeTravelGroup::paginate(10);
    }
    public function save($data)
    {
        $vehiclTimeTravelGroup= new VTimeTravelGroup;
        $vehiclTimeTravelGroup->name=$data['name'];
        $slugs=str_slug($data['name']);
        $vehiclTimeTravelGroup->slug=$slugs;
        $vehiclTimeTravelGroup->status=$data['status'];
        $vehiclTimeTravelGroup->save();
    }
    public function findById($id){
        return $vehiclTimeTravelGroup=VTimeTravelGroup::find($id);
    }
    public function update($data, $id){
        $vehiclTimeTravelGroup=VTimeTravelGroup::find($id);
        $vehiclTimeTravelGroup->name=$data['name'];
        $slugs=str_slug($data['name']);
        $vehiclTimeTravelGroup->slug=$slugs;
        $vehiclTimeTravelGroup->status=$data['status'];
       // dd($vehiclTimeTravelGroup);
        $vehiclTimeTravelGroup->save();
    }
    public function delete($id){
        $vehiclTimeTravelGroup=VTimeTravelGroup::find($id);
        if($vehiclTimeTravelGroup->status==0){
            $vehiclTimeTravelGroup->status=1;
            $vehiclTimeTravelGroup->save(); 
            }
            else{
                $vehiclTimeTravelGroup->status=0;
            $vehiclTimeTravelGroup->save();
            }
            return true;
    }
}
<?php

namespace App\Repositories\Courier\QuantityGroup;

interface QuantityGroupInterface {

    public function all();
    public function packageType();
    public function transportMedia();
    public function save($data);
    public function findById($id);
}
<?php

namespace App\Repositories\Courier\QuantityGroup;

use Illuminate\Http\Request;
use App\Repositories\Courier\QuantityGroup\QuantityGroupInterface as QuantityGroupInterface;
use App\Models\QuantityGroup;
use App\Models\PackageType;
use App\Models\TransportMedia;


class QuantityGroupRepository implements QuantityGroupInterface
{
    public function all(){
        return $quantityGroup=QuantityGroup::get();
    }
    public function packageType(){
        return $packageType=PackageType::all();
    }
    public function transportMedia(){
        return $transportMedia=TransportMedia::all();
    }
    public function save($data){
        $quantityGroup= new QuantityGroup;
        $quantityGroup->group_name=$data['group_name'];
        $slugs=str_slug($data['group_name']);
        $quantityGroup->slug=$slugs;
        $quantityGroup->status=$data['status'];
        $quantityGroup->created_by=authEmployee()->id;
        $quantityGroup->updated_by=authEmployee()->id;
        $quantityGroup->save($data);
    }
    public function findById($id){
        return $quantityGroup=QuantityGroup::findOrFail($id);
    }
    public function update($data,$id){
        $quantityGroup=QuantityGroup::find($id);
        $quantityGroup->group_name=$data['group_name'];
        $slugs=str_slug($data['group_name']);
        $quantityGroup->slug=$slugs;
        $quantityGroup->status=$data['status'];
        $quantityGroup->created_by=authEmployee()->id;
        $quantityGroup->updated_by=authEmployee()->id;
        $quantityGroup->save($data);
    }
    public function delete($id){
        $quantityGroup=QuantityGroup::find($id);
        if($quantityGroup->status==0){
        $quantityGroup->status=1;
        $quantityGroup->save(); 
        }
        else{
            $quantityGroup->status=0;
        $quantityGroup->save();
        }
    }
   
}
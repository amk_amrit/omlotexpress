<?php

namespace App\Repositories\Courier\VehicleAgentCharge;

interface VehicleAgentChargeInterface {

    public function all();
    public function save($data);
    public function findById($id);
    public function update($data, $id);
    public function delete($id);
    public function getCountry();
    public function getVehicle();
    public function getTimeTravelGroup();
    public function getVehicleData($data);

    
}
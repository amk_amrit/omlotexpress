<?php

namespace App\Repositories\Courier\VehicleAgentCharge;

use App\Models\Country;
use App\Models\QuantityGroupCountry;
use App\Models\TransportVehicle;
use App\Models\VehicleAgentCharge;
use App\Models\VTimeTravelGroup;
use Illuminate\Http\Request;
use App\Repositories\Courier\VehicleAgentCharge\VehicleAgentChargeInterface as VehicleAgentChargeInterface;
use DB;


class VehicleAgentChargeRepository implements VehicleAgentChargeInterface
{
    public function all(){
        return $vehiclAgenctCharge=DB::table('vehicle_agent_charges')
                                     ->join('v_time_travel_groups' , 'v_time_travel_groups.id' , '=' , 'vehicle_agent_charges.type')
                                     ->join('quantity_group_countries' , 'quantity_group_countries.id', '=' , 'vehicle_agent_charges.quantity_group_id')
                                     ->join('quantity_groups', 'quantity_groups.id', '=' , 'quantity_group_countries.quantity_group_id')
                                     ->join('countries' , 'countries.id', '=' , 'vehicle_agent_charges.country_id')
                                     ->join('transport_vehicles' , 'transport_vehicles.id', '=' ,'vehicle_agent_charges.vehicle_id')
                                    ->select('vehicle_agent_charges.*','v_time_travel_groups.name as timeTravelGroupName','quantity_groups.group_name', 'transport_vehicles.name as vehicleName','countries.name as countryName')
                                    ->paginate(10);
    }
    public function save($data)
    {
        foreach($data['rate'] as $key=>$val){
            $vehicleAgentCharge= new VehicleAgentCharge;
            $vehicleAgentCharge->country_id=$data['country_id'];
            $vehicleAgentCharge->quantity_group_id=$data['quantity_group_id'][$key];
            $vehicleAgentCharge->rate=$data['rate'][$key];
            $vehicleAgentCharge->vehicle_id=$data['vehicle_id'];
            $vehicleAgentCharge->type=$data['type'];
            $currency=Country::where('id', $data['country_id'])->first();
            $vehicleAgentCharge->currency=$currency->currency;
            $vehicleAgentCharge->status='1';
             $vehicleAgentCharge->save();
         }  
          }
    public function findById($id){
      return   $vehiclAgenctCharge=DB::table('vehicle_agent_charges')
                                        ->join('v_time_travel_groups' , 'v_time_travel_groups.id' , '=' , 'vehicle_agent_charges.type')
                                        ->join('quantity_group_countries' , 'quantity_group_countries.id', '=' , 'vehicle_agent_charges.quantity_group_id')
                                        ->join('quantity_groups', 'quantity_groups.id', '=' , 'quantity_group_countries.quantity_group_id')
                                        ->join('countries' , 'countries.id', '=' , 'vehicle_agent_charges.country_id')
                                        ->join('transport_vehicles' , 'transport_vehicles.id', '=' ,'vehicle_agent_charges.vehicle_id')
                                        ->select('vehicle_agent_charges.*','v_time_travel_groups.name as timeTravelGroupName','quantity_groups.group_name', 'transport_vehicles.name as vehicleName','countries.name as countryName')
                                        ->where('vehicle_agent_charges.id', $id)
                                        ->first();
    }
    public function update($data, $id){
        $vehiclAgenctCharge=VehicleAgentCharge::find($id);
        $vehiclAgenctCharge->type=$data['type'];
        $vehiclAgenctCharge->quantity_group_id=$data['quantity_group_id'];
        $vehiclAgenctCharge->country_id=$data['country_id'];
        $vehiclAgenctCharge->vehicle_id=$data['vehicle_id'];
        $vehiclAgenctCharge->rate=$data['rate'];
        $currency=Country::where('id', $data['country_id'])->first();
        $vehiclAgenctCharge->currency=$currency->currency;
        $vehiclAgenctCharge->save();
    }
    public function delete($id){
        $vehiclAgenctCharge=VehicleAgentCharge::find($id);
        if($vehiclAgenctCharge->status==0){
            $vehiclAgenctCharge->status=1;
            $vehiclAgenctCharge->save(); 
            }
            else{
                $vehiclAgenctCharge->status=0;
            $vehiclAgenctCharge->save();
            }
            return true;
    }
    public function getCountry()
    {
        return $country=Country::all();
    }
    public function getVehicle()
    {
        return $vehicle=TransportVehicle::all();
    }
    public function getTimeTravelGroup()
    {
        return $timeTravelGroup=VTimeTravelGroup::all();
    }
    public function getVehicleData($data){
       $quantityGroupList=DB::table('vehicle_agent_charges')
                            ->where('country_id', $data['country_id'])
                            ->where('vehicle_id', $data['vehicle_id'])
                            ->where('type' , $data['time_trave_group_id'])
                            ->pluck('quantity_group_id')
                            ->toArray();
    $quantityGroupData=QuantityGroupCountry::with('quantityGroup')->whereNotIn('id' ,$quantityGroupList)->get();
    return $quantityGroupData;
    }
}
<?php

namespace App\Repositories\Courier\Service;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Repositories\Courier\Service\ServiceInterface as ServiceInterface;
use DB;

class ServiceRepository implements ServiceInterface
{
    public function all()
    {
        return $service=Service::get();
    }
    public function findById($id)
    {
        return $service=Service::find($id);
    }
    public function update($data, $id)
    {
       $service=Service::find($id);
       $service->name=$data['name'];
        $service->status=$data['status'];
        $slugs=str_slug($data['name']);
        $service->slug=$slugs;
        $service->save();
    }
    public function delete($id){
        $service=Service::find($id);
        if($service->status==0){
        $service->status=1;
        $service->save(); 
        }
        else{
            $service->status=0;
        $service->save();
        }
    
    }
    public function save($data){
            $service=new Service;
            $service->name=$data['name'];
            $service->status=$data['status'];
            $slugs=str_slug($data['name']);
            $service->slug=$slugs;
            $service->save();
    }
    
}
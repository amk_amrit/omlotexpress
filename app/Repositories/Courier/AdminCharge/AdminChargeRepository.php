<?php
namespace App\Repositories\Courier\AdminCharge;

use App\Models\AdminCharge;
use Illuminate\Http\Request;
use App\Repositories\Courier\AdminCharge\AdminChargeInterface as AdminChargeInterface;
use DB;
use App\Models\Country;
use App\models\Office;
use App\Models\PackageType;
use App\Models\QuantityGroup;
use App\Models\QuantityGroupRange;
use App\Models\TransportMedia;

class AdminChargeRepository implements AdminChargeInterface
{
    public function all()
    {
        return $adminCharge=AdminCharge::with('office')->groupBy('office_id')->get();

    }                       

    public function update($data, $id)
    {
        foreach($data['rate'] as $key=>$val){
            $adminCharge= AdminCharge::find($data['id'][$key]);
            $adminCharge->office_id=$id;
            $adminCharge->quantity_group_range_id=$data['quantity_group_range_id'][$key];
            $adminCharge->rate=$data['rate'][$key];
            $adminCharge->rate_type=$data['rate_type'][$key];
            $currency=Country::where('id', authEmployee()->office->country_id)->first();
            $adminCharge->currency=$currency->currency;
            $adminCharge->created_by = authEmployee()->id;
            $adminCharge->updated_by = authEmployee()->id;
            $adminCharge->status = '1';
             $adminCharge->save();
         }

    }
    public function delete($id){
        $adminCharge=AdminCharge::find($id);
        if($adminCharge->status==0){
        $adminCharge->status=1;
        $adminCharge->save(); 
        }
        else{
            $adminCharge->status=0;
        $adminCharge->save();
        }
    
    }
    public function save($data){
        foreach($data['rate'] as $key=>$val){
            $adminCharge= new AdminCharge;
            $adminCharge->office_id=$data['office_id'];
            $adminCharge->quantity_group_range_id=$data['quantity_group_range_id'][$key];
            $adminCharge->rate=$data['rate'][$key];
            $adminCharge->rate_type=$data['rate_type'][$key];
            $currency=Country::where('id', authEmployee()->office->country_id)->first();
            $adminCharge->currency=$currency->currency;
            $adminCharge->created_by = authEmployee()->id;
            $adminCharge->updated_by = authEmployee()->id;
            $adminCharge->status = '1';
             $adminCharge->save();
         }
        
    }
    public function country(){
        return $country=Country::all();
    }
    public function packageType(){
        return $packageType=DB::table('quantity_group_countries')
                            ->join('quantity_groups', 'quantity_groups.id', '=' ,'quantity_group_countries.quantity_group_id')
                            ->select('quantity_group_countries.*', 'quantity_groups.group_name')
                            ->get();
    }
    public function transportMedia(){
        return $transportMedia=TransportMedia::all();
    }
    public function adminChargeCreateData($data){
        $country_id=$data['country_id'];
        $transportMedia_id=$data['transport_meida_id'];
        $user_id=$data['user_id'];
        $storeData=AdminCharge::where('country_id', $country_id)->where('user_id', $user_id)->where('transport_meida_id', $transportMedia_id)->pluck('quantity_group_id')->toArray();
        $quantityData=DB::table('quantity_group_countries')
                            ->join('quantity_groups', 'quantity_groups.id', '=' ,'quantity_group_countries.quantity_group_id')
                            ->select('quantity_group_countries.*', 'quantity_groups.group_name')
                            ->whereNotIn('quantity_group_countries.id', $storeData)
                            ->get();
        return $quantityData;
    }
    public function qunatityGroup($createData){
        foreach($createData as $createDatas){
            $quantityGroupName=QuantityGroup::where('id', $createDatas->quantity_group_id)->first();
        }
        array_merge($createData, $quantityGroupName);
        return $createData;
        
    }
    public function office(){
       return $office=Office::where('country_id', authEmployee()->office->country_id)->get();
    }
    public function quantityGroupName(){
        return $office=QuantityGroup::all();
     }
     public function getQuantityGroupRange($data){
        $quantityGroupDataId=AdminCharge::where('office_id', $data['office_id'])->pluck('quantity_group_range_id')->toArray();
        $quantityGroupData=QuantityGroupRange::whereNotIn('id',$quantityGroupDataId )
                                ->where('country_id', authEmployee()->office->country_id)
                                ->where('quantity_group_id', $data['quantity_group_id'])
                                ->get();
            return $quantityGroupData;
    }
    public function officeName($id){

        return $airportLinkData=DB::table('admin_charges')
                                    ->join('offices', 'offices.id', '=', 'admin_charges.office_id')
                                    ->select('offices.*')
                                    ->where('offices.id', $id)
                                    ->groupBy('admin_charges.office_id')
                                    ->get();
    }
    public function findById($id){
        return $adminChargeData=DB::table('admin_charges')
                                    ->join('quantity_group_ranges', 'quantity_group_ranges.id', '=' ,'admin_charges.quantity_group_range_id')
                                    ->join('quantity_groups', 'quantity_groups.id', '=' , 'quantity_group_ranges.quantity_group_id')
                                    ->select('quantity_groups.group_name as groupName','quantity_group_ranges.min_weight','quantity_group_ranges.max_weight','quantity_group_ranges.id as quantityGroupId','admin_charges.*')
                                    ->where('admin_charges.office_id', $id)
                                    ->get();
    }

}
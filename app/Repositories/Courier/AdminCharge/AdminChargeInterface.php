<?php

namespace App\Repositories\Courier\AdminCharge;

interface AdminChargeInterface {

    public function all();
    public function findById($id);
    public function update($data, $id);
    public function delete($id);
    public function save($data);
    public function country();
    public function packageType();
    public function transportMedia();
    public function adminChargeCreateData($data);
    public function qunatityGroup($createData);
    public function officeName($id);
    public function quantityGroupName();
    public function getQuantityGroupRange($data);
}
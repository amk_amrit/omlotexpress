<?php

namespace App\Repositories\Courier\TransitClassCharge;
use App\Models\QuantityGroupRange;
use App\Models\TransitClassCharge;
class TransitClassChargeRepository
{
    public function save($request)
    {
        foreach ($request->quantity_group_range_id as $key => $quantityGroupRange) {

            TransitClassCharge::updateOrCreate(
                [
                    'courier_transit_class_id' => $request->courier_transit_class_id,
                    'quantity_group_range_id' => $quantityGroupRange,
                    'country_id' => authEmployee()->office->country_id
                ],
                [
                    'rate' => $request->rate[$key],
                    'rate_type' => $request->rate_type[$key],
                    'currency' => authEmployee()->office->country->currency,
                    'created_by' => authEmployee()->id,
                    'updated_by' => authEmployee()->id,

                ]
            );
        }
    }

    public function getQuantityGroupRange($request)
    {
         
        $transitCharges = TransitClassCharge::where('courier_transit_class_id', $request->courier_transit_class_id);
        $quantityGroupWithRates = QuantityGroupRange::leftJoinSub($transitCharges, 'transit_charges',  function($join){
                $join->on('quantity_group_ranges.id', '=', 'transit_charges.quantity_group_range_id');
             })->where('quantity_group_id', $request->quantity_group_id)
                ->select('quantity_group_ranges.*', 'transit_charges.rate', 'transit_charges.rate_type')
                ->get();

        // $quantityGroupWithRates = QuantityGroup::leftJoin('quantity_group_ranges', 'quantity_group_ranges.quantity_group_id', 'quantity_groups.id')
        //     ->leftJoin('transit_class_charges', 'transit_class_charges.quantity_group_range_id', 'quantity_group_ranges.id')
        //     ->where('quantity_groups.id', $request->quantity_group_id)
        //     ->where('quantity_group_ranges.country_id', authEmployee()->office->country_id)
        //     ->where('transit_class_charges.courier_transit_class_id', $request->courier_transit_class_id)
        //     ->select('quantity_group_ranges.*', 'transit_class_charges.rate', 'transit_class_charges.rate_type')
        //     ->orderBy('quantity_group_ranges.id', 'asc')
        //     ->get();
        return $quantityGroupWithRates;
    }
}

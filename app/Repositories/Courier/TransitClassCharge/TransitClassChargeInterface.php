<?php

namespace App\Repositories\Courier\TransitClassCharge;

interface TransitClassChargeInterface {

    public function all();
    public function findById($id);
    public function update($data);
    public function delete($id);
    public function save($data);
    public function country();
    public function transitClass();
    public function getQuantityGroupData($country_id, $class_id);

}
<?php

namespace App\Repositories\Courier\TransitCharge;

use App\Admin\TransitCharge;
use App\Models\QuantityGroupCountry;
use App\Models\Transit;
use App\Models\TransitClass;
use Illuminate\Http\Request;
use App\Repositories\Courier\TransitCharge\TransitChargeInterface as TransitChargeInterface;
use DB;

class TransitChargeRepository implements TransitChargeInterface
{
    public function all()
    {
       return  $transitCharge=DB::table('transit_charges')
                        ->join('transit_class_countries', 'transit_charges.transit_class_id', '=' , 'transit_class_countries.id')
                        ->join('transit_classes','transit_class_countries.transit_class_id', '=' ,'transit_classes.id')
                        ->join('quantity_group_countries', 'quantity_group_countries.id' , '=' ,'transit_charges.quantity_group_id')
                        ->join('quantity_groups', 'quantity_group_countries.quantity_group_id', '=' ,'quantity_groups.id')
                        ->select('transit_charges.*', 'transit_classes.name', 'transit_class_countries.*','quantity_group_countries.*', 'quantity_groups.group_name')
                        ->get();
    }
    public function findById($id)
    {
        // return $transit=Transit::with('country','transitClass','office')->find($id);
    }
    public function update($data, $id)
    {
        // $transit=Transit::find($id);
        // $transit->update($data);
    }
    public function delete($id){
        // $transit=Transit::find($id);
        // if($transit->status==0){
        // $transit->status=1;
        // $transit->save(); 
        // }
        // else{
        //     $transit->status=0;
        // $transit->save();
        // }
    
    }
    public function save($data){
            // $transit=new Transit;
            // $transit->create($data);
    }
    public function quantityGroupCountry($data){
            $transitId=$data['transit_id']; 
            $transit=Transit::where('id',$transitId )->first();
            $transitAllData=TransitCharge::where('transit_class_id', $transit->id)->pluk('quantity_group_id')->toArray();
    }
    public function transitClass(){
        return $transitClass=DB::table('transit_class_countries')
                            ->join('transit_classes', 'transit_class_countries.transit_class_id' , '=' , 'transit_classes.id')
                            ->get();
                            
    }
    public function transit(){
        return $transit=Transit::all();
    }
    
}
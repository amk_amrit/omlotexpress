<?php

namespace App\Repositories\Courier\TransitCharge;

interface TransitChargeInterface {

    public function all();
    public function findById($id);
    public function update($data, $id);
    public function delete($id);
    public function save($data);
    public function transitClass();
    public function quantityGroupCountry($data);
    public function transit();
}
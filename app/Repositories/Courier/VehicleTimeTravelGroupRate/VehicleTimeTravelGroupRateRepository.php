<?php

namespace App\Repositories\Courier\VehicleTimeTravelGroupRate;

use Illuminate\Http\Request;
use App\Repositories\Courier\VehicleTimeTravelGroupRate\VehicleTimeTravelGroupRateInterface as VehicleTimeTravelGroupRateInterface;
use DB;
use App\Models\VTimeTravelGroupRate;
use App\Models\Country;
use App\Models\VTimeTravelGroup;


class VehicleTimeTravelGroupRateRepository implements VehicleTimeTravelGroupRateInterface
{
    public function all()
    {
        return $vehicleTimeTravelGroupRate=VTimeTravelGroupRate::with('country','vTimeTravelGroup')->paginate(10);
    }
    public function findById($id)
    {
        return $vehicleTimeTravelGroupRate=VTimeTravelGroupRate::with('country','vTimeTravelGroup')->where('id', $id)->first();
    }
    public function update($data, $id)
    {
        $vehicleTimeTravelGroupRate=VTimeTravelGroupRate::find($id);
        $vehicleTimeTravelGroupRate->update($data);
    }
    public function delete($id){
        $vehicleTimeTravelGroupRate=VTimeTravelGroupRate::find($id);
        if($vehicleTimeTravelGroupRate->status==0){
        $vehicleTimeTravelGroupRate->status=1;
        $vehicleTimeTravelGroupRate->save(); 
        }
        else{
            $vehicleTimeTravelGroupRate->status=0;
        $vehicleTimeTravelGroupRate->save();
        }
    
    }
    public function country(){
        return $country=Country::all();
    }
    public function createDataAll($data){
        $country_id=$data['country'];
        $country=Country::find($country_id);
        $timeTravelGroupIds=$country->vehicleTimeTravelGroupRate()->pluck('time_travel_group_id')->toArray();
        $timeTravelGroupRate=VTimeTravelGroup::whereNotIn('id',$timeTravelGroupIds)->get();
        return $timeTravelGroupRate;
    }
    public function save($data){
        foreach($data['rate'] as $key=>$val){
            $vehicleTimeTravelGroupRate= new VTimeTravelGroupRate;
            $vehicleTimeTravelGroupRate->country_id=$data['country_id'];
            $vehicleTimeTravelGroupRate->time_travel_group_id=$data['time_travel_group_id'][$key];
            $vehicleTimeTravelGroupRate->rate=$data['rate'][$key];
            $currency=Country::where('id', $data['country_id'])->first();
            $vehicleTimeTravelGroupRate->currency=$currency->currency;
            $vehicleTimeTravelGroupRate->status='1';
             $vehicleTimeTravelGroupRate->save();
         }
    }
}
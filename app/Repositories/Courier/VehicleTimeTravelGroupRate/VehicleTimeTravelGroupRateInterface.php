<?php

namespace App\Repositories\Courier\VehicleTimeTravelGroupRate;

interface VehicleTimeTravelGroupRateInterface {

    public function all();
    public function findById($id);
    public function update($data, $id);
    public function delete($id);
    public function createDataAll($data);   
    public function country(); 
    public function save($data);
}
<?php

namespace App\Repositories\Courier\AgencyClass;

interface AgencyClassInterface {

    public function all();
    public function findById($id);
    public function update($data, $id);
    public function delete($id);
    public function save($data);
}
<?php

namespace App\Repositories\Courier\AgencyClass;

use App\Models\AgencyClass;
use App\Models\Airport;
use Illuminate\Http\Request;
use App\Repositories\Courier\AgencyClass\AgencyClassInterface as AgencyClassInterface;
use DB;
use App\Models\Country;
use App\Models\CourierClass;

class AgencyClassRepository implements AgencyClassInterface
{
    public function all()
    {
        return $agencyClass=CourierClass::ofAgencyType()->get();
    }
    public function findById($id)
    {
        return $agencyClass=CourierClass::ofAgencyType()::find($id);
    }
    public function update($data, $id)
    {
        $agencyClass=CourierClass::ofAgencyType()::find($id);
        if($data['type']==1){
            $className=$data['name']. '-Pickup';
        }
        else{
            $className=$data['name']. '-Drop';
        }
        $agencyClass->name=$className;
        $agencyClass->type=$data['type'];
        $agencyClass->description=$data['description'];
        $agencyClass->status=$data['status'];
        if($data['type']==1){
            $name=$data['name']. '-pickup';
        }
        else{
            $name=$data['name']. '-drop';
        }
        //dd($name);
        $slugs=str_slug($name);
        $agencyClass->slug=$slugs;
        $agencyClass->save();
    }
    public function delete($id){
        $agencyClass=CourierClass::ofAgencyType()::find($id);
        $agencyClass->delete();
    
    }
    public function save($data){
        $agencyClass=new CourierClass();
        if($data['type']==1){
            $className=$data['name']. '-Pickup';
        }
        else{
            $className=$data['name']. '-Drop';
        }
        $agencyClass->name=$className;
        $agencyClass->type=$data['type'];
        $agencyClass->description=$data['description'];
        $agencyClass->status=$data['status'];
        if($data['type']==1){
            $name=$data['name'].'pickup';
        }
        else{
            $name=$data['name'].'drop';
        }
        $slugs=str_slug($name);
        $agencyClass->slug=$slugs;
        $agencyClass->save();
    }

    
}
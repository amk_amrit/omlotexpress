<?php

namespace App\Repositories\Courier\AgencyClassCharge;

use App\Models\QuantityGroup;
use App\Models\AgencyClassCharge;

use App\Models\QuantityGroupRange;
use App\Models\TransitClassCharge;

class AgencyClassChargeRepository
{
    public function save($request)
    {
        foreach ($request->quantity_group_range_id as $key => $quantityGroupRange) {

            AgencyClassCharge::updateOrCreate(
                [
                    'courier_agency_class_id' => $request->courier_agency_class_id,
                    'quantity_group_range_id' => $quantityGroupRange,
                    'country_id' => authEmployee()->office->country_id
                ],
                [
                    'origin_rate' => $request->origin_rate[$key],
                    'destination_rate' => $request->destination_rate[$key],
                    'rate_type' => $request->rate_type[$key],
                    'currency' => authEmployee()->office->country->currency,
                    'created_by' => authEmployee()->id,
                    'updated_by' => authEmployee()->id,

                ]
            );
        }
    }

    public function getQuantityGroupRange($request)
    {

        // $quantityGroupRanges = QuantityGroupRange::where('quantity_group_id', $request->quantity_group_id)->get();

        $agencyCharges = AgencyClassCharge::where('courier_agency_class_id', $request->courier_agency_class_id);
        $quantityGroupWithRates = QuantityGroupRange::leftJoinSub($agencyCharges, 'agency_charges',  function ($join) {
            $join->on('quantity_group_ranges.id', '=', 'agency_charges.quantity_group_range_id');
            })->where('quantity_group_id', $request->quantity_group_id)
                ->select('quantity_group_ranges.*', 'agency_charges.origin_rate', 'agency_charges.destination_rate', 'agency_charges.rate_type')
                ->get();

        return $quantityGroupWithRates;
    }

    public function editQuantityGroupRange($request)
    {

        $quantityGroupWithRates = QuantityGroup::leftJoin('quantity_group_ranges', 'quantity_group_ranges.quantity_group_id', 'quantity_groups.id')
            ->leftJoin('agency_class_charges', 'agency_class_charges.quantity_group_range_id', 'quantity_group_ranges.id')
            ->where('quantity_groups.id', $request->quantity_group_id)
            ->where('quantity_group_ranges.country_id', authEmployee()->office->country_id)
            ->where('agency_class_charges.courier_agency_class_id', $request->courier_agency_class_id)
            ->orWhere('agency_class_charges.courier_agency_class_id', Null)
            ->select('quantity_group_ranges.*', 'agency_class_charges.origin_rate', 'agency_class_charges.destination_rate', 'agency_class_charges.rate_type')
            ->orderBy('quantity_group_ranges.id', 'asc')
            ->get();
        return $quantityGroupWithRates;
    }
}

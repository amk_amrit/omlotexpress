<?php

namespace App\Repositories\Courier\AgencyClassCharge;

interface AgencyClassChargeInterface {

    public function all();
    public function findById($id);
    public function update($data);
    public function delete($id);
    public function save($data);
    public function country();
    public function agencyClass();
    public function getQuantityGroupData($country_id, $class_id);
    public function quantityGroup();
}
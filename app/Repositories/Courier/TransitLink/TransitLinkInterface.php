<?php

namespace App\Repositories\Courier\TransitLink;

interface TransitLinkInterface {

    public function all();
    public function findById($id);
    public function update($data);
    public function delete($id);
    public function save($data);
    public function transitLinkData($transit_id);
    public function transit();
}
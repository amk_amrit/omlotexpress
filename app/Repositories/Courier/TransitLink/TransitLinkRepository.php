<?php

namespace App\Repositories\Courier\TransitLink;

use App\Models\Agency;
use App\Models\AgencyClass;
use App\Models\AgencyLink;
use App\Models\Airport;
use Illuminate\Http\Request;
use App\Repositories\Courier\TransitLink\TransitLinkInterface as TransitLinkInterface;
use DB;
use App\Models\Country;
use App\Models\Transit;
use App\Models\TransitLink;

class TransitLinkRepository implements TransitLinkInterface
{
    public function all()
    {
        return $transtlink=TransitLink::with('transits','transits')->groupBy('transit_id')->get();
    }
    public function findById($id)
    {
         return $transitLinkData=TransitLink::with('transits','transits')->where('transit_id', $id)->get();
    }
    public function update($data)
    {
       // dd($data);
        foreach($data['distance'] as $key=>$val){
            $transitLinkData=TransitLink::find($data['link_transit_id'][$key]);
             $transitLinkData->transit_id=$data['transit_id'];
             $transitLinkData->link_id=$data['link_id'][$key];
             $transitLinkData->distance=$data['distance'][$key];
             $transitLinkData->save();
         }
        
    }
    public function delete($id){
        // $agency=Agency::find($id);
        // if($agency->status==0){
        // $agency->status=1;
        // $agency->save(); 
        // }
        // else{
        //     $agency->status=0;
        // $agency->save();
        // }
    
    }
    public function save($data){
        foreach($data['distance'] as $key=>$val){
            $transitLink= new TransitLink;
            $transitLink->transit_id=$data['transit_id'];
            $transitLink->link_id=$data['link_id'][$key];
            $transitLink->distance=$data['distance'][$key];
            $transitLink->save();
         }
    }
    public function transitLinkData($transit_id){
        $transitLinkDataId=TransitLink::where('transit_id', $transit_id)->pluck('link_id')->toArray();
        array_push($transitLinkDataId, $transit_id);
        $getTransitData=Transit::whereNotIn('id', $transitLinkDataId)->get();
        return $getTransitData;
    }
    public function transit(){
         return $transit=Transit::all();
    }
    
}
<?php

namespace App\Repositories\Courier\AirportCharge;

use App\Models\Airport;
use App\Models\AirportCharge;
use Illuminate\Http\Request;
use App\Repositories\Courier\AirportCharge\AirportChargeInterface as AirportChargeInterface;
use DB;
use App\Models\Country;
use App\Models\AirportLink;
use App\Models\QuantityGroup;
use App\Models\QuantityGroupRange;

class AirportChargeRepository implements AirportChargeInterface
{
    public function all()
    {

    }
    public function findById($id)
    {
        return $airportLinkData = AirportLink::with(['airportOne' => function ($query) {
            $query->select('id', 'name as airportOneName');
        }, 'airportTwo' => function ($query) {
            $query->select('id', 'name as airportTwoName');
        }])
            ->where('created_by', authEmployee()->id)
            ->where('id', $id)
            ->get();
    }
    public function update($data, $id)
    {
        //dd($data);
        foreach ($data['rate'] as $key => $val) {
            $airportChargeData = AirportCharge::find($data['id'][$key]);
            $airportChargeData->airport_link_id=$id;
            $airportChargeData->quantity_group_range_id=$data['quantity_group_range_id'][$key];
            $airportChargeData->rate=$data['rate'][$key];
            $airportChargeData->rate_type=$data['rate_type'][$key];
            $linkType=AirportLink::where('id', $id)->first();
            if($linkType->air_link_type='domestic'){
                $currency=Country::where('id', authEmployee()->office->country_id)->first();
                $airportChargeData->currency=$currency->currency;
            }
            else{
                $airportChargeData->currency='$';
            }
            $airportChargeData->created_by = authEmployee()->id;
            $airportChargeData->updated_by = authEmployee()->id;
            $airportChargeData->status = '1';
            $airportChargeData->save();
        }

    }
    public function delete($id){
        // $airport=Airport::find($id);
        // if($airport->status==0){
        // $airport->status=1;
        // $airport->save(); 
        // }
        // else{
        //     $airport->status=0;
        // $airport->save();
        // }
    
    }
    public function save($data){
    //     foreach ($data['rate'] as $key => $val) {
    //         if(isset($data['id'][$key])){
    //         $airportCharge = AirportCharge::find($data['id'][$key]);
    //         $airportCharge->airport_link_id = $data['airport_link_id'];
    //         $airportCharge->quantity_group_range_id = $data['quantity_group_range_id'][$key];            $airportCharge->quantity_group_range_id = $data['quantity_group_range_id'][$key];
    //         $airportCharge->rate = $data['rate'][$key];
    //         $airportCharge->rate_type = $data['rate_type'][$key];
    //         $linkType=AirportLink::where('id', $data['airport_link_id'])->first();
    //         if($linkType->air_link_type='domestic'){
    //             $currency=Country::where('id', authEmployee()->office->country_id)->first();
    //             $airportCharge->currency=$currency->currency;
    //         }
    //         else{
    //             $airportCharge->currency='$';
    //         }
    //         $airportCharge->created_by = authEmployee()->id;
    //         $airportCharge->updated_by = authEmployee()->id;
    //         $airportCharge->status = '1';
    //         $airportCharge->save();
    //     }
    //     else{

    //         $airportCharge = new AirportCharge ;
    //         $airportCharge->airport_link_id = $data['airport_link_id'];
    //         $airportCharge->quantity_group_range_id = $data['quantity_group_range_id'][$key];            $airportCharge->quantity_group_range_id = $data['quantity_group_range_id'][$key];
    //         $airportCharge->rate = $data['rate'][$key];
    //         $airportCharge->rate_type = $data['rate_type'][$key];
    //         $linkType=AirportLink::where('id', $data['airport_link_id'])->first();
    //         if($linkType->air_link_type='domestic'){
    //             $currency=Country::where('id', authEmployee()->office->country_id)->first();
    //             $airportCharge->currency=$currency->currency;
    //         }
    //         else{
    //             $airportCharge->currency='$';
    //         }
    //         $airportCharge->created_by = authEmployee()->id;
    //         $airportCharge->updated_by = authEmployee()->id;
    //         $airportCharge->status = '1';
    //         $airportCharge->save();
    //     }
    // }
    foreach ($data['rate'] as $key => $val) {
         $linkType=AirportLink::where('id', $data['airport_link_id'])->first();
            if($linkType->air_link_type='domestic'){
                $currency=Country::where('id', authEmployee()->office->country_id)->first();
                $airportChargeCurrency=$currency->currency;
            }
            else{
                $airportChargeCurrency='$';
            }

            $airportCharge=AirportCharge::updateOrCreate([
                'airport_link_id'=>$data['airport_link_id'],
                'quantity_group_range_id'=>$data['quantity_group_range_id'][$key]
            ],
            [
                'rate' => $data['rate'][$key],
                'currency'=> $airportChargeCurrency,
                'rate_type' =>  $data['rate_type'][$key],
                'created_by' => authEmployee()->id,
                'updated_by' => authEmployee()->id,
                'status' => '1',
            ],
        );
    }

    }
    public function getQuantityGroupRange($data){
        $quantityGroupDataId=AirportCharge::where('airport_link_id', $data['airport_link_id'])->pluck('quantity_group_range_id')->toArray();
        $quantityGroupData=QuantityGroupRange::where('country_id', authEmployee()->office->country_id)
                                ->where('quantity_group_id', $data['quantity_group_id'])
                                ->whereNotIn('id',$quantityGroupDataId )
                                ->get();
        $airportCharge =AirportCharge ::where('airport_link_id', $data['airport_link_id']);
        $quantityGroupWithRates = QuantityGroupRange::leftJoinSub($airportCharge, 'airport_charge',  function ($join) {
                                    $join->on('quantity_group_ranges.id', '=', 'airport_charge.quantity_group_range_id');
                                    })->where('quantity_group_id', $data['quantity_group_id'])
                                        ->select('quantity_group_ranges.*', 'airport_charge.rate', 'airport_charge.rate_type','airport_charge.id as airportChargeId')
                                        ->get();
            return $quantityGroupWithRates;
    }
    public function transit(){
        
    }
    public function quantityGroup(){
        return $quantityGroup=QuantityGroup::get();
    }
    public function findByIdData($id){
        return $airportChargeData=DB::table('airport_charges')
                                    ->join('quantity_group_ranges', 'quantity_group_ranges.id', '=' ,'airport_charges.quantity_group_range_id')
                                    ->join('quantity_groups', 'quantity_groups.id', '=' , 'quantity_group_ranges.quantity_group_id')
                                    ->join('airport_links', 'airport_links.id', '=' ,'airport_charges.airport_link_id')
                                    ->join('airports as a1', 'a1.id', '=', 'airport_links.airport_one_id')
                                    ->join('airports as a2', 'a2.id', '=', 'airport_links.airport_two_id')
                                    ->select('quantity_groups.group_name as groupName','quantity_group_ranges.min_weight','quantity_group_ranges.max_weight','quantity_group_ranges.id as quantityGroupId','airport_charges.*','a1.name as airportOneName','a2.name as airportTwoName')
                                    ->where('airport_charges.airport_link_id', $id)
                                    ->get();
    }
    public function airPortLinkName($id){

        return $airportLinkData=DB::table('airport_links')
                                    ->join('airports as a1', 'a1.id', '=', 'airport_links.airport_one_id')
                                    ->join('airports as a2', 'a2.id', '=', 'airport_links.airport_two_id')
                                    ->select('a1.name as airportOneName','a2.name as airportTwoName','airport_links.id')
                                    ->where('airport_links.id', $id)
                                    ->get();
    }
}
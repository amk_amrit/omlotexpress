<?php

namespace App\Repositories\Courier\AirportCharge;

interface AirportChargeInterface {

    public function all();
    public function findById($id);
    public function update($data, $id);
    public function delete($id);
    public function save($data);
    public function getQuantityGroupRange($data);
    public function quantityGroup();
    public function findByIdData($id);
    public function airPortLinkName($id);
}
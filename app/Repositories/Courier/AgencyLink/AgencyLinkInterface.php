<?php

namespace App\Repositories\Courier\AgencyLink;

interface AgencyLinkInterface {

    public function all();
    public function findById($id);
    public function update($data);
    public function delete($id);
    public function save($data);
    public function getAgancyData($agency_id);
    public function agency();
}
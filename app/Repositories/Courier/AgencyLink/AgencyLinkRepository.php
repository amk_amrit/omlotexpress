<?php

namespace App\Repositories\Courier\AgencyLink;

use App\Models\Agency;
use App\Models\AgencyClass;
use App\Models\AgencyLink;
use App\Models\Airport;
use Illuminate\Http\Request;
use App\Repositories\Courier\AgencyLink\AgencyLinkInterface as AgencyLinkInterface;
use DB;
use App\Models\Country;

class AgencyLinkRepository implements AgencyLinkInterface
{
    public function all()
    {
        return $agencyLink=AgencyLink::with('agency', 'agencyLink')->groupBy('agency_id')->get();
    }
    public function findById($id)
    {
        return $agencyLinkData=AgencyLink::with('agency','agencyLink')->where('agency_id', $id)->get();
    }
    public function update($data)
    {
        //dd($data['agency_id']);
        foreach($data['distance'] as $key=>$val){
            $agencyLinkData=AgencyLink::find($data['link_agency_id'][$key]);
             $agencyLinkData->agency_id=$data['agency_id'];
             $agencyLinkData->link_id=$data['link_id'][$key];
             $agencyLinkData->distance=$data['distance'][$key];
             $agencyLinkData->save();
         }
        
    }
    public function delete($id){
        // $agency=Agency::find($id);
        // if($agency->status==0){
        // $agency->status=1;
        // $agency->save(); 
        // }
        // else{
        //     $agency->status=0;
        // $agency->save();
        // }
    
    }
    public function save($data){
        foreach($data['distance'] as $key=>$val){
            $agencyLink= new AgencyLink;
            $agencyLink->agency_id=$data['agency_id'];
            $agencyLink->link_id=$data['link_id'][$key];
            $agencyLink->distance=$data['distance'][$key];
            $agencyLink->save();
         }
    }
    public function getAgancyData($agency_id){
        $agencyLinkDataId=AgencyLink::where('agency_id', $agency_id)->pluck('link_id')->toArray();
        array_push($agencyLinkDataId, $agency_id);
        $getAgencyData=Agency::whereNotIn('id', $agencyLinkDataId)->get();
        return $getAgencyData;
    }
    public function agency(){
        return $agency=Agency::all();
    }
    
}
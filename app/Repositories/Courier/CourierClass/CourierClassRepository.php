<?php

namespace App\Repositories\Courier\CourierClass;

use App\Models\CourierClass;
use Illuminate\Http\Request;
use App\Repositories\Courier\CourierClass\CourierClassInterface as CourierClassInterface;
use DB;
class CourierClassRepository implements CourierClassInterface
{
    public function all()
    {
        return $courierClass=CourierClass::get();
    }
    public function findById($id)
    {
        return $courierClass=CourierClass::find($id);
    }
    public function update($data, $id)
    {
        $courierClass=CourierClass::find($id);
        $courierClass->name=$data['name'];
        $courierClass->description=$data['description'];
        $courierClass->status=$data['status'];
        $slugs=str_slug($data['name']);
        $courierClass->slug=$slugs;
        $courierClass->country_id=authEmployee()->office->country_id;
        $courierClass->created_by=authEmployee()->id;
        $courierClass->updated_by=authEmployee()->id;
        $courierClass->save();
    }
    public function delete($id){
        $courierClass=CourierClass::find($id);
        if($courierClass->status==0){
        $courierClass->status=1;
        $courierClass->save(); 
        }
        else{
            $courierClass->status=0;
        $courierClass->save();
        }
    
    }
    public function save($data){
        //dd($data);
        foreach($data['name'] as $key=>$val){
            $courierClass= new CourierClass;
            $courierClass->name=$data['name'][$key];
            $courierClass->description=$data['description'][$key];
            $slugs=str_slug($data['name'][$key]);
            $courierClass->for_office_type=$data['for_office_type'];
            $courierClass->slug=$slugs;
            $courierClass->country_id=authEmployee()->office->country_id;
            $courierClass->created_by=authEmployee()->id;
            $courierClass->updated_by=authEmployee()->id;
            $courierClass->save();
         }
    }
    
}
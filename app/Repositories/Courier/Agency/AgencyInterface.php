<?php

namespace App\Repositories\Courier\Agency;

interface AgencyInterface {

    public function all();
    public function findById($id);
    public function update($data, $id);
    public function delete($id);
    public function save($data);
    public function country();
    public function agencyClass();
}
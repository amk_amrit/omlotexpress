<?php

namespace App\Repositories\Courier\Agency;

use App\Models\Agency;
use App\Models\AgencyClass;
use App\Models\Airport;
use Illuminate\Http\Request;
use App\Repositories\Courier\Agency\AgencyInterface as AgencyInterface;
use DB;
use App\Models\Country;
use App\Models\Transit;

class AgencyRepository implements AgencyInterface
{
    public function all()
    {
        return $agency=Agency::with('country','sourceAgencyClass','destinationAgencyClass' ,'transit')->get();
    }
    public function findById($id)
    {
        return $agency=Agency::with('country','sourceAgencyClass','destinationAgencyClass', 'transit')->find($id);
    }
    public function update($data, $id)
    {
        //dd($data);
        $agency=Agency::find($id);
        $agency->name=$data['name'];
        $agency->email=$data['email'];
        $agency->phone=$data['phone'];
        $agency->transit_link_id=$data['transit_link_id'];
        $agency->address=$data['address'];
        $agency->latitude=$data['latitude'];
        $agency->longitude=$data['longitude'];
        $agency->country_id=$data['country_id'];
        $agency->orgin_class_id=$data['orgin_class_id'];
        $agency->destination_class_id=$data['destination_class_id'];
        $agency->status=1;
        $agency->update($data);
    }
    public function delete($id){
        $agency=Agency::find($id);
        if($agency->status==0){
        $agency->status=1;
        $agency->save(); 
        }
        else{
            $agency->status=0;
        $agency->save();
        }
    
    }
    public function save($data){
       // dd($data);
        $agency=new Agency;
        $agency->name=$data['name'];
        $agency->email=$data['email'];
        $agency->phone=$data['phone'];
        $agency->transit_link_id=$data['transit_link_id'];
        $agency->address=$data['address'];
        $agency->latitude=$data['latitude'];
        $agency->longitude=$data['longitude'];
        $agency->country_id=$data['country_id'];
        $agency->orgin_class_id=$data['orgin_class_id'];
        $agency->destination_class_id=$data['destination_class_id'];
        $agency->status=$data['status'];

        $agency->save($data);
    }
    public function country(){
        return $country=Country::all();
    }
    public function agencyClass(){
        return $agencyClass=AgencyClass::all();
    }
    public function transit(){
        return $transit=Transit::all();
    }
    
}
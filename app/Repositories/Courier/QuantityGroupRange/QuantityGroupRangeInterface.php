<?php

namespace App\Repositories\Courier\QuantityGroupRange;

interface QuantityGroupRangeInterface {

    public function all();
    public function quanityGroup($quantityGroupRange);
    public function country();
    public function save($data, $min, $max);
    public function findById($id);
    public function findSingleById($id);
    public function update(array $data ,$max, $min, $id);
}
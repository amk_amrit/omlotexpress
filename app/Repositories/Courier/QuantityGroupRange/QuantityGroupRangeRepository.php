<?php

namespace App\Repositories\Courier\QuantityGroupRange;

use Illuminate\Http\Request;
use App\Repositories\Courier\QuantityGroupRange\QuantityGroupRangeInterface as QuantityGroupRangeInterface;
use App\Models\QuantityGroup;
use App\Models\QuantityGroupRange;
use App\Models\Country;
use DB;


class QuantityGroupRangeRepository implements QuantityGroupRangeInterface
{
   public function all(){
    return  $country=DB::table('quantity_groups')
                        ->join('quantity_group_ranges', 'quantity_group_ranges.quantity_group_id', '=' ,'quantity_groups.id')
                        ->select('quantity_groups.*')
                        ->addSelect(DB::raw('count(quantity_group_ranges.id) as count_quantity_group_ranges'))
                        ->addSelect(DB::raw('substring_index(group_concat(quantity_group_ranges.min_weight,"-",quantity_group_ranges.max_weight  ORDER BY quantity_group_ranges.id ASC SEPARATOR " , "), " , ", 4) as quantiry_group_ranges'))
                        ->groupBy('quantity_group_ranges.quantity_group_id')
                        ->get();
  }
  public function quanityGroup($quantityGroupRange)
  {
      //dd($quantityGroupRange);
      $quantityGroupId=[];
      foreach($quantityGroupRange as $quantityGroupRanges){
          $quantityGroupId[]=$quantityGroupRanges->id;
      }
      return  $quanityGroup= QuantityGroup::whereNotIn('id',$quantityGroupId )
                                            ->where('status',1)
                                            ->get();
            //dd($quanityGroup);
  }
  public function country()
  {
      return $country=Country::all();
  }
  public function save($data, $min, $max){
      //dd($data);
      $country_id=authEmployee()->office->country_id;
      $created_employe=authEmployee()->id;
      $previousDatas=QuantityGroupRange::where('quantity_group_id', $data['quantity_group_id'])->where('country_id',authEmployee()->office->country_id )->get(); 
      foreach($data['max_weight'] as $key=>$val){ 
            if(isset($data['id'][$key])){
                $quantityGroupRange=QuantityGroupRange::find($data['id'][$key]);
                $quantityGroupRange->country_id=authEmployee()->office->country_id;
                $quantityGroupRange->quantity_group_id=$data['quantity_group_id'];
                $quantityGroupRange->min_weight=$min[$key];
                $quantityGroupRange->max_weight=$max[$key];
                $quantityGroupRange->created_by=authEmployee()->id;
                $quantityGroupRange->updated_by=authEmployee()->id;
                $quantityGroupRange->update();
            }
             else{
                $quantityGroupRange= new QuantityGroupRange;
                $quantityGroupRange->country_id=authEmployee()->office->country_id;
                $quantityGroupRange->quantity_group_id=$data['quantity_group_id'];
                $quantityGroupRange->min_weight=$min[$key];
                $quantityGroupRange->max_weight=$max[$key];
                $quantityGroupRange->created_by=authEmployee()->id;
                $quantityGroupRange->updated_by=authEmployee()->id;
                $quantityGroupRange->save();
             }
        }
     
  }
  public function findById($id){
      $country_id=authEmployee()->office->country_id;
    return $quantityGroupRange=QuantityGroupRange::with('quantityGroup')->where('quantity_group_id', $id)->where('country_id', $country_id)->get();
    }
    public function findSingleById($id){
        $country_id=authEmployee()->office->country_id;
        return $quantiryGroupSingle=QuantityGroup::where('id', $id)->firstOrFail();
    }
    public function deletePerviousData($data){
        $numberOfArray=count($data['deleteId']);
        
        for($i=0; $i<$numberOfArray; $i++){
         $result = DB::table('quantity_group_ranges')->where('id', $data['deleteId'][$i])->delete();
         }
            return true;
    }
    public function update(array $data ,$max, $min, $id){
        foreach($data['max_weight'] as $key=>$val){
        $quantityGroupCountry= new QuantityGroupCountry;
        $quantityGroupCountry->country_id=$data['country_id'];
        $quantityGroupCountry->quantity_group_id=$data['quantity_group_id'];
        $quantityGroupCountry->min_weight=$max[$key];
        $quantityGroupCountry->max_weight=$min[$key];
         $quantityGroupCountry->save();
         }
        return true;
        }
}
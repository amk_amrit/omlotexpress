<?php

namespace App\Repositories\Courier\AgencyTransitVehicleCharge;

interface AgencyTransitVehicleChargeInterface {

    public function agencyTransitLinkName($id);
    public function quantityGroupName();
    public function getQuantityGroupRange($data);
    public function save($data);
    public function update($data, $id);
    public function quantityGroupRange($id);
}
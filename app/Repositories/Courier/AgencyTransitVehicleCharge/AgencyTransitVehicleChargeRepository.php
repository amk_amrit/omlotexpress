<?php

namespace App\Repositories\Courier\AgencyTransitVehicleCharge;

use App\Models\AgencyTransitVehicleCharge;
use App\Models\QuantityGroup;
use App\Models\Country;
use App\Models\QuantityGroupRange;
use Illuminate\Http\Request;

use App\Repositories\Courier\AgencyTransitVehicleCharge\AgencyTransitVehicleChargeInterface as AgencyTransitVehicleChargeInterface;
use DB;


class AgencyTransitVehicleChargeRepository implements AgencyTransitVehicleChargeInterface
{
    public function agencyTransitLinkName($id)
    {
        return $agencyTransitLinked=DB::table('agency_transit_connections')
                                        ->join('agencies', 'agencies.id', '=' , 'agency_transit_connections.agency_id')
                                        ->join('transits', 'transits.id', '=' , 'agency_transit_connections.transit_id')
                                        ->join('offices as o1' ,'o1.id', '=' ,'agencies.office_id')
                                        ->join('offices as o2', 'o2.id', '=' , 'transits.office_id')
                                        ->select('o1.name as agencyName', 'o2.name as transitName','agency_transit_connections.id as agenct_transit_connection_id')
                                        ->where('agency_transit_connections.id', $id)
                                        ->get();

    }
    public function quantityGroupName(){
        return $quantityGroupName=QuantityGroup::all();
    }                       

    public function getQuantityGroupRange($data){
        $quantityGroupDataId=AgencyTransitVehicleCharge::where('agency_transit_connection_id', $data['agency_transit_connection_id'])->pluck('quantity_group_range_id')->toArray();
        $quantityGroupData=QuantityGroupRange::whereNotIn('id',$quantityGroupDataId )
                                ->where('country_id', authEmployee()->office->country_id)
                                ->where('quantity_group_id', $data['quantity_group_id'])
                                ->get();
            return $quantityGroupData;
    }
    public function save($data){
        foreach($data['rate'] as $key=>$val){
            $agencyTransitVehicleCharge= new AgencyTransitVehicleCharge();
            $agencyTransitVehicleCharge->agency_transit_connection_id=$data['agency_transit_connection_id'];
            $agencyTransitVehicleCharge->quantity_group_range_id=$data['quantity_group_range_id'][$key];
            $agencyTransitVehicleCharge->rate=$data['rate'][$key];
            $agencyTransitVehicleCharge->rate_type=$data['rate_type'][$key];
            $currency=Country::where('id', authEmployee()->office->country_id)->first();
            $agencyTransitVehicleCharge->currency=$currency->currency;
            $agencyTransitVehicleCharge->created_by = authEmployee()->id;
            $agencyTransitVehicleCharge->updated_by = authEmployee()->id;
             $agencyTransitVehicleCharge->save();
         }
    }
    public function quantityGroupRange($id){
        return $quantityGroupRange=DB::table('agency_transit_vehicle_charges')
                                        ->join('quantity_group_ranges', 'quantity_group_ranges.id', '=', 'agency_transit_vehicle_charges.quantity_group_range_id')
                                        ->join('quantity_groups', 'quantity_groups.id', '=' ,'quantity_group_ranges.quantity_group_id')
                                        ->select('agency_transit_vehicle_charges.*', 'quantity_groups.group_name', 'quantity_group_ranges.min_weight','quantity_group_ranges.max_weight')
                                        ->where('agency_transit_vehicle_charges.agency_transit_connection_id', $id)
                                        ->get();
    }
    public function update($data, $id){
        foreach($data['rate'] as $key=>$val){
            $agencyTransitVehicleCharge= AgencyTransitVehicleCharge::find($data['id'][$key]);
            $agencyTransitVehicleCharge->agency_transit_connection_id=$id;
            $agencyTransitVehicleCharge->quantity_group_range_id=$data['quantity_group_range_id'][$key];
            $agencyTransitVehicleCharge->rate=$data['rate'][$key];
            $agencyTransitVehicleCharge->rate_type=$data['rate_type'][$key];
            $currency=Country::where('id', authEmployee()->office->country_id)->first();
            $agencyTransitVehicleCharge->currency=$currency->currency;
            $agencyTransitVehicleCharge->created_by = authEmployee()->id;
            $agencyTransitVehicleCharge->updated_by = authEmployee()->id;
             $agencyTransitVehicleCharge->save();
         }
    }
}
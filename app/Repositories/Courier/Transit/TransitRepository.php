<?php

namespace App\Repositories\Courier\Transit;

use App\Models\Country;
use App\models\Office;
use App\Models\Service;
use App\Models\Transit;
use App\Models\TransitClass;
use Illuminate\Http\Request;
use App\Repositories\Courier\Transit\TransitInterface as TransitInterface;
use DB;

class TransitRepository implements TransitInterface
{
    public function all()
    {
        return $transit=Transit::with('country','transitClass','office')->get();
    }
    public function findById($id)
    {
        return $transit=Transit::with('country','transitClass','office')->find($id);
    }
    public function update($data, $id)
    {
        //dd($data);
        //dd($id);
        $transit=Transit::find($id);
        $transit->name=$data['name'];
        $transit->address=$data['address'];
        $transit->email=$data['email'];
        $transit->phone=$data['phone'];
        $transit->description=$data['description'];
        $transit->transit_class_id=$data['transit_class_id'];
        $transit->country_id=$data['country_id'];
        $transit->status=$data['status'];
        $transit->longitude=$data['longitude'];
        $transit->latitude=$data['latitude'];
        $transit->office_id=$data['office_id'];

        //dd($transit);

        $transit->save($data);
    }
    public function delete($id){
        $transit=Transit::find($id);
        if($transit->status==0){
        $transit->status=1;
        $transit->save(); 
        }
        else{
            $transit->status=0;
        $transit->save();
        }
    
    }
    public function save($data){
       // dd($data);
            $transit=new Transit;
            $transit->insert($data);
    }
    public function transitClass(){
        return $transitClass=TransitClass::all();
    }
    public function country(){
        return $country=Country::all();
    }
    public function office(){
        return $office=Office::all();
    }
    
}
<?php

namespace App\Repositories\Courier\Airport;

use App\Models\Airport;
use Illuminate\Http\Request;
use App\Repositories\Courier\Airport\AirportRateInterface as AirportRateInterface;
use DB;
use App\Models\Country;
use App\Models\Transit;

class AirportRepository implements AirportRateInterface
{
    public function all()
    {
        // return $airport=Airport::groupBy('scope')->get();
       return  $country=DB::table('airports')
                        ->select('airports.*')
                        ->addSelect(DB::raw('count(airports.id) as count_country_airports'))
                        ->addSelect(DB::raw('substring_index(group_concat(airports.name  ORDER BY airports.created_at DESC SEPARATOR " , "), " , ", 3) as country_airports'))
                        ->groupBy('airports.scope')
                        ->get();
                       //dd($country);
    }
    public function findById($id)
    {
        return $airport=DB::table('airports')
                            ->join('transits', 'transits.id', '=' ,'airports.operate_by')
                            ->join('offices', 'offices.id', '=', 'transits.office_id')
                            ->select('airports.*','offices.name as officeName')
                            ->where('offices.country_id', authEmployee()->office->country_id)
                            ->where('airports.scope', $id)
                            ->get();
    }
    public function update($data, $id)
    {
        //dd($data);
        $airport=Airport::find($id);
        $airport->name=$data['name'];
            $airport->address=$data['address'];
            $airport->country_id=authEmployee()->office->country_id;
            $slugs=str_slug($data['name']);
            $airport->slug=$slugs;
            $airport->status=$data['status'];
            $airport->scope=$data['scope'];
            $airport->operate_by=$data['operate_by'];
            $airport->latitude=$data['latitude'];
            $airport->longitude=$data['longitude'];
            $airport->created_by=authEmployee()->id;
            $airport->updated_by=authEmployee()->id;
            $airport->save($data);
    }
    public function delete($id){
        $airport=Airport::find($id);
        if($airport->status==0){
        $airport->status=1;
        $airport->save(); 
        }
        else{
            $airport->status=0;
        $airport->save();
        }
    
    }
    public function save($data){
            $airport= new Airport;
            $airport->name=$data['name'];
            $airport->address=$data['address'];
            $airport->country_id=authEmployee()->office->country_id;
            $slugs=str_slug($data['name']);
            $airport->slug=$slugs;
            $airport->status=$data['status'];
            $airport->scope=$data['scope'];
            $airport->operate_by=$data['operate_by'];
            $airport->latitude=$data['latitude'];
            $airport->longitude=$data['longitude'];
            $airport->created_by=authEmployee()->id;
            $airport->updated_by=authEmployee()->id;
            $airport->save($data);

    }
    public function findBySingleId($id){
        return $country=Airport::find($id);
    }
    public function transit(){
        return $transit=DB::table('transits')
                        ->join('offices', 'offices.id', '=', 'transits.office_id')
                        ->where('country_id', authEmployee()->office->country_id)
                        ->get();
    }
}
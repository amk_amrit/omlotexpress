<?php

namespace App\Repositories\Courier\Airport;

interface AirportRateInterface {

    public function all();
    public function findById($id);
    public function update($data, $id);
    public function delete($id);
    public function save($data);
    public function findBySingleId($id);
}
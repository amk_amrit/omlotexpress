<?php

namespace App\Repositories\Courier\AirportLink;

interface AirportLinkInterface {

    public function all();
    public function findById($data);
    public function update($data, $id);
    public function delete($id);
    public function save($data);
    public function country();
    public function editData($id);
    public function airport();
    public function quantiryName();
    public function getDataCountryOne($country_id, $scope);
    public function getDataCountryTwo($country_id, $scope, $airport_one_id);

}
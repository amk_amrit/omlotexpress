<?php

namespace App\Repositories\Courier\AirportLink;

use App\Models\Airport;
use App\Models\AirportLink;
use Illuminate\Http\Request;
use App\Repositories\Courier\AirportLink\AirportLinkInterface as AirportLinkInterface;
use DB;
use App\Models\Country;
use App\Models\QuantityGroup;
use App\Models\QuantityGroupCountry;

class AirportLinkRepository implements AirportLinkInterface
{
    public function all()
    {

        return $airportLink = AirportLink::with(['airportOne' => function ($query) {
            $query->select('id', 'name as airportOneName');
        }, 'airportTwo' => function ($query) {
            $query->select('id', 'name as airportTwoName');
        }])
            ->where('created_by', authEmployee()->id)
            ->get();
    }
    public function findById($data)
    {
        $airport_id = $data['airport'];
        $airpot = Airport::find($airport_id);
        $country_id = $data['country'];
        $country = Country::find($country_id);
        $category_id = $data['quantity_id'];
        $quantityGroup = QuantityGroupCountry::where('id', $category_id)->first();
        $airportLink = AirportLink::where('source_airport_id', $airpot->id)->where('quantity_group_id', $quantityGroup->id)->where('country_id', $country->id)->pluck('destination_airport_id')->toArray();
        array_push($airportLink, $country->id);
        $airpotData = Airport::whereNotIn('id', $airportLink)->get();
        return $airpotData;
    }
    public function update($data, $id)
    {
        $airportLink = AirportLink::find($id);
        $airportLink->source_airport_id = $data['source_airport_id'];
        $airportLink->quantity_group_id = $data['quantity_group_id'];
        $airportLink->country_id = $data['country_id'];
        $airportLink->destination_airport_id = $data['destination_airport_id'];
        $airportLink->rate = $data['rate'];
        $currency = Country::find($data['country_id']);
        $airportLink->currency = $currency->currency;
        $airportLink->status = $data['status'];
        $airportLink->save();
    }
    public function delete($id)
    {
        $airportLink = AirportLink::find($id);
        if ($airportLink->status == 0) {
            $airportLink->status = 1;
            $airportLink->save();
        } else {
            $airportLink->status = 0;
            $airportLink->save();
        }
    }
    public function save($data)
    {
         //dd($data);
        foreach ($data['airport_two_id'] as $key => $val) {
            $airportLink = new AirportLink;
            $airportLink->airport_one_id = $data['airport_one_id'];
            $airportLink->airport_two_id = $data['airport_two_id'][$key];
            if($data['country_id_one']==$data['country_id_two']){
                $airportLink->air_link_type='domestic';
            }
            else{
                $airportLink->air_link_type='international';

            }
            $airportLink->created_by = authEmployee()->id;
            $airportLink->updated_by = authEmployee()->id;
            $airportLink->status = '1';
            $airportLink->save();
        }
    }
    public function country()
    {
        return $country=Country::where('status', 1)->get();
    }
    public function airport()
    {
        return $airpots = Airport::all();
    }
    public function quantityGroup()
    {
        return $quantityGroup = QuantityGroupCountry::with('quantityGroup')->get();
    }
    public function editData($id)
    {
        return $airportLink = AirportLink::with('country', 'airport', 'quantityGroupCountry')->find($id);
    }
    public function quantiryName()
    {
        return $quantityGroup = QuantityGroup::all();
    }
    public function getDataCountryOne($country_id, $scope){
        $airportLinkData=Airport::where('country_id', $country_id)->where('scope', $scope)->get();
        return $airportLinkData;
    }
    public function getDataCountryTwo($country_id, $scope, $airport_one_id){
        $airportLinkData=AirportLink::where('airport_one_id', $airport_one_id)->pluck('airport_two_id')->toArray();
        array_push($airportLinkData, $airport_one_id);
        $airportNewEntryData=Airport::where('country_id', $country_id)
                                ->where('scope', $scope)
                                ->whereNotIn('id',$airportLinkData )
                                ->get();
        return $airportNewEntryData;
    }

}

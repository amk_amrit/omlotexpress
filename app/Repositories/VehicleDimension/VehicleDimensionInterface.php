<?php

namespace App\Repositories\VehicleDimension;

interface VehicleDimensionInterface {


    public function all();

    public function save($data);

    public function find($id);

    public function update(array $data, $id);

    public function changeStatus($id);

    public function fetchData();

}
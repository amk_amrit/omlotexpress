<?php

namespace App\Repositories\VehicleDimension;

use Illuminate\Http\Request;
use App\Repositories\VehicleDimension\VehicleDimensionInterface as VehicleDimensionInterface;
use App\Models\TransportVehicleDimension;
use App\Models\TransportVehicle;
use Illuminate\Support\Str;


class VehicleDimensionRepository implements VehicleDimensionInterface
{
   

    public function all()
    {
        return TransportVehicleDimension::all();
    }

    public function find($id)
    {
        return TransportVehicleDimension::where('id', $id)->first();
    }


    public function changeStatus($id)
    {
        
        $vehicleDimension = TransportVehicleDimension::find($id);
        if($vehicleDimension->status == 0)
        {
            $vehicleDimension->status = 1;
        }else{
            
            $vehicleDimension->status = 0;
        }
        $vehicleDimension->save();
        return true;
    }

    public function save($data){
        $vehicleDimension = new TransportVehicleDimension;
        $vehicleDimension->create($data);
        if($vehicleDimension){
            return true;
        }
        else{
            return false;
        }
    }

    public function update(array $data, $id){
        $vehicleDimension = TransportVehicleDimension::find($id);
        $vehicleDimension->update($data);
        if($vehicleDimension){
            return true;
        }
        else{
            return false;
        }
    }

    public function fetchData(){
        $data['vehicles'] = TransportVehicle::where('status', 1)->get();
        return $data;
    }
}
<?php

namespace App\Repositories\TransportMedia;

use Illuminate\Http\Request;
use App\Repositories\TransportMedia\TransportMediaInterface as TransportMediaInterface;
use App\Models\TransportMedia;

class TransportMediaRepository implements TransportMediaInterface
{
   

    public function all()
    {
        return TransportMedia::all();
    }

    public function find($id)
    {
        return TransportMedia::where('id', $id)->first();
    }


    public function changeStatus($id)
    {
        
        $transportMedia = TransportMedia::find($id);
        if($transportMedia->status == 0)
        {
            $transportMedia->status = 1;
        }else{
            
            $transportMedia->status = 0;
        }
        $transportMedia->save();
        return true;
    }

    public function save($data){
        $transportMedia = new TransportMedia;
        $transportMedia->create($data);
        if($transportMedia){
            return true;
        }
        else{
            return false;
        }
    }

    public function update($data, $id){
        $transportMedia = TransportMedia::find($id);
        $transportMedia->update($data);
        if($transportMedia){
            return true;
        }
        else{
            return false;
        }
    }
}
<?php

namespace App\Repositories\PackageTypeRate;

use App\Models\Country;
use App\Repositories\BaseRepoInterface;
use App\Models\PackageTypeRate;

class PackageTypeRateRepository implements BaseRepoInterface
{
   

    public function all()
    {
        return PackageTypeRate::all();
    }

    public function find($id)
    {
        return PackageTypeRate::where('id', $id)->first();
    }


    public function changeStatus($rate)
    {
        if($rate->status == 0)
        {
            $rate->status = 1;
        }else{
            
            $rate->status = 0;
        }
        $rate->save();
    }

    public function save($request){
        $rate = new PackageTypeRate;
        $rate->package_type_id = $request->package_type_id;
        // $rate->country_id = authEmployee()->office->country_id;
        // $rate->currency = authEmployee()->office->country->currency;
        $rate->country_id = $request->country_id;
        $rate->currency = Country::find($request->country_id)->currency;
        $rate->handling_rate = $request->handling_rate;
        $rate->status = $request->status;
        $rate->save();
        return true;
    }

    public function update($request, $rate){

        $rate->package_type_id = $request->package_type_id;
        // $rate->country_id = authEmployee()->office->country_id;
        // $rate->currency = authEmployee()->office->country->currency;
        
        $rate->country_id = $request->country_id;
        $rate->currency = Country::find($request->country_id)->currency;
        $rate->handling_rate = $request->handling_rate;
        $rate->status = $request->status;
        $rate->save();
        return true;
    }

    public function fetchData(){
      
    }
}
<?php
 namespace App\Repositories\Units;

interface UnitInterfaces
{
    public function all();
    public function categoryall();
    public function save($data);
    public function getById($id);
    public function update(array $data, $id);
}



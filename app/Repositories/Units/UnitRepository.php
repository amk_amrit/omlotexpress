<?php 
namespace App\Repositories\Units;
use App\Models\Unit;
use App\Models\UnitCategory;

class UnitRepository implements UnitInterfaces
{
    public function all(){
        return $unit=Unit::with('category')->paginate(10);
    }
    public function categoryall(){
        return $unitCategory=UnitCategory::get();
    }
    public function save($data){
        $unit= new Unit;
        $unit->insert($data);
        return true;
    }
    public function getById($id){
        return $unit=Unit::with('category')
                        ->where('id', $id)
                        ->first();
    }
    public function update(array $data, $id){
        $unit=Unit::find($id);
        $unit->update($data);
        return true;
    }
}
<?php

namespace App\Repositories\Role;

use Illuminate\Http\Request;
use App\Repositories\BaseRepoInterface;
use App\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RoleRepository implements BaseRepoInterface
{
   

    public function all()
    {
        return Role::all();
    }

    public function find($id)
    {
        return Role::where('id', $id)->first();
    }


    public function changeStatus($id)
    {
        
    }

    public function save($data){
        $office = auth()->user()->employee->office;
        $role = new Role;
        $role->name = $data['name'];
        $role->slug = Str::slug($office->name.' '.$data['name'], '-');
        $role->office_id = $office->id;
        $role->description = $data['description'];
        $role->save();
        $role->permissions()->sync($data['permission_id']);
    }

    public function update($data, $id){
        $office = auth()->user()->employee->office;
        $role = Role::find($id);
        $role->name = $data['name'];
        $role->slug = Str::slug($office->name.' '.$data['name'], '-');
        $role->office_id = $office->id;
        $role->description = $data['description'];
        $role->save();
        $role->permissions()->sync($data['permission_id']);
    }
}
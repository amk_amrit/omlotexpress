<?php 
namespace App\Repositories\PackageSensitivity;

use App\Models\PackageSensitivity;

class PackageSensitivitiyRepository implements PackageSensitivityInterfaces
{

    public function all(){
        return $packageSensitivity=PackageSensitivity::latest()->get();
    }
    public function findById($id){
        return $packageSensitivity=PackageSensitivity::find($id);

    }
    public function update(array $data , $id){
        $packageSensitivity=PackageSensitivity::find($id);
        $packageSensitivity->title=$data['title'];
        $packageSensitivity->status=$data['status'];
        $slug=str_slug($data['title'], '-');
        $packageSensitivity->slug=$slug;
        if($packageSensitivity->update($data)){
            return true;
        }
        else{
            return false;
        }


    }
    public function save($data){
        $packageSensitivity= new PackageSensitivity;
        $packageSensitivity->title=$data['title'];
        $packageSensitivity->status=$data['status'];
        $slug=str_slug($data['title'], '-');
        $packageSensitivity->slug=$slug;
        if($packageSensitivity->save($data)){
            return true;
        }
        else{
            return false;
        }


    }
    public function delete($id){
        $packageSensitivity=PackageSensitivity::find($id);
        if($packageSensitivity->status==0){
            $packageSensitivity->status=1;
            $packageSensitivity->save(); 
            }
            else{
                $packageSensitivity->status=0;
            $packageSensitivity->save();
            }
            return true;
    }
}
<?php
 namespace App\Repositories\PackageSensitivity;

interface PackageSensitivityInterfaces
{
    public function all();
    public function findById($id);
    public function update(array $data , $id);
    public function save($data);
    public function delete($id);
}



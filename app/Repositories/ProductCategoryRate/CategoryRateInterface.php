<?php
 namespace App\Repositories\ProductCategoryRate;

interface CategoryRateInterface
{
    public function all();
    public function findById($id);
    public function update(array $data ,$min,$max, $id);
    public function save($data, $min, $max);
    public function findByStatus();
    public function findByFlag();
    public function findByProductCategoryId($productCategoryId);
    public function findByCountryIdFirst($coId, $caId);
    public function deletePerviousData($data);
}



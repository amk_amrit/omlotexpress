<?php 
namespace App\Repositories\ProductCategoryRate;

use App\Models\ProductCategoryRate;
use App\Models\ProductCategory;
use App\Models\Country;
use DB;
use Exception;

class CatergoryRateRepository implements CategoryRateInterface
{
    public function all(){
         return $categoryRates=ProductCategoryRate::with('ProductCategory', 'Country')->groupBy('country_id', 'product_category_id')->get();
    }
    public function findById($id ){
        return $categoryRate=ProductCategoryRate::with('ProductCategory', 'Country')->where('id', $id)->first();
    }
    public function findByIdRange($id)
    {
        return $categoryRate=ProductCategoryRate::with('ProductCategory', 'Country')->where('id', $id)->get();

    }
    public function findByProductCategoryId($productCategoryId){
        
        return $categoryRate=ProductCategoryRate::with('ProductCategory', 'Country')->where('country_id', authEmployee()->office->country_id)->where('product_category_id', $productCategoryId)->get();
    }
    public function findByCountryIdFirst($coId, $caId){
        return $categoryRate=ProductCategoryRate::with('ProductCategory', 'Country')->where('country_id', $coId)->where('product_category_id', $caId)->first();

    }
    public function deletePerviousData($data){
        $numberOfArray=count($data['deleteId']);
        
        for($i=0; $i<$numberOfArray; $i++){
         $result = DB::table('product_category_rates')->where('id', $data['deleteId'][$i])->delete();
         }
            return true;
    }
    public function update(array $data ,$max, $min, $id){
        $countryId = authEmployee()->office->country_id;
        $currency = Country::find($countryId)->currency;
        //dd($data);
        // $categoryRate=ProductCategoryRate::find($id);
        foreach($data['max_weight'] as $key=>$val){
            $categoryRate=new ProductCategoryRate;
            $categoryRate->country_id = $countryId;
            $categoryRate->currency = $currency;
            $categoryRate->product_category_id=$data['product_category_id'];
            $categoryRate->min_weight=$max[$key];
            $categoryRate->max_weight=$min[$key];
            $categoryRate->rate=$data['rate'][$key];
            $categoryRate->save();
         }
        return true;
    }
    public function save($data, $min, $max){
        
        $countryId = authEmployee()->office->country_id;
        if($this->dataExists($data['product_category_id'], $countryId)){
            throw new Exception('Rates for the ProductCategory Already Exists Please Try to Update ');
        }
        $currency = Country::find($countryId)->currency;
         foreach($data['max_weight'] as $key=>$val){
            $categoryRate= new ProductCategoryRate;
            $categoryRate->country_id = $countryId;
            $categoryRate->currency = $currency;
            $categoryRate->product_category_id=$data['product_category_id'];
            $categoryRate->min_weight=$min[$key];
            $categoryRate->max_weight=$max[$key];
            $categoryRate->rate=$data['rate'][$key];
            $categoryRate->save();
         }
            if($categoryRate){
                return true;
            }
            else{
                return false;
            }
    }
    public function findByStatus(){
        return $countrys=Country::where('status', 1)->get();
    }
    public function findByFlag(){
        return $categorys=ProductCategory::where('status', 1)->get();
    }

    public function dataExists($productCategoryId, $countryId){
        $rates = ProductCategoryRate::where('country_id', $countryId)->where('product_category_id', $productCategoryId)->get();
        if(!empty($rates))
            return true;
        return false;
    }
    
}
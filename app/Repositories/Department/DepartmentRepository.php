<?php

namespace App\Repositories\Department;

use Illuminate\Http\Request;
use App\Repositories\Department\DepartmentInterface as DepartmentInterface;
use App\Models\Department;


class DepartmentRepository implements DepartmentInterface
{
   

    public function all()
    {
        return Department::latest()->get();
    }

    public function find($id)
    {
        return Department::where('id', $id)->first();
    }


    public function changeStatus($id)
    {
        
        $department = Department::find($id);
        if($department->status == 0)
        {
            $department->status = 1;
        }else{
            
            $department->status = 0;
        }
        $department->save();
        return true;
    }

    public function save($data){
        $department = new Department;
        $data['created_by'] = auth()->user()->employee->id;
        $data['updated_by'] = auth()->user()->employee->id;
        $department->create($data);
        if($department){
            return true;
        }
        else{
            return false;
        }
    }

    public function update(array $data, $id){
        $department = Department::find($id);
        $data['updated_by'] = auth()->user()->employee->id;
        $department->update($data);
        if($department){
            return true;
        }
        else{
            return false;
        }
    }
}
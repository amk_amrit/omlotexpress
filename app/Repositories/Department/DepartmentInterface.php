<?php

namespace App\Repositories\Department;

interface DepartmentInterface {


    public function all();

    public function save($data);

    public function find($id);

    public function update(array $data, $id);

    public function changeStatus($id);

}
<?php

namespace App\Repositories;

interface BaseRepoInterface {


    public function all();

    public function save($data);

    public function find($id);

    public function update($data, $id);

    public function changeStatus($id);

}
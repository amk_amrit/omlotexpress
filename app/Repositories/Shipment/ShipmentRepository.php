<?php

namespace App\Repositories\Shipment;

use Illuminate\Http\Request;
use App\Repositories\Shipment\ShipmentInterface as ShipmentInterface;
use App\Models\Shipment;
use App\Models\Country;
use App\Models\PackageSensitivity;
use App\Models\TerminalPoint;
use App\Models\TransportMedia;
use App\Models\ProductCategory;


class ShipmentRepository implements ShipmentInterface
{
    public $Shipment;


    function __construct(Shipment $shipment) 
    {
	    $this->shipment = $shipment;
    }


    public function all()
    {
        return $this->shipment::all();
    }

    public function store($request){
        Shipment::create($request->all());
    }

    public function find($id)
    {
        
    }


    public function delete($id)
    {
        
    }

    public function fetch_data()
    {
        $data = [];
        $data['transport_medias'] = TransportMedia::all();
        $data['countries'] = Country::all();
        $data['categories'] = ProductCategory::all();
        $data['terminals'] = TerminalPoint::all();
        $data['sensitivities'] = PackageSensitivity::all();

        return $data;

    }
}
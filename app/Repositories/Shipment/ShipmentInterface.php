<?php

namespace App\Repositories\Shipment;

interface ShipmentInterface {


    public function all();

    public function store($request);

    public function find($id);

    public function fetch_data();

    public function delete($id);
}
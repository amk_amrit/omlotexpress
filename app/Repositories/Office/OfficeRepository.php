<?php

namespace App\Repositories\Office;


use App\Repositories\Office\OfficeInterface as OfficeInterface;
use App\Models\Office;
use App\Models\Country;
use Illuminate\Support\Str;



class OfficeRepository implements OfficeInterface
{
   

    public function all()
    {
        return Office::all();
    }

    public function find($id)
    {
        return Office::where('id', $id)->first();
    }


    public function changeStatus($id)
    {
        
        $office = Office::findOrFail($id);
        if($office->status == 0)
        {
            $office->status = 1;
        }else{
            
            $office->status = 0;
        }
        $office->save();
        return true;
    }

    public function save($data){
        $data=$data->except('_token');
        $data['slug']= Str::slug($data['name'] , '-');
        $data['parent_office_id'] = auth()->user()->employee->office->id;
        $data['created_by'] = auth()->user()->employee->id;
        $data['updated_by'] = auth()->user()->employee->id;
        $office = Office::create($data);
        return $office;
    }


    public function update($data, $id){
        $data=$data->except('_token');
        $data['slug']= Str::slug($data['name'] , '-');
        $data['updated_by'] = auth()->user()->employee->id;
        $office = Office::findOrFail($id);
        $office->update($data);
        
    }

    public function fetchData($id = '0'){
        // $office = Office::find($id);
        // $except = $office ? $office->country_id : '0';
        $data['countries'] = Country::where('status', 1)->get();
        return $data;
    }

    public function saveAgentOffice($data){

        $data=$data->except('_token');
        $data['slug']= Str::slug($data['name'] , '-');
        $data['country_id'] = authEmployee()->getWorkingCountry()->id;
        $data['parent_office_id'] = authEmployee()->office->id;
        $data['office_type'] ='agency';
        //dd(auth()->user()->employee->id);
        $data['created_by'] = auth()->user()->employee->id;
        $data['updated_by'] = auth()->user()->employee->id;

        //dd($data);
        $office = Office::create($data);

        $agency = $office->agency()->create([
            'courier_agency_class_id' => $data['agency_class_id'],
            'working_as' => $data['working_as'],

        ]);
        //saving in agency_transit_connections table
        $agency->agencyTransitLinks()->attach([
            $data['transit_connection'] => [
                'distance' => $data['distance'],
                'vehicle_travel_time'=>$data['travel_time'],
                'status' => $data['status'],
                'created_by' => auth()->user()->employee->id,
                'updated_by' => auth()->user()->employee->id,
            ],
        ]);
        return $office;
    }

    public function updateAgentOffice($data, $office){
        $data=$data->except('_token');
        $data['slug']= Str::slug($data['name'] , '-');
        $data['updated_by'] = auth()->user()->employee->id;
        //$office = Office::findOrFail($id);
        $office->update($data);

        $agency = $office->agency()->update([
            'courier_agency_class_id' => $data['agency_class_id'],
            'working_as' => $data['working_as'],
        ]);

        return $office;

    }

    public function saveTransitOffice($data){

        $data=$data->except('_token');
        $data['slug']= Str::slug($data['name'] , '-');
        $data['country_id'] = authEmployee()->getWorkingCountry()->id;
        $data['parent_office_id'] = authEmployee()->office->id;
        $data['office_type'] ='transit';
        $data['created_by'] = auth()->user()->employee->id;
        $data['updated_by'] = auth()->user()->employee->id;

        $office = Office::create($data);

        //saving in transit table
        $transit = $office->transit()->create([
            'courier_transit_class_id' => $data['transit_class_id'],
        ]);

        //saving in transit_links table
        $transit->transitLinks()->attach([
            $data['transit_connection'] => [
                'distance' => $data['distance'],
                'travel_time'=>$data['travel_time'],
                'travel_medium' => $data['travel_medium']
            ],
        ]);
        return $office;
    }

    public function updateTransitOffice($data, $office){
        $data=$data->except('_token');
        $data['slug']= Str::slug($data['name'] , '-');
        $data['updated_by'] = auth()->user()->employee->id;
        $office->update($data);

        $transit = $office->transit()->update([
            'courier_transit_class_id' => $data['transit_class_id'],
        ]);
        return $office;

    }

    public function storeTransitConnection($data,$office){

        $data=$data->except('_token');
        //$office = Office::findOrFail($officeId);

        $transit= $office->transit;
        //saving in transit_links table
        $transit->transitLinksOne()->attach([
            $data['transit_connection'] => [
                'distance' => $data['distance'],
                'travel_time'=>$data['travel_time'],
                'travel_medium' => $data['travel_medium']
            ],
        ]);

        return $office;
    }


    public function updateTransitConnection($data,$transitLink){
        $data=$data->except('_token');
        //$office = Office::findOrFail($id);
        //dd($data);
        $transitLink->update($data);

        return $transitLink;
    }

    public function toggleConnectionStatusOfTransit($transitLink){

        if($transitLink->status == 0)
        {
            $transitLink->status = 1;
        }else{

            $transitLink->status = 0;
        }
        $transitLink->save();
        return true;
    }

    public function deleteConnectionOfTransit($transitLink){
        $transitLink->delete();
    }

    public function storeTransitConnectionOfAgency($data,$office){
        $data=$data->except('_token');

        $agency= $office->agency;
        //saving in transit_links table
        $agency->agencyTransitLinks()->attach([
            $data['transit_connection'] => [
                'distance' => $data['distance'],
                'vehicle_travel_time'=>$data['travel_time'],
                'created_by' => auth()->user()->employee->id,
                'updated_by' => auth()->user()->employee->id,
            ],
        ]);

        return $office;
    }

    public function updateAgencyTransitConnection($data,$transitConnection){

        $data=$data->except('_token');

        $data['updated_by'] = auth()->user()->employee->id;

        $transitConnection->distance = $data['distance'];
        $transitConnection->vehicle_travel_time =$data['travel_time'];

        $transitConnection->updated_by = $data['updated_by'];

        $transitConnection->save();

        return $transitConnection;
    }

    public function toggleTransitConnectionStatusOfAgency($transitConnection){

        if($transitConnection->status == 0)
        {
            $transitConnection->status = 1;
        }else{

            $transitConnection->status = 0;
        }
        $transitConnection->save();
        return true;
    }

    public function deleteTransitConnectionOfAgency($transitConnection){

        $transitConnection->delete();
    }
}
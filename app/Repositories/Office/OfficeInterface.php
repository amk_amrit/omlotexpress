<?php

namespace App\Repositories\Office;

interface OfficeInterface {


    public function all();

    public function save($data);

    public function find($id);

    public function update($data, $id);

    public function changeStatus($id);

    public function fetchData();

}
<?php
 namespace App\Repositories\PackageType;

interface PackageTypeInterfaces
{
    public function getAll();
    public function findById($id);
    public function store($data);
    public function update(array $data , $id);
    public function delete($id);
}



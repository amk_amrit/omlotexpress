<?php

namespace App\Repositories\PackageType;

use App\Models\PackageType;

class PackageTypeRepository implements PackageTypeInterfaces
{

    public function getAll()
    {
        return $packageType = PackageType::latest()->get();
    }

    public function store($request)
    {
        $packageType = new PackageType;
        $packageType->type_name = $request->type_name;
        $packageType->description = $request->description;
        $packageType->slug = str_slug($request->type_name, '-');
        $packageType->status = $request->status;
        $packageType->save();
    }

    public function findById($id)
    {
        return $packageType = PackageType::find($id);
    }

    public function update($request, $id)
    {

        $packageType = PackageType::find($id);
        $packageType->type_name = $request->type_name;
        $packageType->description = $request->description;
        $packageType->slug = str_slug($request->type_name, '-');
        $packageType->status = $request->status;
        $packageType->save();
    }


    public function delete($id)
    {
        $packageType = PackageType::find($id);
        if ($packageType->status == 0) {
            $packageType->status = 1;
        } else {
            $packageType->status = 0;
        }
        $packageType->save();
        return true;
    }
}

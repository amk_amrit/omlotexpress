<?php

namespace App\Repositories\Employee;

use App\Models\Designation;
use App\Repositories\Employee\EmployeeInterface as EmployeeInterface;
use App\Models\Employee;
use App\Models\Office;
use App\User;
use Illuminate\Support\Facades\DB;

class EmployeeRepository implements EmployeeInterface
{
    public function all()
    {
        return Employee::paginate(10);
    }

    public function find($id)
    {
        return Employee::where('id', $id)->first();
    }


    public function changeStatus($employee)
    {
        if ($employee->user->status == 'unverified') {
            $employee->status =1;
            $employee->user->status = 'verified';
            
        }else {
            $employee->status = 0;
            $employee->user->status = 'unverified';  
        }
        $employee->save();
        $employee->user->save();
        return true;
    }

    public function save($data)
    {
      
        DB::beginTransaction();
        $user = new User;
      
        $user->name = $data->name;
        $user->email =  $data->email;
        $user->password = bcrypt($data->password);
        $user->address = $data->address;
        $user->mobile = $data->mobile;
        $user->status = 'verified';
        $user->save();

        $employee = new Employee;
        $employee->user_id = $user->id;
        $employee->designation_id = $data->designation_id;
        $employee->office_id = $data->office_id;
        $employee->status == 1;
        $employee->created_by = auth()->user()->employee->id;
        $employee->updated_by = auth()->user()->employee->id;
        if($data->employee_type == 'office_head' ){
            $employee->is_head = 1;
            $employee->save();
       
        }elseif($data->employee_type == 'department_head'){
            $employee->is_department_head = 1;
            $employee->save();
            $employee->department()->attach($data->department_id);
        }else{
            $employee->save();
            $employee->department()->attach($data->department_id);
            
        }

        $employee->roles()->sync($data->role_id);
        DB::commit();
    }

    public function update($data, $employee)
    {
        DB::beginTransaction();
        $user = User::find($employee->user->id);
      
        $user->name = $data->name;
        $user->email =  $data->email;
        $user->password = bcrypt($data->password);
        $user->address = $data->address;
        $user->mobile = $data->mobile;
        $user->save();

        $employee->user_id = $user->id;
        $employee->designation_id = $data->designation_id;
        $employee->office_id = $data->office_id;
        $employee->created_by = auth()->user()->employee->id;
        $employee->updated_by = auth()->user()->employee->id;
        if($data->employee_type == 'office_head' ){
            $employee->is_head = 1;
            $employee->save();
       
        }elseif($data->employee_type == 'department_head'){
            $employee->is_department_head = 1;
            $employee->save();
            $employee->department()->sync([$data->department_id]);
        }else{
            $employee->save();
            $employee->department()->sync([$data->department_id]);
            
        }
        $employee->roles()->sync($data->role_id);
        DB::commit();
    }

    public function fetchData($id = '0')
    {
        //Rewrite these statements in service tomorrow
        
        $designations = Designation::where('status', 1)->get();
        if(auth()->user()->employee->is_head == true) {
            if(auth()->user()->employee->office->office_type == 'super_head'){
                $office = Office::where('status', 1);
            }
        }
        $offices = Office::where('status', 1)->get();

        $data['designations'] = $designations;
        return $data;
    }
}

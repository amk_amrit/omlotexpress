<?php

namespace App\Repositories\Api\V1;

use App\Http\Resources\user\UserResource;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

class AuthRepository
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Authenticate User
     *
     * @return JSON-Response
     */
    public function login()
    {
        // $this->request->validate([
        //     'email' => 'required|string|email',
        //     'password' => 'required|string'
        // ]);
        $credentials = request(['email', 'password']);

        if (!auth()->attempt($credentials))
            return sendResponse("Invalid username and password.", true);

        $user = auth()->user();


        $http = new \GuzzleHttp\Client;

        try {
            $response = $http->post(url('oauth/token'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' =>  config('services.passport.client_secret'),
                    'username' => $this->request['email'],
                    'password' => $this->request['password'],
                    'scope' => '',
                ],
            ]);

            $data = [
                'data' => new  UserResource($user),
                'token_credentials' =>  json_decode((string) $response->getBody(), true)
            ];

            return sendMinimalResponse($data);
        } catch (GuzzleException $e) {
            return sendResponse($e->getMessage(), true, $e->getCode());
        }
    }

    public function refreshToken()
    {

        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->post(url('oauth/token'), [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $this->request['refresh_token'],
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' =>  config('services.passport.client_secret'),
                    'scope' => '',
                ],
            ]);
        } catch (GuzzleException $e) {
            return sendResponse($e->getMessage(), true, $e->getCode());
            // return sendResponse('Invalid refresh token.', true, $e->getCode());
        }
        $tokencredentials = [
            'token_credentials' => json_decode($response->getBody(), true)
        ];

        return sendMinimalResponse($tokencredentials);
    }


    public function getUser()
    {
        return new UserResource(auth()->user());
    }
}

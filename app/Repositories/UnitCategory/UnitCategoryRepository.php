<?php 
namespace App\Repositories\UnitCategory;
use App\Models\UnitCategory;

class UnitCategoryRepository implements UnitCategoryInterfaces
{
    public function all(){
        return $unitCategory=UnitCategory::paginate(10);
    }
    public function save($data){
        $unitCategory= new UnitCategory;
        if($unitCategory->insert($data)){
            return true;
        }
        else{
            return false;
        }
    }
    public function findById($id){
        return $unitCategory=UnitCategory::find($id);
    }
    public function update(array $data, $id){
        $unitCategory=UnitCategory::find($id);
        if($unitCategory->update($data)){
            return true;
        }
        else{
            return false;
        }
    }
    public function delete($id){
        $unitCategory=UnitCategory::find($id);
        if($unitCategory->status==0){
        $unitCategory->status=1;
        $unitCategory->save(); 
        }
        else{
            $unitCategory->status=0;
        $unitCategory->save();
        }
        return true;
}}
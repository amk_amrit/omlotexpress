<?php
 namespace App\Repositories\UnitCategory;

interface UnitCategoryInterfaces
{
    public function all();
    public function findById($id);
    public function update(array $data, $id);
}



<?php

namespace App\Repositories\TransportVehicle;

use Illuminate\Http\Request;
use App\Repositories\TransportVehicle\TransportVehicleInterface as TransportVehicleInterface;
use App\Models\TransportVehicle;
use App\Models\TransportMedia;


class TransportVehicleRepository implements TransportVehicleInterface
{
   

    public function all()
    {
        return $transportVehicle = TransportVehicle::all();
    }

    public function find($id)
    {
        return TransportVehicle::where('id', $id)->first();
    }


    public function changeStatus($id)
    {
        
        $transportVehicle = TransportVehicle::find($id);
        if($transportVehicle->status == 0)
        {
            $transportVehicle->status = 1;
        }else{
            
            $transportVehicle->status = 0;
        }
        $transportVehicle->save();
        return true;
    }

    public function save($data){
        $transportVehicle = new TransportVehicle;
        $transportVehicle->create($data);
        if($transportVehicle){
            return true;
        }
        else{
            return false;
        }
    }

    public function update($data, $id){
        $transportVehicle = TransportVehicle::find($id);
        $transportVehicle->update($data);
        if($transportVehicle){
            return true;
        }
        else{
            return false;
        }
    }

    public function fetchData($id = '0'){
        $transportVehicle = TransportVehicle::find($id);
        $except = $transportVehicle ? $transportVehicle->transport_media_id : '0';
        $data['transportMedias'] = TransportMedia::where('id','!=', $except)->where('status', 1)->get();
        return $data;
    }
}
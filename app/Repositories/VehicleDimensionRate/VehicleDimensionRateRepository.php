<?php

namespace App\Repositories\VehicleDimensionRate;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Repositories\BaseRepoInterface;
use App\Models\VehicleDimensionRate;
use App\Models\TransportVehicle;



class VehicleDimensionRateRepository implements BaseRepoInterface
{
   

    public function all()
    {
        return VehicleDimensionRate::all();
    }

    public function find($id)
    {
        return VehicleDimensionRate::where('id', $id)->first();
    }


    public function changeStatus($rate)
    {
        if($rate->status == 0)
        {
            $rate->status = 1;
        }else{
            
            $rate->status = 0;
        }
        $rate->save();
    }

    public function save($request){
        $rate = new VehicleDimensionRate;
        $rate->vehicle_dimension_id = $request->vehicle_dimension_id;
        // $rate->country_id = authEmployee()->office->country_id;
        // $rate->currency = authEmployee()->office->country->currency;
        $rate->country_id = $request->country_id;
        $rate->currency = Country::find($request->country_id)->currency;
        $rate->per_km_charge = $request->per_km_charge;
        $rate->status = $request->status;
        $rate->min_charge = $request->minimum_charge;
        $rate->save();
        return true;
    }

    public function update($request, $rate){

        $rate->vehicle_dimension_id = $request->vehicle_dimension_id;
        // $rate->country_id = authEmployee()->office->country_id;
        // $rate->currency = authEmployee()->office->country->currency;
        
        $rate->country_id = $request->country_id;
        $rate->currency = Country::find($request->country_id)->currency;
        $rate->per_km_charge = $request->per_km_charge;
        $rate->status = $request->status;
        $rate->min_charge = $request->minimum_charge;
        $rate->save();
        return true;
    }

    public function fetchData(){
        $data['vehicles'] = VehicleDimensionRate::where('status', 1)->get();
        return $data;
    }
}
<?php 
namespace App\Repositories\TerminalPoint;

use App\Models\TerminalPoint;
use App\Models\Country;

use DB;

class TerminalPointRepository implements TerminalPointInterface
{
   public function all(){
       
       return $terminalPoint=TerminalPoint::paginate(10);

   }
   public function allcountry(){
       return $countrys=Country::all();
   }
   public function findById($id){
       return $terminalPoint=TerminalPoint::find($id);
   }
   public function save($data){
       $terminalPoint= new TerminalPoint;
       if($terminalPoint->insert($data)){
           return true;
       }
       else{
           return false;
       }

   }
   public function delete($id){
        $terminalPoint=TerminalPoint::find($id);
        if($terminalPoint->status==0){
        $terminalPoint->status=1;
        $terminalPoint->save(); 
        }
        else{
            $terminalPoint->status=0;
        $terminalPoint->save();
        }
        
        return true;
   }

   public function update($data, $id){
    $terminalPoint=TerminalPoint::find($id);
    if($terminalPoint->update($data)){
        return true;
    }
    else{
        return false;
            }
        }
    
}
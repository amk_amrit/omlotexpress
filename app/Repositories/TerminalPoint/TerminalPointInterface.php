<?php
 namespace App\Repositories\TerminalPoint;

interface TerminalPointInterface
{
    public function all();
    public function findById($id);
    public function update(array $data , $id);
    public function save($data);
    public function allcountry();
    public function delete($id);
}
<?php 
namespace App\Repositories\LoadInfromation;
use App\Models\FullLoadInformation;
use App\Models\PartLoadInformation;

class LoadInfromationRepository implements LoadInformationInterfaces
{
    public function allPartLoad(){
        return $partLoad=PartLoadInformation::with('shipment', 'packageType')->paginate(10);
    }
    public function partLoadShow($id){
        return $partLoad=PartLoadInformation::with('shipment', 'packageType', 'unit')->first();
    }
    public function allFullLoad(){
        return $fullLoad=FullLoadInformation::with('shipment','vehicleDimension')->paginate(10);
    }
}
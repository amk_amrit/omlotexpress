<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 1/31/2020
 * Time: 12:40 PM
 */

namespace App\Traits;


use App\Services\CompareWeightRangeService;

trait WeightRangeService
{
    public function testWeightRange(array $array1, array $array2){


        $compareWeightRange = new CompareWeightRangeService();
        $array2=$compareWeightRange->pushValueToMaxArray($array1,$array2);

        if (!compareArraysLength($array1,$array2)){
            throw new \Exception('Arrays length not equal');
        }

        $areArraysInRange = $compareWeightRange->rangeTest($array1,$array2);

        if (!$areArraysInRange){
            throw new \Exception('Invalid Weight Ranges');
        }

        return $array2;
    }
}
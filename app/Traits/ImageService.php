<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 1/28/2020
 * Time: 12:26 PM
 */

namespace App\Traits;



use App\Services\FileStorageService;

trait ImageService
{

    public function storeImageInServer($image,$imagePath,bool $multiSize=false){

        //ImageStorageService::saveImage($image,$imagePath,true);
        return FileStorageService::saveFile($image,$imagePath,$multiSize); //returns file name
    }

    public function deleteImageFromServer($imagePath,$toBeDeletedImage){

        //ImageStorageService::saveImage($image,$imagePath,true);
        FileStorageService::deleteFile($imagePath,$toBeDeletedImage); //returns file name
    }
}
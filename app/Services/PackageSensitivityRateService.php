<?php


namespace App\Services;

use App\Models\PackageSensitivityRate;
use App\Repositories\PackageSensitivityRate\PackageSensitivityRateRepository;

class PackageSensitivityRateService
{
    protected $packageRateRepo;
    public function __construct(PackageSensitivityRateRepository $packageRateRepo)
    {
        $this->packageRateRepo = $packageRateRepo;
    }
    public function index(){
        if(authEmployee()->isSuperAdmin()){
            $rates = PackageSensitivityRate::latest()->orderBy('country_id');
        }else{
            $rates = PackageSensitivityRate::where('country_id', authEmployee()->office->country_id)->latest();
        }
        return $rates->get();
    }

    public function save($request){
        $this->packageRateRepo->save($request);
        return true;
    }

    public function update($request, $rate){
        $this->packageRateRepo->update($request, $rate);
        return true;
    }

    public function destroyRate($rate){
        $this->packageRateRepo->changeStatus($rate);
    }
}
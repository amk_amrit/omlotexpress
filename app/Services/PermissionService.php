<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 2/10/2020
 * Time: 2:19 PM
 */

namespace App\Services;


use App\Services\ClassHelper\ClassHelperService;

class PermissionService
{
 // seedable
    public function getAllModelPermissions(){

        //$ship = new \App\Models\Shipment();
        // \App\Services\PermissionService();
         //dd($ship);
        $permissionsArr =[];

        $classHelper = new ClassHelperService();

        $allModels = $classHelper->getAllClasses();

        foreach ($allModels as $model){
            
            $modelObject = new $model();
  
            if(method_exists($modelObject,'getPermissions')){
               // $permissionsArr[$i] = $modelObject->getPermissions();
                foreach ($modelObject->getPermissions() as $permission){
                    array_push($permissionsArr,$permission);
                }
            }
        }

        return $permissionsArr;
    }

    public function getGroupedPermissions(){
        return collect($this->getAllModelPermissions())->groupBy('category')->toArray();
    }

    public function getAllModelAssociatedPermissions(){
        $permissionsArr=[];

        $classHelper = new ClassHelperService();

        $allModels = $classHelper->getAllClasses();

        foreach ($allModels as $model){

            $modelObject = new $model();
       
            if(method_exists($modelObject,'getAssociatedPermissions')){
                // $permissionsArr[$i] = $modelObject->getPermissions();
                $permissionsArr= array_merge($permissionsArr,$modelObject->getAssociatedPermissions());

            }
        }

        return $permissionsArr;
    }


}
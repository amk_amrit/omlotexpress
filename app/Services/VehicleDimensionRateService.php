<?php


namespace App\Services;

use App\Models\VehicleDimensionRate;
use App\Repositories\VehicleDimensionRate\VehicleDimensionRateRepository;

class VehicleDimensionRateService
{
    protected $vehicleRateRepo;
    public function __construct(VehicleDimensionRateRepository $vehicleRateRepo)
    {
        $this->vehicleRateRepo = $vehicleRateRepo;
    }
    public function index(){
        if(authEmployee()->isSuperAdmin()){
            $rates = VehicleDimensionRate::all();
        }else{
            $rates = VehicleDimensionRate::where('country_id', authEmployee()->office->country_id)->get();
        }
        return $rates;
    }

    public function save($request){
        $this->vehicleRateRepo->save($request);
        return true;
    }

    public function update($request, $rate){
        $this->vehicleRateRepo->update($request, $rate);
        return true;
    }

    public function destroyRate($rate){
        $this->vehicleRateRepo->changeStatus($rate);
    }
}
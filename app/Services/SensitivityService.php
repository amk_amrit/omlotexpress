<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 1/21/2020
 * Time: 1:59 PM
 */

namespace App\Services;


use App\Models\PackageSensitivity;

class SensitivityService
{
    public static function getSensitivityBySlug($slug){

        $sensitivity = PackageSensitivity::active()->where('slug',$slug)->firstOrFail();

        return $sensitivity;
    }

    public static function getSensitivityRateBySlug($slug){
        $sensitivity = self::getSensitivityBySlug($slug);

        return $sensitivity->rate;
    }
}
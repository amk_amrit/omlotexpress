<?php


namespace App\Services;


use App\Models\TransportVehicleDimension;

class TransportService
{
    // with parents(media) active //for validation
    public static function getActiveVehicleDimensionArrayByMedia($mediaSlug){

        $vehicleDimensions = TransportVehicleDimension::active()->whereHas('vehicle.transportMedia',function ($q) use ($mediaSlug){
            $q->where('slug',$mediaSlug)->where('status',1);
        })->pluck('slug')->toArray();

        return $vehicleDimensions;
    }

    public static function getActiveVehicleDimensionBySlug($slug){

        $vehicleDimension = TransportVehicleDimension::active()->where('slug',$slug)->firstOrFail();

        return $vehicleDimension;
    }
}

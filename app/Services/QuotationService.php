<?php


namespace App\Services;


use App\Models\TransportQuotation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class QuotationService
{
    public $totalChargeAmount;

    public function calculateShipment($request){
        $calculateShipment = new CalculateShipmentRateService();
        $totalChargeAmount =$calculateShipment->calculateShipmentRate($request);

        return $totalChargeAmount;
    }

    public function storeQuotation($request){

        try{

            $quotation = new TransportQuotation();
            $quotation->name = $request->name;
            $quotation->email = $request->email;
            $quotation->phone = $request->phone;
            $quotation->save();
            $shipmentService = new ShipmentService();

            $request['total_bill_amount'] =  $request->filled('total_bill_amount') ?
                null: $this->totalChargeAmount ;

            $shipment =$shipmentService->storeShipment($request);

            //$shipment->transportQuotation()->save($quotation);
            $shipment->transportQuotation()->sync([$quotation->id]);

            return $shipment;

        }catch (\Exception $e){
            throw $e;
        }

    }

    public function calculateAndStoreShipment($request){

        $this->totalChargeAmount = $this->calculateShipment($request);

        $shipment = $this->storeQuotation($request);

        return $shipment;
    }

    public static function getAllQuotationsWithShipment(){

        $quotations = TransportQuotation::with(['shipment'])->latest()->first();

        return $quotations;
    }
}

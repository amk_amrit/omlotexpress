<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 1/31/2020
 * Time: 11:41 AM
 */

namespace App\Services;


class CompareWeightRangeService
{

    public function rangeTest(array $min ,array $max){

//        $testArray = [[5,10],[6,8],[11,20],[0,4],[15,18],[21,35]];
//        $resultArray = [[0,4],[5,10],[6,8],[11,20],[15,18],[21,35]];
//        $resultArray = [[0,4],[5,10],[9,16],[11,15],[30,40]];
//        $resultArray = [[1,10],[10,100],[11,20],[21,30],[31,40],[50,60],[90,100]];
//        $resultArray= [[0,0],[10,10],[11,11],[1,100],[100,1]];
//        $resultArray = [[0,0],[10,10]];

        $minMaxArray = [];
        $sortedMinArray = $min;
        sort($sortedMinArray);
//        array inside array
        foreach ($min as $minKey=>$mValue)
        {
            array_push($minMaxArray,[$mValue,$max[$minKey]]); //returns in format [[5,10],[6,8],[11,20],[0,4],[15,18],[21,35]];
        }
        $resultArray = [];
//        sorting the array w.r.t min value [[0,4],[5,10],[6,8],[11,20],[15,18],[21,35]];
        foreach ($sortedMinArray as $mValue)
        {
            foreach ($minMaxArray as  $newKey => $nValue) {

                if ($nValue[0] == $mValue) {
                    array_push($resultArray, $nValue);
                    unset($minMaxArray[$newKey]);

                }
            }
        }
//        comparing /validating range
        foreach ($resultArray as $key=>$value) {

            if ($key != 0)
            {
                $previousMaxValue= $resultArray[$key-1][1];

                if($value[0] < $previousMaxValue){
                    return false;

                }
            }
        }
        return true;

    }

    public function pushValueToMaxArray(array $min,array $max){

        //if arrays length not equal..append 9999 in the end of $max array
        $maxValue = (int)999999999999;
        if(!compareArraysLength($min,$max)){

            array_push($max,$maxValue);

            return $max;
        }
        return $max;
    }


}
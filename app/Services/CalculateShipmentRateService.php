<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 1/21/2020
 * Time: 5:01 PM
 */

namespace App\Services;


class CalculateShipmentRateService
{
    private $totalDistance;
    private $country;
    private $productCategory;
    private $sensitivityRate;
    private $totalChargeAmount;

    public function calculateShipmentRate($inputData)
    {
        $this->totalDistance = (double)$inputData->total_distance; //float number
        $this->country = CountryService::getCountryByIso($inputData->destination_country);
        $this->productCategory = ProductCategoryService::getActiveProductCategoryBySlug($inputData->product_category);

        $this->sensitivityRate = (double)SensitivityService::getSensitivityRateBySlug($inputData->sensitivity);
        //dd($productCategory->id);
        switch ($inputData->load_type){

            case 'part_load':
                $this->totalChargeAmount=$this->calculatePartLoad($inputData);
                break;
            case 'full_load':
                $this->totalChargeAmount=$this->calculateFullLoad($inputData);
                break;

        }

        return $this->totalChargeAmount;

    }

    private function calculatePartLoad($inputData){

        $numOfPackages = (double)$inputData->item_num;
        $inputWeight = $inputData->total_weight;
        $packageTypeRate = (double)PackageTypeService::getPackageTypeRateBySlug($inputData->package_type);


        if ($inputData->package_type == 'box') {
            $volumetricWeight = ($inputData->height * $inputData->length * $inputData->width) / 500;
            $totalWeight = $inputWeight > $volumetricWeight ? $inputWeight : $volumetricWeight;
        }
        elseif ($inputData->package_type == 'sack') {

            $totalWeight = $inputWeight;
        }
        $totalWeight=(double)$totalWeight;
        //dd($totalDistance);

        $productCategoryRate = (double)ProductCategoryService::getProductCategoryRate($this->country->id, $this->productCategory->id, $totalWeight);
        $partLoadAmount = $this->totalDistance * $totalWeight * $productCategoryRate;
        $packageHandlingAmount = $packageTypeRate * $numOfPackages * $this->sensitivityRate;

        $totalChargeAmount = $partLoadAmount + $packageHandlingAmount;

        return $totalChargeAmount;

    }
    private function calculateFullLoad($inputData){

        $vehicleDimension = TransportService::getActiveVehicleDimensionBySlug($inputData->vehicle_type);
        $numOfVehicles = $inputData->no_of_vehicles;
        $totalChargeAmount = ($this->totalDistance * $vehicleDimension->per_km_charge) + $vehicleDimension->min_charge;

        $totalChargeAmount *=$numOfVehicles;
        //dd($vehicleDimension);

        return $totalChargeAmount;
    }
}
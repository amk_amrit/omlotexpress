<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 2/11/2020
 * Time: 11:15 AM
 */

namespace App\Services;


class UserService
{

    public static function getUserAssociativePermissions(array $userPermissions){


        /*
         * user permissions array format
        [
            {
            "name": "add-department",
            "route": "departments.create",
            "display_name": "Add Department",
            "description": "can add department",
            "category": "department-category"
            },
            {
            "name": "update-department",
            "route": "departments.edit",
            "display_name": "Update Department",
            "description": "can update department detail",
            "category": "department-category"
            }
        ]
        */

        $ps = new PermissionService();

       // $userPermissions = array_column($ps->getAllModelPermissions(),'route');
        $userPermissions= array_column($userPermissions,'route');
        $userPermissions = array_flatten($userPermissions);

        foreach($ps->getAllModelAssociatedPermissions() as $key => $value){
            if(in_array($key,$userPermissions)){
                array_push($userPermissions, $value);
            }
        }

        $allMergedPermissions = array_flatten($userPermissions);

        return $allMergedPermissions;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 2/10/2020
 * Time: 10:44 AM
 */

namespace App\Services\ClassHelper;

use Symfony\Component\Finder\Finder;

class ClassHelperService
{

    private function removeExtension($filename) {
        $a = explode('.', $filename);
        array_pop($a);
        return implode('.', $a);

        /*$directoriesAndFilename = explode('/', $filename);
        $filename = array_pop($directoriesAndFilename);
        $nameAndExtension = explode('.', $filename);
        $className = array_shift($nameAndExtension);

        return $className;*/
    }

    public function getAllClasses(){

        $absoluteFilePaths=[];
        $classNameWithNameSpaceArr=[];

        $path = base_path();//"E:\office projects\omlot-express"

        $dir =app_path('Models');//"E:\office projects\omlot-express\app\Models";

        $finder = new Finder();

        // find all files in the directory
        $finder->files()->in($dir);

        // check if there are any search results
        if ($finder->hasResults()) {
            foreach ($finder as $file) {
                // dd($file);
                $absoluteFilePath = $file->getRealPath();//"E:\office projects\omlot-express\app\Models\Country.php"
                // $fileNameWithExtension = $file->getRelativePathname();
                array_push($absoluteFilePaths,$absoluteFilePath);

            }

            $classNameWithNameSpaceArr= $this->getClassNameWithNameSpace($absoluteFilePaths,$path);
        }
        else{
            throw new \ErrorException('No files in the directory');
        }

        return $classNameWithNameSpaceArr;
    }

    private function getClassNameWithNameSpace(array $absoluteFilePaths,$path){

        $classNameWithNameSpaceArr=[];
        foreach ($absoluteFilePaths as $filePath){

            //dd($path);
            $className =$this->removeExtension($filePath);
            $path = str_replace('/','\\',$path);//needed for linux

            $classNameWithNameSpace = str_replace('/','\\',$className);//needed for linux
            $classNameWithNameSpace = ucwords(str_replace($path.'\\','',$classNameWithNameSpace));//App\Models\Country
            
            //dd($classNameWithNameSpace);
            array_push($classNameWithNameSpaceArr,$classNameWithNameSpace);

            
        
        }
        return $classNameWithNameSpaceArr;
    }
}
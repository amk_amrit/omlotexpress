<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 2/14/2020
 * Time: 3:06 PM
 */

namespace App\Services;


use App\Models\Department;

class DepartmentService
{

    public function getEmployeesByDepartmentName($departmentName){

        $department = Department::where('name',$departmentName)->first();

        return $department->employees;
    }
}
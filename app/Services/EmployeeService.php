<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 2/13/2020
 * Time: 4:36 PM
 */

namespace App\Services;

use App\Models\Department;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\Office;
use App\Repositories\Employee\EmployeeRepository;
use Exception;

class EmployeeService
{
    protected $employeeRepo;
    public function __construct(EmployeeRepository $employeeRepo)
    {
        $this->employeeRepo = $employeeRepo;
    }
    
    public function getOfficeHead($officeId, $employeeId){

        $officeHead = Employee::active()->where('office_id',$officeId)->where('is_head',1)->where('id', '!=', $employeeId )->first();

        return $officeHead;
    }

    public function getDepartmentHead($officeId , $departmentId, $employeeId){
        

        $departmentHeads = Employee::active()->select('employee_id')
            ->join('employee_department', 'employees.id', '=', 'employee_department.employee_id')
            ->where('employees.office_id', '=', $officeId)
            ->where('employees.is_department_head', '=', 1)
            ->where('employee_department.department_id', '=', $departmentId)
            ->where('employees.id', '!=', $employeeId)
            ->get();

        return $departmentHeads;
    }

    public function hasOfficeHead($officeId, $employeeId){

        if (!is_null($this->getOfficeHead($officeId, $employeeId))){
            return true;
        }
        return false;
    }

    public function hasDepartmentHead($officeId, $departmentId, $employeeId){
        
        if (count($this->getDepartmentHead($officeId, $departmentId, $employeeId))>0){
            return true;
        }
        return false;
    }

    public function checkEmployee($request, $employeeId = null){
        $employeeType = $request->employee_type;
        if($employeeType == 'office_head'){
            if($this->hasOfficeHead($request->office_id, $employeeId)){
                throw new \Exception('Office Head Already Exists for this Office');
            }
            return true;
        }

        if($employeeType == 'department_head'){
            if($this->hasDepartmentHead($request->office_id, $request->department_id, $employeeId)){
                throw new \Exception('Department Head Already Exists for the department in this office');
            }
            return true;
        }

        return true;

    }

    public function storeEmployee($request){
        if($this->checkEmployee($request)){
            //store Employee
            $this->employeeRepo->save($request);
        }
    }

    public function updateEmployee($request, $employee){
        if($this->checkEmployee($request, $employee->id)){
            //update Employee
            $this->employeeRepo->update($request, $employee);
        }
    }

    public function getOfficesByEmployee(Employee $employee){
        
        $offices = Office::active()->where('id', $employee->office->id)->orWhere('parent_office_id', $employee->office->id);
        return $offices->get();
    }

    public static function getOfficeIdsByEmployee($employee){
        $offices = Office::active()->where('id', $employee->office->id)->orWhere('parent_office_id', $employee->office->id)->pluck('id')->toArray();
        return $offices;

    }

    public function getEmployeesByEmployee(Employee $employee){
        $offices = Office::active()->where('id', $employee->office->id)->orWhere('parent_office_id', $employee->office->id)->pluck('id'); // Get office id of head and its child
        $employees = Employee::whereIn('office_id', $offices)->paginate(10);
        return $employees;

    }

    public function destroyEmployee(Employee $employee){
        $this->employeeRepo->changeStatus($employee);
    }

}
<?php


namespace App\Services;


use App\Models\Country;
use App\Models\TerminalPoint;

class CountryService
{

    public static function getCountryByIso($iso){
        $iso = strtoupper($iso);
        return Country::where('iso',$iso)->firstOrFail();
    }

    public static function getCountriesByIso(array $countriesIso){
        $countries = Country::whereIn('iso',$countriesIso)->get();

        return $countries;
    }

    public static function getActiveTerminalPointsByCountryIso($iso){

        $country = self::getCountryByIso($iso);
        return $country->activeTerminalPoints();
    }

    public static function getActiveTerminalPointsByCountriesIso($countryOneIso,$countryTwoIso){

        $country1 = self::getCountryByIso($countryOneIso);
        $country2 =self::getCountryByIso($countryTwoIso);
        $terminalPoints =TerminalPoint::active()->whereIn('country_one_id',[$country1->id,$country2->id])
            ->whereIn('country_two_id',[$country1->id,$country2->id])->get();
        return $terminalPoints;
    }
    public static function getActiveTerminalPointsByCountriesIsoArray($countryOneIso,$countryTwoIso){

        $terminalPoints = self::getActiveTerminalPointsByCountriesIso($countryOneIso,$countryTwoIso)->pluck('id')->toArray();

        return $terminalPoints;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 2/13/2020
 * Time: 2:38 PM
 */

namespace App\Services;

use App\Models\AgencyTransitConnection;
use App\Models\Airport;
use App\Models\Employee;
use App\Models\Office;
use App\Models\TransitLink;
use App\Repositories\Office\OfficeInterface;
use DB;

class OfficeService
{
    protected $officeRepo;
    public function __construct(OfficeInterface $officeRepo){
        $this->officeRepo = $officeRepo;
    }
    
    public function hasSuperHeadOffice($officeId){
        $office = Office::where('office_type','super_head')->where('id', '!=', $officeId )->first();

        if (!is_null($office)) {
            return true;
        }

        return false;
    }

    public function hasCountryHeadOffice($countryId, $officeId){
        $office = Office::where('office_type','country_head')->where('country_id',$countryId)->where('id', '!=', $officeId )->get();

        if (count($office) > 0) {
            return true;
        }

        return false;
    }

    //returns true if valid office
    public function checkOffice($request, $officeId = null){

        $officeType = $request['office_type'];

        if($officeType == 'super_head'){
            if ($this->hasSuperHeadOffice($officeId)) {
                throw new \Exception('Super head office already exists.');
            }

            return true;
        }
        if($officeType == 'country_head'){
            if ($this->hasCountryHeadOffice($request['country_id'], $officeId )){
                throw new \Exception('Head office for the country already exists.');
            }

           return true;
        }

        return true;

    }

    public function storeOffice($request){

        if($this->checkOffice($request)){
            return $this->officeRepo->save($request);
        }
    }


    public function updateOffice($request, $id){

        if($this->checkOffice($request, $id)){
            //store office
            return $this->officeRepo->update($request, $id);
        }


    }

    public function getOfficesByEmployee(Employee $employee){
        $offices = Office::where('parent_office_id', $employee->office->id)->get();
        return $offices;
    }

    //agent office related
    public function storeAgentOffice($request){

        $courierClass = (new CourierClassService())
            ->findUniqueCourierClass('agency',authEmployee()->getWorkingCountry()->id,$request['agency_class']);

        $request['agency_class_id'] = $courierClass->id;

        return $this->officeRepo->saveAgentOffice($request);

    }

    public function updateAgentOffice($request,$id){

        $agencyOffice = $this->findAgencyOfficeById($id);
        $courierClass = (new CourierClassService())
            ->findUniqueCourierClass('agency',authEmployee()->getWorkingCountry()->id,$request['agency_class']);

        $request['agency_class_id'] = $courierClass->id;

        return $this->officeRepo->updateAgentOffice($request,$agencyOffice);

    }

    public function getAgentOfficesByEmployee(Employee $employee){

        $offices = Office::where('office_type','agency');

        if ($employee->getOfficeType() == 'super_head'){

            $offices = $offices->get();
            return $offices;
        }
        elseif ($employee->getOfficeType() == 'country_head'){
            $offices = $offices->where('country_id',$employee->getWorkingCountry()->id)->get();
            return $offices;
        }
        elseif ($employee->getOfficeType() == 'region_id'){
            $offices = $offices->where('country_id',$employee->getWorkingCountry()->id)
                ->where('parent_office_id',$employee->office->id)->get();
            return $offices;
        }
        else{
            throw new \Exception('Access Denied');
        }
    }

    public function getTransitOfficesByEmployee(Employee $employee){

        $offices = Office::where('office_type','transit');

        if ($employee->getOfficeType() == 'super_head'){

            $offices = $offices->get();
            return $offices;
        }
        elseif ($employee->getOfficeType() == 'country_head'){
            $offices = $offices->where('country_id',$employee->getWorkingCountry()->id)->get();
            return $offices;
        }
        elseif ($employee->getOfficeType() == 'region_head'){
            $offices = $offices->where('country_id',$employee->getWorkingCountry()->id)
                ->where('parent_office_id',$employee->office->id)->get();
            return $offices;
        }
        else{
            throw new \Exception('Access Denied');
        }
    }

    public function findAgencyOfficeById($id){

        $office = Office::with(['agency'])
            ->where('office_type','agency')->where('id',$id)->firstOrfail();

        return $office;
    }

    public function changeStatus($id){

        $this->officeRepo->changeStatus($id);
    }

    public function findTransitOfficeById($id){

        $office = Office::with(['transit'])
            ->where('office_type','transit')->where('id',$id)->firstOrfail();

        return $office;
    }

    //used in transit create/edit form
    public function getActiveTransitOffices($exceptOfficeId=null){

        $transitOffices = Office::active()->with(['transit'])
            ->where('office_type','transit')->where('id', '!=', $exceptOfficeId )->get();

        return $transitOffices;
    }


    public function storeTransitOffice($request){

        $courierClass = (new CourierClassService())
            ->findUniqueCourierClass('transit',authEmployee()->getWorkingCountry()->id,$request['transit_class']);

        $request['transit_class_id'] = $courierClass->id;

        return $this->officeRepo->saveTransitOffice($request);

    }

    public function updateTransitOffice($request,$id){

        $transitOffice= $this->findTransitOfficeById($id);

        if(!$this->isOfficeChildOfEmployeeOffice($transitOffice)){
            throw new \Exception('Access Denied');
        }
        $courierClass = (new CourierClassService())
            ->findUniqueCourierClass('transit',authEmployee()->getWorkingCountry()->id,$request['transit_class']);

        $request['transit_class_id'] = $courierClass->id;

        return $this->officeRepo->updateTransitOffice($request,$transitOffice);
    }


    public function findTransitOfficeSlug($slug){

        $office = Office::with(['transit'])->where('office_type','transit')->where('slug',$slug)->firstOrfail();

        return $office;
    }


    public function getTransitOfficeConnectionsByOfficeId($officeId){

        $office = $this->findTransitOfficeById($officeId);

        if($this->isOfficeChildOfEmployeeOffice($office)){
            $transit = $office->transit;
            //$transitConnections1 = $transit->transit_links;

            $transitConnections = DB::table('transit_links')->join('transits as t1','t1.id','=','transit_links.transit_one_id')
            ->join('transits as t2','t2.id','=','transit_links.transit_two_id')
                ->join('offices as o1','o1.id','=','t1.office_id')
                ->join('offices as o2','o2.id','=','t2.office_id')
                ->join('countries as c1','c1.id','=','o1.country_id')
                ->join('countries as c2','c2.id','=','o2.country_id')
              //  ->where('transitLink1.transit_one_id',$transit->id)
                ->where('t1.id',$transit->id)->orWhere('t2.id',$transit->id)
                //->orWhere('transitLink2.transit_two_id',$transit->id)
                ->select('transit_links.*',
                    'o1.id as o1_id',
                    'o2.id as o2_id',
                    'o1.name as o1_office',
                    'o2.name as o2_office'
//                    'o1.country_id as o1_country_id',
//                    'o2.country_id as o2_country_id',
//                     'c1.name as c1_name',
//                    'c2.name as c2_name'
                )
                ->addSelect(DB::raw('
                        (
                        CASE WHEN o1.id = ' . $officeId . ' 
                        THEN o2.name
                        ELSE o1.name
                         END
                        
                        ) AS my_connected_office_name
                '))
                ->addSelect(DB::raw('
                        (
                        CASE WHEN o1.id = ' . $officeId . ' 
                        THEN o2.slug
                        ELSE o1.slug
                         END
                        
                        ) AS my_connected_office_slug
                '))
                ->addSelect(DB::raw('
                        (
                        CASE WHEN o1.id = ' . $officeId . '
                        THEN c2.name
                        ELSE c1.name
                         END

                        ) AS my_connected_office_country
                '))
                ->get();

            return $transitConnections;
        }
        else{
            throw new \Exception('Access Denied');
        }
    }

    public function isOfficeChildOfEmployeeOffice($office){
        if($office->parent_office_id == authEmployee()->office->id){
            return true;
        }
        return false;
    }

    public function getConnectionlessTransitForOffice($officeId){

        // return offices(transit type) that doesn't have connection with the office of officeId


        //mileko xaina haii
        $existingTransitConnectionsIds = $this->getTransitOfficeConnectionsByOfficeId($officeId)->pluck('id')->toArray();

        $connectionLessOffices = Office::with(['transit'=>function($q) use($existingTransitConnectionsIds){
            $q->whereNotIn('id', $existingTransitConnectionsIds);
        }])->whereHas('transit',function ($query) use ($existingTransitConnectionsIds){
            $query->whereNotIn('id', $existingTransitConnectionsIds);
        })->where('id','<>',$officeId)->get();

        return $connectionLessOffices;
    }

    public function getAirportsOperatedByTransit($transitOfficeId){

        $airports = Airport::active()->where('operate_by',$transitOfficeId)->get();

        return $airports;
    }

    public function isTransitOfficeConnectedToAirport($transitOfficeId){

        $airports =$this->getAirportsOperatedByTransit($transitOfficeId);

        if (count($airports) > 0){
            return true;
        }

        return false;
    }

    public function storeTransitConnection($request,$officeId){

        $office = $this->findTransitOfficeById($officeId);

        if($this->isOfficeChildOfEmployeeOffice($office)){

            if ($request['travel_medium'] == 'air'){

                //if both the transits are not connected to at least one airport
                if (!$this->isTransitOfficeConnectedToAirport($office->transit->id)
                    && !$this->isTransitOfficeConnectedToAirport($request['transit_connection'])){

                    throw new \Exception('Both the transit offices should be connected to at least one airport');
                }
            }

            return $this->officeRepo->storeTransitConnection($request,$office);
        }
        else{
            throw new \Exception('Access Denied');
        }
    }
    


    //w.r.t to employee
    public function getTransitLinkOfOffice($officeId,$transitLinkId){

        $office = $this->findTransitOfficeById($officeId);

        if($this->isOfficeChildOfEmployeeOffice($office)){

            //
            $transit = $office->transit;
            $transitLink = TransitLink::where('id',$transitLinkId)->where(function ($query) use ($transit) {
                return $query->where('transit_two_id', $transit->id)->orWhere('transit_one_id',$transit->id);
            })->firstOrFail();

            return $transitLink;
        }
        else{
            throw new \Exception('Access Denied');
        }
    }

    public function updateConnectionOfTransit($request,$officeId,$transitLinkId){
        $connectedOffice = $this->findTransitOfficeSlug($request['connected-office-slug']);// just for assurance

        $transitLink= $this->getTransitLinkOfOffice($officeId,$transitLinkId);

        $transitLink = $this->officeRepo->updateTransitConnection($request,$transitLink);
        return $transitLink;
    }

    public function toggleConnectionStatusOfTransit($request,$officeId,$transitLinkId){

        $connectedOffice = $this->findTransitOfficeSlug($request['connected-office-slug']);// just for assurance

        $transitLink= $this->getTransitLinkOfOffice($officeId,$transitLinkId);

        $transitLink = $this->officeRepo->toggleConnectionStatusOfTransit($transitLink);
        return $transitLink;
    }

    public function deleteConnectionOfTransit($request,$officeId,$transitLinkId){

        $connectedOffice = $this->findTransitOfficeSlug($request['connected-office-slug']);// just for assurance

        $transitLink= $this->getTransitLinkOfOffice($officeId,$transitLinkId);

        $this->officeRepo->deleteConnectionOfTransit($transitLink);

    }
    
    public function getTransitConnectionsOfAgency($officeId){

        $agencyOffice = $this->findAgencyOfficeById($officeId);

        $agencyTransitConnections = $agencyOffice->agency->agencyTransitLinks;

        return $agencyTransitConnections;
    }

    public function storeTransitConnectionOfAgency($request,$officeId){

        $office = $this->findAgencyOfficeById($officeId);

        if($this->isOfficeChildOfEmployeeOffice($office)){

            return $this->officeRepo->storeTransitConnectionOfAgency($request,$office);
        }
        else{
            throw new \Exception('Access Denied');
        }
    }

    public function findAgencyTransitConnection($agencyOfficeId,$transitOfficeId){

        //agencyOfficeId and transitOfficeId are ids of agencies and transits table ids resp.
        $agencyTransitConnection= AgencyTransitConnection::where('agency_id',$agencyOfficeId)
            ->where('transit_id',$transitOfficeId)->first();


        if (is_null($agencyTransitConnection)){
            throw new \Exception('Cannot Find Connection');
        }

        return $agencyTransitConnection;
    }

    public function updateTransitConnectionOfAgency($request,$agencyOfficeId,$transitOfficeId){

        if ($agencyOfficeId == $transitOfficeId){
            throw new \Exception('Cannot Have Own Connection');
        }

        $agencyOffice = $this->findAgencyOfficeById($agencyOfficeId);

        if($this->isOfficeChildOfEmployeeOffice($agencyOffice)){

            $transitOffice = $this->findTransitOfficeById($transitOfficeId);

            $transitConnection = $this->findAgencyTransitConnection($agencyOffice->agency->id,$transitOffice->transit->id);

            return $this->officeRepo->updateAgencyTransitConnection($request,$transitConnection);
        }

        else{
            throw new \Exception('Access Denied');
        }

    }

    public function toggleTransitConnectionStatusOfAgency($agencyOfficeId,$transitOfficeId){

        $agencyOffice = $this->findAgencyOfficeById($agencyOfficeId);

        if($this->isOfficeChildOfEmployeeOffice($agencyOffice)){

            $transitOffice = $this->findTransitOfficeById($transitOfficeId);

            $transitConnection = $this->findAgencyTransitConnection($agencyOffice->agency->id,$transitOffice->transit->id);

            return $this->officeRepo->toggleTransitConnectionStatusOfAgency($transitConnection);
        }

        else{
            throw new \Exception('Access Denied');
        }
    }

    public function deleteTransitConnectionOfAgency($agencyOfficeId,$transitOfficeId){

        $agencyOffice = $this->findAgencyOfficeById($agencyOfficeId);

        if($this->isOfficeChildOfEmployeeOffice($agencyOffice)){

            $transitOffice = $this->findTransitOfficeById($transitOfficeId);

            $transitConnection = $this->findAgencyTransitConnection($agencyOffice->agency->id,$transitOffice->transit->id);

            return $this->officeRepo->deleteTransitConnectionOfAgency($transitConnection);
        }

        else{
            throw new \Exception('Access Denied');
        }
    }
}
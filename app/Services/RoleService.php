<?php

namespace App\Services;
use App\Models\Permission;
use App\Repositories\Role\RoleRepository;



class RoleService
{
    protected $roleRepo;
    public function __construct(RoleRepository $roleRepo)
    {
        $this->roleRepo = $roleRepo;
    }
    public function getPermissionByEmployee($employee){
        return $employee->collectPermissions();
    }

    public function getGroupedPermissionOfEmployee($employee){

        $permissions=Permission::join('role_permission','permissions.id','=','role_permission.permission_id')
            ->join('roles','roles.id','=','role_permission.role_id')
            ->join('employee_role','roles.id','=','employee_role.role_id')
            ->where('employee_role.employee_id',$employee->id)
            ->select('permissions.*')->get();

        $permissions= $permissions->groupBy('category');
        return $permissions;
            
    }

    public function store($request){
        $this->roleRepo->save($request);
    }

    public function update($request, $id){
        $this->roleRepo->update($request, $id);
    }

    public static function getEmployeePermissionsIdArray(){
       // $permissions = auth()->user()->employee->permissionsId();
        $permissions = authEmployee()->permissionsId();
        return $permissions;

    }
}
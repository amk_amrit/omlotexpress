<?php


namespace App\Services;

use App\Models\PackageTypeRate;
use App\Repositories\PackageTypeRate\PackageTypeRateRepository;

class PackageTypeRateService
{
    protected $packageRateRepo;
    public function __construct(PackageTypeRateRepository $packageRateRepo)
    {
        $this->packageRateRepo = $packageRateRepo;
    }
    public function index(){
        if(authEmployee()->isSuperAdmin()){
            $rates = PackageTypeRate::all();
        }else{
            $rates = PackageTypeRate::where('country_id', authEmployee()->office->country_id)->get();
        }
        return $rates;
    }

    public function save($request){
        $this->packageRateRepo->save($request);
        return true;
    }

    public function update($request, $rate){
        $this->packageRateRepo->update($request, $rate);
        return true;
    }

    public function destroyRate($rate){
        $this->packageRateRepo->changeStatus($rate);
    }
}
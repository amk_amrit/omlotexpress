<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 1/21/2020
 * Time: 2:06 PM
 */

namespace App\Services;


use App\Models\PackageType;
use App\Repositories\PackageType\PackageTypeRepository;

class PackageTypeService
{

    private $packageTypeRepo;

    public function __construct(PackageTypeRepository $packageTypeRepo){
        $this->packageTypeRepo=$packageTypeRepo;
    }

    public static function getPackageTypeBySlug($slug){

        $packageType = PackageType::active()->where('slug',$slug)->firstOrFail();

        return $packageType;
    }

    public static function getPackageTypeRateBySlug($slug){

        $packageType= self::getPackageTypeBySlug($slug);

        return $packageType->handling_rate;
    }

    public function getLatestPackageTypes(){

        $latestPackageTypes= $this->packageTypeRepo->getAll();

        return $latestPackageTypes;
    }

    public function paginatedPackageTypes(){

        $latestPackageTypes= PackageType::latest()->get();

        return $latestPackageTypes;
    }

    public function findById($id){
        return $packageType=PackageType::find($id);
    }

    public function save($request){
        $this->packageTypeRepo->store($request);
        return true;
    }

    public function update($request, $id){
        $this->packageTypeRepo->update($request, $id);
        return true;
    }

    public function delete($id){
        return $this->packageTypeRepo->delete($id);
        
    }

}
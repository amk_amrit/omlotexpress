<?php


namespace App\Services;

use App\Models\FullLoadInformation;
use App\Models\PackageType;
use App\Models\PartLoadInformation;
use App\Models\Shipment;
use App\Models\TransportMedia;
use App\Models\TransportVehicleDimension;

class ShipmentService
{

    public function storeShipment($inputData){

        try{
            $transportMedia= TransportMedia::getActiveMediaBySlug($inputData->transport_media);
            $originCountry = CountryService::getCountryByIso($inputData->origin_country);
            $destinationCountry = CountryService::getCountryByIso($inputData->destination_country);
            $productCategory = ProductCategoryService::getActiveProductCategoryBySlug($inputData->product_category);
            $sensitivity = SensitivityService::getSensitivityBySlug($inputData->sensitivity);

            $shipment = new Shipment();
            $shipment->load_type = $inputData->load_type;
            $shipment->transport_media_id = $transportMedia->id;
            $shipment->origin_country_id = $originCountry->id;
            $shipment->local_origin = $inputData->origin_local_address;
            $shipment->destination_country_id = $destinationCountry->id;
            $shipment->local_destination = $inputData->destination_local_address;
            $shipment->product_category_id = $productCategory->id;
            $shipment->terminal_point_id = $inputData->terminal_point;
            $shipment->sensitivity_id = $sensitivity->id;
            $shipment->user_bill_amount = $inputData->user_bill_amount;
            $shipment->total_bill_amount = $inputData->total_bill_amount;
            $shipment->save();

            if ($shipment->load_type === 'part_load'){
                $this->storePartLoadInformation($inputData,$shipment->id);
            }
            else{
                $this->storeFullLoadInformation($inputData,$shipment->id);
            }
            return $shipment;
           // $shipment->total_bill_amount = $inputData->load_type;

        }catch (\Exception $e){
            throw $e;
        }

    }

    private function storePartLoadInformation($inputData,$shipmentId){

        try{
            $packageType = PackageType::getActivePackageTypeBySlug($inputData->package_type);
            $inputWeight = $inputData->total_weight;
            $volumetricWeight = ($inputData->height * $inputData->length * $inputData->width) / 500;
            $totalWeight = $inputWeight > $volumetricWeight ? $inputWeight : $volumetricWeight;

            $partLoad = new PartLoadInformation();
            $partLoad->shipment_id = $shipmentId;
            $partLoad->package_type_id = $packageType->id;
            $partLoad->num_of_items = $inputData->item_num;
            $partLoad->height = $inputData->height;
            $partLoad->width = $inputData->width;
            $partLoad->length = $inputData->length;
            $partLoad->dimension_unit = 2;
            $partLoad->total_weight = $totalWeight;
            $partLoad->weight_unit = 1;

            $partLoad->save();
        }catch (\Exception $e){
            throw $e;
        }
    }

    private function storeFullLoadInformation($inputData,$shipmentId){

        try{
            $vehicleDimension = TransportVehicleDimension::getActiveVehicleDimensionBySlug($inputData->vehicle_type);

            $fullLoad = new FullLoadInformation();
            $fullLoad->shipment_id = $shipmentId;
            $fullLoad->vehicle_dimension_id = $vehicleDimension->id;
            $fullLoad->no_of_vehicles = $inputData->no_of_vehicles;
            $fullLoad->save();
        }catch (\Exception $e){
            throw  $e;
        }
    }


}

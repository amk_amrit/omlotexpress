<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 3/13/2020
 * Time: 2:46 PM
 */

namespace App\Services;


use App\Models\CourierClass;

class CourierClassService
{

    public static function getAgencyTypeCourierClasses($countryId){

        $agencyCourierClasses = CourierClass::active()->where('for_office_type','agency')->where('country_id',$countryId)->get();

        return $agencyCourierClasses;
    }

    public static function getTransitTypeCourierClasses($countryId){

        $transitCourierClasses = CourierClass::active()->where('for_office_type','transit')->where('country_id',$countryId)->get();

        return $transitCourierClasses;
    }

    public function findUniqueCourierClass($officeType,$countryId,$slug){

        //courier class is unique by combination of name,office_type& country
        $courierClass = CourierClass::active()->where('for_office_type',$officeType)
            ->where('country_id',$countryId)->where('slug',$slug)->firstOrFail();

        return $courierClass;

    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 1/21/2020
 * Time: 11:14 AM
 */

namespace App\Services;


use App\Models\ProductCategory;
use App\Models\ProductCategoryRate;

class ProductCategoryService
{

    public static function getActiveProductCategoryBySlug($slug){

        $productCategory = ProductCategory::active()->where('slug',$slug)->firstOrFail();
        return $productCategory;
    }

    public static function getProductCategoryRate($countryId,$productCategoryId,$weight){

        $rate = ProductCategoryRate::where('country_id',$countryId)->where('product_category_id',$productCategoryId)
            ->where('min_weight','<=',$weight)->where('max_weight','>=',$weight)->firstOrFail();

        return $rate->rate;
    }
}
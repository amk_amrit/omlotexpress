<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageSensitivity extends Model
{
    protected $fillable=['type','slug','rate'];


    //get rate for the sensitivity
    public function sensitivityRate(){
        return $this->hasMany(PackageSensitivityRate::class,'package_sensitivity_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public static function getActiveSensitivities(){
        return self::active()->get(); //mathi ko active scope call hunxa
    }

    public static function getArrayOfSlugs(){
        return self::getActiveSensitivities()->pluck('slug')->toArray();
    }

    public function getImagePath(){

        return 'uploads/package-sensitivity/';
    }
}

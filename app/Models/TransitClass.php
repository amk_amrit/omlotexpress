<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransitClass extends Model
{
    protected $fillable=['name','description','slug','status'];
    //
    public function transit(){
       return $this->hasMany(Transit::class, 'transit_class_id');
    }
    public function transits(){
        return $this->hasMany(TransitLink::class, 'transit_id');
    }
    public function transitLink(){
        return $this->hasMany(TransitLink::class, 'link_id');
    }
}

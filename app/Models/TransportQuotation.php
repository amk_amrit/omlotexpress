<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportQuotation extends Model
{
    public function shipment()
    {
        return $this->belongsToMany(Shipment::class,'transportquotations_shipments',
            'quotation_id', 'shipment_id')->withTimestamps();
    }

    public function shipmentFirst()
    {
        return $this->shipment()->latest()->first();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    
    Protected $fillable = ['load_type', 
                            'transport_media_id', 
                            'origin_country_id', 
                            'local_origin', 
                            'destination_country_id', 
                            'local_destination', 
                            'product_category_id', 
                            'terminal_point_id',
                            'sensitivity_id',
                            'total_bill_amount',
                        ];

    //get part load information for the shipment
    public function partLoadInformation(){
        return $this->hasOne(PartLoadInformation::class,'shipment_id');
    }

    //get full load information for the shipment
    public function fullLoadInformation(){
        return $this->hasOne(FullLoadInformation::class,'shipment_id');
    }

    public function transportQuotation()
    {
        return $this->belongsToMany(TransportQuotation::class,'transportquotations_shipments',
            'shipment_id', 'quotation_id')->withTimestamps();
    }

    public function transportQuotationFirst()
    {
        return $this->transportQuotation()->latest()->first();
    }

    // Get the sensitivity rate that owns the shipment.
    public function sensitivityRate(){
        return $this->belongsTo(PackageSensitivityRate::class, 'sensitivity_rate_id')->withDefault();
    }

    // Get the transport media that owns the shipment.
    public function transportMedia(){
        return $this->belongsTo(TransportMedia::class, 'transport_media_id')->withDefault();
    }

    // Get the originCountry that owns the shipment.
    public function originCountry(){
        return $this->belongsTo(Country::class, 'origin_country_id')->withDefault();
    }

    // Get the destination country that owns the shipment.
    public function destinationCountry(){
        return $this->belongsTo(Country::class, 'destination_country_id')->withDefault();
    }

    // Get the product category rate that owns the shipment.
    public function productCategoryRate(){
        return $this->belongsTo(ProductCategoryRate::class, 'product_category_rate_id')->withDefault();
    }

    // Get the terminal point that owns the shipment.
    public function terminalPoint(){
        return $this->belongsTo(TerminalPoint::class, 'terminal_point_id')->withDefault();
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    //get productRate  for the product category
    public function productRate(){
        return $this->hasMany(ProductCategoryRate::class,'product_category_id');
    }


    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public static function activeProductCategories(){
        return self::active()->get(); //mathi ko active scope call hunxa
    }

    public static function getArrayOfNames(){
        return self::activeProductCategories()->pluck('name')->toArray();
    }

    public static function getArrayOfSlugs(){
        return self::activeProductCategories()->pluck('slug')->toArray();
    }


    public function getImagePath(){

        return 'uploads/products/categories/';
    }
}

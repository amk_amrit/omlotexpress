<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransitVehicleCharge extends Model
{
    protected $fillable=['courier_transit_one_class_id','courier_transit_two_class_id','time_travel_group_range_id','quantity_group_range_id','rate','rate_type','currency','created_by','updated_by'];
    //
    public function createdBy(){
        return $this->belongsTo(Employee::class ,'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class,'updated_by')->withDefault();
    }
    public function transitVehicleOneClass(){
        return $this->belongsTo(CourierClass::class, 'courier_transit_one_class_id')->withDefault();
    }
    public function transitVehicleTwoClass(){
        return $this->belongsTo(CourierClass::class, 'courier_transit_two_class_id')->withDefault();
    }
    public function quantityGroupRange(){
        return $this->belongsTo(QuantityGroupRange::class, 'quantity_group_range_id')->withDefault();
    }
    public function timeTravelGroupRate(){
        return $this->belongsTo(VehicleTimeTravelRange::class, 'time_travel_group_range_id')->withDefault();
    }
}

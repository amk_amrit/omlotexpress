<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name','slug', 'status', 'description', 'created_by', 'updated_by'];

    public function employees()
    {
        return $this->belongsToMany(Employee::class,'employee_department',
            'department_id', 'employee_id')->withTimestamps();
    }


    //for seeding
    public function getPermissions(){
        
        $permissions =[
            /*Role Permissions*/

            [
                'name' => 'add-department',
                'route'=>'admin.departments.create',
                'display_name' => 'Add Department',
                'description' => 'can add department',
                'category' => 'department-category'
            ],

            [
                'name' => 'update-department',
                'route'=>'admin.departments.edit',
                'display_name' => 'Update Department',
                'description' => 'can update department detail',
                'category' => 'department-category'
            ],
            [
                'name' => 'view-department',
                'route'=>'admin.departments.index',
                'display_name' => 'View Departments',
                'description' => 'can view department details',
                'category' => 'department-category'
            ],
            [
                'name' => 'delete-department',
                'route'=>'admin.departments.destroy',
                'display_name' => 'Delete Department',
                'description' => 'can delete department detail',
                'category' => 'department-category'
            ],
            [
                'name' => 'Show-department',
                'route'=>'admin.departments.show',
                'display_name' => 'Show Department',
                'description' => 'can view individual department detail',
                'category' => 'department-category'
            ],
        ];
        
        return $permissions;
    }

    public function getAssociatedPermissions(){

        $associatedPermissions =[
            //'user.index'=>['user.data','user.status'],
            'admin.departments.create'=>['admin.departments.store'],
            'admin.departments.edit'=>['admin.departments.update'],

        ];

        return $associatedPermissions;
    }

    public function scopeActive($query){
        return $query->where('status', 1);
    }

    public static function getActiveDepartments(){
        return self::active()->get();
    }
}

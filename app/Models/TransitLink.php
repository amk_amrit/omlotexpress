<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransitLink extends Model
{
    protected $table ='transit_links';

    protected $fillable=['transit_one_id', 'transit_two_id', 'distance','travel_time','travel_medium'];

}

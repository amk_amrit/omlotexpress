<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageType extends Model
{
    protected $fillable=['type_name', 'slug', 'handling_rate', 'description'];

    //get rate for the package
    public function packageRate(){
        return $this->hasMany(PackageTypeRate::class,'package_type_id');
    }
    //get package type
    public function quantityGroup(){
        return $this->hasMany(QuantityGroup::class,'package_type_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public static function activePackageTypes(){
        return self::active()->get(); //mathi ko active scope call hunxa
    }

    public static function getArrayOfNames(){
        return self::activePackageTypes()->pluck('type_name')->toArray();
    }

    public static function getArrayOfSlugs(){
        return self::activePackageTypes()->pluck('slug')->toArray();
    }

    public static function getActivePackageTypeBySlug($slug){

        return self::active()->where('slug',$slug)->firstOrFail();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\Constraint\Count;

class AdminCharge extends Model
{
    protected $fillable=['office_id','quantity_group_range_id','rate','currency','status','created_by','updated_by'];
    //
   public function createdBy(){
       return $this->belongsTo(Employee::class, 'created_by')->withDefault();
   }
   public function updatedBy(){
       return $this->belongsTo(Employee::class,'updated_by')->withDefault();
   }
   public function quantityGroupRange(){
       return $this->belongsTo(QuantityGroupRange::class, 'quantity_group_range_id')->withDefault();
   }
   public function office(){
       return $this->belongsTo(Office::class, 'office_id')->withDefault();
   }
   public function getquantityGroupRangeId($quantityGroupId){
        
    return $countryTwoAiroportId=QuantityGroupRange::where('quantity_group_id', $quantityGroupId)->pluck('id')->toArray();
}
public function getpreviousQuantityGroupRangeId($adminId){
    
    return $countryTwoAiroportId=AdminCharge::where('office_id', $adminId)->pluck('quantity_group_range_id')->toArray();
}
public function getPreviousAdminChargeId($adminId){
    
    return $previousAdminCharge=AdminCharge::where('office_id', $adminId)->pluck('id')->toArray();
}

}

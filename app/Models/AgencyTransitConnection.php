<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgencyTransitConnection extends Model
{
    protected $table ='agency_transit_connections';

    protected $fillable=['agency_id','transit_id','distance','vehicle_travel_time','status','created_by','updated_by'];
    //
    public function createdBy(){
        return $this->belongsTo(Employee::class, 'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class, 'updated_by')->withDefault();
    }
    public function agencyConnection(){
        return $this->belongsTo(Agency::class, 'agency_id')->withDefault();
    }
    public function transitConncetion(){
        return $this->belongsTo(Transit::class, 'transit_id')->withDefault();
    }
    public function agencyTransitVehicleCharge(){
        return $this->hasMany(AgencyTransitVehicleCharge::class, 'agency_transit_connection_id');
    }
}

<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $fillable = ['title', 'slug', 'status', 'description', 'created_by', 'updated_by', 'created_at', 'updated_at'];

    //get employees  for the designation
    public function employees(){
        return $this->hasMany(Employee::class,'designation_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public static function getActiveDesignations(){
        return self::active()->get();
    }
}

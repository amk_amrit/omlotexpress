<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuantityGroupRange extends Model
{
    protected $fillable=['quantity_group_id','country_id','min_weight','max_weight','created_by','updated_by'];
    //

    public function scopeOfCountry($query){
        return $query->where('country_id', authEmployee()->office->country_id);
    }

    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withDefault();
    }
    public function createdBy(){
        return $this->belongsTo(Employee::class,'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class, 'updated_by')->withDefault();
    }
    public function agencyCharge(){
        return $this->hasMany(AgencyClassCharge::class, 'quantity_group_range_id');
    }
    public function transitCharge(){
        return $this->hasMany(TransitClassCharge::class, 'quantity_group_range_id');
    }
    public function adminCharge(){
        return $this->hasMany(AdminCharge::class, 'quantity_group_range_id');
    }
    public function airportCharge(){
        return $this->hasMany(AirportCharge::class, 'quantity_group_range_id');
    }
    public function agencyTransitVechileCharge(){
        return $this->hasMany(AgencyTransitVehicleCharge::class, 'quantity_group_range_id');
    }
    public function transitVehicleCharge(){
        return $this->hasMany(TransitVehicleCharge::class, 'quantity_group_range_id');
    }
    public function quantityGroup(){
        return $this->belongsTo(QuantityGroup::class, 'quantity_group_id')->withDefault();
    }
    public function getArrayOfQuantiryGroupId($quantityGroupId){
        return $quantityGroupId=QuantityGroupRange::where('quantity_group_id', $quantityGroupId)->where('country_id', authEmployee()->office->country_id)->pluck('id')->toArray();
    }

    
}

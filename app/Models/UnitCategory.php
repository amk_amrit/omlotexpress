<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnitCategory extends Model
{
    protected $fillable=['name'];
    //get units  for the category
    public function units(){
        return $this->hasMany(UnitCategory::class,'unit_category_id');
    }
}

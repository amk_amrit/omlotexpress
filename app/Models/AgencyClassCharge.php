<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgencyClassCharge extends Model
{
    protected $fillable=['courier_agency_class_id','quantity_group_range_id','country_id', 'origin_rate','destination_rate','rate_type','currency','created_by','updated_by'];
    //
    public function country(){
        return $this->belongsTo(Country::class, 'country_id')->withDefault();
    }
    public function createdBy(){
        return $this->belongsTo(Employee::class, 'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class,'updated_by')->withDefault();
    }
    public function agencyChargeClass(){
        return $this->belongsTo(CourierClass::class, 'courier_agency_class_id')->withDefault();
    }
    public function quantityGroupRange(){
        return $this->belongsTo(QuantityGroupRange::class, 'quantity_group_range_id')->withDefault();
    }
}

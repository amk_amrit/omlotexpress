<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartLoadInformation extends Model
{
    // Get the shipment that owns the part load info.
    public function shipment(){
        return $this->belongsTo(Shipment::class,'shipment_id')->withDefault();
    }

    // Get the package type rate that owns the part load info.
    public function packageTypeRate(){
        return $this->belongsTo(PackageTypeRate::class, 'package_type_rate_id')->withDefault();
    }

    // Get the dimension units type that owns the part load info.
    public function dimensionUnit(){
        return $this->belongsTo(Unit::class,'dimension_unit')->withDefault();
    }

    // Get the weight units type that owns the part load info.
    public function weightUnit(){
        return $this->belongsTo(Unit::class,'weight_unit')->withDefault();
    }

}

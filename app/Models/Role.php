<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'role_permission',
            'role_id', 'permission_id')->withTimestamps();
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class,'employee_role',
            'role_id', 'employee_id')->withTimestamps();
    }

    public static function findByOffice($office){
        return self::where('office_id', $office->id)->latest()->get();
    }

    public static function getRoleIds($office){
        return self::where('office_id', $office->id)->pluck('id')->toArray();
    }    


    public function permissionIds(){
        $permissionIds = [];
        foreach($this->permissions as $permission){
            array_push($permissionIds, $permission->id);
        }
        return $permissionIds;
    }


    public static function checkSlug($name, $except = null){
        $office = auth()->user()->employee->office;
        $slug = Str::slug($office->name.' '.$name);
        if(self::where('slug', $slug)->where('id', '!=', $except)->first()){
            return true;
        }
        return false;
    }

    //for seeding
    public function getPermissions(){
        $permissions = [

            /*Role Permissions*/

            [
                'name' => 'add-role',
                'route'=>'admin.roles.create',
                'display_name' => 'Add Role',
                'description' => 'can add role',
                'category' => 'role-category'
            ],

            [
                'name' => 'update-role',
                'route'=>'admin.roles.edit',
                'display_name' => 'Update Role',
                'description' => 'can update role detail',
                'category' => 'role-category'
            ],
            [
                'name' => 'view-role',
                'route'=>'admin.roles.index',
                'display_name' => 'View Roles',
                'description' => 'can view role details',
                'category' => 'role-category'
            ],
            [
                'name' => 'delete-role',
                'route'=>'admin.roles.destroy',
                'display_name' => 'Delete Role',
                'description' => 'can delete role detail',
                'category' => 'role-category'
            ],


        ];
        return $permissions;
    }

    public function getAssociatedPermissions(){

        $associatedPermissions =[
            //'user.index'=>['user.data','user.status'],
            'admin.roles.create'=>['admin.roles.store'],
            'admin.roles.edit'=>['admin.roles.update'],

        ];

        return $associatedPermissions;
    }
}

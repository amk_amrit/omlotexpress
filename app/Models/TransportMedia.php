<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportMedia extends Model
{
    
    protected $fillable = ['name', 'slug', 'status'];

    //get vehicles  for the media
    public function vehicles(){
        return $this->hasMany(TransportVehicle::class,'transport_media_id');
    }

    //get shipment  for the media
    public function shipment(){
        return $this->hasMany(Shipment::class,'transport_media_id');
    }

    //
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public static function getActiveMediaBySlug($slug){

        return self::active()->where('slug',$slug)->firstOrfail();
    }

    public static function activeTransportMedias(){
        return self::active()->get(); //mathi ko active scope call hunxa
    }

    public static function getArrayOfId(){
        return self::activeTransportMedias()->pluck('id')->toArray();
    }

    public static function getArrayOfSlug(){
        return self::activeTransportMedias()->pluck('slug')->toArray();
    }

    public function getImagePath(){

        return 'uploads/transport/medias';
    }
    public function quantityGroup(){
        return $this->hasMany(QuantityGroup::class, 'transport_media_id');
    }
}

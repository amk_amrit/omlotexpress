<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TerminalPoint extends Model
{
    protected $fillable=['name', 'country_one_id','country_two_id','latitude', 'longitude'];
    // Get the country that owns the terminal point.
    public function country(){
        return $this->belongsTo(Country::class, 'country_id')->withDefault();
    }

    //get shipment  for the terminal point
    public function Shipment(){
        return $this->hasMany(Shipment::class,'terminal_point_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransitClassCharge extends Model
{
    protected $fillable=['courier_transit_class_id', 'quantity_group_range_id' ,'country_id','rate','currenct','rate_type','created_by','updated_by'];
    //
    public function country(){
        return $this->belongsTo(Country::class, 'country_id')->withDefault();
    }
    public function createdBy(){
        return $this->belongsTo(Employee::class, 'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class, 'updated_by')->withDefault();
    }
    public function transitChargeClass(){
        return $this->belongsTo(CourierClass::class, 'transit_class_charges')->withDefault();
    }

    public function transitClass(){
        return $this->belongsTo(CourierClass::class, 'courier_transit_class_id')->withDefault();
    }

    public function quantityGroupRange(){
        return $this->belongsTo(QuantityGroupRange::class, 'quantity_group_range_id');
    }
}

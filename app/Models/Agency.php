<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $fillable=['office_id', 'courier_agency_class_id','working_as','created_by','updated_by'];
    //
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function agencyClass(){
        return $this->belongsTo(CourierClass::class, 'courier_agency_class_id')->withDefault();
    }
    public function office(){
        return $this->belongsTo(Office::class, 'office_id')->withDefault();
    }
//    public function agencyConnection(){
//        return $this->hasMany(AgencyTransitConnection::class, 'agency_id');
//    }

    //transit links
    public function agencyTransitLinks(){
        return $this->belongsToMany(Transit::class, 'agency_transit_connections', 'agency_id', 'transit_id')
            ->withPivot('id','distance', 'vehicle_travel_time','status','created_by','updated_by')->withTimestamps();
    }

    public function getWorkingAsOptions(){

        //key for view and value for db

        return [
            'Source' => 'source',
            'Destination' => 'destination',
            'Both' => 'all',
        ];
    }
}

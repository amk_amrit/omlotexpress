<?php

namespace App\Models;

use App\Models\AdminCharge;
use App\Models\Country;
use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $fillable = ['name', 'slug', 'country_id', 'parent_office_id',
        'office_type', 'address','phone','email', 'status', 'latitude','longitude','created_by', 'updated_by'];

    //
    // public function office(){
    //     return $this->hasMany(Transit::class, 'office_id');
 
    // }
    public function country(){
        return $this->belongsTo(Country::class, 'country_id')->withDefault();
    }

    //get employees  for the office
    public function employees(){
        return $this->hasMany(Employee::class,'office_id');
    }

    //get employee who created
    public function createdBy(){
        return $this->belongsTo(Employee::class, 'created_by')->withDefault();
    }

    //get employee who updated
    public function updatedBy(){
        return $this->belongsTo(Employee::class, 'updated_by')->withDefault();
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public static function activeOffices(){
        return self::active()->get(); //mathi ko active scope call hunxa
    }

    public static function getActiveBranchOffices(){

        return self::active()->where('office_type','branch')->get();
    }

    public static function getActiveCountryHeadOffices(){

        return self::active()->where('office_type','country_head')->get();
    }

    public static function getActiveSuperHeadOffice(){

        return self::active()->where('office_type','super_head')->first();
    }


    public function isBranchOffice(){

        if($this->office_type === 'branch'){
            return true;
        }
        return false;
    }

    public function isCountryHeadOffice(){
        if($this->office_type === 'country_head'){
            return true;
        }
        return false;
    }

    public function isSuperHeadOffice(){
        if($this->office_type === 'super_head'){
            return true;
        }
        return false;
    }

    //for seeding
    public function getPermissions(){
        
        $permissions =[
            /*Role Permissions*/

            [
                'name' => 'add-office',
                'route'=>'admin.offices.create',
                'display_name' => 'Add office',
                'description' => 'can add office',
                'category' => 'office-category'
            ],

            [
                'name' => 'update-office',
                'route'=>'admin.offices.edit',
                'display_name' => 'Update office',
                'description' => 'can update office detail',
                'category' => 'office-category'
            ],
            [
                'name' => 'view-office',
                'route'=>'admin.offices.index',
                'display_name' => 'View offices',
                'description' => 'can view office details',
                'category' => 'office-category'
            ],
            [
                'name' => 'delete-office',
                'route'=>'admin.offices.destroy',
                'display_name' => 'Delete office',
                'description' => 'can delete office detail',
                'category' => 'office-category'
            ],
            [
                'name' => 'Show-office',
                'route'=>'admin.offices.show',
                'display_name' => 'Show office',
                'description' => 'can view individual office detail',
                'category' => 'office-category'
            ],
        ];
        
        return $permissions;
    }

    public function getAssociatedPermissions(){

        $associatedPermissions =[
            //'user.index'=>['user.data','user.status'],
            'admin.offices.create'=>['admin.offices.store'],
            'admin.offices.edit'=>['admin.offices.update'],

        ];

        return $associatedPermissions;
    }
    public function agency(){
        return $this->hasOne(Agency::class, 'office_id');
    }
    public function transit(){
        return $this->hasOne(Transit::class, 'office_id');
    }
    public function adminCharge(){
        return $this->hasMany(AdminCharge::class, 'office_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleDimensionRate extends Model
{
    //get full load information for the vehicle dimension rate
    public function fullLoadInformation(){
        return $this->hasMany(FullLoadInformation::class,'vehicle_dimension_rate_id');
    }

    public function country(){
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function vehicleDimension(){
        return $this->belongsTo(TransportVehicleDimension::class, 'vehicle_dimension_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    protected $fillabl=['name','address','country_id','scope','operate_by','status','created_by','updated_by'];
    //

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function country(){
        return $this->belongsTo(Country::class, 'country_id')->withDefault();
    }
    public function createdBy(){
        return $this->belongsTo(Employee::class, 'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class, 'updated_by')->withDefault();
    }
    public function airportLinkOne(){
        return $this->hasMany(AirportLink::class, 'airport_one_id');
    }
    public function airportLinkTwo(){
        return $this->hasMany(AirportLink::class, 'airport_two_id');
    }
    public function operateTransit(){
        return $this->belongsTo(Transit::class, 'operate_by')->withDefault();
    }
}

<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;


class Employee extends Model
{
    /**
     * Get the user that owns the employee.
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get the office that owns the employee.
     */
    public function office()
    {
        return $this->belongsTo(Office::class, 'office_id', 'id')->withDefault();
    }

    /**
     * Get the designation that owns the employee.
     */
    public function designation()
    {
        return $this->belongsTo(Designation::class, 'designation_id', 'id')->withDefault();
    }

    public function department()
    {
        return $this->belongsToMany(Department::class,'employee_department',
            'employee_id', 'department_id')->withTimestamps();
    }

    public function departmentFirst()
    {

        return $this->department()->latest()->first();
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class,'employee_role',
            'employee_id', 'role_id')->withTimestamps();
    }

    public function roleIds()
    {
        $ids = [];
        foreach($this->roles as $role){
            array_push($ids, $role->id);
        }
        return $ids;
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function collectPermissions(){

        $allPermissions =[];

        foreach ($this->roles as $role){
           // array_push($allPermissions,$role->permissions->toArray());
            $allPermissions = array_merge($allPermissions,$role->permissions->toArray());
        }
        
        return $allPermissions;
    }
    

    /**
     * Get the aaray of permission ids owned by the authenticted employee.
     * use it for validation in Rule::in()
     */

    public function permissionsId(){

        $allPermissions =[];

        foreach ($this->roles as $role){
            $permissions = $role->permissions;
            foreach($permissions as $permission){
                array_push($allPermissions, $permission->id);
            }

        }
        
        return $allPermissions;
    }

    public function getWorkingCountry(){

        return $this->office->country;
    }

    public function getOfficeType(){

        return $this->office->office_type;
    }

    public function isOfficeHead(){

        if ($this->is_head === 1){
            return true;
        }

        return false;
    }

    public function isSuperAdmin(){

        if($this->isOfficeHead()  && $this->getOfficeType() === 'super_head'){
            return true;
        }
        return false;
    }

    public function isCountryHead(){

        if($this->isOfficeHead()  && $this->getOfficeType() === 'country_head'){
            return true;
        }
        return false;
    }

    public function isRegionHead(){

        if($this->isOfficeHead()  && $this->getOfficeType() === 'region_head'){
            return true;
        }
        return false;
    }

     //for seeding
     public function getPermissions(){
        
        $permissions =[
            /*Role Permissions*/

            [
                'name' => 'add-employee',
                'route'=>'admin.employees.create',
                'display_name' => 'Add Employee',
                'description' => 'can add employee',
                'category' => 'employee-category'
            ],

            [
                'name' => 'update-employee',
                'route'=>'admin.employees.edit',
                'display_name' => 'Update Employee',
                'description' => 'can update employee detail',
                'category' => 'employee-category'
            ],
            [
                'name' => 'view-employee',
                'route'=>'admin.employees.index',
                'display_name' => 'View Employees',
                'description' => 'can view employee details',
                'category' => 'employee-category'
            ],
            [
                'name' => 'delete-employee',
                'route'=>'admin.employees.destroy',
                'display_name' => 'Delete Employee',
                'description' => 'can delete employee detail',
                'category' => 'employee-category'
            ],
            [
                'name' => 'Show-employee',
                'route'=>'admin.employees.show',
                'display_name' => 'Show Employee',
                'description' => 'can view individual employee detail',
                'category' => 'employee-category'
            ],
        ];
        
        return $permissions;
    }

    public function getAssociatedPermissions(){

        $associatedPermissions =[
            //'user.index'=>['user.data','user.status'],
            'admin.employees.create'=>['admin.employees.store'],
            'admin.employees.edit'=>['admin.employees.update'],

        ];

        return $associatedPermissions;
    }

    public function can($checkPermission){
        if(authEmployee()->isSuperAdmin()){
            return true;
        }
        $permissions = $this->collectPermissions();
        foreach($permissions as $permission){
            if($permission['name'] == $checkPermission){
                return true;
            }
        }
        return false;
    }
    public function courierClassCreatedBy(){
        return $this->hasMany(CourierClass::class, 'created_by');
    }
    public function courierClassUpdatedBy(){
        return $this->hasMany(CourierClass::class, 'updated_by');
    }

    public function agencyClassChargeCreatedBy(){
        return $this->hasMany(AgencyClassCharge::class, 'created_by');
    }
    public function agencyClassChargeUpdatedBy(){
        return $this->hasMany(AgencyClassCharge::class, 'updated_by');
    }
    public function quantityGroupCreatedBy(){
        return $this->hasMany(QuantityGroup::class, 'created_by');
    }
    public function quantityGroupUpdatedBy(){
        return $this->hasMany(QuantityGroup::class, 'updated_by');
    }
    public function transitClassChargeCreatedBy(){
        return $this->hasMany(TransitClass::class, 'created_by');
    }
    public function transitClassChargeUpdatedBy(){
        return $this->hasMany(TransitClass::class, 'updated_by');
    }
    public function vehicleTimeTravelRangeCreatedBy(){
        return $this->hasMany(VehicleTimeTravelRange::class, 'created_by');
    }
    public function vehicleTimeTravelRangeUpdatedBy(){
        return $this->hasMany(VehicleTimeTravelRange::class, 'updated_by');
    }
    public function quantityGroupRangeCreatedBy(){
        return $this->hasMany(QuantityGroupRange::class, 'created_by');
    }
    public function quantityGroupRangeUpdatedBy(){
        return $this->hasMany(QuantityGroupRange::class, 'updated_by');
    }
    public function serviceCreatedBy(){
        return $this->hasMany(Service::class, 'created_by');
    }
    public function serviceUpdatedBy(){
        return $this->hasMany(Service::class, 'updated_by');
    }
    public function adminChargeCreatedBy(){
        return $this->hasMany(AdminCharge::class, 'created_by');
    }
    public function adminChargeUpdatedBy(){
        return $this->hasMany(AdminCharge::class, 'updated_by');
    }
    public function airportCreatedBy(){
        return $this->hasMany(Airport::class, 'created_by');
    }
    public function airportUpdatedBy(){
        return $this->hasMany(Airport::class, 'updated_by');
    }
    public function airportChargeCreatedBy(){
        return $this->hasMany(AirportCharge::class, 'created_by');
    }
    public function airportChargeUpdatedBy(){
        return $this->hasMany(AirportCharge::class, 'updated_by');
    }
    public function agencyTransitConnectionCreatedBy(){
        return $this->hasMany(AgencyTransitConnection::class, 'created_by');
    }
    public function agencyTransitConnectionUpdatedBy(){
        return $this->hasMany(AgencyTransitConnection::class, 'updated_by');
    }
    public function agencyTransitVehicleChargeCreatedBy(){
        return $this->hasMany(AgencyTransitVehicleCharge::class, 'created_by');
    }
    public function agencyTransitVehicleChargeUpdatedBy(){
        return $this->hasMany(AgencyTransitVehicleCharge::class, 'updated_by');
    }
    public function transitVehicleChargeCreatedBy(){
        return $this->hasMany(TransitVehicleCharge::class, 'created_by');
    }
    public function transitVehicleChargeUpdatedBy(){
        return $this->hasMany(TransitVehicleCharge::class, 'updated_by');
    }
    public function airportLinkCreatedBy(){
        return $this->hasMany(AirportLink::class, 'created_by');
    }
    public function airportLinkUpdatedBy(){
        return $this->hasMany(AirportLink::class, 'updated_by');
    }

}

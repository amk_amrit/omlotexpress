<?php

namespace App\Models;

use App\Models\Office;
use Illuminate\Database\Eloquent\Model;

class Transit extends Model
{
    protected $fillable=['office_id', 'courier_transit_class_id','created_by','updated_by'];
    //

    public function transitClass(){
       return $this->belongsTo(CourierClass::class, 'courier_transit_class_id')->withDefault();
    }
    public function office(){
       return $this->belongsTo(Office::class, 'office_id');
    }
    public function transitConnection(){
      return $this->hasMany(AgencyTransitConnection::class, 'transit_id');
  }
  public function operateByAirport(){
     return $this->hasMany(Airport::class, 'operate_by');
  }

    //transit links
    public function transitLinksOne(){
        return $this->belongsToMany(self::class, 'transit_links', 'transit_one_id', 'transit_two_id')->withPivot('id','distance', 'travel_time','travel_medium','status')->withTimestamps();
    }

    public function transitLinksTwo(){
        return $this->belongsToMany(self::class, 'transit_links', 'transit_two_id', 'transit_one_id')->withPivot('id','distance', 'travel_time','travel_medium','status')->withTimestamps();
    }

    public function transitLinks(){
        return ($this->transitLinksOne()->count()) ? $this->transitLinksOne() : $this->transitLinksTwo();
    }

    //agency transit links
    public function agencyTransitLinks(){
        return $this->belongsToMany(Transit::class, 'agency_transit_connections', 'transit_id', 'agency_id')
            ->withPivot('id','distance', 'vehicle_travel_time','status','created_by','updated_by')->withTimestamps();
    }

    //accessor
    public function getTransitLinksAttribute(){
        $transitLinksOne = $this->transitLinksOne->load('office');

        //dd($transitLinksOne);
        $transitLinksTwo = $this->transitLinksTwo->load('office');
       // dd($transitLinksTwo);

        $transitLinks = $transitLinksOne->concat($transitLinksTwo);

        //dd($transitLinks);
        return $transitLinks;
    }

    public function travelMediums(){

        //key for view and value for db

        return [
            'Air' => 'air',
            'Road' => 'road',
        ];
    }
}

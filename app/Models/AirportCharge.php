<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AirportCharge extends Model
{
    protected $fillable=['airport_link_id','quantity_group_range_id','rate','currency','status','created_by','updated_by'];
    //
    public function createdBy(){
        return $this->belongsTo(Employee::class, 'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class, 'updated_by')->withDefault();
    }
    public function quantityGroupRange(){
        return $this->belongsTo(QuantityGroupRange::class, 'quantity_group_range_id')->withDefault();
    }
    public function airport(){
        return $this->hasOne(AirportLink::class, 'airport_link_id');
    }
    public function getquantityGroupRangeId($quantityGroupId){
        
        return $countryTwoAiroportId=QuantityGroupRange::where('quantity_group_id', $quantityGroupId)->pluck('id')->toArray();
    }
    public function getpreviousQuantityGroupRangeId($airportLinkId){
        
        return $countryTwoAiroportId=AirportCharge::where('airport_link_id', $airportLinkId)->pluck('quantity_group_range_id')->toArray();
    }
    public function getPreviousAirportChargeId($airportLinkId){
        
        return $countryTwoAiroportId=AirportCharge::where('airport_link_id', $airportLinkId)->pluck('id')->toArray();
    }
    
    
}

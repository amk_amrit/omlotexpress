<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportVehicle extends Model
{
    protected $fillable = ['name', 'slug', 'transport_media_id', 'description', 'status'];
    // Get the media that owns the vehicle.
    public function transportMedia()
    {
        return $this->belongsTo(TransportMedia::class, 'transport_media_id')->withDefault();
    }

    //get vehicle dimensions  for the vehicle
    public function vehicleDimensions()
    {
        return $this->hasMany(TransportVehicleDimension::class, 'vehicle_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    //return empty array if no vehicledimensions for the vehicle
    public function scopeOfAllActive($query)
    {
        //parent,child all active and minimum 1 child
        return $query->active()->whereHas('transportMedia', function ($q) {
            $q->where('status', 1);
        })->whereHas('vehicleDimensions', function ($q) {
                $q->where('status', 1);
            });
    }

    //eager loading
    public function scopeOfWithActive($query)
    {
        //parent,child all active and minimum 1 child
        return $query->ofAllActive()->with(
            [
                'transportMedia'=>function ($q) {$q->where('status', 1);} ,
                'vehicleDimensions'=>function ($q) {$q->where('status', 1);} ,
            ]
        );
    }


    public static function activeVehicles()
    {
        return self::active()->get(); //mathi ko active scope call hunxa
    }


    public static function getArrayOfId()
    {
        return self::activeTransportMedias()->pluck('id')->toArray();
    }
}

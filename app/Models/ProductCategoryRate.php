<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryRate extends Model
{
    protected $fillable = ['country_id','product_category_id','min_weight','max_weight','rate'];

    // Get the country that owns the rate.
    public function country(){
        return $this->belongsTo(Country::class, 'country_id')->withDefault();
    }

    // Get the product category that owns the rate.
    public function productCategory(){
        return $this->belongsTo(ProductCategory::class, 'product_category_id')->withDefault();
    }

    //get shipment rate for the product category
    public function shipment(){
        return $this->hasMany(Shipment::class,'product_category_rate_id');
    }
}

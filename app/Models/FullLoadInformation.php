<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FullLoadInformation extends Model
{

    // Get the shipment that owns the full load info.
    public function shipment(){
        return $this->belongsTo(Shipment::class, 'shipment_id')->withDefault();
    }

    // Get the vehicle dimension rate that owns the full load info.
    public function vehicleDimensionRate(){
        return $this->belongsTo(VehicleDimensionRate::class, 'vehicle_dimension_rate_id')->withDefault();

    }
}

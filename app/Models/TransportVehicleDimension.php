<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportVehicleDimension extends Model
{
    protected $fillable = ['title','slug', 'vehicle_id', 'per_km_charge', 'min_charge', 'capacity', 'description', 'status'];
    // Get the parent vehicle that owns the vehicle dimension.
    public function vehicle(){
        return $this->belongsTo(TransportVehicle::class, 'vehicle_id')->withDefault();
    }


    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public static function activeVehicleDimensions(){
        return self::active()->get(); //mathi ko active scope call hunxa
    }

    public static function getActiveVehicleDimensionBySlug($slug){

        return self::active()->where('slug',$slug)->firstOrFail();
    }

    public function getImagePath(){

        return 'uploads/vehicles/dimensions';
    }
}

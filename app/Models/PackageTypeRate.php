<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageTypeRate extends Model
{
    //get package for the rate
    public function packageType(){
        return $this->belongsTo(PackageType::class,'package_type_id');
    }

    //get country for the rate
    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withDefault();
    }

    //get part load information for the package type
    public function partLoadInformation(){
        return $this->hasMany(PartLoadInformation::class,'package_type_rate_id');
    }
}

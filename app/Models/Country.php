<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //get shipment  for the origin country
    public function originShipment(){
        return $this->hasMany(Shipment::class,'origin_country_id');
    }

    //get shipment  for the destination country
    public function destinationShipment(){
        return $this->hasMany(Shipment::class,'destination_country_id');
    }

    public static function getArrayOfIso(){
        return self::pluck('iso')->toArray();
    }

    //get terminal points  for the country
    public function terminalPoints(){
        return $this->hasMany(TerminalPoint::class,'country_id');
    }

    //active terminal points
    public function activeTerminalPoints(){
        return $this->terminalPoints()->where('status',1)->get();
    }

    //get productRate  for the country
    public function productRate(){
        return $this->hasMany(ProductCategoryRate::class,'country_id');
    }
    public static function getArrayOfId(){
        return self::where('status', 1)->pluck('id')->toArray();
    }
    public function scopeActive($query){
        return $query->where('status',1);
    }

    public static function getActiveCountries(){
        return self::active()->get();
    }
    public function agencyClassCharge(){
        return $this->hasMany(AgencyClassCharge::class,'country_id');
    }
    public function transitClassCharge(){
        return $this->hasMany(TransitClassCharge::class,'country_id');
    }
    public function vehicleTimeTravelRange(){
        return $this->hasMany(VehicleTimeTravelRange::class,'country_id');
    }
    public function courierClass(){
        return $this->hasMany(CourierClass::class,'country_id');
    }
    public function quantityGroupRange(){
        return $this->hasMany(QuantityGroupRange::class, 'country_id');
    }
    public function airport(){
        return $this->hasMany(Airport::class, 'country_id');
    }
}

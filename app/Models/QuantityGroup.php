<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuantityGroup extends Model
{
    protected $fillable=['group_name','slug','status', 'created_by','updated_by'];
    //
    public function createdBy(){
        return $this->belongsTo(Employee::class, 'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class,'updated_by')->withDefault();
    }
    public function quantityGroupRange(){
        return $this->hasMany(QuantityGroupRange::class, 'quantity_group_id');
    }

    public function scopeActive($query){
        return $query->where('status', 1);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleTimeTravelRange extends Model
{
    protected $fillable=['country_id','min_time','max_time','created_by','updated_by'];
    //
    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withDefault();
    }
    public function createdBy(){
        return $this->belongsTo(Employee::class,'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class,'updated_by')->withDefault();
    }
    public function transitVehicleCharge(){
        return $this->hasMany(TransitVehicleCharge::class, 'time_travel_group_range_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageSensitivityRate extends Model
{
    //get package sensitivity for the rate
    public function packageSensitivity(){
        return $this->belongsTo(PackageSensitivity::class,'package_sensitivity_id')->withDefault();
    }

    //get country for the rate
    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withDefault();
    }

    //get shipment for the sensitivity rate
    public function shipment(){
        return $this->hasMany(Shipment::class,'sensitivity_rate_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AirportLink extends Model
{
    protected $fillable=['airport_one_id','airport_two_id','air_link_type','disatance','time_travaling','status','created_by','updated_by'];
    //
    public function createdBy(){
        return $this->belongsTo(Employee::class ,'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class,'updated_by')->withDefault();
    }
    public function airportOne(){
        return $this->belongsTo(Airport::class, 'airport_one_id')->withDefault();
    }
    public function airportTwo(){
        return $this->belongsTo(Airport::class, 'airport_two_id')->withDefault();
    }
    public function airportCharge(){
        return $this->hasOne(AirportLink::class, 'airport_link_id');
    }
    public function getcountryTwoAirportId($country_id_two){
        
        return $countryTwoAiroportId=Airport::where('country_id', $country_id_two)->pluck('id')->toArray();
    }
    public function getcountryOneAirportId($country_id_one){
        
        return $countryTwoAiroportId=Airport::where('country_id', $country_id_one)->pluck('id')->toArray();
    }
}


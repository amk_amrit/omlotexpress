<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable=['name','unit_category_id'];
    // Get the category that owns the unit.
    public function category(){
        return $this->belongsTo(UnitCategory::class, 'unit_category_id')->withDefault();
    }

    public static function dimensionUnits(){
        return self::whereHas('category',function($q){
            $q->where('name','Length');
        })->get();
    }
    public static function weightUnits(){
        return self::whereHas('category',function($q){
            $q->where('name','Weight');
        })->get();
    }

    public static function getArrayOfUnitId($categoryName){
        return self::whereHas('category',function ($q) use ( $categoryName){
            $q->where('name',$categoryName);
        })->pluck('id')->toArray();
    }
    public function PartLoadInformation(){
        return $this->hasMany(PartLoadInformation::class, 'id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourierClass extends Model
{
    protected $fillable=['name','slug','for_office_type','description','country_id','status','created_by','updated_by'];
    //
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeOfAgencyType($query)
    {
        return $query->where('for_office_type', 'agency');
    }


    public function scopeOfTransitType($query)
    {
        return $query->where('for_office_type', 'transit');
    }

    public function scopeOfCountry($query){
        return $query->where('country_id', authEmployee()->office->country_id);
    }


    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withDefault();
    }
    public function createdBy(){
        return $this->belongsTo(Employee::class, 'created_by')->withDefault();
    }
    public function updatedBy(){
        return $this->belongsTo(Employee::class, 'updated_by')->withDefault();
    }
    public function agencyOfficeClass(){
        return $this->hasMany(Agency::class, 'courier_agency_class_id');
    }
    public function transitOfficeClass(){
        return $this->hasMany(Transit::class, 'courier_transit_class_id');
    }
    public function agencyChargeClass(){
        return $this->hasMany(AgencyClassCharge::class,'courier_agency_class_id');
    }
    public function transitChargeClass(){
        return $this->hasMany(TransitClassCharge::class,'courier_transit_class_id');
    }
    public function transitVehicleOneClass(){
        return $this->hasMany(TransitVehicleCharge::class, 'courier_transit_one_class_id');
    }
    public function transitVehicleTwoClass(){
        return $this->hasMany(TransitVehicleCharge::class, 'courier_transit_two_class_id');
    }
   
}

<?php


namespace App\ValidationRules;

use App\Models\QuantityGroupRange;
use Illuminate\Validation\Rule;
class QuantityGroupRangeRule extends BaseRule
{
    public function getQuantityGroupRangeValidationRules($ruleKey , $qgid){
      $quantityGroupArrayId = (new QuantityGroupRange())->getArrayOfQuantiryGroupId($qgid);
      $updateValidationMessages = [];
    if($ruleKey == 'update'){
      foreach ($this->request->get('min_weight') as $key => $val) {
        $updateValidationMessages['min_weight.' . $key . '.required'] = 'Minimum  Weight at ' . (str_ordinal($key + 1, true)) . '" entry position is required.';
        $updateValidationMessages['max_weight.' . $key . '.required'] = 'Maxmium Weight at ' . (str_ordinal($key + 1, true)) . '" entry position is required.';
        $updateValidationMessages['min_weight.' . $key . '.gte'] = 'Minimum  Weight at ' . (str_ordinal($key + 1, true)) . '" entry position should be non negative number.';
        $updateValidationMessages['max_weight.' . $key . '.gte'] = 'Maxmium Weight at ' . (str_ordinal($key + 1, true)) . '" entry position should be non negative number.';
       

      }
      $updateValidationMessages['id.*.in'] = 'Data Invalid ';
    }
        $rules =[
            'create' => [
              'quantity_group_id' => 'required',
              'min_weight.*'=> 'required',
              'max_weight.*'=>'required',
            ],
            'update' => [
              [
              'quantity_group_id' => ['required'],
              'min_weight.*'=> ['required','gte:0','lt:max_weight.*'],
              'max_weight.*'=>['required','gte:0','gt:min_weight.*'],
              'id.*' => [Rule::in($quantityGroupArrayId)],
            ],
              $updateValidationMessages
          ],
          
        ];

        return $rules[$ruleKey];
    }

}

<?php


namespace App\ValidationRules;

use App\Models\Airport;
use App\Models\AirportCharge;
use Illuminate\Validation\Rule;
use App\Models\AirportLink;

class AirportChargeRule extends BaseRule
{
    public function getAirportChargeRules($ruleKey, $airportLinkId, $quantityGroupId){

        $quantityGroupRange = (new AirportCharge())->getquantityGroupRangeId($quantityGroupId);
        $previousQuantityGroupRange = (new AirportCharge())->getPreviousQuantityGroupRangeId($airportLinkId);
        $previousAirportChargeId = (new AirportCharge())->getPreviousAirportChargeId($airportLinkId);


        $CreateValidationMessages = [];
        if($ruleKey == 'create'){
          foreach ($this->request->get('rate') as $key => $val) {
            $CreateValidationMessages['rate.*' . $key . '.required'] = 'This Rate of  ' . (str_ordinal($key + 1)) . ' entry position is required.';    
          }
          $CreateValidationMessages['quantity_group_range_id'] = 'Data Invalite !!';

        }
        $UpdateValidationMessages = [];
        if($ruleKey == 'update'){
          foreach ($this->request->get('quantity_group_range_id') as $key => $val) {
            $updateValidationMessages['rate.' . $key . '.required'] = 'This rate of   ' . (str_ordinal($key + 1, true)) . '" entry position is required.'; 
            $updateValidationMessages['rate_type.' . $key . '.required'] = 'This rate type of   ' . (str_ordinal($key + 1, true)) . '" entry position is required.';    
          }
          $UpdateValidationMessages['quantity_group_range_id.*.in'] = 'Data Invalite !!';

        }
        $rules =[
            'create' => [
            [
              'quantity_group_range_id.*' => [Rule::in($previousQuantityGroupRange)],
              'rate.*' => ['required'],
              'rate_type' => ['required','array'],
              'rate_type.*' => ['required', Rule::in('fix', 'per_kg')],
            ],
             $CreateValidationMessages
        ],

            'update' => [
                [
              'quantity_group_range_id.*' => [Rule::in($quantityGroupRange)],
              'rate.*' => ['required'],
              'rate_type' => ['required','array'],
              'rate_type.*' => ['required', Rule::in('fix', 'per_kg')],

            ],
            $UpdateValidationMessages
        ],
        ];

        return $rules[$ruleKey];
    }


}

<?php


namespace App\ValidationRules;

class QuotationRule extends BaseRule
{
    public function getQuotationValidationRules($ruleKey){

        $rules =[
            'create' => [
                'name' => 'required',
                'phone' => 'required',
                'email' =>'required|email',
            ],

            'update' => [
                'name' => 'required',
                'phone' => 'required',
                'email' =>'required|email',
            ],
        ];

        return $rules[$ruleKey];
        //return array_merge($quotationRules,$shipmentRules);
    }

    public function getAllQuotationRules($ruleKey){
        $quotationRules = $this->getQuotationValidationRules($ruleKey);
        $shipmentRules = (new ShipmentRule($this->request))->getAllShipmentValidationRules($ruleKey);

        return array_merge($quotationRules,$shipmentRules);
    }

    public function getAllQuotationMessages($ruleKey){
        $shipmentMessages = (new ShipmentRule($this->request))->getAllShipmentValidationMessages($ruleKey);

        return $shipmentMessages;
    }

}

<?php

namespace App\ValidationRules;

class PackageTypeRateRule extends BaseRule
{
    public function getPackageTypeRateValidationRules($ruleKey, $except = NULL)
    {
        // $countryId = authEmployee()->office->country_id;
        $countryId = $this->request->country_id;
        $rules = [
            'create' => [
                [
                'handling_rate' => ['required', 'numeric', 'min:1'],
                'package_type_id' => ['required','unique:package_type_rates,package_type_id,NULL,id,country_id,' . $countryId],
                'status' => ['boolean'],
                ],
                [
                    'package_type_id.required' => 'Package Type is Required',
                    'package_type_id.unique' => 'Rate has Already Been Mentioned for the Package Type in your Country',
                ]
        ],

            'update' => [
                [
                    'package_type_id' => ['required','unique:package_type_rates,package_type_id,'.$except.',id,country_id,' . $countryId],
                    'handling_rate' => ['required', 'numeric', 'min:1'],
                    'status' => ['boolean'],
                ],
                [
                    'package_type_id.required' => 'Package Type is Required',
                    'package_type_id.unique' => 'Rate has Already Been Mentioned for the Package Type in your Country',
                ]
            ]
           
        ];



        return $rules[$ruleKey];
    }
}

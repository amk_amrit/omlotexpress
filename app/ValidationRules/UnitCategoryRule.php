<?php


namespace App\ValidationRules;

class UnitCategoryRule extends BaseRule
{
    public function getUnitCategoryValidationRules($ruleKey){

        $rules =[
            'create' => [
              'name' => 'required | unique:unit_categories',
            ],
            'update' => [
                'name' => 'required',
            ],
        ];

        return $rules[$ruleKey];
    }

}

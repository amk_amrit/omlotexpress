<?php

namespace App\ValidationRules;

class DepartmentRule extends BaseRule
{
    public function getDepartmentValidationRules($ruleKey, $except = 0)
    {

        $rules = [
            'create' => [
                'name' => ['required', 'unique:departments', 'max:191'],
                'status' => ['boolean'],
                'description' => ['required'],
            ],

            'update' => [
                'name' => ['required', 'unique:departments,name,' . $except, 'max:191'],
                'status' => ['boolean'],
                'description' => ['required'],
            ],
        ];

        return $rules[$ruleKey];
    }
}

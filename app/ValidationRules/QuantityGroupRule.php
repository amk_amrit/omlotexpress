<?php


namespace App\ValidationRules;

class QuantityGroupRule extends BaseRule
{
    public function getQuantityGroupValidationRules($ruleKey , $id=0){

        $rules =[
            'create' => [
              'group_name'=>'required| unique:quantity_groups',
            ],
            'update' => [
                'group_name'=>['required', 'unique:quantity_groups,group_name,'.$id],
            ],
        ];

        return $rules[$ruleKey];
    }

}

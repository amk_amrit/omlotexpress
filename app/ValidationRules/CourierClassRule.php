<?php


namespace App\ValidationRules;

use Illuminate\Validation\Rule;

class CourierClassRule extends BaseRule
{
  public function getCourierClassRules($ruleKey, $id = 0)
  {
    $for_office_type = $this->request->for_office_type;
    $country_id = authEmployee()->office->country_id;
    $createValidationMessages = [];
    if($ruleKey == 'create'){
      $createValidationMessages['for_office_type'] = 'Office Type is Required';
      foreach ($this->request->get('name') as $key => $val) {
        $createValidationMessages['name.' . $key . '.unique'] = 'Class Name  : ' . $val . ' at ' . (str_ordinal($key + 1, true)) . ' entry position has already existed in your country.';
        $createValidationMessages['name.' . $key . '.required'] = 'Class Name at ' . (str_ordinal($key + 1, true)) . '" entry position is required.';
      }
    }
    
   // return ($createValidationMessages);
    $rules = [
      'create' => [
        [
          'for_office_type' => 'required',
          'name.*' => ['required', Rule::unique('courier_classes', 'name')->where(function ($query) use ($for_office_type, $country_id) {
            return $query->where('for_office_type', $for_office_type)->where('country_id', $country_id);
          })]

        ],
        $createValidationMessages
      ],
      'update' => [
        [
          'for_office_type' => 'required',
          'name' => ['required', Rule::unique('courier_classes', 'name')->where(function ($query) use ($for_office_type, $country_id) {
            return $query->where('for_office_type', $for_office_type)->where('country_id', $country_id);
          })->ignore($id)],
        ],
        [
          'name.unique' => 'This courier class name already exit in your country',
          'name.required' => 'The name filed is required'
        ],
      ],
    ];

    return $rules[$ruleKey];
  }
}

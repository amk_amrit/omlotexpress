<?php

namespace App\ValidationRules;

class VehicleDimensionRateRule extends BaseRule
{
    public function getDimensionRateValidationRules($ruleKey, $except = NULL)
    {
        // $countryId = authEmployee()->office->country_id;
        $countryId = $this->request->country_id;
        $rules = [
            'create' => [
                [
                'per_km_charge' => ['required', 'numeric', 'min:0.01'],
                'minimum_charge' => ['required', 'numeric', 'min:0.01'],
                // 'country_id' => ['unique:vehicle_dimension_rates,country_id,NULL,id,vehicle_dimension_id,' . $vehicle_dimension_id],
                'vehicle_dimension_id' => ['required','unique:vehicle_dimension_rates,vehicle_dimension_id,NULL,id,country_id,' . $countryId],
                'status' => ['boolean'],
                ],
                [
                    'vehicle_dimension_id.required' => 'Vehicle Dimension is Required',
                    'vehicle_dimension_id.unique' => 'Rate has Already Been Mentioned for the Vehicle Dimension in your Country',
                ]
        ],

            'update' => [
                [
                    'vehicle_dimension_id' => ['required','unique:vehicle_dimension_rates,vehicle_dimension_id,'.$except.',id,country_id,' . $countryId],
                    'per_km_charge' => ['required', 'numeric', 'min:0.01'],
                    'minimum_charge' => ['required', 'numeric', 'min:0.01'],
                    'status' => ['boolean'],
                ],
                [
                    'vehicle_dimension_id.required' => 'Vehicle Dimension is Required',
                    'vehicle_dimension_id.unique' => 'Rate has Already Been Mentioned for the Vehicle Dimension in your Country',
                ]
            ]
           
        ];



        return $rules[$ruleKey];
    }
}

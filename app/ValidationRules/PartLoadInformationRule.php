<?php


namespace App\ValidationRules;


use Alphametric\Validation\Rules\Decimal;
use App\Models\PackageType;
use App\Models\Unit;
use Illuminate\Validation\Rule;

class PartLoadInformationRule extends BaseRule
{
    public function modifyRequest(){

        $this->request['total_weight'] =  $this->request->filled('total_weight') ?
            convertToDecimal($this->request->total_weight): null ;

        $this->request['height'] =  $this->request->filled('height') ?
            convertToDecimal($this->request->height): null ;

        $this->request['width'] =  $this->request->filled('width') ?
            convertToDecimal($this->request->width): null ;

        $this->request['length'] =  $this->request->filled('length') ?
            convertToDecimal($this->request->length): null ;
    }

    public function getPartLoadInformationValidationRules($ruleKey){

        $this->modifyRequest();

        $packageTypeSlugsArray = PackageType::getArrayOfSlugs();

        $weightUnitIdArray = Unit::getArrayOfUnitId('Weight');
        $dimensionUnitIdArray = Unit::getArrayOfUnitId('Length');

        $rules =[
            'create' => [
                'package_type' => ['required',Rule::in($packageTypeSlugsArray)],
                'item_num' => 'required|max:191',
                'total_weight' =>['required'],
                //'weight_unit' =>[ 'required',Rule::in($weightUnitIdArray)],
                //'dimension_unit' => [($this->request->package_type == 'Box') ? ['required',Rule::in($dimensionUnitIdArray)]: ''],
                //'height' => ['required_if:package_type,Box',new Decimal(12,4)],
                'height' => [($this->request->package_type == 'box') ? ['required',new Decimal(12,4)] : ''],
                'width' => [
                    ($this->request->package_type == 'box') ? ['required',new Decimal(12,4)] : ''
                ],
                'length' => [($this->request->package_type == 'box') ? ['required',new Decimal(12,4)] : ''],
            ],
            'update' => [
                'package_type' => ['required',Rule::in($packageTypeSlugsArray)],
                'item_num' => 'required|max:191',
                'total_weight' =>[ 'required',new Decimal(12,4)],
                'weight_unit' =>[ 'required',Rule::in($weightUnitIdArray)],
                'dimension_unit' => [($this->request->package_type == 'Box') ? ['required',Rule::in($dimensionUnitIdArray)]: ''],
                'height' => [($this->request->package_type == 'Box') ? ['required',new Decimal(12,4)] : ''],
                'width' => [
                    ($this->request->package_type == 'Box') ? ['required',new Decimal(12,4)] : ''
                ],
                'length' => [($this->request->package_type == 'Box') ? ['required',new Decimal(12,4)] : ''],
            ],
        ];

        return $rules[$ruleKey];
        //return array_merge($quotationRules,$shipmentRules);
    }
}

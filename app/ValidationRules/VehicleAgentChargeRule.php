<?php


namespace App\ValidationRules;

class VehicleAgentChargeRule extends BaseRule
{
    public function getVehicleAgentChargeRules($ruleKey, $id=0){

        $rules =[
            'create' => [
              'type' => 'required',
              'quantity_group_id' => 'required',
              'vehicle_id' =>'required',
              'country_id'=>'required',
              'rate.*' => 'required',
            ],
            'update' => [
                'type' => 'required',
                'quantity_group_id' => 'required',
                'vehicle_id' =>'required',
                'country_id'=>'required',
                'rate' => 'required',
            ],
        ];

        return $rules[$ruleKey];
    }


}

<?php 
namespace App\ValidationRules;

class CategoryRateRule extends BaseRule
{
    public function getCategoryRateValidationRules($ruleKey){

        $rules =[
            'create' => [
            //   'country_id' => 'required',
              'product_category_id' => 'required',
              'product_category_id' => 'required',
              'min_weight'=> 'required | array',
               'min_weight.*'=>'required|integer',
               'max_weight'=> 'required | array',
               'max_weight.*' => 'required| integer',
                'rate.*' => 'required',
            ],
            'update' => [
                // 'country_id' => 'required',
                'product_category_id' => 'required',
                'product_category_id' => 'required',
                'max_weight'=> 'required | array',
                'max_weight.*' => 'required| integer',
                'min_weight'=> 'required | array',
                'min_weight.*'=>'required| integer',
                'rate.*' => 'required',
            ],
        ];

        return $rules[$ruleKey];
        //return array_merge($quotationRules,$shipmentRules);
    }

}

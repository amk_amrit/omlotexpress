<?php


namespace App\ValidationRules;

class TransitLinkRule extends BaseRule
{
    public function getTransitLinkRules($ruleKey, $id=0){

        $rules =[
            'create' => [
              'transit_id' => 'required',
              'link_id.*' => 'required',
              'distance.*' =>'required',
            ],
            'update' => [
                'transit_id' => ['required'],
                'link_id.*' => 'required',
              'distance.*' =>'required',
            ],
        ];

        return $rules[$ruleKey];
    }


}

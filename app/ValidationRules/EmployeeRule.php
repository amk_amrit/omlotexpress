<?php

namespace App\ValidationRules;

use App\Models\Role;
use App\Services\EmployeeService;
use Illuminate\Validation\Rule;

class EmployeeRule extends BaseRule
{
  public function getEmployeeValidationRules($ruleKey, $except = 0)
  {
    $officeRoles = Role::getRoleIds(authEmployee()->office);
    $allowedOfficeIds = EmployeeService::getOfficeIdsByEmployee(authEmployee());
    $rules = [
      'create' => [
        'name' => ['required', 'max:191', 'unique:users'],
        'email' => ['required', 'email', 'unique:users'],
        'password' => ['required_with:confirm_password', 'same:confirm_password', 'min:6'],
        'address' => ['required', 'max:191'],
        'mobile' => ['required', 'integer', 'min:1', 'digits_between:5,10'],
        'designation_id' => ['required', 'exists:designations,id'],
        'status' => ['boolean'],
        'office_id' => ['required', Rule::in($allowedOfficeIds)],
        'employee_type' => ['required', 'in:office_head,department_head,employee'],
        'role_id' => ['required','array'],
        'role_id.*' => [Rule::in($officeRoles)],
        'department_id' => $this->request->employee_type != 'office_head' ? ['required', 'exists:departments,id'] : '',
      ],

      'update' => [
        'name' => ['required', 'max:191', 'unique:users,name,'.$except],
        'email' => ['required', 'email', 'unique:users,email,'.$except],
        'address' => ['required', 'max:191'],
        'mobile' => ['required', 'integer', 'min:1', 'digits_between:5,10'],
        'designation_id' => ['required', 'exists:designations,id'],
        'status' => ['boolean'],
        'office_id' => ['required', Rule::in($allowedOfficeIds)],
        'employee_type' => ['required', 'in:office_head,department_head,employee'],
        'role_id' => ['required','array'],
        'role_id.*' => [Rule::in($officeRoles)],
        'department_id' => $this->request->employee_type != 'office_head' ? ['required', 'exists:departments,id'] : '',
      ],
    ];

    return $rules[$ruleKey];
  }
}

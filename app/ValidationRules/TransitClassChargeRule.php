<?php


namespace App\ValidationRules;
use Illuminate\Validation\Rule;


class TransitClassChargeRule extends BaseRule
{
    public function getTransitClassChargeRules($ruleKey, $id=0){
      $countryId = authEmployee()->office->country_id;
      $courierTransitClassId = $this->request->courier_transit_class_id;
      $rules =[
                
                'create' => [
                  [
                    'courier_transit_class_id' => 'required',
                    'quantity_group_id' => 'required',
                    'quantity_group_range_id.*' => 'required|distinct',
                    'rate.*' => 'required',
                    
                    // 'quantity_group_range_id.*' => ['required',Rule::unique('transit_class_charges','quantity_group_range_id')->where(function ($query) use ($countryId,$courierTransitClassId) {
                    //                               return $query->where('country_id', $countryId)->where('courier_transit_class_id',$courierTransitClassId);
                    //     })],
    
                  ],[
                    'rate.*.required' => 'Please Fill Missing rate Inputs'
                  ]
                  
                ],
                'update' => [
                  'transit_class_id.*' => 'required',
                  'quantity_group_id.*' => 'required',
                  'rate.*' => 'required',
                ]
            
      ];
            

        return $rules[$ruleKey];
    }


}

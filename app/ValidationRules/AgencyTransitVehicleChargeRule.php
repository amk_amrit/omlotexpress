<?php


namespace App\ValidationRules;

use App\Models\AgencyTransitVehicleCharge;
use Illuminate\Validation\Rule;

class AgencyTransitVehicleChargeRule extends BaseRule
{
  public function getAgencyTransitVehicleChargeRules($ruleKey, $agencyTransitConnectionId, $quantityGroupId){

    $quantityGroupRange = (new AgencyTransitVehicleCharge())->getquantityGroupRangeId($quantityGroupId);
    $previousQuantityGroupRange = (new AgencyTransitVehicleCharge())->getPreviousQuantityGroupRangeId($agencyTransitConnectionId);
    $previousAgencyTransitVehicleChargeChargeId = (new AgencyTransitVehicleCharge())->getPreviousAgencyTransitVehicleChargeChargeId($agencyTransitConnectionId);


    $UpdateValidationMessages = [];
    if($ruleKey == 'update'){
      foreach ($this->request->get('rate') as $key => $val) {
        $UpdateValidationMessages['rate.*' . $key . '.required'] = 'This Rate of  ' . (str_ordinal($key + 1)) . ' entry position is required.';    
      }
      $UpdateValidationMessages['quantity_group_range_id.*.in'] = 'Data Invalite !!';

    }
    $CreateValidationMessages = [];
    if($ruleKey == 'create'){
      foreach ($this->request->get('quantity_group_range_id') as $key => $val) {
        $CreateValidationMessages['rate.' . $key . '.required'] = 'This rate of   ' . (str_ordinal($key + 1, true)) . '" entry position is required.'; 
        $CreateValidationMessages['rate_type.*' . $key . '.required'] = 'This rate type of   ' . (str_ordinal($key + 1, true)) . '" entry position is required.';    
      }
      $CreateValidationMessages['quantity_group_range_id.*.in'] = 'Data Invalite !!';

    }
    $rules =[
        'update' => [
        [
          'rate.*' => ['required'],
          'rate_type' => ['required','array'],
          'rate_type.*' => ['required', Rule::in('fix', 'per_kg')],
        ],
         $UpdateValidationMessages
    ],

        'create' => [
            [
          'quantity_group_range_id.*' => [Rule::in($quantityGroupRange)],
          'rate.*' => ['required'],
          'rate_type' => ['required','array'],
          'rate_type.*' => ['required', Rule::in('fix', 'per_kg')],

        ],
        $CreateValidationMessages
    ],
    ];

    return $rules[$ruleKey];
}


}

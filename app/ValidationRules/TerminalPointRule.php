<?php


namespace App\ValidationRules;

class TerminalPointRule extends BaseRule
{
    public function getTerminalPointValidationRules($ruleKey){

        $rules =[
            'create' => [
              'name' => 'required | unique:terminal_points',
              'country_one_id' => 'required ',
              'country_two_id' => 'required',
              'latitude'=>'required',
              'longitude'=>'required'
            ],
            'update' => [
                'name' => 'required',
                'country_one_id' => 'required ',
                'country_two_id' => 'required',
                'latitude'=>'required',
                'longitude'=>'required'
            ],
        ];

        return $rules[$ruleKey];
        //return array_merge($quotationRules,$shipmentRules);
    }

    // public function getAllCountryRules($ruleKey){
    //     $countryRules = $this->getCountryValidationRules($ruleKey);
    //     $countryRules = (new CountryRule($this->request))->getAllCountryValidationRules($ruleKey);

    //     return array_merge($countryRules,$countryRules);
    // }

    // public function getAllCountaryMessages($ruleKey){
    //     $countryMessages = (new CountryRule($this->request))->getAllCountryValidationMessages($ruleKey);

    //     return $countryMessages;
    // }

}

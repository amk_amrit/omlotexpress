<?php


namespace App\ValidationRules;

class AgencyClassChargeRule extends BaseRule
{
    public function getAgencyClassChargeRules($ruleKey, $id=0){

      $rules =[
                
        'create' => [
          [
            'courier_agency_class_id' => 'required',
            'quantity_group_id' => 'required',
            'quantity_group_range_id.*' => 'required',
            'origin_rate.*' => 'required',
            'destination_rate.*' => 'required',

          ],[
            'origin_rate.*.required' => 'Please Fill Missing Origin rate Inputs',
            'destination_rate.*.required' => 'Please Fill Missing Origin Destination Inputs'
          ]
          
        ],
        'update' => [
          'agency_class_id.*' => 'required',
          'quantity_group_id.*' => 'required',
          'rate.*' => 'required',
        ]
    
];

        return $rules[$ruleKey];
    }


}

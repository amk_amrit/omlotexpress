<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 3/13/2020
 * Time: 10:30 AM
 */

namespace App\ValidationRules;



use App\Models\Agency;
use App\Repositories\Office\OfficeRepository;
use App\Services\CourierClassService;
use App\Services\OfficeService;
use Illuminate\Validation\Rule;

class AgentOfficeRule extends BaseRule
{

    public function getAgentOfficeValidationRules($ruleKey , $except = 0,$officeId=null){

        $agencyClassSlugsArray =CourierClassService::getAgencyTypeCourierClasses(authEmployee()->getWorkingCountry()->id)->pluck('slug')->toArray();

        $agencyWorkingOptions = (new Agency())->getWorkingAsOptions();

        //transit table id itself
        $transitOfficesIdArray = (new OfficeService(new OfficeRepository()))->getActiveTransitOffices()->pluck('transit.id')->toArray();

        //dd($officeTypeOptions);

        $requestedTransitId=0;
        $agencyId=0;
        if ($ruleKey=='addConnection' && !is_null($officeId)){

            $requestedTransitId =(int)$this->request['transit_connection'];
            // dd($connectionLessTransitOfficeIds);

            $agencyId = (new OfficeService(new OfficeRepository()))
                ->findAgencyOfficeById($officeId)->agency->id;

            //dd($requestedTransitId,$transitId);
        }

        $rules =[
            'create' => [
                'name' => ['required', 'unique:offices,name' ,'max:191'],
                'address' => ['required', 'max:255'],
                'phone' => ['required','max:191'],
                'email' => ['required','email'],
                'latitude' => ['sometimes','nullable','numeric'],
                'longitude' => ['sometimes','nullable','numeric'],
                'agency_class' => ['required',Rule::in($agencyClassSlugsArray)],
                'working_as' => ['required',Rule::in($agencyWorkingOptions)],
                'transit_connection' => ['required',Rule::in($transitOfficesIdArray)],
                'distance' => ['required','numeric'],
                'travel_time' => ['required','numeric'],
                'status' => ['boolean'],


            ],

            'update' => [
                'name' => ['required', 'unique:offices,name,'.$except,'max:191'],
                'address' => ['required', 'max:255'],
                'phone' => ['required','max:191'],
                'email' => ['required','email'],
                'latitude' => ['sometimes','nullable','numeric'],
                'longitude' => ['sometimes','nullable','numeric'],
                'agency_class' => ['required',Rule::in($agencyClassSlugsArray)],
                'working_as' => ['required',Rule::in($agencyWorkingOptions)],
                'status' => ['boolean'],

            ],
            'addConnection' =>[

                'transit_connection' => [
                    'required',
                    Rule::in($transitOfficesIdArray),
                    Rule::unique('agency_transit_connections','transit_id')->where(function ($query) use ($requestedTransitId,$agencyId) {
                        return $query->where('agency_id', $agencyId)->where('transit_id',$requestedTransitId);
                    }),
                ],

                'distance' => ['required','numeric'],
                'travel_time' => ['required','numeric'],
            ],
            'editConnection' => [
                // 'travel_medium' => ['required',Rule::in($transitMediumOptions)],
                'distance' => ['required','numeric'],
                'travel_time' => ['required','numeric'],
            ]
        ];

        return $rules[$ruleKey];
    }

}
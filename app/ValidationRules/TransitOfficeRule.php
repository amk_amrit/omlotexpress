<?php
/**
 * Created by PhpStorm.
 * User: Prajwal
 * Date: 3/16/2020
 * Time: 2:45 PM
 */

namespace App\ValidationRules;


use App\Models\Transit;
use App\Repositories\Office\OfficeRepository;
use App\Services\CourierClassService;
use App\Services\OfficeService;
use Illuminate\Validation\Rule;

class TransitOfficeRule extends BaseRule
{

    public function getTransitOfficeValidationRules($ruleKey , $except = 0,$officeId=null){

        $transitClassSlugsArray =CourierClassService::getTransitTypeCourierClasses(authEmployee()->getWorkingCountry()->id)->pluck('slug')->toArray();

        $transitMediumOptions = (new Transit())->travelMediums();

        //transit table id itself
        $transitOfficesIdArray = (new OfficeService(new OfficeRepository()))->getActiveTransitOffices()->pluck('transit.id')->toArray();

        $connectionLessTransitOfficeIds=[];
        $requestedTransitId=0;
        $transitId=0;
        if ($ruleKey=='addConnection' && !is_null($officeId)){

            /*$connectionLessTransitOfficeIds = (new OfficeService(new OfficeRepository()))
                ->getConnectionlessTransitForOffice($officeId)->pluck('transit.id')->toArray();*/

            $requestedTransitId =(int)$this->request['transit_connection'];
           // dd($connectionLessTransitOfficeIds);

            $transitId = (new OfficeService(new OfficeRepository()))
                ->findTransitOfficeById($officeId)->transit->id;

            //dd($requestedTransitId,$transitId);
        }


        $rules =[
            'create' => [
                'name' => ['required', 'unique:offices,name' ,'max:191'],
                'address' => ['required', 'max:255'],
                'phone' => ['required','max:191'],
                'email' => ['required','email'],
                'latitude' => ['sometimes','nullable','numeric'],
                'longitude' => ['sometimes','nullable','numeric'],
                'transit_class' => ['required',Rule::in($transitClassSlugsArray)],
                'transit_connection' => ['required',Rule::in($transitOfficesIdArray)],
                'travel_medium' => ['required',Rule::in($transitMediumOptions)],
                'distance' => ['required','numeric'],
                'travel_time' => ['required','numeric'],
                'status' => ['boolean'],

            ],

            'update' => [
                'name' => ['required', 'unique:offices,name,'.$except,'max:191'],
                'address' => ['required', 'max:255'],
                'phone' => ['required','max:191'],
                'email' => ['required','email'],
                'latitude' => ['sometimes','nullable','numeric'],
                'longitude' => ['sometimes','nullable','numeric'],
                'transit_class' => ['required',Rule::in($transitClassSlugsArray)],
                //'transit_connection' => ['required',Rule::in($transitOfficesIdArray)],
                //'travel_medium' => ['required',Rule::in($transitMediumOptions)],
               // 'distance' => ['required','numeric'],
               // 'travel_time' => ['required','numeric'],
                'status' => ['boolean'],

            ],

            'addConnection' =>[

                'transit_connection' => [
                    'required',
                    Rule::in($transitOfficesIdArray)
                ],
                'travel_medium' => [
                    'required',
                    Rule::unique('transit_links','travel_medium')->where(function ($query) use ($requestedTransitId,$transitId) {
                        return $query->where('transit_one_id', $requestedTransitId)->where('transit_two_id',$transitId);
                    }),
                    Rule::unique('transit_links','travel_medium')->where(function ($query) use ($requestedTransitId,$transitId) {
                        return $query->where('transit_one_id', $transitId)->where('transit_two_id',$requestedTransitId);
                    }),
                    Rule::in($transitMediumOptions)
                ],
                'distance' => ['required','numeric'],
                'travel_time' => ['required','numeric'],
            ],
            'editConnection' => [
               // 'travel_medium' => ['required',Rule::in($transitMediumOptions)],
                'distance' => ['required','numeric'],
                'travel_time' => ['required','numeric'],
            ]

        ];

        return $rules[$ruleKey];
    }
}

/*->where(function ($q){
    $q->where('transport_medium','air')->where('transport_medium','road');
});*/
<?php


namespace App\ValidationRules;


use Illuminate\Http\Request;

class BaseRule
{
   protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}

<?php

namespace App\ValidationRules;

class TransportVehicleRule extends BaseRule
{
    public function getTransportVehicleValidationRules($ruleKey, $except = 0)
    {

        $rules = [
            'create' => [
                'name' => ['required', 'unique:transport_vehicles', 'max:191'],
                'transport_media_id' => ['required', 'max:191'],
                'status' => ['boolean'],
                'description' => ['required'],
            ],

            'update' => [
                'name' => ['required', 'unique:transport_vehicles,name,' . $except, 'max:191'],
                'transport_media_id' => ['required', 'max:191'],
                'status' => ['boolean'],
                'description' => ['required'],
            ],
        ];

        return $rules[$ruleKey];
    }
}

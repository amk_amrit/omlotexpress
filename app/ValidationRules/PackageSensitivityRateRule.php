<?php

namespace App\ValidationRules;

use Illuminate\Validation\Rule;

class PackageSensitivityRateRule extends BaseRule
{
    public function getPackageSensitivityRateValidationRules($ruleKey, $except = NULL)
    {
        // $countryId = authEmployee()->office->country_id;
        $countryId = $this->request->country_id;
        $packageSensitivityId = $this->request->package_sensitivity_id;
        $rules = [
            'create' => [
                [
                'handling_rate' => ['required', 'numeric', 'min:1'],
                //'package_sensitivity_id' => ['required','unique:package_sensitivity_rates,package_sensitivity_id,NULL,id,country_id,' . $countryId],
                // 'package_sensitivity_iddd' => ['required',Rule::unique('package_sensitivity_rates','package_sensitivity_id')->where(function ($query) use ($countryId,$packageSensitivityId) {
                //     return $query->where('country_id', $countryId);
                // })],
                'package_sensitivity_id' => ['required',Rule::unique('package_sensitivity_rates')->where(function ($query) use ($countryId,$packageSensitivityId) {
                    return $query->where('country_id', $countryId)->where('package_sensitivity_id',$packageSensitivityId);
                })],

                'status' => ['boolean'],
                ],
                [
                    'package_sensitivity_id.required' => 'Package sensitivity is Required',
                    'package_sensitivity_id.unique' => 'Rate has Already Been Mentioned for the Package Sensitivity in your Country',
                ]
        ],

            'update' => [
                [
                    // 'package_sensitivity_id' => ['required','unique:package_sensitivity_rates,package_sensitivity_id,'.$except.',id,country_id,' . $countryId],
                    'package_sensitivity_id' => ['required',Rule::unique('package_sensitivity_rates')->where(function ($query) use ($countryId,$packageSensitivityId) {
                        return $query->where('country_id', $countryId)->where('package_sensitivity_id',$packageSensitivityId);
                    })->ignore($except)],
                    'handling_rate' => ['required', 'numeric', 'min:1'],
                    'status' => ['boolean'],
                ],
                [
                    'package_sensitivity_id.required' => 'Package sensitivity is Required',
                    'package_sensitivity_id.unique' => 'Rate has Already Been Mentioned for the Package Sensitivity in your Country',
                ]
            ]
           
        ];



        return $rules[$ruleKey];
    }
}

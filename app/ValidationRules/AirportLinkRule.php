<?php


namespace App\ValidationRules;

use App\Models\Airport;
use Illuminate\Validation\Rule;
use App\Models\AirportLink;

class AirportLinkRule extends BaseRule
{
  public function getAirportLinkRules($ruleKey, $country_id_one, $country_id_two)
  {

    $countryTwoAirportId = (new AirportLink())->getcountryTwoAirportId($country_id_two);
    $countryOneAirportId = (new AirportLink())->getcountryOneAirportId($country_id_one);
    $CreateValidationMessages = [];
    if ($ruleKey == 'create') {
      foreach ($this->request->get('airport_two_id') as $key => $val) {
        $CreateValidationMessages['airport_two_id.' . $key . '.required'] = 'This Airport  ' . (str_ordinal($key + 1, true)) . '" entry position is required.';
      }
      $CreateValidationMessages['airport_one_id'] = 'You have to select first country!!';
    }
    $updateValidationMessages = [];
    $updateRules = [];


    if ($ruleKey == 'update') {

      $updateRules = [
        'airport_one_id' => ['required', Rule::in($countryOneAirportId)],
        'airport_two_id' => ['required', 'array']
      ];

      foreach ($this->request->get('airport_two_id') as $key => $airport2) {
        $updateRules['airport_two_id.' . $key] = [
          'required',
          Rule::notIn([$this->request->airport_one_id]),
          Rule::in($countryTwoAirportId),
          Rule::unique('airport_links', 'airport_two_id')->where(function ($query) use ($airport2) {
            return $query->where('airport_two_id', $airport2)
              ->where('airport_one_id', $this->request->airport_one_id);
          }),
          Rule::unique('airport_links', 'airport_one_id')->where(function ($query) use ($airport2) {
            return $query->where('airport_two_id', $this->request->airport_one_id)
              ->where('airport_one_id', $airport2);
          }),
        ];
        $updateValidationMessages['airport_two_id.' . $key . '.not_in'] = 'The Airport at  ' . (str_ordinal($key + 1, true)) . '" entry position Cannot be set same as the airport for which links are being made.';
        $updateValidationMessages['airport_two_id.' . $key . '.required'] = 'This Airport  ' . (str_ordinal($key + 1, true)) . '" entry position is required.';
        $updateValidationMessages['airport_two_id.' . $key . '.unique'] = '(Duplicate Entry)Airports at  ' . (str_ordinal($key + 1, true)) . '" entry position have already been connected with each other.';
      }
      $updateValidationMessages['airport_one_id.in'] = 'Data Invalite !!';
      $updateValidationMessages['airport_two_id.*.in'] = 'Data Invalite !!';
    }

   // return $updateRules;

    $rules = [
      [
        'create' => [
          'airport_one_id' => 'required ',
          'airport_two_id.*' => 'required',
        ],
        $CreateValidationMessages
      ],

      'update' => [
        $updateRules,
        $updateValidationMessages
      ],
    ];

    return $rules[$ruleKey];
  }
}

<?php


namespace App\ValidationRules;

class VehicleTimeTravelRangeRule extends BaseRule
{
    public function getVehicleTimeTravelRangeRules($ruleKey, $id = NULL){

        $rules =[
            'create' => [
              'min_time.*' => 'required',
              'max_time.*' => 'required',
            ],
            'update' => [
                'min_time.*' => 'required',
                'max_time.*' => 'required',
            ],
        ];

        return $rules[$ruleKey];
    }


}

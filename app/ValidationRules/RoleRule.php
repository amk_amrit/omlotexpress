<?php

namespace App\ValidationRules;

use App\Services\RoleService;
use Illuminate\Validation\Rule;

class RoleRule extends BaseRule
{
    public function getRoleValidationRules($ruleKey, $except = 0)
    {

        $permissions = RoleService::getEmployeePermissionsIdArray();

        $rules = [
            'create' => [
                'name' => ['required', 'max:191'],
                'description' => ['required'],
                'permission_id'=>['required','array'],
                'permission_id.*' => [Rule::in($permissions)]
            ],

            'update' => [
                'name' => ['required', 'max:191'],
                'description' => ['required'],
                'permission_id'=>['required','array'],
                'permission_id.*' => [Rule::in($permissions)]
            ],
        ];

        return $rules[$ruleKey];
    }
}

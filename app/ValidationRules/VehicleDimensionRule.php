<?php

namespace App\ValidationRules;

class VehicleDimensionRule extends BaseRule
{
    public function getVehicleDimensionValidationRules($ruleKey, $except = '0')
    {

        $rules = [
            'create' => [
                'title' => ['required', 'unique:transport_vehicle_dimensions', 'max:191',],
                'vehicle_id' => ['required'],
                'dimension' => ['required', 'max:191'],
                'capacity' => ['required', 'max:191'],
                'status' => ['boolean'],
                'description' => ['required'],
            ],

            'update' => [
                'title' => ['required', 'unique:transport_vehicle_dimensions,title,' . $except, 'max:191'],
                'vehicle_id' => ['required'],
                'dimension' => ['required', 'max:191'],
                'capacity' => ['required', 'max:191'],
                'status' => ['boolean'],
                'description' => ['required'],
            ],
        ];

        return $rules[$ruleKey];
    }
}

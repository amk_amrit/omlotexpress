<?php


namespace App\ValidationRules;

class AgencyClassRule extends BaseRule
{
    public function getAgencyClassRules($ruleKey, $id=0){

        $rules =[
            'create' => [
              'name' => 'required | unique:agency_classes',
              'type' => 'required',
              'description' =>'required',
            ],
            'update' => [
                'name' => 'required',
                'type' => 'required',
              'description' =>'required',
            ],
        ];

        return $rules[$ruleKey];
    }


}

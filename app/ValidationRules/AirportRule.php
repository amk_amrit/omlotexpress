<?php


namespace App\ValidationRules;

class AirportRule extends BaseRule
{
    public function getAirportRules($ruleKey, $id=0){

        $rules =[
            'create' => [
              'name' => 'required | unique:airports',
              'address' => 'required',
              'operate_by' => 'required',
              'latitude' => 'required | numeric|between:0,99.99999999999999',
              'longitude' => 'required | numeric|between:0,99.99999999999999',
            ],
            'update' => [
                'name' => ['required', 'unique:airports,name,'.$id],
                'address' => 'required',
                'operate_by' =>'required',
                'latitude' => 'required | numeric|between:0,99.99999999999999',
               'longitude' => 'required | numeric|between:0,99.99999999999999',
            ],
        ];

        return $rules[$ruleKey];
    }


}

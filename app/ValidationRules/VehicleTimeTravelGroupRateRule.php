<?php


namespace App\ValidationRules;

class VehicleTimeTravelGroupRateRule extends BaseRule
{
    public function getVehicleTimeTravelGroupRateRules($ruleKey, $id=0){

        $rules =[
            'create' => [
              'country_id' => 'required',
              'time_travel_group_id.*'=>'required',
              'rate.*'=>'required',

            ],
            'update' => [
              'rate'=>'required',
            ],
        ];

        return $rules[$ruleKey];
    }


}

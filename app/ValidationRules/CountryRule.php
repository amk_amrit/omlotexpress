<?php


namespace App\ValidationRules;

class CountryRule extends BaseRule
{
    public function getCountryValidationRules($ruleKey){

        $rules =[
            'create' => [
              'iso' => ['required'],
              'name' => ['required'],
              'nicename' => ['required'],
              'phonecode' => ['required'],
              'currency' => ['required'],
            ],

            'update' => [
              'iso' => ['required'],
              'name' => ['required'],
              'nicename' => ['required'],
              'phonecode' => ['required'],
              'currency' => ['required'],
            ],
        ];

        return $rules[$ruleKey];
        //return array_merge($quotationRules,$shipmentRules);
    }

    // public function getAllCountryRules($ruleKey){
    //     $countryRules = $this->getCountryValidationRules($ruleKey);
    //     $countryRules = (new CountryRule($this->request))->getAllCountryValidationRules($ruleKey);

    //     return array_merge($countryRules,$countryRules);
    // }

    // public function getAllCountaryMessages($ruleKey){
    //     $countryMessages = (new CountryRule($this->request))->getAllCountryValidationMessages($ruleKey);

    //     return $countryMessages;
    // }

}

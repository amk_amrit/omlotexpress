<?php


namespace App\ValidationRules;

use App\Models\AdminCharge;
use Illuminate\Validation\Rule;

class AdminChargeRule extends BaseRule
{
  public function getAdminChargeRules($ruleKey, $adminId, $quantityGroupId){

    $quantityGroupRange = (new AdminCharge())->getquantityGroupRangeId($quantityGroupId);
    $previousQuantityGroupRange = (new AdminCharge())->getPreviousQuantityGroupRangeId($adminId);
    $previousAdminChargeId = (new AdminCharge())->getPreviousAdminChargeId($adminId);


    $CreateValidationMessages = [];
    if($ruleKey == 'create'){
      foreach ($this->request->get('rate') as $key => $val) {
        $CreateValidationMessages['rate.*' . $key . '.required'] = 'This Rate of  ' . (str_ordinal($key + 1)) . ' entry position is required.';    
      }
      $CreateValidationMessages['quantity_group_range_id.*.in'] = 'Data Invalite !!';
      $CreateValidationMessages['id.*'] = 'Data Invalite !!';

    }
    $updateValidationMessages = [];
    if($ruleKey == 'update'){
      foreach ($this->request->get('quantity_group_range_id') as $key => $val) {
        $updateValidationMessages['rate.' . $key . '.required'] = 'This rate of   ' . (str_ordinal($key + 1, true)) . '" entry position is required.'; 
        $updateValidationMessages['rate_type.*' . $key . '.required'] = 'This rate type of   ' . (str_ordinal($key + 1, true)) . '" entry position is required.';    
      }
      $updateValidationMessages['quantity_group_range_id.*.in'] = 'Data Invalite !!';

    }
    $rules =[
        'create' => [
        [
          'quantity_group_range_id.*' => [Rule::in($previousQuantityGroupRange)],
          'rate.*' => ['required'],
          'rate_type' => ['required','array'],
          'rate_type.*' => ['required', Rule::in('fix', 'per_kg')],
          'id.*' =>  [Rule::in($previousAdminChargeId)],
        ],
         $CreateValidationMessages
    ],

        'update' => [
            [
          'quantity_group_range_id.*' => [Rule::in($quantityGroupRange)],
          'rate.*' => ['required'],
          'rate_type' => ['required','array'],
          'rate_type.*' => ['required', Rule::in('fix', 'per_kg')],

        ],
        $updateValidationMessages
    ],
    ];

    return $rules[$ruleKey];
}


}

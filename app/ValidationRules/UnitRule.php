<?php


namespace App\ValidationRules;

class UnitRule extends BaseRule
{
    public function getUnitValidationRules($ruleKey){

        $rules =[
            'create' => [
              'name' => 'required',
              'unit_category_id' => 'required'
            ],
            'update' => [
                'name' => 'required',
              'unit_category_id' => 'required'
            ],
        ];

        return $rules[$ruleKey];
        //return array_merge($quotationRules,$shipmentRules);
    }

    // public function getAllCountryRules($ruleKey){
    //     $countryRules = $this->getCountryValidationRules($ruleKey);
    //     $countryRules = (new CountryRule($this->request))->getAllCountryValidationRules($ruleKey);

    //     return array_merge($countryRules,$countryRules);
    // }

    // public function getAllCountaryMessages($ruleKey){
    //     $countryMessages = (new CountryRule($this->request))->getAllCountryValidationMessages($ruleKey);

    //     return $countryMessages;
    // }

}

<?php

namespace App\ValidationRules;

class DesignationRule extends BaseRule
{
    public function getDesignationValidationRules($ruleKey)
    {

        $rules = [
            'create' => [
                'title' => ['required', 'max:191'],
                'status' => ['boolean'],
                'description' => ['required'],
            ],

            'update' => [
                'title' => ['required', 'max:191'],
                'status' => ['boolean'],
                'description' => ['required'],
            ],
        ];

        return $rules[$ruleKey];
    }
}

<?php


namespace App\ValidationRules;


use App\Services\TransportService;
use Illuminate\Validation\Rule;

class FullLoadInformationRule extends BaseRule
{

    public function getFullLoadInformationValidationRules($ruleKey){

        $mediaType = $this->request->filled('transport_media') ? $this->request->transport_media: 'by-road' ;
        $activeVehicles = TransportService::getActiveVehicleDimensionArrayByMedia($mediaType);

        $rules =[
            'create' => [
              'no_of_vehicles' => ['required','integer','min:1'],
              'vehicle_type' => ['required',Rule::in($activeVehicles)],
            ],

            'update' => [
                'no_of_vehicles' => ['required','integer','min:1'],
                'vehicle_type' => ['required',Rule::in($activeVehicles)],
            ],
        ];

        return $rules[$ruleKey];
        //return array_merge($quotationRules,$shipmentRules);
    }

}

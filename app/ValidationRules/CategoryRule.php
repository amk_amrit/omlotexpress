<?php


namespace App\ValidationRules;

class CategoryRule extends BaseRule
{
    public function getCategoryValidationRules($ruleKey){

        $rules =[
            'create' => [
              'name' => 'required | unique:product_categories',
              'hsn_code' => ' required | unique:product_categories',
            ],
            'update' => [
              'name' => 'required',
              'hsn_code' => 'required'
            ],
        ];

        return $rules[$ruleKey];
        //return array_merge($quotationRules,$shipmentRules);
    }

    // public function getAllCountryRules($ruleKey){
    //     $countryRules = $this->getCountryValidationRules($ruleKey);
    //     $countryRules = (new CountryRule($this->request))->getAllCountryValidationRules($ruleKey);

    //     return array_merge($countryRules,$countryRules);
    // }

    // public function getAllCountaryMessages($ruleKey){
    //     $countryMessages = (new CountryRule($this->request))->getAllCountryValidationMessages($ruleKey);

    //     return $countryMessages;
    // }

}

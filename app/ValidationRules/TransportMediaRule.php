<?php

namespace App\ValidationRules;

class TransportMediaRule extends BaseRule
{
    public function getTransportMediaValidationRules($ruleKey, $except = 0)
    {

        $rules = [
            'create' => [
                'status' => ['boolean'],
                'name' => ['required', 'unique:transport_media', 'max:191'],
            ],

            'update' => [
                'status' => ['boolean'],
                'name' => ['required', 'unique:transport_media,name,' . $except, 'max:191'],
            ],
        ];

        return $rules[$ruleKey];
    }
}

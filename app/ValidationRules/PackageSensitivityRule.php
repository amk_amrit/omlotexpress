<?php


namespace App\ValidationRules;

class PackageSensitivityRule extends BaseRule
{
    public function getPackageSeniRuleValidationRules($ruleKey, $except = NULL){

        $rules =[
            'create' => [
              'title' => ['required','unique:package_sensitivities'],
            ],
            'update' => [
                'title' => ['required','unique:package_sensitivities,id,'.$except],
            ],
        ];

        return $rules[$ruleKey];
    }

}

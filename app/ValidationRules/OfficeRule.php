<?php 
namespace App\ValidationRules;

use Illuminate\Validation\Rule;
use App\Models\Country;

class OfficeRule extends BaseRule
{
    public function getOfficeValidationRules($ruleKey , $except = 0){

        $activeCountries =  Country::getArrayOfId();

        $officeTypeOptions = $this->getOfficeTypeOptions();

        //dd($officeTypeOptions);

        $rules =[
            'create' => [
              'name' => ['required', 'unique:offices,name' ,'max:191'],
              'country_id' => [Rule::in($activeCountries)],
              'office_type' => [Rule::in($officeTypeOptions)],
              'status' => ['boolean'],
              'address' => ['required', 'max:255'],

            ],

           'update' => [
              'name' => ['required', 'unique:offices,name,'.$except,'max:191'],
              'country_id' => [Rule::in($activeCountries)],
              'office_type' => [Rule::in($officeTypeOptions)],
              'status' => ['boolean'],
              'address' => ['required', 'max:255'],
              
            ],
        ];

        return $rules[$ruleKey];
    }

    public function getOfficeTypeOptions(){

        $officeType = authEmployee()->getOfficeType();

        if($officeType === 'super_head'){

            return ['country_head'];
        }
        elseif ($officeType === 'country_head'){
            return ['region_head'];
        }
        elseif ($officeType === 'region_head'){
            return ['transit','agency'];
        }
        else{
            return [];
        }
    }
}

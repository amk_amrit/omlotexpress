<?php


namespace App\ValidationRules;

class ServiceRule extends BaseRule
{
    public function getServiceRules($ruleKey, $id=0){

        $rules =[
            'create' => [
              'name' => 'required | unique:services',
            ],
            'update' => [
                'name' => ['required', 'unique:services,name,'.$id],
            ],
        ];

        return $rules[$ruleKey];
    }


}

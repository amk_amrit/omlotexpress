<?php


namespace App\ValidationRules;

class PackageTypeRule extends BaseRule
{
    public function getPackageTypeValidationRules($ruleKey, $except= Null){

        $rules =[
            'create' => [
              'type_name' => ['required','unique:package_types'],
              'description' => ['required','max:255'],
            ],
            'update' => [
              'type_name' => ['required','unique:package_types,id,'.$except],
              'description' => ['required','max:255'],
            ],
        ];

        return $rules[$ruleKey];
    }

}

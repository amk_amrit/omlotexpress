<?php


namespace App\ValidationRules;


use Alphametric\Validation\Rules\Decimal;
use App\Models\Country;
use App\Models\PackageSensitivity;
use App\Models\ProductCategory;
use App\Models\TransportMedia;
use App\Services\CountryService;
use Illuminate\Validation\Rule;

class ShipmentRule extends BaseRule
{
    public function modifyRequest(){
        $this->request['total_weight'] =  $this->request->filled('total_weight') ?
                                            convertToDecimal($this->request->total_weight): null ;
    }

    public function getShipmentValidationRules($ruleKey){

        $this->modifyRequest();

        $countryIsoArray = Country::getArrayOfIso();
        $activeTransportMediasSlug= TransportMedia::getArrayOfSlug();
        $productCategorySlugsArray = ProductCategory::getArrayOfSlugs();
        $sensitivitySlugsArray = PackageSensitivity::getArrayOfSlugs();
        $terminalPointsID = [];
        if ($this->request->origin_country && $this->request->destination_country){
            $terminalPointsID = CountryService::getActiveTerminalPointsByCountriesIsoArray($this->request->origin_country,$this->request->destination_country);
        }


        $rules = [
            'create' => [
                'load_type' => [Rule::in(['part_load','full_load'])],
                'transport_media' => ['required',Rule::in(['by-road'])],
                'origin_country' => ['required',Rule::in($countryIsoArray)],
                'origin_local_address' => ['required','max:191'],
                'destination_country' => ['required',Rule::in($countryIsoArray)],
                'destination_local_address' => ['required','max:191'],
                'terminal_point' => ['required',Rule::in($terminalPointsID)],
                'sensitivity' => ['required',Rule::in($sensitivitySlugsArray)],
                'product_category' => ['required',Rule::in($productCategorySlugsArray)],
                'total_distance' =>['required'],
                'user_bill_amount' => ['sometimes','nullable','numeric','min:0.1','max:999999999999999999'],
            ]
        ];


        return $rules[$ruleKey];
    }

    public function getShipmentValidationMessages($ruleKey){

        $messages=[
            'create'=>[
                'total_distance.required' => 'Unable to calculate total distance. Please fill the addresses properly.',
                ]
        ];

        return $messages[$ruleKey];
    }

    public function getAllShipmentValidationRules($ruleKey){

        $shipmentRule = $this->getShipmentValidationRules($ruleKey);

        if ($this->request->load_type == 'full_load'){
            $loadInformationRule = (new FullLoadInformationRule($this->request))->getFullLoadInformationValidationRules($ruleKey);
        }
        else{
            $loadInformationRule = (new PartLoadInformationRule($this->request))->getPartLoadInformationValidationRules($ruleKey);
        }

        return array_merge($shipmentRule,$loadInformationRule);
    }

    public function getAllShipmentValidationMessages($ruleKey){

        $shipmentMessages = $this->getShipmentValidationMessages($ruleKey);

        return $shipmentMessages;
    }
}
